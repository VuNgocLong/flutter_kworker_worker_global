import 'package:flutter/material.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/interactions.dart';

class HomeResources{

  Future<ApiResponseData> getWorkerStatus(BuildContext context, Map<String, String> params) async {
    return await ApiConnection().get(context, urlProfileWorker, params);
  }

  Future<ApiResponseData> changeWorkerStatus(BuildContext context, Map<String, dynamic> params) async {
    return await ApiConnection().post(context, urlProfileChangeStatus, params);
  }
}