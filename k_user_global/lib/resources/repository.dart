import 'dart:io';
import 'package:flutter/material.dart';
import 'package:k_user_global/common/interactions.dart';
import 'package:k_user_global/models/account_verify_request_model.dart';
import 'package:k_user_global/models/choose_services_request_model.dart';
import 'package:k_user_global/models/login_social_request_model.dart';
import 'package:k_user_global/models/otp_confirm_model.dart';
import 'package:k_user_global/models/request_otp_model.dart';
import 'package:k_user_global/models/update_account_resquest_model.dart';
import 'package:k_user_global/models/update_infomation_request_model.dart';
import 'package:k_user_global/resources/home_resources.dart';
import 'package:k_user_global/resources/job_resources.dart';
import 'package:k_user_global/resources/location_resources.dart';
import 'package:k_user_global/resources/login_resources.dart';
import 'package:k_user_global/resources/manage_proflie_resources.dart';
import 'package:k_user_global/resources/requirement_resources.dart';
import 'account_verification_resources.dart';
import 'package:k_user_global/resources/user_resources.dart';

class Repository {
  LocationResources _locationResources = LocationResources();
  LoginResources _loginResources = LoginResources();
  HomeResources _homeResources = HomeResources();
  UserResources _userResources = UserResources();
  RequirementResources _requirementResources = RequirementResources();
  AccountVerificationResource _accountVerificationResource =
      AccountVerificationResource();
  JobResources _jobResources = JobResources();
  ManageProfileResources _manageProfileResources = ManageProfileResources();
  Future<ApiResponseData> updateInformation(
          BuildContext context, UpdateInfoRequestModel model) =>
      _accountVerificationResource.updateInformation(context, model);

//  AccountVerificationResource _accountVerificationResource =AccountVerificationResource();
  Future<ApiResponseData> accountVerification(
          BuildContext context,
          File imageFront,
          File imageAvatar,
          File imageBehind,
          File healthCertificate,
          File degree,
          VerifyAccountRequestModel model) =>
      _accountVerificationResource.accountVerification(context, imageFront,
          imageAvatar, imageBehind, healthCertificate, degree, model);
  Future<ApiResponseData> getListServices(BuildContext context) =>
      _accountVerificationResource.getListServices(context);

  //manage profile
  Future<ApiResponseData> workerChoseCareer(BuildContext context) =>
      _manageProfileResources.workerChoseCareer(context);
  Future<ApiResponseData> workerChooseCareer(
          BuildContext context, ChooseServicesRequestModel model) =>
      _manageProfileResources.workerChooseCareer(context, model);
  Future<ApiResponseData> getIDCard(BuildContext context) =>
      _manageProfileResources.getIDCard(context);
  Future<ApiResponseData> updateIDCard(BuildContext context, File imageFront,
          File imageBehind, VerifyAccountRequestModel model) =>
      _manageProfileResources.updateIDCard(
          context, imageFront, imageBehind, model);
  Future<ApiResponseData> getAvatar(BuildContext context) =>
      _manageProfileResources.getAvatar(context);
  Future<ApiResponseData> updateAvatar(
          BuildContext context, File avatar, int status) =>
      _manageProfileResources.updateAvatar(context, avatar, status);
  Future<ApiResponseData> getAccountInformation(BuildContext context) =>
      _manageProfileResources.getAccountInformation(context);
  Future<ApiResponseData> updateAccountInformation(
          BuildContext context, UpdateAccountRequestModel model) =>
      _manageProfileResources.updateAccountInformation(context, model);

  Future<ApiResponseData> getSexList(BuildContext context) =>
      _accountVerificationResource.getSexList(context);
  Future<ApiResponseData> submitOtp(
          BuildContext context, OTPConfirmModel model) =>
      _loginResources.submitOtp(context, model);
  Future<ApiResponseData> sendOTP(
          BuildContext context, RequestOtpModel model) =>
      _loginResources.sendOTP(context, model);
  Future<ApiResponseData> loginSocial(
          BuildContext context, LoginSocialRequestModel model) =>
      _loginResources.loginSocial(context, model);
  Future<ApiResponseData> getListRequirement(BuildContext context, int limit) =>
      _requirementResources.getListRequirement(context, limit);
  Future<ApiResponseData> getRefreshToken(BuildContext context) =>
      _loginResources.getRefreshToken(context);
  Future<ApiResponseData> placeDetail(BuildContext context, String id) =>
      _locationResources.placeDetail(context, id);
  Future<ApiResponseData> placeAutocomplete(
          BuildContext context, String text) =>
      _locationResources.placeAutocomplete(context, text);

  // home repo
  Future<ApiResponseData> getWorkerStatus(
          BuildContext context, Map<String, String> params) =>
      _homeResources.getWorkerStatus(context, params);
  Future<ApiResponseData> changeWorkerStatus(
          BuildContext context, Map<String, dynamic> params) =>
      _homeResources.changeWorkerStatus(context, params);

  // user repo
  Future<ApiResponseData> getProfileInfo(
          BuildContext context, Map<String, String> params) =>
      _userResources.getProfileInfo(context, params);
  Future<ApiResponseData> updateAddress(
          BuildContext context, Map<String, dynamic> params) =>
      _userResources.updateAddress(context, params);
  Future<ApiResponseData> updateEmail(
          BuildContext context, Map<String, dynamic> params) =>
      _userResources.updateEmail(context, params);
  Future<ApiResponseData> logout(
          BuildContext context, Map<String, dynamic> params) =>
      _userResources.logout(context, params);

  // job
  Future<ApiResponseData> getJobDetail(
          BuildContext context, Map<String, String> params) =>
      _jobResources.getJobDetail(context, params);

  // refer
  Future<ApiResponseData> getReferInfo(
          BuildContext context, Map<String, String> params) =>
      _userResources.getReferInfo(context, params);
  Future<ApiResponseData> getReferList(
          BuildContext context, Map<String, String> params) =>
      _userResources.getReferList(context, params);
  Future<ApiResponseData> addReferCode(
          BuildContext context, Map<String, dynamic> params) =>
      _userResources.addReferCode(context, params);
  Future<ApiResponseData> removeRefer(
          BuildContext context, Map<String, dynamic> params) =>
      _userResources.removeRefer(context, params);

  // history
  Future<ApiResponseData> getHistory(
          BuildContext context, Map<String, String> params) =>
      _userResources.getHistory(context, params);

  // request
  Future<ApiResponseData> getRequestList(
          BuildContext context, Map<String, String> params) =>
      _userResources.getRequestList(context, params);

  // menu
  Future<ApiResponseData> getProfileMenu(
          BuildContext context, Map<String, String> params) =>
      _userResources.getProfileMenu(context, params);

  // question
  Future<ApiResponseData> getProfileQuestion(BuildContext context, Map<String, String> params) => _userResources.getProfileQuuestion(context, params);
  Future<ApiResponseData> postChooseQustion(BuildContext context, Map<String, dynamic> params) => _userResources.getProfileChoosenQuestion(context, params);

  // list degree
  Future<ApiResponseData> getListDegree(BuildContext context, Map<String, String> params) => _userResources.getListDegree(context, params);

  // list service
  Future<ApiResponseData> getListService(BuildContext context, Map<String, String> params) => _userResources.getListService(context, params);

  // list work service
  Future<ApiResponseData> getListWorkService(BuildContext context, Map<String, String> params) => _userResources.getListWorkService(context, params);

  //location
  Future<ApiResponseData> getListLocation(BuildContext context, Map<String, String> params) => _userResources.getListLocation(context, params);
  Future<ApiResponseData> removeSavedLocation(BuildContext context, Map<String, dynamic> params) => _userResources.removeSavedLocation(context, params);
}
