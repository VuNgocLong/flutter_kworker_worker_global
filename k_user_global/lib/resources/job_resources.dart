import 'package:flutter/material.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/interactions.dart';

class JobResources{
  ApiConnection _interaction = ApiConnection();

  Future<ApiResponseData> getJobDetail(BuildContext context, Map<String, String> params) async {
    return await _interaction.get(context, urlDetailJob, null, params);
  }
}