import 'package:flutter/material.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/interactions.dart';
import 'package:k_user_global/models/login_social_request_model.dart';
import 'package:k_user_global/models/otp_confirm_model.dart';
import 'package:k_user_global/models/request_otp_model.dart';

class LoginResources{
  ApiConnection _interaction = ApiConnection();

  Future<ApiResponseData> sendOTP(BuildContext context, RequestOtpModel model) async {
    return await _interaction.post(context, urlSendOtp, RequestOtpModel().toJson(model));
  }
  Future<ApiResponseData> submitOtp(BuildContext context, OTPConfirmModel model) async {
    return await _interaction.post(context, urlCheckOtp, OTPConfirmModel().toJson(model));
  }
  Future<ApiResponseData> loginSocial(BuildContext context, LoginSocialRequestModel model) async {
    return await _interaction.post(context, urlCheckOtp, LoginSocialRequestModel().toJson());
  }
  Future<ApiResponseData> getRefreshToken(BuildContext context) async {
    return await _interaction.post(context, urlRefreshToken, null);
  }


}