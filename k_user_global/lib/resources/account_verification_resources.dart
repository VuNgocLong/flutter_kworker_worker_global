import 'dart:convert';
import 'dart:io';
import 'package:image/image.dart' as Img;
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/interactions.dart';
import 'package:k_user_global/models/account_verify_request_model.dart';
import 'package:k_user_global/models/choose_services_request_model.dart';
import 'package:k_user_global/models/update_infomation_request_model.dart';
import 'package:http_parser/http_parser.dart';
import 'package:path/path.dart';

class AccountVerificationResource {
  ApiConnection _interaction = ApiConnection();

  Future<ApiResponseData> getSexList(BuildContext context) {
    return _interaction.get(context, urlSexList, null);
  }

  Future<ApiResponseData> getListServices(BuildContext context) {
    return _interaction.get(context, urlGetService, null);
  }

//  urlUpdateInfo
  Future<ApiResponseData> updateInformation(
      BuildContext context, UpdateInfoRequestModel model) {
    return _interaction.post(context, urlUpdateInfo, model.toJson());
  }

  Future<ApiResponseData> accountVerification(
      BuildContext context,
      File imageFront,
      File imageAvatar,
      File imageBehind,
      File healthCertificate,
      File degree,
      VerifyAccountRequestModel model) async {
    http.MultipartFile avatar = imageAvatar == null
        ? null
        : await http.MultipartFile.fromPath(
        "avatar", imageAvatar.path,
        contentType: MediaType.parse("multipart/form-data"),
        filename:  basename(imageAvatar.path));
    http.MultipartFile front = imageFront == null
        ? null
        : await http.MultipartFile.fromPath(
        'identity_card_before', imageFront.path,
        contentType: MediaType.parse("multipart/form-data"),
        filename:   basename(imageFront.path));
    http.MultipartFile behind = imageBehind == null
        ? null
        : await http.MultipartFile.fromPath(
        'identity_card_after', imageBehind.path,
        contentType: MediaType.parse("multipart/form-data"),
        filename:  basename(imageBehind.path));
    http.MultipartFile healthCA = healthCertificate == null
        ? null
        : await http.MultipartFile.fromPath("health_certificate", healthCertificate.path,
        contentType: MediaType.parse("multipart/form-data"),
        filename:  basename(healthCertificate.path));
    http.MultipartFile imageCertificate = degree == null
        ? null
        : await http.MultipartFile.fromPath("degree", degree.path,
        contentType: MediaType.parse("multipart/form-data"),
        filename: basename(degree.path));
//    Map<String, dynamic> params = Map<String, dynamic>();
//    params['id_code'] = model.idCode??"";
//    params['date_range'] = model.dateRange??"";
    return await ApiConnection().postMultiPart(
      context,
      urlVerification,
      [avatar, front, behind, healthCA, imageCertificate],
      model.toJson(),
    );
  }


}
