import 'package:flutter/material.dart';
import 'package:k_user_global/common/interactions.dart';
import 'package:k_user_global/ultilites/ultility.dart';

class LocationResources{

  Future<ApiResponseData> placeAutocomplete(BuildContext context, String text) async {
    return ApiConnection().getGoogleApi(context, getAddressAutoComplete(text));
  }
  Future<ApiResponseData> placeDetail(BuildContext context, String id) async {
    return await ApiConnection().getGoogleApi(context, getPlaceDetail(id));
  }
}