import 'package:flutter/material.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/interactions.dart';

class UserResources{

  Future<ApiResponseData> getProfileInfo(BuildContext context, Map<String, String> params) async {
    return await ApiConnection().get(context, urlProfileInfo, params, null, true);
  }

  Future<ApiResponseData> updateAddress(BuildContext context, Map<String, dynamic> params) async {
    return await ApiConnection().post(context, urlUpdateAddress, params, null, true);
  }

  Future<ApiResponseData> updateEmail(BuildContext context, Map<String, dynamic> params) async {
    return await ApiConnection().post(context, urlUpdateEmail, params, null, true);
  }

  Future<ApiResponseData> logout(BuildContext context, Map<String, dynamic> params) async {
    return await ApiConnection().post(context, urlLogout, params, null, true);
  }

  // history
  Future<ApiResponseData> getHistory(BuildContext context, Map<String, String> params) async {
    return await ApiConnection().get(context, urlListHistory, null,params);
  }

  //refer
  Future<ApiResponseData> getReferInfo(BuildContext context, Map<String, String> params) async {
    return await ApiConnection().get(context, urlReferInfo, params);
  }

  Future<ApiResponseData> getReferList(BuildContext context, Map<String, String> params) async {
    return await ApiConnection().get(context, urlReferList, params);
  }

  Future<ApiResponseData> addReferCode(BuildContext context, Map<String, dynamic> params) async {
    return await ApiConnection().post(context, urlAddReferCode, params);
  }

  Future<ApiResponseData> removeRefer(BuildContext context, Map<String, dynamic> params) async {
    return await ApiConnection().post(context, urlRemoveRefer, params);
  }

  // request
  Future<ApiResponseData> getRequestList(BuildContext context, Map<String, String> params) async {
    return await ApiConnection().get(context, urlListRequest, params);
  }

  // menu
  Future<ApiResponseData> getProfileMenu(BuildContext context, Map<String, String> params) async {
    return await ApiConnection().get(context, urlProfileMenu, params);
  }

  // question
  Future<ApiResponseData> getProfileQuuestion(BuildContext context, Map<String, String> params) async {
    return await ApiConnection().get(context, urlProfileQuestion, null, params);
  }

  Future<ApiResponseData> getProfileChoosenQuestion(BuildContext context, Map<String, dynamic> params) async {
    return await ApiConnection().post(context, urlProfileChooseQuestion, params);
  }

  // list degree
  Future<ApiResponseData> getListDegree(BuildContext context, Map<String, String> params) async {
    return await ApiConnection().get(context, urlListDegree, null,params);
  }

  // list service
  Future<ApiResponseData> getListService(BuildContext context, Map<String, String> params) async {
    return await ApiConnection().get(context, urlListService, null,params);
  }

  // list work service
  Future<ApiResponseData> getListWorkService(BuildContext context, Map<String, String> params) async {
    return await ApiConnection().get(context, urlProfileListWorkService, null,params);
  }

  // location
  Future<ApiResponseData> getListLocation(BuildContext context, Map<String, String> params) async {
    return await ApiConnection().get(context, urlLocationList, null,params);
  }

  Future<ApiResponseData> removeSavedLocation(BuildContext context, Map<String, dynamic> params) async {
    return await ApiConnection().post(context, urlRemoveLocation,params);
  }
}