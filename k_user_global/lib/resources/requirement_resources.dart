

import 'package:flutter/material.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/interactions.dart';

class RequirementResources {
  Future<ApiResponseData> getListRequirement(
      BuildContext context, int limit) async {
    return await ApiConnection()
        .get(context, urlListRequest, null,{'limit': limit.toString()});
  }
}
