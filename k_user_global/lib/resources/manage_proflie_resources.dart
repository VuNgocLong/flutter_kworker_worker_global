import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/interactions.dart';
import 'package:k_user_global/models/account_verify_request_model.dart';
import 'package:k_user_global/models/choose_services_request_model.dart';
import 'package:http_parser/http_parser.dart';
import 'package:k_user_global/models/update_account_resquest_model.dart';
import 'package:path/path.dart';

class ManageProfileResources {
  ApiConnection _interaction = ApiConnection();
  // get worker service
//  urlWorkerService
  Future<ApiResponseData> workerChoseCareer(BuildContext context) {
    return _interaction.get(context, urlWorkerService, null);
  }

  Future<ApiResponseData> workerChooseCareer(
      BuildContext context, ChooseServicesRequestModel model) {
    return _interaction.post(context, urlWorkerService, model.toJson());
  }

  // get id card
  Future<ApiResponseData> getIDCard(BuildContext context) {
    return _interaction.get(context, urlIDCard, null);
  }

  Future<ApiResponseData> updateIDCard(BuildContext context, File imageFront,
      File imageBehind, VerifyAccountRequestModel model) async {
    http.MultipartFile front = imageFront == null
        ? null
        : await http.MultipartFile.fromPath(
            'identity_card_before', imageFront.path,
            contentType: MediaType.parse("multipart/form-data"),
            filename: basename(imageFront.path));
    http.MultipartFile behind = imageBehind == null
        ? null
        : await http.MultipartFile.fromPath(
            'identity_card_after', imageBehind.path,
            contentType: MediaType.parse("multipart/form-data"),
            filename: basename(imageBehind.path));
    return _interaction.postMultiPart(
        context, urlIDCard, [front, behind], model.toJson());
  }

  //avatar
  Future<ApiResponseData> getAvatar(BuildContext context) {
    return _interaction.get(context, urlAvatar, null);
  }

  Future<ApiResponseData> updateAvatar(
      BuildContext context, File avatar, int status) async {
    http.MultipartFile imageAvatar = avatar == null
        ? null
        : await http.MultipartFile.fromPath(
            'avatar', avatar.path,
            contentType: MediaType.parse("multipart/form-data"),
            filename: basename(avatar.path));
    return _interaction.postMultiPart(
        context, urlAvatar, [imageAvatar], {"status_id": status});
  }

  // get account information
  Future<ApiResponseData> getAccountInformation(BuildContext context) {
    return _interaction.get(context, urlAccount, null);
  }

  Future<ApiResponseData> updateAccountInformation(BuildContext context,UpdateAccountRequestModel model) {
    return _interaction.post(context, urlAccount, model.toJson());
  }
}
