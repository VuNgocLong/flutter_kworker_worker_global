import 'package:flutter/material.dart';

class Globals{
  static double maxWidth;
  static double maxHeight;
  static double maxPadding;
  static double minPadding;
  static double statusBarHeight;

  init(BuildContext context){
    maxWidth = MediaQuery.of(context).size.width;
    maxHeight = MediaQuery.of(context).size.height;
    statusBarHeight = MediaQuery.of(context).padding.top;
    maxPadding = maxWidth * 0.05;
    minPadding = maxPadding / 2;
  }
}