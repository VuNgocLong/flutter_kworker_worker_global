import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:k_user_global/models/list_service_response_model.dart';
import 'package:k_user_global/ultilites/hex_color.dart';
// URL
final urlServer = "https://kworkerworkerapi.staging.k-soft.asia/api";
final urlServerImage = "https://kworkerworkerapi.staging.k-soft.asia/";
final urlSendOtp = "/auth/send-otp";
final urlCheckOtp = "/auth/check-otp";
final urlSexList = "/sex/list";
final urlGetService = "/service/list";
final urlUpdateInfo = "/auth/update-info";
final urlVerification ="/profile/verification";
final urlSocial = "/auth/social";
final urlRefreshToken = "/auth/refresh";
final urlListRequest = "/worker-request";
final urlLogout = "/auth/logout";
final urlRegisterDevice = "/profile/register-device";
final urlEnrolWorker = "/booking/enrol";
final urlListBooking = "/booking/list-booking";
final urlDetailBooking= "/job/detail";
final urlStatus = "/job/status";
final urlProfileWorker = "/profile/worker";
final urlProfileChangeStatus = "/profile/change-status";
final urlProfileInfo = "/profile/info";
final urlUpdateEmail = "/profile/update-email";
final urlUpdateAddress = "/profile/update-address";
//final urlListRequest = "/worker-request";
final urlListHistory = "/job/history-booking";
final urlDetailJob = "/job/detail";
final urlReferInfo = "/profile/refer-info";
final urlAddReferCode = "/profile/add-refer-code";
final urlRemoveRefer = "/profile/remove-refer";
final urlReferList = "/profile/list-refer";
final urlWorkerService = "/profile/worker-service";
final urlProfileMenu = "/profile/menu";
final urlAccount = "/profile/account";
final urlProfileQuestion = "/profile/question";
final urlProfileChooseQuestion = "/profile/choose-question";
final urlAvatar = "/profile/avatar";
final urlIDCard = "/profile/id-card";
final urlListDegree = "/profile/list-degree";
final urlListService = "/profile/list-service";
final urlProfileListWorkService = "/profile/list-work-service";

final urlWalletInfo = "/wallet/info";
final urlWalletHistory = "/wallet/history";
final urlWalletPaymentInfo = "/wallet/payment-info";
final urlWalletAlepay = "/wallet/init-alepay";
final urlLocationList = "/location/list-location";
final urlAddLocation = "/location/add";
final urlRemoveLocation = "/location/remove";

// Key
final keyToken = "TOKEN";
final keyAutocomplete = "AUTOCOMPLETE";
final keyLogin = "LOGIN";
final keyIsLogin = "IS_LOGIN";
final keyLanguage = "LANGUAGE";

final keyIsUpdateInfo = "IS_UPDATED_INFO";
final keyAccVerify = "ACCOUNT_VERIFY";
final keyModelAccVerify = "MODULE_ACCOUNT";
final keyIsChoose = "CHOOSE";
final keyModelUser = "MODULE_USER";
final keyIDUser = "USER_ID";
final keyNameUser = "NAME_USER";
final keyEmailUser = "EMAIL_USER";
final keyPhoneUser = "PHONE_USER";
final keyAvtUser = "AVATAR_USER";
final keyPassUser = "PASS_USER";
final keyCurrentAddress = "CURRENT_ADDRESS";
final keyReferCode = "REFER_CODE";
final keyImei = "IMEI";
final keyNotificationToken = "NOTIFICATION_TOKEN";
// Google API key
final googleKey = "AIzaSyCOQljFKBBgtDxzFo56aZPwFHXimeUHQS4";
// Color
final primaryColor = HexColor("0AC67F");
final colorButton = HexColor("0AC67F");
final colorText = HexColor("0AC67F");
final colorBlue = HexColor("72ACDA");
final colorDes = HexColor("993C3C43");
final colorHint = HexColor("4D3C3C43");
final colorOtp = HexColor("0AC67F");
final backgroundWhiteF2 = HexColor("F2F2F2");
final colorGreen = HexColor("0AC67F");
final grayLightColor = HexColor("BFBFBF");
final colorBackground = HexColor("FBFBFB");
final colorLine = HexColor("F2F2F2"); //F2F2F2
final buttonCancel = HexColor("1A1A1A");
final colorNumber = HexColor("30A8FF");
final colorWhitePopup = HexColor("FAFAFA");
final greenLight = HexColor("00E08B");
final greenFont = HexColor("0AC67F");
final colorRed = HexColor("E02020");
final grayTextInputColor = HexColor("#d8d8d8");
final colorBlack = HexColor("#000000");
final colorBlueButton = HexColor("32C5FF");
final statusbarGreen = HexColor("58db89");
final colorTextGrayNew = HexColor("3C3C43");
final colorBottomBar = HexColor("CCCCCC");
final colorSearchBar = HexColor("F3F3F3");
final colorGray3C3 = HexColor("3C3C34");
final colorStatus2 = HexColor("F6BD12");
final colorStatus3 = HexColor("F48120");
final colorLineGrey = HexColor("979797");
final orangeColor = HexColor("FA6400");

//color status job
final colorWaiting = HexColor("60DDE6");
final colorMoving = HexColor("4BABF0");
final colorWaitingToDo = HexColor("954BF0");
final colorWorking = HexColor("F0784B");
final colorWaitingToPay  = HexColor("0AC67F");
final colorWaitingToConfirm = HexColor("0DA0AA");
final colorCancelJob = HexColor("E02020");


final Gradient colorGradientGreen = LinearGradient(
  colors: <Color>[HexColor('00E08B'), HexColor('0AC67F')],
  stops: [0.0, 1.0],
);

final Gradient colorGradientBlue = LinearGradient(
  colors: <Color>[HexColor('32C5FF'), HexColor('0091FF')],
  stops: [0.0, 1.0],
); //linear-gradient(220.1deg, #32C5FF -46.38%, #0091FF 92.03%);
final colorDateSelected = HexColor("0091FF"); //linear-gradient(232.33deg, #32C5FF 10.59%, #0091FF 129.37%);
// Icon
final iconMenu = "assets/icons/menu.png";
final iconCalendar = "assets/icons/calendar.png";
final iconCancel = "assets/icons/cancel.png";
final iconArrowToRight = "assets/icons/arrow-to-right.png";
final iconManageProfile = "assets/icons/icon-manage-profile.png";
final iconOffline = "assets/icons/icon-offline.png";
final iconOnline = "assets/icons/icon_online.png";
final iconCircleGray = "assets/icons/circle-gray.png";
final iconCircleGreen = "assets/icons/iconCircleGreen.png";
final iconBackBlack = "assets/icons/icon-back-black.png";
final iconNextGreen = "assets/icons/icon-next-green.png";
final iconFaceBook = "assets/icons/icon-facebook.png";
final iconGoogle = "assets/icons/icon-google.png";
final iconMarker = "assets/icons/icon-marker.png";
final iconEditLocation = "assets/icons/ic-edit-location.png";
final iconFaceBookNonActive = "assets/icons/icon_facebook_nonactive.png";
final iconStatisticInCome = "assets/icons/analytics.png";
final iconCreateTeam = "assets/icons/multiple-users-silhouette.png";
final iconCompany = "assets/icons/company.png";
final iconSavedPlace = "assets/icons/saved_place.png";
final iconInviteFriend = "assets/icons/love-letter.png";
final iconUrgentCall = "assets/icons/urgent-call.png";
final iconTranslate = "assets/icons/translate.png";
final iconSetting = "assets/icons/settings.png";
final iconSupport = "assets/icons/phone-call.png";
final iconMore = "assets/icons/list.png";
final iconRightArrowBlack = "assets/icons/right_arrow_black.png";
final iconSuccess = "assets/icons/icon-checked.png";
final iconChecked = "assets/icons/checked.png";
final iconMember = "assets/icons/member.png";
final iconArrowToRightBlack = "assets/icons/arrow-pointing-to-right.png";
final iconLoveLetterLarge = "assets/icons/love-letter_large.png";
final iconHome = "assets/icons/home.png";
final iconRequest = "assets/icons/request.png";
final iconWallet = "assets/icons/wallet.png";
final iconClock = "assets/icons/clock.png";
final iconPlus = "assets/icons/plus.png";
final iconVacuum = "assets/icons/vacuum.png";
final iconSearch = "assets/icons/search.png";
final pickerCamera = "assets/icons/icon-picker-camera.png";
final iconAddJob = "assets/icons/icon-add-job.png";
final iconItemJob = "assets/icons/icon-job.png";
final iconPoint = "assets/icons/ic-point.png";
final iconPayIn = "assets/icons/ic-payin.png";
final iconPlusGreen = "assets/icons/plus-green.png";
final iconBlueCircle = "assets/icons/blue-circle.png";
final iconDefaultAvatar = "assets/icons/default_avatar.png";
final iconCameraWhite = "assets/icons/camera-white.png";
final iconCameraGreen = "assets/icons/camera-green.png";
final iconCalendarGreen = "assets/icons/calendar-green.png";
final iconPlusBlue = "assets/icons/plus-blue.png";
final iconTickWhite = "assets/icons/tick-white.png";
final iconCameraPng = "assets/icons/icon-camera.png";
final iconGroup = "assets/icons/icon-group.png";
final iconMsg = "assets/icons/icon-msg.png";
final iconPhone = "assets/icons/icon-phone.png";
final iconTag = "assets/icons/icon-tag.png";
final iconCoin = "assets/icons/icon-coin.png";
final iconMoneyBlack = "assets/icons/icon-money.png";
final phone = "assets/icons/phone.png";
final iconMes = "assets/icons/icon-message.png";
final iconNavigation = "assets/icons/navigation.png";
final iconClearText = "assets/icons/clear_text.png";
// Image
final logo = "assets/images/logo.png";
final splash = "assets/images/splash-k-worker.png";
final bgTop = "assets/images/bg-lang-top.png";
final bgBottom = "assets/images/bg-lang-bottom.png";
final appbar = "assets/images/background_top.png";
final background = "assets/images/background.png";
final logoGreen = "assets/images/logo-green.png";
final noMember = "assets/images/non_member.png";
final noFriend = "assets/images/no_friend.png";
final emptyHistory = "assets/images/empty_history.png";
final defaultAvatar = "assets/images/default-avatar.png";
final noDataJobManage = "assets/images/no-data-job-manage.png";
// SVG
final iconRightArrow = "assets/svg_images/right-arrow-black.svg";
final iconCamera = "assets/svg_images/camera.svg";
final iconEdit = "assets/svg_images/edit.svg";
final iconGrayArrowSvg = "assets/svg_images/gray_arrow.svg";
final iconBlackRightArrow = "assets/svg_images/back_right_arrow.svg";
final iconCloseSvg = "assets/svg_images/close.svg";
final iconBackSvg = "assets/svg_images/icon-back.svg";
final iconCloseWhite = "assets/svg_images/close-white.svg";
final iconPersonSvg = "assets/svg_images/person.svg";
final iconGroupSvg = "assets/svg_images/group.svg";
final iconRightArrowWhiteSvg = "assets/svg_images/right-arrow-white.svg";
final iconEditLocationSvg = "assets/svg_images/ic_edit_location.svg";
final iconEditSvg = "assets/svg_images/edit1.svg";
final iconRightArrowSvg = "assets/svg_images/right-arrow.svg";
final iconMoneySvg = "assets/svg_images/money.svg";
final iconTick = "assets/svg_images/tick.svg";
final iconTip = "assets/svg_images/tip.svg";
final iconGroup3 = "assets/svg_images/group_3.svg";
//String
final niHon = "日本語";
final korean = "한국어";
final english = "English";
final vn = "Tiếng Việt";
// Size
const sizeAppBar = 26.0;
const sizeTitle = 20.0;
const sizeBody1 = 14.0;
const sizeBody2 = 18.0;
const sizeText15 = 15.0;

const sizeText = 13.0;

// Text Style
TextStyle styleLanguage =
    TextStyle(fontSize: 17, color: Colors.black, fontWeight: FontWeight.bold);

TextStyle styleTitleBlackBold =
    TextStyle(fontSize: 20, color: Colors.black, fontWeight: FontWeight.bold);

TextStyle styleTitleWhiteBold = TextStyle(
    fontSize: 20,
    color: Colors.white,
    letterSpacing: 0.75,
    fontWeight: FontWeight.bold);

TextStyle styleHint =
    TextStyle(fontSize: 15, color: colorHint, fontWeight: FontWeight.normal);

TextStyle styleOrder =
TextStyle(fontSize: 13, color: colorDes, fontWeight: FontWeight.normal);
TextStyle styleContent =
    TextStyle(fontSize: 15, color: colorDes, fontWeight: FontWeight.normal);

TextStyle styleTitle =
    TextStyle(fontSize: 15, color: colorText, fontWeight: FontWeight.bold);

TextStyle styleTextBlack =
    TextStyle(fontSize: 15, color: Colors.black, fontWeight: FontWeight.normal);
TextStyle styleOrderContent =
TextStyle(fontSize: 13, color: Colors.black, fontWeight: FontWeight.normal);


TextStyle styleTextBlackNormal = TextStyle(
    fontSize: 17,
    letterSpacing: -0.408,
    color: colorDes,
    fontWeight: FontWeight.normal);
TextStyle styleTextBlackBold = TextStyle(
    fontSize: 17,
    letterSpacing: -0.408,
    color: HexColor("000000"),
    fontWeight: FontWeight.bold);

TextStyle stylePromoTitle =
    TextStyle(fontSize: 13, color: Colors.black, fontWeight: FontWeight.bold);

TextStyle styleContentPromo =
    TextStyle(fontSize: 13, color: colorDes, fontWeight: FontWeight.normal);

TextStyle styleTextWhite =
    TextStyle(fontSize: 15, color: Colors.white, fontWeight: FontWeight.normal);

TextStyle styleTextWhiteBold =
    TextStyle(fontSize: 17, color: Colors.white, letterSpacing: -0.408, fontWeight: FontWeight.bold);
TextStyle styleNumberPromo =
    TextStyle(fontSize: 13, color: colorNumber, fontWeight: FontWeight.bold);

// Font
final String fontPrimary = "UTM Avo";
final String fontSFPro = "SFProText";

// Format
final dateFormat = DateFormat("dd/MM/yyyy");
final vnMoney = NumberFormat("###,###");
final phoneFormat = NumberFormat('###');
final jobFormat = DateFormat("dd/MM/yyyy HH:mm:ss");
final timeFormat = DateFormat("HH:mm");
final findWorkerFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
final dateOrderFormat = DateFormat("HH:mm dd/MM/yyyy");
final dateSelectedFormat = DateFormat("yyyy-MM-dd");
// Callback
typedef LocationCallback = Function();
typedef FavoriteWorkerCallback = Function();

// Force Refresh Job List
class TabBarDefinition {
  static final TabBarDefinition _singleton = new TabBarDefinition._internal();
  TabBarDefinition._internal();
  static TabBarDefinition get instance => _singleton;

  StreamController controller = StreamController<bool>.broadcast();
  var forceRefresh = false;
  var isOpenWebView = false;
  List<Service> selectedJob;

  dispose(){
    controller.close();
  }
}