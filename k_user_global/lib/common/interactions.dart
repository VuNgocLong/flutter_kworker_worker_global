import 'package:http/http.dart' as http;
import 'package:connectivity/connectivity.dart';
import 'package:k_user_global/models/response_model.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'dart:async';
import 'dart:io';
import 'constant.dart';
import 'package:flutter/material.dart';

import 'language/english.dart';
import 'language/korean.dart';
import 'language/language.dart';
import 'language/nihongo.dart';
import 'language/vietnamese.dart';

class ApiConnection {
  final int timeOut = 60;
  SharedPreferences _prefs;
  Language _language;
  ApiConnection() {
    SharedPreferences.getInstance().then((data) {
      _prefs = data;
      String _model = _prefs.getString(keyLanguage);
      if (_model != null) {
        if (_model == 'vi') {
          _language = Vietnamese();
        } else if (_model == 'eng') {
          _language = English();
        } else if (_model == 'korean') {
          _language = Korean();
        } else if (_model == 'japanese') {
          _language = Japanese();
        }
      }
    });
  }

  Future<Map<String, String>> headers(Map<String, String> content) async {
    Map<String, String> headers = {
      "Content-Type": 'application/json',
      "Locale": "vi",
      "Accept": 'application/json'
    };
    if (_prefs == null) _prefs = await SharedPreferences.getInstance();

    String token = _prefs.getString(keyToken) ?? "";
    String language = _prefs.getString(keyLanguage) ?? "";
    if (language != "" && language == "vi") {
      headers["Locale"] = "vi";
    } else if (language != "" && language == "eng") {
      headers["Locale"] = "en";
    } else if (language != "" && language == "japanese") {
      headers["Locale"] = "ja";
    } else if (language != "" && language == "korean") {
      headers["Locale"] = "ko";
    }

    print("Token: " +  token);


    if (token != "") headers["Authorization"] = "Bearer " + token;
    // if (token != "") headers["Authorization"] = token;

    if (content != null) {
      content.forEach((key, value) {
        if (key == "Content-Type")
          headers.update("Content-Type", (val) => value);
        else
          headers[key] = value;
      });
    }
    print(headers.toString());
    return headers;
  }

  Future<ApiResponseData> get(BuildContext context, String url,
      [Map<String, String> header,
      Map<String, dynamic> query,
      bool autoShowDialog = true]) async {
    ApiResponseData data = await _checkConnectivity(context);
    if (data != null) return data;

    String fullUrl = urlServer + url;

    if (query != null) {
      String body = "?";
      query.forEach((key, value) {
        try {
          List<dynamic> values = value;
          values.forEach((data) {
            body = body + key + "=" + data.toString() + "&";
          });
        } catch (ex) {
          body = body + key + "=" + value.toString() + "&";
        }
      });
      body = body.substring(0, body.length - 1);
      fullUrl = fullUrl + body;
    }
    print(fullUrl);
    var response;
    try {
      response = await http
          .get(fullUrl, headers: await headers(header))
          .timeout(new Duration(seconds: timeOut));
    } on TimeoutException catch (_) {
      response = ApiResponseData();
      response.error = _language.stringTimeoutError;
      response.success = false;
      //await showCustomAlertDialog(context, _language.stringNotification, _language.stringTimeoutError);
      return response;
    } on SocketException catch (_) {
      response = ApiResponseData();
      response.error = _language.stringTimeoutError;
      response.success = false;
      await showCustomAlertDialog(context, _language.stringNotification,
          _language.stringTimeoutError, _language.cancel, _language.agree);
      return response;
    } catch (ex) {
      response = ApiResponseData();
      response.error = _language.stringNoConnectionError;
      response.success = false;
      await showCustomAlertDialog(context, _language.stringNotification,
          _language.stringNoConnectionError, _language.cancel, _language.agree);
      return response;
    }
//    print(fullUrl);
    return await _handleResponse(context, response, autoShowDialog);
  }

  Future<ApiResponseData> post(
      BuildContext context, String url, Map<String, dynamic> params,
      [Map<String, String> header, bool autoShowDialog = true]) async {
    ApiResponseData data = await _checkConnectivity(context);
    if (data != null) return data;

    bool isJson = true;
    if (header != null) {
      isJson = !header.values.contains("application/x-www-form-urlencoded");
    }

    print(urlServer + url);

    var response;
    try {
      response = await http
          .post(urlServer + url,
              headers: await headers(header),
              body: isJson ? json.encode(params) : params)
          .timeout(new Duration(seconds: timeOut));
    } on TimeoutException catch (_) {
      response = ApiResponseData();
      response.error = _language.stringTimeoutError;
      response.success = false;
      await openDialog(context, _language.stringTimeoutError,
          _language.stringNotification, _language.agree);
      return response;
    } on SocketException catch (_) {
      response = ApiResponseData();
      response.error = _language.stringTimeoutError;
      response.success = false;
      await openDialog(context, _language.stringTimeoutError,
          _language.stringNotification, _language.agree);
//      await showCustomAlertDialog(context, _language.stringNotification,
//          _language.stringTimeoutError, _language.cancel, _language.agree);
      return response;
    } catch (ex) {
      response = ApiResponseData();
      response.error = _language.stringNoConnectionError;
      response.success = false;
      await openDialog(context, _language.stringNoConnectionError,
          _language.stringNotification, _language.agree);
//      await showCustomAlertDialog(context, _language.stringNotification,
//          _language.stringNoConnectionError, _language.cancel, _language.agree);
      return response;
    }
    print(params);
    return await _handleResponse(context, response, autoShowDialog);
  }

  Future<ApiResponseData> getGoogleApi(BuildContext context, String url) async {
    ApiResponseData data = await _checkConnectivity(context);
    if (data != null) return data;

    var response;
    try {
      response = await http.get(url).timeout(new Duration(seconds: timeOut));
    } on TimeoutException catch (_) {
      response = ApiResponseData();
      response.error = _language.stringTimeoutError;
      response.success = false;
      await openDialog(context, _language.stringTimeoutError,
          _language.stringNotification, _language.agree);
//      await showCustomAlertDialog(context, _language.stringNotification,
//          _language.stringTimeoutError, _language.cancel, _language.agree);
      return response;
    } on SocketException catch (_) {
      response = ApiResponseData();
      response.error = _language.stringTimeoutError;
      response.success = false;
      await openDialog(context, _language.stringTimeoutError,
          _language.stringNotification, _language.agree);
//      await showCustomAlertDialog(context, _language.stringNotification,
//          _language.stringTimeoutError, _language.cancel, _language.agree);
      return response;
    } catch (ex) {
      response = ApiResponseData();
      response.error = _language.stringNoConnectionError;
      response.success = false;
      await openDialog(context, _language.stringNoConnectionError,
          _language.stringNotification, _language.agree);
//      await showCustomAlertDialog(context, _language.stringNotification,
//          _language.stringNoConnectionError, _language.cancel, _language.agree);
      return response;
    }

    return ApiResponseData.fromJson(json.decode(response.body));
  }

  Future<ApiResponseData> postMultiPart(BuildContext context, String url,
      List<http.MultipartFile> files, Map<String, dynamic> params,
      [Map<String, String> header, bool autoShowDialog = true]) async {
    ApiResponseData data = await _checkConnectivity(context);
    if (data != null) return data;

    var request = http.MultipartRequest("POST", Uri.parse(urlServer + url));

    print(urlServer + url);
    String language = _prefs.getString(keyLanguage) ?? "";
    if (language != "" && language == "vi") {
      request.headers["Locale"] = "vi";
    } else if (language != "" && language == "eng") {
      request.headers["Locale"] = "en";
    } else if (language != "" && language == "japanese") {
      request.headers["Locale"] = "ja";
    } else if (language != "" && language == "korean") {
      request.headers["Locale"] = "ko";
    }

    request.headers[HttpHeaders.authorizationHeader] =
        "Bearer " + _prefs.getString(keyToken);
    request.headers[HttpHeaders.contentTypeHeader] = "multipart/form-data";
    if (params != null && params.length > 0) {
      params.forEach((key, value) {
        print(key + ": " + value);
        request.fields[key] = value.toString();
//        try {
//          Map<String, dynamic> map = value;
//          request.fields[key] = json.encode(map).toString();
//        } catch (_) {
//          request.fields[key] = value.toString();
//          print(utf8.decode(utf8.encode(key)));
//        }
      });
    }

    if (files != null) request.files.addAll(files);

    var response;
    var result;
    try {
    result = await request.send();
//      print("Response: "+ await result.stream.bytesToString());
    } on TimeoutException catch (_) {
      response = ApiResponseData();
      response.error = _language.stringTimeoutError;
      response.success = false;
      await openDialog(context, _language.stringTimeoutError,
          _language.stringNotification, _language.agree);
      return response;
    } on SocketException catch (_) {
      response = ApiResponseData();
      response.error = _language.stringTimeoutError;
      response.success = false;
      await openDialog(context, _language.stringTimeoutError,
          _language.stringNotification, _language.agree);
      return response;
    } catch (ex) {
      print(ex.toString());
      response = ApiResponseData();
      response.error = _language.stringNoConnectionError;
      response.success = false;
      await openDialog(context, _language.stringNoConnectionError,
          _language.stringNotification, _language.agree);
      return response;
    }
    response = http.Response(
      await result.stream.bytesToString(),
      result.statusCode,
    );


    return await _handleResponse(context, response, autoShowDialog);
  }

  Future<ApiResponseData> _checkConnectivity(BuildContext context) async {
    var connectivityResult = await (new Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      var response = ApiResponseData();
      response.error = _language.stringNoConnectionError;
      response.success = false;
      await openDialog(context, _language.stringNoConnectionError,
          _language.stringNotification, _language.agree);
      return response;
    }

    return null;
  }

  Future<ApiResponseData> _handleResponse(
      BuildContext context, http.Response response, bool autoShowDialog) async {
    print(response.statusCode.toString() + 'bbbbbb');
    print("Response body:" + response.body.toString());
    if (response.statusCode == 401) {
      if (response.body == "") {
        var response = ApiResponseData();
        await openDialog(context, _language.stringErrorCodeData,
            _language.stringNotification, _language.agree);
        response.error = _language.stringErrorCodeData;
        response.success = false;
        return response;
      } else {
        final data = ResponseModel.fromJson(json.decode(response.body));
        if (data.status == -1) {
          await logout(context, true, _language);
          var response = ApiResponseData();
          response.error = data.messager;
          response.success = false;
          return response;
        } else {
          var responseData =
              ApiResponseData.fromJson(json.decode(response.body));
          return responseData;
        }
      }
    } else if (response.statusCode == 200) {
      if (response.body == "") {
        var response = ApiResponseData();
        await openDialog(context, _language.stringErrorCodeData,
            _language.stringNotification, _language.agree);
        response.error = _language.stringErrorCodeData;
        response.success = false;
        return response;
      } else {
        final data = ResponseModel.fromJson(json.decode(response.body));
        if (data.status == 1)
          return ApiResponseData.fromJson(json.decode(response.body));
        else if (data.status == -1) {
          await openDialog(context, data.messager, _language.stringNotification,
              _language.agree);
          var response = ApiResponseData();
          response.error = data.messager;
          response.success = false;
          return response;
        } else {
          return ApiResponseData.fromJson(json.decode(response.body));
        }
      }
    } else if (response.statusCode >= 500) {
      await openDialog(context, _language.stringServerError,
          _language.stringNotification, _language.agree);
      var response = ApiResponseData();
      response.error = _language.stringErrorCodeData;
      response.success = false;
      return response;
    } else {
      if (response.body == "") {
        var response = ApiResponseData();
        await openDialog(context, _language.stringErrorCodeData,
            _language.stringNotification, _language.agree);
        response.error = _language.stringErrorCodeData;
        response.success = false;
        return response;
      } else {
        final data = ResponseModel.fromJson(json.decode(response.body));
        if (data.status == -1) {
          await openDialog(context, data.messager, _language.stringNotification,
              _language.agree);
          var response = ApiResponseData();
          response.error = _language.stringNotification;
//          response.data = json.decode(response.body);
          response.success = false;
          return response;
        } else {
          var responseData =
              ApiResponseData.fromJson(json.decode(response.body));
          return responseData;
        }
      }
    }
  }

  Future<ApiResponseData> placeAutocomplete(
      BuildContext context, String text, String key) async {
    ApiResponseData data = await _checkConnectivity(context);
    if (data != null) return data;

    var response;
    try {
      response = await http
          .get('https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' +
              text +
              '&components=country:vn&location=16.179526,107.623369&radius=1600&key=' +
              key)
          .timeout(new Duration(seconds: timeOut));
    } on TimeoutException catch (_) {
      response = ApiResponseData();
      response.error = _language.stringTimeoutError;
      response.success = false;
      await openDialog(context, _language.stringTimeoutError,
          _language.stringNotification, _language.agree);
//      await showCustomAlertDialog(context, _language.stringNotification,
//          _language.stringTimeoutError, _language.cancel, _language.agree);
      return response;
    } on SocketException catch (_) {
      response = ApiResponseData();
      response.error = _language.stringTimeoutError;
      response.success = false;
      await openDialog(context, _language.stringTimeoutError,
          _language.stringNotification, _language.agree);
//      await showCustomAlertDialog(context, _language.stringNotification,
//          _language.stringTimeoutError, _language.cancel, _language.agree);
      return response;
    } catch (ex) {
      response = ApiResponseData();
      response.error = _language.stringNoConnectionError;
      response.success = false;
      await openDialog(context, _language.stringNoConnectionError,
          _language.stringNotification, _language.agree);
//      await showCustomAlertDialog(context, _language.stringNotification,
//          _language.stringNoConnectionError, _language.cancel, _language.agree);
      return response;
    }

    return ApiResponseData.fromJson(json.decode(response.body));
  }
}

class ApiResponseData {
  bool success;
  String error;
  Map<String, dynamic> data;
  ApiResponseData() {
    this.success = false;
    this.error = "";
  }

  ApiResponseData.fromJson(Map<String, dynamic> json) {
    if (json != null) {
      data = json;
      success = true;
      error = '';
    } else {
      data = null;
      success = false;
      error = Language().stringErrorCodeData;
    }
  }
}
