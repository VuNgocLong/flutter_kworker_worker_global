class Language {
  String stringNoConnectionError;
  String stringErrorCodeData;
  String stringTimeoutError;
  String stringServerError;
  String stringNotification;
  String hi;
  String inputText;
  String loginGoogle;
  String textPhone;
  String otp;
  String resendOtp ;
  String textOtp;
  String receiveOtp;
  String stringMissPhone;
  String agree;
  String cancel;
  String monetaryUnit;
  String missToken; // "Token Expired"
  String countryCode;
  String missCountryCode;
  String manageProfile;
  String statisticIncome;
  String createTeam;
  String group;
  String savedLocation;
  String inviteFriend;
  String urgentCall;
  String language;
  String setting;
  String support;
  String more;
  String change;
  String begin;
  String experience;
  String welcome;
  String changeInformation;
  String inputYourName;
  String inputYourEmail;
  String connectWithOtherApplication;
  String stringReturn;
  String stringSeeUNextTime;
  String stringLogoutDescription1;
  String stringLogoutDescription2;
  String logOut;
  String complete;
  String technique;
  String inputFullyToComplete;
  String nameTeam;
  String address;
  String description;
  String stringContinue;
  String yourTechnique;

  String manageTeam;
  String update;
  String member;
  String waitingForConfirm;
  String addMember;

  String updateInfo;
  String updateInfoTeam;
  String basicInfo;
  String addTechnique;

  String nullMemberDescription1;
  String nullMemberDescription2;
  String addMemberDescription;
  String phoneYouWantToFind;

  String listInvitedFriend;
  String inviteFriendDescription1;
  String inviteFriendDescription2;
  String addFriend;
  String addFriend1;
  String addFriendDescription1;
  String addFriendDescription2;
  String inviteCode;
  String orShareLink;
  String kWorker;
  String hintInviteCode;
  String copy;

  String home;
  String wallet;
  String request;
  String history;
  String chooseNewMajor;
  String chooseMajor;
  String searchMajor;
  String confirm;

  String stringEmptyHistory;
  String stringAll;
  String stringCompleted;
  String stringDenied;
  String balance;
  String nearlyTransaction;
  String watchAll;
  String payIn;
  String addLocation;
  String changeLanguage;
  String settingNotification;
  String settingPrivacy;
  String version;
  String setting1;
  String setting2;
  String setting3;
  String confirm1;
  String settingDescription1;
  String settingDescription2;
  String settingDescription3;
  String logout;

  String statusProfile1;
  String statusProfile2;
  String statusProfile3;
  String statusProfile4;
  String statusProfile5;
  String personalInformation;
  String submitResume;
  String training;
  String account;
  String chooseJob;
  String yourImage;
  String yourIDCardImage;
  String oneWorkerInformation;
  String twoPersonalInformation;
  String completeToFindJob;
  String takePortraitPhoto;
  String confirmAccount;
  String phone;
//  String idCardImage;
  String idCardNumberOrPassport;
  String createDate;
  String createdDateErrorData;
  String confirmImage;
  String frontImage;
  String backImage;
  String time;
  String typeOfJob;
  String totalInCome;

  String inputYourAddress;
  String productivity;
  String nonProductivity;
  String findMajor;
  String Member;
  String accountInformation;
  String lastName;
  String firstName;
  String gender;
  String dob;
  String permanent;

  String questionTesting;
  String completeAllQuestion;

  String order;
  String confirmJob;
  String denyJob;

  String monetary;
  String caller;
  String quantity;
  String note;
  String confirmJobSuccess;
  String confirmJobSuccessDescription;
  String denyJobByCustomer;
  String denyJobByCustomerDescription;
  String watchNow;
  String denyJobTitle;
  String denyJobDescription;
  String cancel1;

  String manageMember;
  String choosePayIn;
  String inputMoney;
  String choosePaymentMethod;
  String memberInformation;
  String workingTime;
  String workingTimeInDay;
  String workingTimeOnlyChangeAfter14Days;
  String startTime;
  String endTime;
  String totalWorkingTimeInDay;
  String hour;

  String paymentReceipt;
  String payInFrom;

  String stringDeleteAddress;
  String stringDeleteAddressDescription;
}


