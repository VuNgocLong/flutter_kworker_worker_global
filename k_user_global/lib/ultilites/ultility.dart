import 'dart:io';

import 'package:android_intent/android_intent.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/interactions.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/models/temp_address.dart';
import 'package:k_user_global/modules/login_module/splash_module/src/ui/splash_ui.dart';
import 'package:k_user_global/modules/user_module/manage_profile_module/account_information_module/src/ui/account_information_ui.dart';
import 'package:k_user_global/modules/user_module/manage_profile_module/id_card_module/src/ui/id_card_ui.dart';
import 'package:k_user_global/modules/user_module/manage_profile_module/portrait_module/src/ui/portrail_ui.dart';
import 'package:k_user_global/modules/user_module/manage_team_module/testing_module/src/ui/testing_ui.dart';
import 'package:k_user_global/resources/repository.dart';
import 'package:k_user_global/widgets/widget.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './progress_dialog.dart';
import 'package:flutter/cupertino.dart';

ProgressDialog pr;

showProgressDialog(BuildContext context) {
  pr = ProgressDialog(context);
  pr.show();
}

hideProgressDialog() {
  if (pr != null) if (pr.isShowing()) {
    pr.hide();
    pr = null;
  }
}

hideKeyboard(BuildContext context) {
  FocusScope.of(context).unfocus();
}

fieldFocus(BuildContext context, FocusNode focusNode) {
  FocusScope.of(context).requestFocus(focusNode);
}

fieldFocusChange(BuildContext context, FocusNode present, FocusNode after) {
  present.unfocus();
  FocusScope.of(context).requestFocus(after);
}

navigatorPushAndRemoveUntil(BuildContext context, Widget screen) {
  Navigator.of(context, rootNavigator: true).pushAndRemoveUntil(
      MaterialPageRoute(builder: (context) => screen),
      (Route<dynamic> route) => false);
}

navigatorPush(BuildContext context, Widget screen, {bool root = true}) async {
  return await Navigator.of(context, rootNavigator: root).push(SlideLeftRoute(
    page: screen,
  ));
}

navigatorPop(BuildContext context, {bool root = true}) {
  Navigator.of(context, rootNavigator: root).pop();
}

navigatorPopWithData(BuildContext context, dynamic event, {bool root = true}) {
  Navigator.of(context, rootNavigator: root).pop(event);
}

navigatorPushReplacement(BuildContext context, Widget screen,
    {bool root = true}) {
  Navigator.of(context, rootNavigator: root).pushReplacement(SlideLeftRoute(
    page: screen,
  ));
}

showCustomAlertDialog(BuildContext context, String title, String content,
    String titleButton1, String titleButton2,
    {Function onSubmitted,
    bool enableCancel = false,
    bool cancelable = true}) async {
  await Navigator.of(context).push(
    PageRouteBuilder(
      opaque: false,
      pageBuilder: (BuildContext context, Animation<double> animation,
          Animation<double> secondaryAnimation) {
        return CustomDialog(
          screen: CustomAlertDialog(
            title: title,
            content: content,
            onSubmitted: onSubmitted,
            enableCancel: enableCancel,
            titleButton1: titleButton1,
            titleButton2: titleButton2,
          ),
          cancelable: cancelable,
        );
      },
    ),
  );
}

showCustomPopupDialog(BuildContext context, Widget title, Widget child,
    {bool root = true, bool isExpanded = false}) async {
  return Navigator.of(context, rootNavigator: root).push(
    PageRouteBuilder(
      opaque: false,
      pageBuilder: (BuildContext context, Animation<double> animation,
          Animation<double> secondaryAnimation) {
        return FadeTransition(
          opacity: animation,
          child: Platform.isAndroid
              ? CustomDialog(
                  screen: CustomPopupDialog(
                    title: title,
                    child: child,
                    isExpanded: isExpanded,
                  ),
                )
              : FormKeyboardActions(
                  child: Container(
                  height: Globals.maxHeight,
                  child: CustomDialog(
                    screen: CustomPopupDialog(
                      title: title,
                      child: child,
                      isExpanded: isExpanded,
                    ),
                  ),
                )),
        );
      },
    ),
  );
}

showCancelSuccessDialog(BuildContext context, String icon, String content,
    {bool root = true, bool cancelable = true}) async {
  await Navigator.of(context, rootNavigator: root).push(
    PageRouteBuilder(
      opaque: false,
      pageBuilder: (BuildContext context, Animation<double> animation,
          Animation<double> secondaryAnimation) {
        return FadeTransition(
            opacity: animation,
            child: CustomDialog(
              cancelable: cancelable,
              screen: Container(
                height: Globals.maxHeight / 3,
                padding: EdgeInsets.symmetric(
                    horizontal: Globals.minPadding,
                    vertical: Globals.maxPadding),
                decoration: BoxDecoration(
                    color: colorWhitePopup,
                    borderRadius: BorderRadius.all(Radius.circular(8.0))),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: 86.0,
                      width: 86.0,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: AssetImage(icon), fit: BoxFit.cover)),
                    ),
                    Container(
                      height: Globals.maxPadding,
                    ),
                    Text(
                      content,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 22,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ));
      },
    ),
  );
}

//navigatorPushReplacement(BuildContext context, Widget screen,
//    {bool root = true}) {
//  Navigator.of(context, rootNavigator: root).pushReplacement(SlideLeftRoute(
//    page: screen,
//  ));
//}
openDialog(
    BuildContext context, String message, String title, String agree) async {
  return await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          title: Text(
            title,
            textAlign: TextAlign.center,
            style: styleTitleBlackBold,
          ),
          content: Text(
            message,
            textAlign: TextAlign.center,
          ),
          actions: <Widget>[
            SimpleDialogOption(
              child: Text(
                // 'Đồng ý',
                agree,
                style: styleTitleBlackBold,
              ),
              onPressed: () => Navigator.of(context).pop(),
            )
          ],
        );
      });
}

Future<bool> checkGPS(BuildContext context) async {
  Geolocator geolocator = Geolocator()..forceAndroidLocationManager = true;
  GeolocationStatus geolocationStatus =
      await geolocator.checkGeolocationPermissionStatus();
  if (geolocationStatus.value == 2) {
    if (await geolocator.isLocationServiceEnabled()) {
      return true;
    } else {
      showCustomAlertDialog(
          context,
          'Thông báo',
          "Không thể lấy vị trí hiện tại.\nVui lòng kiểm tra GPS !!!",
          '',
          "Đồng ý", onSubmitted: () async {
        if (await geolocator.isLocationServiceEnabled()) {
          navigatorPop(context, root: true);
        } else {
          final AndroidIntent intent = AndroidIntent(
              action: 'android.settings.LOCATION_SOURCE_SETTINGS');

          intent.launch();
        }
      }, enableCancel: false);
//      openDialogWithAction(context,
//          "Không thể lấy vị trí hiện tại.\nVui lòng kiểm tra GPS !!!", [
//            SimpleDialogOption(
//              child: Text(
//                "Đồng ý",
//                style: TextStyle(color: orangeAppColor),
//              ),
//              onPressed: () async {
//                if (await geolocator.isLocationServiceEnabled()) {
//                  navigatorPop(context, root: true);
//                } else {
//                  final AndroidIntent intent = AndroidIntent(
//                      action: 'android.settings.LOCATION_SOURCE_SETTINGS');
//
//                  intent.launch();
//                }
//              },
//            )
//          ]);
      return false;
    }
  } else {
//    openDialog(context,
//        "Ứng dụng cần truy cập vị trí hiện tại của bạn vui lòng cho phép tại Cài đặt");
    return false;
  }
}

logout(BuildContext context, bool expired, Language language) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String _language = prefs.getString(keyLanguage);
  bool chooseLanguage = prefs.getBool(keyIsChoose);
//  String basicInfo = prefs.getString(keyBasicInfo) ?? "";
  bool accVerify = prefs.getBool(keyAccVerify);
  if (expired) {
    await showCustomAlertDialog(context, language.stringNotification,
        language.missToken, language.cancel, language.agree, enableCancel: true,
        onSubmitted: () {
      prefs.clear();
      prefs.setString(keyLanguage, _language);
      prefs.setBool(keyIsChoose, chooseLanguage);
      prefs.setString(keyLanguage, _language);
      prefs.setBool(keyIsChoose, chooseLanguage);
      Navigator.of(context).popUntil((route) => route.isFirst);
      navigatorPushReplacement(context, ScreenSplash());
    }, cancelable: false);
  } else {
    showProgressDialog(context);
    ApiResponseData response = await Repository().logout(context, null);
    hideProgressDialog();
    if (response.success) {
      prefs.clear();
      prefs.setString(keyLanguage, _language);
      prefs.setBool(keyIsChoose, chooseLanguage);
//      prefs.setString(keyBasicInfo, basicInfo);
      prefs.setBool(keyAccVerify, accVerify);
      Navigator.of(context).popUntil((route) => route.isFirst);
      navigatorPushReplacement(context, ScreenSplash());
    }
  }
}

dialogChooLocation(
    BuildContext context, String title, double lng, double lat, bool  isRegister) async {
  TextEditingController _controller = TextEditingController();
  return await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          contentPadding: EdgeInsets.zero,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(8.0))),
          content: Container(
              decoration: BoxDecoration(
                  color: colorWhitePopup,
                  borderRadius: new BorderRadius.all(Radius.circular(5))),
              height: Globals.maxHeight * 0.4,
              padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 14.0),
              child: Column(
                children: <Widget>[
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(bottom: 11),
                    child: Text(
                      title,
                      style: TextStyle(
                          color: colorGreen,
                          fontSize: 17,
                          fontWeight: FontWeight.bold),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Expanded(
                      child: Container(
//                        constraints: BoxConstraints(maxHeight: Globals.maxHeight*0.75),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 15.0, bottom: 5.0),
                          child: Text(
                            'Thêm mô tả',
                            style: TextStyle(
                                fontSize: 13.0, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                          height: 80.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          child: TextField(
                            controller: _controller,
                            style:
                                TextStyle(color: Colors.grey, fontSize: 15.0),
                            maxLines: 4,
                            decoration: InputDecoration(
                                hintText: '- Tầng 2 tòa nhà A, lô B',
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.only(left: 20.0),
                                hintMaxLines: 2),
                          ),
                        )
                      ],
                    ),
                  )),
                  Container(
                      margin: EdgeInsets.only(bottom: 20.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: InkWell(
                                child: Container(
                                    margin:
                                        EdgeInsets.only(left: 10.0, right: 7.5),
                                    height: 50.0,
                                    decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(26)),
                                      color: Colors.black,
                                    ),
                                    child: Center(
                                        child: Text(
                                      "Bỏ qua",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ))),
                                onTap: () {
                                  if(isRegister){
                                    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
                                        statusBarColor: Colors.white, // Color for Android
                                        statusBarBrightness:
                                        Brightness.dark // Dark == white status bar -- for IOS.
                                    ));
                                    navigatorPop(context);
                                    navigatorPop(context);
                                  }else
                                  navigatorPop(context);
                                }),
                          ),
                          Expanded(
                            child: InkWell(
                                child: Container(
                                    margin:
                                        EdgeInsets.only(left: 7.5, right: 10.0),
                                    height: 50.0,
                                    decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(26)),
                                      color: greenLight,
                                    ),
                                    child: Center(
                                        child: Text(
                                      "Đồng ý",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ))),
                                onTap: () async {
                                  SystemChrome.setSystemUIOverlayStyle(
                                      SystemUiOverlayStyle.dark.copyWith(
                                          statusBarColor:
                                              Colors.white, // Color for Android
                                          statusBarBrightness: Brightness
                                              .dark // Dark == white status bar -- for IOS.
                                          ));
                                  TempAddress temp = TempAddress();
                                  temp.address = title;
                                  temp.lng = lng;
                                  temp.lat = lat;
                                  temp.note = _controller.value.text;
                                  navigatorPop(context);
                                  Navigator.of(context).pop(temp);
                                }),
                          ),
                        ],
                      ))
                ],
              )),
        );
      });
}
String getGraphAPIFb(String field, String token) {
  return 'https://graph.facebook.com/v2.12/me?fields=' +
      field +
      '&access_token=' +
      token;
}
String getAddressAutoComplete(String text) {
  return 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' +
      text +
      '&components=country:vn&location=16.179526,107.623369&radius=1600&key=' +
      googleKey;
}

String getGeocode(double lat, double lng) {
//  String key = TabBarDefinition.instance.googleKeyFB != ""
//      ? TabBarDefinition.instance.googleKeyFB
//      : TabBarDefinition.instance.googleAPIKey;
  String url =
      'https://maps.google.com/maps/api/geocode/json?language=vi&latlng=' +
          lat.toString() +
          ',' +
          lng.toString() +
          '&key=' +
          googleKey;
  print(url);
  return url;
}

String getPlaceDetail(String id) {
//  String key = TabBarDefinition.instance.googleKeyFB != ""
//      ? TabBarDefinition.instance.googleKeyFB
//      : TabBarDefinition.instance.googleAPIKey;
  String url =
      'https://maps.googleapis.com/maps/api/place/details/json?placeid=' +
          id +
          '&key=' +
          googleKey;
  print(url);
  return url;
}

changeSetting(
    BuildContext context, String title, String desc1, String confirm) async {
  return await showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) {
        return AlertDialog(
          contentPadding: EdgeInsets.zero,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(15.0))),
          content: Container(
            decoration: BoxDecoration(
                color: colorWhitePopup,
                borderRadius: new BorderRadius.all(Radius.circular(5))),
            height: 200,
            width: Globals.maxWidth * 0.8,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 20.0, left: 10.0, right: 10.0),
                  child: AutoSizeText(
                    title,
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 24.0,
                        fontFamily: fontSFPro),
                    maxLines: 1,
                    minFontSize: 16.0,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                      top: 20.0, left: 10.0, right: 10.0, bottom: 20.0),
                  child: AutoSizeText(desc1,
                      style: TextStyle(
                          color: colorHint,
                          fontSize: 16.0,
                          fontFamily: fontSFPro),
                      maxLines: 2,
                      textAlign: TextAlign.center,
                      minFontSize: 8.0),
                ),
                Container(
                  height: 0.8,
                  color: colorHint.withOpacity(0.3),
                  width: Globals.maxWidth,
                ),
                InkWell(
                  child: Container(
                    margin: EdgeInsets.only(top: 14.0),
                    child: AutoSizeText(confirm,
                        style: TextStyle(
                            color: greenLight,
                            fontSize: 18.0,
                            fontFamily: fontSFPro,
                            letterSpacing: 2.0),
                        maxLines: 1,
                        minFontSize: 8.0),
                  ),
                  onTap: () {
                    Navigator.of(context).pop(true);
                  },
                )
              ],
            ),
          ),
        );
      });
}

confirmJob(
    BuildContext context, String title, String desc1, String confirm) async {
  return await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          contentPadding: EdgeInsets.zero,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(15.0))),
          content: Container(
            decoration: BoxDecoration(
                color: colorWhitePopup,
                borderRadius: new BorderRadius.all(Radius.circular(5))),
            height: 300,
            width: Globals.maxWidth * 0.95,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: 65.0,
                  width: 65.0,
                  padding: EdgeInsets.all(20.0),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle, color: primaryColor),
                  child: Image.asset(iconTickWhite),
                ),
                Container(
                  margin: EdgeInsets.only(top: 20.0, left: 10.0, right: 10.0),
                  child: AutoSizeText(
                    title,
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 24.0,
                        fontFamily: fontSFPro),
                    maxLines: 1,
                    minFontSize: 16.0,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                      top: 20.0, left: 10.0, right: 10.0, bottom: 20.0),
                  child: AutoSizeText(desc1,
                      style: TextStyle(
                          color: colorHint,
                          fontSize: 16.0,
                          fontFamily: fontSFPro),
                      maxLines: 2,
                      textAlign: TextAlign.center,
                      minFontSize: 8.0),
                ),
                Container(
                  height: 0.8,
                  color: colorHint.withOpacity(0.3),
                  width: Globals.maxWidth,
                ),
                InkWell(
                  child: Container(
                    margin: EdgeInsets.only(top: 14.0),
                    child: AutoSizeText(confirm,
                        style: TextStyle(
                            color: greenLight,
                            fontSize: 18.0,
                            fontFamily: fontSFPro,
                            letterSpacing: 2.0),
                        maxLines: 1,
                        minFontSize: 8.0),
                  ),
                  onTap: () {
                    Navigator.of(context).pop(true);
                  },
                )
              ],
            ),
          ),
        );
      });
}

denyJobByCustomer(
    BuildContext context, String title, String desc1, String confirm) async {
  return await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          contentPadding: EdgeInsets.zero,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(15.0))),
          content: Container(
            decoration: BoxDecoration(
                color: colorWhitePopup,
                borderRadius: new BorderRadius.all(Radius.circular(5))),
            height: 300,
            width: Globals.maxWidth * 0.95,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: 65.0,
                  width: 65.0,
                  padding: EdgeInsets.all(20.0),
                  decoration:
                      BoxDecoration(shape: BoxShape.circle, color: colorRed),
                  child: Image.asset(iconTickWhite),
                ),
                Container(
                  margin: EdgeInsets.only(top: 20.0, left: 10.0, right: 10.0),
                  child: AutoSizeText(
                    title,
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 24.0,
                        fontFamily: fontSFPro),
                    maxLines: 1,
                    minFontSize: 16.0,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                      top: 20.0, left: 10.0, right: 10.0, bottom: 20.0),
                  child: AutoSizeText(desc1,
                      style: TextStyle(
                          color: colorHint,
                          fontSize: 16.0,
                          fontFamily: fontSFPro),
                      maxLines: 2,
                      textAlign: TextAlign.center,
                      minFontSize: 8.0),
                ),
                Container(
                  height: 0.8,
                  color: colorHint.withOpacity(0.3),
                  width: Globals.maxWidth,
                ),
                InkWell(
                  child: Container(
                    margin: EdgeInsets.only(top: 14.0),
                    child: AutoSizeText(confirm,
                        style: TextStyle(
                            color: greenLight,
                            fontSize: 18.0,
                            fontFamily: fontSFPro,
                            letterSpacing: 2.0),
                        maxLines: 1,
                        minFontSize: 8.0),
                  ),
                  onTap: () {
                    Navigator.of(context).pop(true);
                    Navigator.of(context).pop(true);
                  },
                )
              ],
            ),
          ),
        );
      });
}

denyJobByWorker(BuildContext context, String title, String desc1,
    String confirm, String deny) async {
  return await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          contentPadding: EdgeInsets.zero,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(15.0))),
          content: Container(
            decoration: BoxDecoration(
                color: colorWhitePopup,
                borderRadius: new BorderRadius.all(Radius.circular(5))),
            height: 220,
            width: Globals.maxWidth * 0.95,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 20.0, left: 10.0, right: 10.0),
                  child: AutoSizeText(
                    title,
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 24.0,
                        fontFamily: fontSFPro),
                    maxLines: 1,
                    minFontSize: 16.0,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                      top: 20.0, left: 10.0, right: 10.0, bottom: 20.0),
                  child: AutoSizeText(desc1,
                      style: TextStyle(
                          color: colorHint,
                          fontSize: 16.0,
                          fontFamily: fontSFPro),
                      maxLines: 2,
                      textAlign: TextAlign.center,
                      minFontSize: 8.0),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    InkWell(
                      child: Container(
                        height: 50.0,
                        width: Globals.maxWidth * 0.35,
                        margin: EdgeInsets.only(top: 14.0),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30.0),
                            color: Colors.black),
                        child: Center(
                          child: AutoSizeText(deny,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18.0,
                                  fontFamily: fontSFPro,
                                  letterSpacing: 2.0,
                                  fontWeight: FontWeight.bold),
                              maxLines: 1,
//                            textAlign: TextAlign.center,
                              minFontSize: 8.0),
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context).pop(true);
                      },
                    ),
                    InkWell(
                      child: Container(
                        height: 50.0,
                        width: Globals.maxWidth * 0.35,
                        margin: EdgeInsets.only(top: 14.0),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30.0),
                            color: primaryColor),
                        child: Center(
                          child: AutoSizeText(confirm,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18.0,
                                  fontFamily: fontSFPro,
                                  letterSpacing: 2.0,
                                  fontWeight: FontWeight.bold),
                              maxLines: 1,
                              minFontSize: 8.0),
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context).pop(true);
                        Navigator.of(context).pop(true);
                      },
                    ),
                  ],
                ),
                Container(
                  height: 20.0,
                )
              ],
            ),
          ),
        );
      });
}
showCustomBottomDialog(BuildContext context, Widget screen,
    {bool root = true}) {
  return Navigator.of(context, rootNavigator: root).push(
    PageRouteBuilder(
      opaque: false,
      pageBuilder: (BuildContext context, Animation<double> animation,
          Animation<double> secondaryAnimation) {
        return FadeTransition(
          opacity: animation,
          child: CustomDialog(
            isBottom: true,
            screen: screen,
            bottom: true,
          ),
        );
      },
    ),
  );
}

switchScreenMenu(BuildContext context, int id) {
  switch (id) {
    case 2:// Thông tin tài khoản
      navigatorPush(context, AccountInformationScreen());
      break;
    case 3: // ảnh chân dung
      navigatorPush(context, PortraitScreen());
      break;
    case 4: // lựa chọn nghề
      navigatorPush(context, PortraitScreen());
      break;
    case 5: // ảnh chứng minh nhân dân
      navigatorPush(context, IdCardScreen());
      break;
    case 8:
      navigatorPush(context, TestingScreen());
      break;
    case 9:
      navigatorPush(context, TestingScreen());
      break;
    case 10:
      navigatorPush(context, TestingScreen());
      break;
  }
}
