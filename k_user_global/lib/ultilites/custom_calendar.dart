import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:k_user_global/common/constant.dart';

class CustomCalendar extends StatefulWidget {
  final List<DateTime> listDateSelected;
  final Color selectedColor;
  final Color currentDateColor;
  final Color dateSelectedColor;
  final Color backgroundColor;

  final Function(DateTime event) onChanged;

  CustomCalendar(
      {this.listDateSelected,
      this.selectedColor = Colors.blue,
      this.currentDateColor = Colors.green,
      this.dateSelectedColor = Colors.red,
      this.backgroundColor = Colors.white,
      this.onChanged})
      : assert(selectedColor != null &&
            currentDateColor != null &&
            dateSelectedColor != null &&
            backgroundColor != null);

  @override
  _CustomCalendarState createState() => _CustomCalendarState();
}

class _CustomCalendarState extends State<CustomCalendar> {
  DateTime _now, _dateSelected, _monthSelected;

  DateFormat _format = DateFormat("M, yyyy");

  double _padding = 20.0;
//  double _padding = Globals.minPadding;

  List<List<DateTime>> _currentMonth;

  SwiperController _controller = SwiperController();

  int _indexInit;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _now = DateTime.now();
    _now = DateTime(_now.year, _now.month, _now.day, 0, 0, 0);
    _dateSelected =
        DateTime.fromMillisecondsSinceEpoch(_now.millisecondsSinceEpoch);
    _monthSelected =
        DateTime.fromMillisecondsSinceEpoch(_now.millisecondsSinceEpoch);

    _addCurrentMonth(_monthSelected, null);
  }

  _addCurrentMonth(DateTime currentMonth, bool isNext) {
    _currentMonth = List<List<DateTime>>();
    int index = 0;
    int indexSwiper;
    DateTime temp = DateTime(currentMonth.year, currentMonth.month, 1, 0, 0, 0);
    if (temp.weekday > 1)
      temp = temp.subtract(Duration(days: temp.weekday - 1));

    while (
        (temp.month <= currentMonth.month && temp.year == currentMonth.year) ||
            temp.year < currentMonth.year) {
      List<DateTime> event = List<DateTime>();
      for (int i = 0; i < 7; i++) {
        if (temp.millisecondsSinceEpoch == _now.millisecondsSinceEpoch)
          indexSwiper = index;

        event.add(temp);
        temp = temp.add(Duration(days: 1));
      }
      _currentMonth.add(event);
      index++;
    }

    if (temp.weekday != 1) {
      do {
        _currentMonth[_currentMonth.length - 1].add(temp);
        temp = temp.add(Duration(days: 1));
      } while (temp.weekday != 1);
    }

    if (_indexInit == null) {
      if (indexSwiper != null) {
        _indexInit = indexSwiper;
      } else {
        if (isNext == null || isNext)
          _indexInit = 0;
        else
          _indexInit = _currentMonth.length - 1;
      }
    }
  }

  String _getWeekDay(int weekDay) {
    if (weekDay == 1) return "T2";
    if (weekDay == 2) return "T3";
    if (weekDay == 3) return "T4";
    if (weekDay == 4) return "T5";
    if (weekDay == 5) return "T6";
    if (weekDay == 6) return "T7";
    return "CN";
  }

  Widget _buildButton(IconData icon, Function onTap) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.all(10.0),
        child: Icon(
          icon,
          size: 20.0,
          color: Colors.grey,
        ),
      ),
    );
  }

  Widget _buildMonth() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        _buildButton(Icons.arrow_back_ios, () {
          _monthSelected = DateTime(
              _monthSelected.month == 1
                  ? _monthSelected.year - 1
                  : _monthSelected.year,
              _monthSelected.month == 1 ? 12 : _monthSelected.month - 1,
              1,
              0,
              0,
              0);
          _addCurrentMonth(_monthSelected, false);

          setState(() {});
        }),
        Expanded(
          child: Text(
            "Tháng " + _format.format(_monthSelected),
            style: TextStyle(fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),
        _buildButton(Icons.arrow_forward_ios, () {
          _monthSelected = DateTime(
              _monthSelected.month == 12
                  ? _monthSelected.year + 1
                  : _monthSelected.year,
              _monthSelected.month == 12 ? 1 : _monthSelected.month + 1,
              1,
              0,
              0,
              0);
          _addCurrentMonth(_monthSelected, true);

          setState(() {});
        }),
      ],
    );
  }

  Widget _buildDate(DateTime dateTime) {
    dateTime = DateTime(dateTime.year, dateTime.month, dateTime.day, 0, 0, 0);

    bool isNow = false;
    bool isSelected = false;
    bool isMonth = false;
    bool isDateSelected = false;

    if (dateTime.millisecondsSinceEpoch == _now.millisecondsSinceEpoch)
      isNow = true;

    if (dateTime.millisecondsSinceEpoch == _dateSelected.millisecondsSinceEpoch)
      isSelected = true;

    if (dateTime.month == _monthSelected.month &&
        dateTime.year == _monthSelected.year) isMonth = true;

    if (widget.listDateSelected != null) {
      try {
        DateTime temp = widget.listDateSelected.firstWhere((model) {
          model = DateTime(model.year, model.month, model.day, 0, 0, 0);
          if (model.millisecondsSinceEpoch == dateTime.millisecondsSinceEpoch)
            return true;
          return false;
        });
        if (temp != null) isDateSelected = true;
      } catch (ex) {}
    }

    return Expanded(
      child: GestureDetector(
        onTap: () {
          setState(() {
            _dateSelected = dateTime;
            if (widget.onChanged != null) widget.onChanged(dateTime);
          });
        },
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 2.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(100.0)),
              gradient: isSelected
                  ? (isNow ? colorGradientGreen : colorGradientBlue)
                  : null,
              color: isSelected
                  ? (isNow ? widget.currentDateColor : widget.selectedColor)
                  : widget.backgroundColor),
          padding: EdgeInsets.symmetric(vertical: _padding),
          child: Column(
            children: <Widget>[
              Expanded(
                child: AutoSizeText(
                  _getWeekDay(dateTime.weekday),
                  style: TextStyle(
                      fontSize: 15,
                      color: isSelected
                          ? widget.backgroundColor
                          : (isNow ? widget.currentDateColor : Colors.black)),
                  minFontSize: 8.0,
                ),
              ),
              Container(
                height: _padding / 2,
              ),
              Text(
                dateTime.day.toString(),
                style: TextStyle(
                    fontSize: 20.0,
                    color: isSelected
                        ? widget.backgroundColor
                        : (isMonth
                            ? (isNow ? widget.currentDateColor : Colors.black)
                            : Colors.grey)),
              ),
              Container(
                height: 2.0,
              ),
              Container(
                width: 5.0,
                height: 5.0,
                decoration: BoxDecoration(
                    color: isDateSelected
                        ? widget.dateSelectedColor
                        : Colors.transparent,
                    shape: BoxShape.circle),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildDates(List<DateTime> models) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 5.0),
      child: Row(
        children: models.map((model) {
          return _buildDate(model);
        }).toList(),
      ),
    );
  }

  Widget _buildListDates() {
    return Container(
      height: _padding * 5,
      child: Swiper(
        itemBuilder: (BuildContext context, int index) {
          return _buildDates(_currentMonth[index]);
        },
        itemCount: _currentMonth.length,
        loop: false,
        controller: _controller,
        index: _indexInit,
        onIndexChanged: (event) {
          _indexInit = event;
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: widget.backgroundColor,
      child: Column(
        children: <Widget>[_buildMonth(), _buildListDates()],
      ),
    );
  }
}
