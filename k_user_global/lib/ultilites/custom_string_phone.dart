import 'package:k_user_global/common/language/language.dart';

String numberPhone(String input, Language language){
  String updatePhone ="";
  if(input.startsWith('0')){
    updatePhone =  "(${language.countryCode??''}) "+ input.substring(1, 4)+" "+ input.substring(4, 7)+" "+input.substring(7, 10);
  } else  updatePhone = "(${language.countryCode??''}) "+ input.substring(0, 3)+" "+ input.substring(3, 6)+" "+input.substring(6, 9);
  return updatePhone;
}