import 'package:flutter/services.dart';
import 'package:k_user_global/common/constant.dart';

class NumberTextInputFormatter extends TextInputFormatter {

  NumberTextInputFormatter();

  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {

    if(newValue.text != ""){
      try{
        int number = int.tryParse(newValue.text.trim());

        if(number == 0){
          return TextEditingValue(
              text: "0",
              composing: newValue.composing,
              selection: newValue.selection
          );
        }
        String phone = phoneFormat.format(number);
        print(phone);
//        return newValue;
        return TextEditingValue(
            text: phone,
            composing: newValue.composing,
            selection: newValue.selection
        )
      ;}
      catch(_){}
    }
    return TextEditingValue(
        text: newValue.text,
        composing: newValue.composing,
        selection: newValue.selection
    );
//  return newValue;
  }

}