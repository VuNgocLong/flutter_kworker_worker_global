
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';

class CustomImagePicker {
  static const MethodChannel _channel =
  MethodChannel("flutter.io/requestPermission");

  static Future<File> pickImage({
    @required ImageSource source,}) async {
    assert(source != null);

    if(Platform.isAndroid){
      bool permission = false;
      if(source == ImageSource.gallery){
        permission = await _channel.invokeMethod<bool>(
            'gallery'
        );
      }
      else{
        permission = await _channel.invokeMethod<bool>(
            'camera'
        );
      }

      if(!permission)
        return null;
      else
        return await ImagePicker.pickImage(source: source);
    } else {
      return await ImagePicker.pickImage(source: source);
    }
  }
}

