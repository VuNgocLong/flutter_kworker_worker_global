import 'package:flutter/cupertino.dart';
import 'package:k_user_global/common/interactions.dart';
import 'package:k_user_global/models/history_model.dart';
import 'package:k_user_global/models/order_model.dart';
import 'package:k_user_global/resources/repository.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:rxdart/rxdart.dart';

class OrderBloc {
  final _repository = Repository();

  final _streamOrder = BehaviorSubject<OrderModel>();

  Observable<OrderModel> get outputOrder => _streamOrder.stream;

  setOrder(OrderModel event) => _streamOrder.sink.add(event);

  getJobDetail(BuildContext context, int jobId) async {
    Map<String, String> params = Map<String, String>();
    params['job_id'] = "22";
    showProgressDialog(context);
    ApiResponseData responseData = await _repository.getJobDetail(context, params);
    hideProgressDialog();
    if (responseData!= null){
      if (responseData.success){
        OrderModel order = OrderModel.fromJson(responseData.data);
        setOrder(order);
      }
    }
  }

  dispose(){
    _streamOrder.close();
  }
}