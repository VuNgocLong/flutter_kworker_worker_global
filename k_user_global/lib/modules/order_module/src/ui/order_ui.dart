import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/models/order_model.dart';
import 'package:k_user_global/modules/order_module/src/bloc/order_bloc.dart';
import 'package:k_user_global/ultilites/hex_color.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:k_user_global/widgets/custom_progress.dart';
import 'package:k_user_global/widgets/widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OrderScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<OrderScreen> with TickerProviderStateMixin{
  SharedPreferences _prefs;
  Language _language;
  AnimationController progressController;
  Animation<double> animation;
  // bloc init
  OrderBloc _bloc;

  @override
  void initState(){
    super.initState();
    _bloc = OrderBloc();

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
        statusBarColor: greenLight, // Color for Android
        statusBarBrightness: Brightness.light // Dark == white status bar -- for IOS.
    ));

    _language = Language();
    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        } else {
          _language = Vietnamese();
        }
      });
    });
    progressController = AnimationController(
        vsync: this, duration: Duration(seconds: 60));
    animation = Tween<double>(begin: 60, end: 0).animate(progressController)
      ..addListener(() {
        setState(() {

        });
      });
    progressController.forward();
    animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
//        _bloc.workerRejectOrder(context);
      }
    });
    WidgetsBinding.instance.addPostFrameCallback((_){
      _bloc.getJobDetail(context, 0);
    });
  }
  dispose() {
    progressController.dispose();
    super.dispose();
  }


  Widget _setupBackground() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(34.0)),
          image: DecorationImage(
            image: AssetImage(background),
            fit: BoxFit.cover,
          )),
    );
  }

  Widget _buildContent(OrderModel order) {
    return Container(
//      padding: EdgeInsets.fromLTRB(Globals.minPadding, 0.0,
//          Globals.minPadding, 0.0),
      margin: EdgeInsets.only(bottom: 20.0),
      padding: EdgeInsets.only(top: 20.0),
      width: Globals.maxWidth,
      height: Globals.maxHeight - AppBar().preferredSize.height-MediaQuery.of(context).padding.top,
      decoration: BoxDecoration(
        color: backgroundWhiteF2,
        borderRadius: BorderRadius.all(Radius.circular(34.0)),
      ),
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(15.0),
              height: 60.0,
              width: 60.0,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8.0),
                  color: HexColor("#FF5E62")
              ),
              child: Image.asset(iconVacuum),
            ),
            Container(
              margin: EdgeInsets.only(top: 5.0, bottom: 5.0),
              child: Text(
                order.data.service.name ?? "",
                style: TextStyle(fontSize: 20.0, fontFamily: fontSFPro, fontWeight: FontWeight.bold),
              ),
            ),
            Center(
              child: Container(
                margin: EdgeInsets.only(left: 30.0, right: 30.0, top: 5.0, bottom: 10.0),
                child: Text(
                  order.data.address ?? "",
                  style: TextStyle(fontSize: 15.0, fontFamily: fontSFPro, color: colorHint),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            _renderItem(_language.caller ?? "", order.data.client.name ?? "", true),
            _renderItem(_language.quantity ?? "", order.data.jobAmount.toString() ?? "", false),
            _renderItem(_language.time ?? "", order.data.createdAt ?? "", false),
            _renderItem(_language.note ?? "", order.data.description ?? "", false),

            Container(
              height: 100.0,
              width: 100.0,
              alignment: Alignment.bottomCenter,
              margin: EdgeInsets.only(bottom: Globals.maxPadding),
              child: CustomPaint(
                foregroundPainter: CircleProgress(animation.value),
                child: Center(
                  child: Text(
                    // timerString,
                    '${animation.value.toInt()}s',
                    style: TextStyle(
                        fontSize: 28,
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _renderBottom(OrderModel order){
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(),
        Container(
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 15.0, bottom: 15.0, left: 20.0, right: 20.0),
                height: 0.5,
                width: Globals.maxWidth,
                color: colorLineGrey,
              ),
              Container(
                padding: EdgeInsets.only(left: 20.0, right: 20.0),
                margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(right: 5.0),
                            child: SvgPicture.asset(iconMoneySvg, height: 20.0, width: 20.0,),
                          ),
                          Expanded(
                            child: Text('Tiền mặt',
                              style: TextStyle(fontSize: 11.0, fontFamily: fontSFPro, fontWeight: FontWeight.bold),
                              overflow: TextOverflow.ellipsis,),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(right: 5.0),
                            child: SvgPicture.asset(iconTick, height: 20.0, width: 20.0,),
                          ),
                          Expanded(
                            child: Text('Khuyến mãi',
                              style: TextStyle(fontSize: 11.0, fontFamily: fontSFPro, fontWeight: FontWeight.bold),
                              overflow: TextOverflow.ellipsis,),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(right: 5.0),
                            child: SvgPicture.asset(iconTip, height: 20.0, width: 20.0,),
                          ),
                          Text('Tip',
                            style: TextStyle(fontSize: 11.0, fontFamily: fontSFPro, fontWeight: FontWeight.bold),
                            overflow: TextOverflow.ellipsis,),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(right: 5.0),
                      child: Text(
                        _language.monetary ?? "",
                        style: TextStyle(fontFamily: fontSFPro, fontSize: 15.0),
                      ),
                    ),
                    Container(
                      child: Text(
                        order.data.totalPayment ?? "",
                        style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, fontFamily: fontSFPro),
                      ),
                    )
                  ],
                ),
              ),
              InkWell(
                child: Container(
                  margin: EdgeInsets.only(right: 20.0, left: 20.0, bottom: 10.0),
                  width: Globals.maxWidth,
                  height: 50.0,
                  decoration: BoxDecoration(
                      color: greenLight,
                      borderRadius: BorderRadius.circular(30.0)
                  ),
                  child: Center(
                    child: Text(
                      _language.confirmJob ?? "",
                      style: TextStyle(fontSize: 20.0, fontFamily: fontSFPro, color: Colors.white),
                    ),
                  ),
                ),
                onTap: (){
                  confirmJob(context, _language.confirmJobSuccess ?? "", _language.confirmJobSuccessDescription ?? "", _language.watchNow ?? "");
                },
              ),
              InkWell(
                child: Container(
                  margin: EdgeInsets.only(right: 20.0, left: 20.0, bottom: 10.0),
                  width: Globals.maxWidth,
                  height: 50.0,
                  child: Center(
                    child: Text(
                      _language.denyJob ?? "",
                      style: TextStyle(fontSize: 20.0, fontFamily: fontSFPro, color: Colors.red),
                    ),
                  ),
                ),
                onTap: (){
                  denyJobByWorker(context, _language.denyJobTitle ?? "", _language.denyJobDescription ?? "", _language.confirm1 ?? "", _language.cancel1 ?? "");
                },
              )
            ],
          ),
        )
      ],
    );
  }

  Widget _renderItem(String title, String content, bool isName){
    return Container(
      margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Container(
              padding: EdgeInsets.only(left: 20.0),
              child: AutoSizeText(
                title + ": " ?? "",
                style: TextStyle(fontFamily: fontSFPro, color: colorHint, fontSize: 15.0, fontWeight: FontWeight.bold),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
          Expanded(
            flex: 4,
            child: Container(
              padding: EdgeInsets.only(right: 20.0),
              child: Text(
                content ?? "",
                style: isName ? TextStyle(fontSize: 15.0, fontFamily: fontSFPro, fontWeight: FontWeight.bold) : TextStyle(fontSize: 15.0, fontFamily: fontSFPro),
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
//    Globals().init(context);
    return Scaffold(
      body: Container(
        color: Colors.black,
        constraints: BoxConstraints.expand(),
        child: StreamBuilder(
          stream: _bloc.outputOrder,
          builder: (_, snapshot){
            if (snapshot.hasData){
              OrderModel model = snapshot.data;
              return Stack(
                children: <Widget>[
                  _setupBackground(),
                  Positioned(
                    left: 0.0,
                    top: 0.0,
                    right: 0.0,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        CustomAppBar(
                          title:  _language.order ?? "",
                          isClose: false,
                        ),
                        _buildContent(model)
                      ],
                    ),
                  ),
                  _renderBottom(model),
                ],
              );
            } else return Container();
          },
        )
      ),
    );
  }
}