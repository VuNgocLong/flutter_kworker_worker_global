import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/modules/request_module/src/bloc/request_bloc.dart';
import 'package:k_user_global/widgets/TitleHeader.dart';

class RequestState extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<RequestState>{
  RequirementBloc _bloc;
  @override
  void initState() {
    super.initState();
    _bloc = RequirementBloc();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
        statusBarColor: greenLight, // Color for Android
        statusBarBrightness: Brightness.light // Dark == white status bar -- for IOS.
    ));
    WidgetsBinding.instance.addPostFrameCallback((_){
      _bloc.getListRequirement(context, 10);
    });
  }

  @override
  Widget build(BuildContext context) {
    return TitleHeader(
        title: 'Quản lý yêu cầu',
        isBack: true,
        customIcon: Container(
            child: SvgPicture.asset(iconCloseWhite)
        ),
        child: Stack(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 10.0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30.0),
                  color: Color(0xffF2F2F2)
              ),
              child: StreamBuilder(
                  stream: _bloc.outputListRequirement,
                  initialData: null,
                  builder: (_, snapshot)  {
                    if(snapshot.data==null) return Container();
                    List<Map<String, dynamic>> models = snapshot.data;
                    if(models.length==0||models==null) return _noData();
                    return ListView(
                      children: <Widget>[
                        _renderRequest('Will', '', 0xffFF5E62, '1h', 'đã gửi một yêu cầu gian hạn', 1),
                        _renderRequest('Mary', '', 0xffFF5E62, '1h', 'đã gửi một yêu cầu gian hạn', 0),
                        _renderRequest('K-worker', '', 0xffFF5E62, '1h', 'đã chấp nhận yêu cầu công việc', 1),
                      ],
                    );
                  }
              ),
            ),
          ],
        )
    );
  }
  Widget _noData() {
    return Container(
      width: Globals.maxWidth,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            height: Globals.maxWidth * 0.7,
            width: Globals.maxWidth * 0.7,
            child: Image.asset(noDataJobManage),
          ),
//        Container(
//            margin: EdgeInsets.only(
//                top: Globals.minPadding,
//                left: Globals.maxPadding,
//                right: Globals.maxPadding),
//            child: Text(
//              'Danh sách công việc đang trống. Hãy giao thêm công việc cho chúng tôi',
//              style: TextStyle(
//                color: grayTextInputColor,
//              ),
//              textAlign: TextAlign.center,
//            )),
//        Container(
//            margin: EdgeInsets.only(top: 10.0),
//            child: Text(_language.stringEmptyPromotion2 ?? "",
//                style: TextStyle(color: grayTextInputColor))
//        )
        ],
      ),
    );
  }


  Widget _renderRequest(String name, String serviceImage, int color, String time, String description, int person){
    return InkWell(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
              height: 60.0,
              margin: EdgeInsets.only(top: 20.0, left: 16.0, right: 16.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Stack(
                        children: <Widget>[
                          Container(
                            height: 60.0,
                            width: 60.0,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
//                            borderRadius: BorderRadius.circular(8.0),
                                color: Color(color)
                            ),
                          ),
                          person == 1 ? Container(
                            margin: EdgeInsets.only(top: 35.0, left: 35.0),
                            height: 26.0,
                            width: 26.0,
                            padding: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0, bottom: 5.0),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: greenLight
                            ),
                            child: SvgPicture.asset(iconPersonSvg),
                          ) :Container(
                            margin: EdgeInsets.only(top: 35.0, left: 35.0),
                            height: 26.0,
                            width: 26.0,
                            padding: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0, bottom: 5.0),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: greenLight
                            ),
                            child: SvgPicture.asset(iconGroupSvg),
                          ),
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(left: 15.0),
                                child: Text(name,
                                  style: TextStyle(fontSize: sizeText, fontFamily: 'SFProText', fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.center,),
                              ),
                              Container(
                                child: Text(' ' +  description,
                                  style: TextStyle(fontSize: sizeText),
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,),
                              )
                            ],
                          ),
                          Container(
                            padding: EdgeInsets.only(left: 15.0),
                            child: Text(
                              time,
                              style: TextStyle(fontSize: sizeText, color: Colors.grey),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ],
              )
          ),
          Container(
            margin: EdgeInsets.only(top: 5.0),
            color: grayLightColor,
            height: 0.5,
            width: MediaQuery.of(context).size.width * 0.9,
          )
        ],
      ),
      onTap: (){
      },
    );
  }
}