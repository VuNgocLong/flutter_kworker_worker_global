import 'package:flutter/material.dart';
import 'package:k_user_global/common/interactions.dart';
import 'package:k_user_global/resources/repository.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:rxdart/rxdart.dart';

class RequirementBloc{
  final _repository = Repository();

  final _streamListRequirement = BehaviorSubject<List<Map<String, dynamic>>>();
  Observable<List<Map<String, dynamic>>> get outputListRequirement => _streamListRequirement.stream;
  setLisRequirement(List<Map<String, dynamic>> event)=> _streamListRequirement.sink.add(event);

  dispose(){
    _streamListRequirement.close();
  }
  getListRequirement(
      BuildContext context, int limit)async{
    showProgressDialog(context);
    ApiResponseData responseData = await _repository.getListRequirement(context, limit);
    hideProgressDialog();
    if(responseData.success){
      setLisRequirement([]);
    }

  }



}