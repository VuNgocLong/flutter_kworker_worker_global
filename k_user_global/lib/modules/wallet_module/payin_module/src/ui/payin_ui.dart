import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/models/temp_cashlist.dart';
import 'package:k_user_global/models/temp_paymentmethod.dart';
import 'package:k_user_global/modules/wallet_module/src/bloc/payin_bloc.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:k_user_global/widgets/widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PayInScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<PayInScreen>{
  SharedPreferences _prefs;
  Language _language;

  // text controller
  TextEditingController _cashController = TextEditingController();

  //bloc init
  PayInBloc _bloc;

  // init list
  PaymentMethod paymentMethod = PaymentMethod();
  TempCashList list = TempCashList();

  @override
  void initState(){
    super.initState();
    _language = Language();
//    list =
    _bloc = PayInBloc();

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
        statusBarColor: greenLight, // Color for Android
        statusBarBrightness: Brightness.light // Dark == white status bar -- for IOS.
    ));

    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        }
      });
    });

    WidgetsBinding.instance.addPostFrameCallback((_){
      initList();
    });
  }

  void dispose(){
    _bloc.dispose();
  }

  Widget _buildContent() {
    return Container(
        padding: EdgeInsets.fromLTRB(Globals.minPadding, 0,
            Globals.minPadding, 0),
        width: Globals.maxWidth,
        height: Globals.maxHeight - AppBar().preferredSize.height-MediaQuery.of(context).padding.top,
        decoration: BoxDecoration(
          color: colorBackground,
          borderRadius: BorderRadius.all(Radius.circular(30.0)),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: ListView(
                children: <Widget>[
                  _inputCash(),
                  _choosePaymentMethod(),
                ],
              ),
            ),
            StreamBuilder(
              stream: _bloc.outputCash,
              builder: (_, snapshot){
                if (snapshot.hasData){
                  String cash = snapshot.data;
                  return StreamBuilder(
                    stream: _bloc.outputPaymentMethod,
                    builder: (_, snapshot){
                      if(snapshot.hasData){
                        PaymentData payment = snapshot.data;
                        if (payment.id == 1){
                          return InkWell(
                            child: Container(
                                height: 50.0,
                                width: 335.0,
                                margin: EdgeInsets.only(
                                    left: 20.0, right: 20.0, bottom: 37.0),
                                decoration: BoxDecoration(
                                    color: Color(0xff0AC67F),
                                    borderRadius: BorderRadius.circular(26.0)),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      _language.payIn ?? "",
                                      style: TextStyle(
                                          fontSize: 20.0,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                )),
                            onTap: (){
//                              payInByCashDialog(context, _language.payInCashTitle, _language.payInCashDesc1,  _language.payInCashDesc2, _language.stringReturn);
                            },
                          );
                        } else if (payment != null && cash != ""){
                          return InkWell(
                            child: Container(
                                height: 50.0,
                                width: 335.0,
                                margin: EdgeInsets.only(
                                    left: 20.0, right: 20.0, bottom: 37.0),
                                decoration: BoxDecoration(
                                    color: Color(0xff0AC67F),
                                    borderRadius: BorderRadius.circular(26.0)),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      _language.payIn ?? "",
                                      style: TextStyle(
                                          fontSize: 20.0,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                )),
                          );
                        }
                      }
                      return _buttonPayInInActive();
                    },
                  );
                }
                return _buttonPayInInActive();
              },
            ),
          ],
        )
    );
  }

  Widget _setupBackground() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
//          borderRadius: BorderRadius.all(Radius.circular(34.0)),
          image: DecorationImage(
            image: AssetImage(background),
            fit: BoxFit.cover,
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.black,
        constraints: BoxConstraints.expand(),
        child: Stack(
          children: <Widget>[
            _setupBackground(),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  CustomAppBar(
                    title: _language.payIn ?? "",
                    onTapLeading: () => navigatorPop(context),
                  ),
                  _buildContent()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _inputCash(){
    return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(10.0),
              child: Text(
                _language.choosePayIn ?? "",
                style: TextStyle(fontSize: 18.0, fontWeight:  FontWeight.bold, fontFamily: fontSFPro),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: list != null && list.data != null ? list.data.map((value) => _renderCash(value)).toList() : List<Container>(),
            ),
            TextField(
              controller: _cashController,
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                WhitelistingTextInputFormatter.digitsOnly,
              ], // Only numbers can be entered
              style: TextStyle(
                  fontSize: 15.0, fontFamily: fontSFPro),
              decoration: InputDecoration(
                hintText: _language.inputMoney ?? "",
                hintStyle: TextStyle(
                    color: colorHint,
                    fontSize: 15.0,
                    fontFamily: fontSFPro),
                contentPadding: EdgeInsets.only(left: 15.0),
              ),
              onChanged: (value){
                _bloc.setCash(value);
              },
            ),
//          Container(
//            margin: EdgeInsets.only(left: 10.0, right: 10.0),
//            height: 1,
//            width: Globals.maxWidth,
//            color: colorHint,
//          ),
          ],
        )
    );
  }

  Widget _choosePaymentMethod(){
    return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 40.0, left: 10.0, bottom: 10.0, right: 10.0),
              child: Text(
                _language.choosePaymentMethod ?? "",
                style: TextStyle(fontSize: 18.0, fontWeight:  FontWeight.bold, fontFamily: fontSFPro),
              ),
            ),
            paymentMethod.data != null && paymentMethod.data.length > 0 ? Column(
                children: paymentMethod.data.map((value) => _renderPaymentItem(value)).toList()
            ): Container()
          ],
        )
    );
  }

  Widget _renderCash(Cash model){
    return model.isSelected ?  InkWell(
      child: Container(
        height: 40.0,
        width: Globals.maxWidth * 0.25,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
          color: colorGreen,
        ),
        child: Center(
          child: Text(
            model.value ?? "",
            style: TextStyle(fontFamily: fontSFPro, color: Colors.white),
          ),
        ),
      ),
      onTap: (){
        for (var o in list.data) {
          o.isSelected = false;
        }
        _cashController.text = "";
        setState(() {
        });
      },
    ) : InkWell(
      child: Container(
        height: 40.0,
        width: Globals.maxWidth * 0.25,
        decoration: BoxDecoration(
          border: Border.all(color: colorHint),
          borderRadius: BorderRadius.circular(5.0),
          color: Colors.white,
        ),
        child: Center(
          child: Text(
            model.value,
            style: TextStyle(fontFamily: fontSFPro),
          ),
        ),
      ),
      onTap: (){
        for (var o in list.data) {
          o.isSelected = false;
        }
        model.isSelected = true;
        _cashController.text = model.value;
        _bloc.setCash(model.value);
        setState(() {
        });
      },
    );
  }

  Widget _renderPaymentItem(PaymentData data) {
    return InkWell(
      child: Container(
        height: 52.0,
        padding: EdgeInsets.only(left: 10.0, bottom: 10.0, right: 10.0),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                    height: 20.0,
                    width: 20.0,
                    child: Image.asset(data.icon)
                ),
                Container(
                  width: Globals.minPadding,
                ),
                Expanded(
                  child: Text(
                    data.name ?? "",
                    style: TextStyle(
                        fontSize: 15.0,
                        fontWeight: data.isSelected != true
                            ? FontWeight.normal
                            : FontWeight.bold),
                  ),
                ),
                data.isSelected == true
                    ? Container(
                  width: 15,
                  height: 15,
                  margin: EdgeInsets.only(right: 15.0),
                  child: Image.asset('assets/icons/checked.png'),
                )
                    : Container(),
              ],
            ),
            Container(
              margin: EdgeInsets.only(top: 15.0),
              height: 0.5,
              color: grayLightColor,
            )
          ],
        ),
      ),
      onTap: () {
        if (data.isSelected == false) {
          for (var o in paymentMethod.data) {
            o.isSelected = false;
          }
          data.isSelected = true;
//          _bloc.setPaymentMethod(data);
        }
        setState(() {
        });
      },
    );
  }

  void initList(){
    paymentMethod.data = new List<PaymentData>();
    list.data = new List<Cash>();

    PaymentData data1 = new PaymentData();
    data1.id = 1;
    data1.isSelected = false;
    data1.name = "Tiền mặt";
    data1.icon = "assets/icons/icon-money.png";
    paymentMethod.data.add(data1);

    PaymentData data = new PaymentData();
    data.id = 2;
    data.isSelected = false;
    data.name = "Thanh toán Alepay";
    data.icon = "assets/icons/icon-alepay.png";

    paymentMethod.data.add(data);

    Cash cash = Cash();
    cash.id = 1;
    cash.value = "100.000";
    cash.isSelected = false;
    Cash cash2 = Cash();
    cash2.id = 2;
    cash2.value = "200.000";
    cash2.isSelected = false;
    Cash cash3 = Cash();
    cash3.id = 3;
    cash3.value = "300.000";
    cash3.isSelected = false;

    list.data.add(cash);
    list.data.add(cash2);
    list.data.add(cash3);
  }

  Widget _buttonPayInInActive(){
    return InkWell(
      child: Container(
          height: 50.0,
          width: 335.0,
          margin: EdgeInsets.only(
              left: 20.0, right: 20.0, bottom: 37.0),
          decoration: BoxDecoration(
              color: Color(0xff0AC67F).withOpacity(0.5),
              borderRadius: BorderRadius.circular(26.0)),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                _language.payIn ?? "",
                style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ],
          )),
    );
  }
}