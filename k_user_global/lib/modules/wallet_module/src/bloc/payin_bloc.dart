import 'package:k_user_global/models/temp_paymentmethod.dart';
import 'package:k_user_global/resources/repository.dart';
import 'package:rxdart/rxdart.dart';

class PayInBloc {
  final _repository = Repository();

  final _streamCash = BehaviorSubject<String>();
  final _streamPaymentMethod = BehaviorSubject<PaymentData>();

  Observable<String> get outputCash => _streamCash.stream;
  Observable<PaymentData> get outputPaymentMethod => _streamPaymentMethod.stream;

  setCash(String model) => _streamCash.sink.add(model);
  setPaymentMethod(PaymentData model) => _streamPaymentMethod.sink.add(model);





  dispose(){
    _streamCash.close();
    _streamPaymentMethod.close();
  }
}