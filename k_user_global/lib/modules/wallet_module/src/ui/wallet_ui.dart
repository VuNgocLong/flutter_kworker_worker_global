import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/models/temp_transaction_model.dart';
import 'package:k_user_global/modules/wallet_module/payin_module/src/ui/payin_ui.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:k_user_global/widgets/widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class WalletScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<WalletScreen> {
  SharedPreferences _prefs;
  Language _language;

  // init data
  List<Transaction> listTransaction = List<Transaction>();

  @override
  void initState() {
    super.initState();
    _language = Language();

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
        statusBarColor: greenLight, // Color for Android
        statusBarBrightness:
            Brightness.light // Dark == white status bar -- for IOS.
        ));

    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        } else {
          _language = Vietnamese();
        }
      });
    });

    WidgetsBinding.instance.addPostFrameCallback((_) {
      initTempData();
    });
  }

  Widget _buildContent() {
    return Container(
      padding:
          EdgeInsets.fromLTRB(Globals.minPadding, 0, Globals.minPadding, 0),
      width: Globals.maxWidth,
      height: Globals.maxHeight -
          AppBar().preferredSize.height -
          MediaQuery.of(context).padding.top,
      decoration: BoxDecoration(
        color: colorBackground,
        borderRadius: BorderRadius.all(Radius.circular(30.0)),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            height: Globals.maxHeight - 180.0,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _renderHeader(),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 10.0),
                        child: Text(
                          _language.nearlyTransaction ?? "",
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 18.0,
                              color: colorHint),
                        ),
                      ),
                      InkWell(
                        child: Container(
                          child: Text(
                            _language.watchAll ?? "",
                            style: TextStyle(fontSize: 15.0, color: greenLight),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: listTransaction != null ?  ListView(
                    children: listTransaction.map((value) => _renderPaymentRecord(value)).toList(),
                  ) : ListView(
                    children: <Widget>[
                      Container()
                    ],
                  ),
                )
//                Expanded(
//                  child: wallet != null && wallet.data != null && wallet.data.transaction != null ?  ListView(
//                    children: wallet.data.transaction.map((value) => _renderPaymentRecord(value)).toList(),
//                  ) : Container(),
//                )
              ],
            ),
          ),
          InkWell(
            child: Container(
                height: 50.0,
                width: 335.0,
                margin: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 37.0),
                decoration: BoxDecoration(
                    color: Color(0xff0AC67F),
                    borderRadius: BorderRadius.circular(26.0)),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(right: 10.0),
                      height: 25.0,
                      width: 30.0,
                      child: Image.asset(iconPayIn),
                    ),
                    Text(
                      _language.payIn ?? "",
                      style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  ],
                )),
            onTap: () {
              navigatorPush(context, PayInScreen());
            },
          )
//          _renderPaymentRecord(),
        ],
      ),
    );
  }

  Widget _setupBackground() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
//          borderRadius: BorderRadius.all(Radius.circular(34.0)),
          image: DecorationImage(
        image: AssetImage(background),
        fit: BoxFit.cover,
      )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.black,
        constraints: BoxConstraints.expand(),
        child: Stack(
          children: <Widget>[
            _setupBackground(),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  CustomAppBar(
                    title: _language.wallet ?? "",
                    onTapLeading: () => navigatorPop(context),
                  ),
                  _buildContent()
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

//  Widget _renderNullPage(){
//    return Column(
//      crossAxisAlignment: CrossAxisAlignment.center,
//      children: <Widget>[
//        Container(
//          height: Globals.maxWidth * 0.1,
//        ),
//        Container(
//          width: Globals.maxWidth * 0.85,
//          height: Globals.maxWidth * 0.85,
//          child: Image.asset(emptyFavoriteWorker),
//        ),
//        Container(
//          margin: EdgeInsets.only(top: 10.0),
//          child: Text(
//            _language.nullFavoriteWorker ?? "",
//            style: TextStyle(color: colorHint, fontSize: 16.0),
//          ),
//        ),
//      ],
//    );
//  }

  Widget _renderPaymentRecord(Transaction data){
    return Container(
      padding: EdgeInsets.only(left: 10.0, right: 10.0),
      child: Column(
        children: <Widget>[
          Container(
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 10.0),
                        child: Text(
                          data.walletableType == "payment" ? _language.payIn : (data.walletableType == "job" ? _language.paymentReceipt : ""),
                          style: TextStyle(fontFamily: fontSFPro, fontSize:  16.0),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 3.0),
                        child: AutoSizeText (
                          data.walletableType == "payment" ?  _language.payInFrom ?? "" : (data.walletableType == "job" && data.payment != null && data.payment.code != null ? _language.order + data.payment.code ?? "" :  _language.order),
                          style: TextStyle(fontFamily: fontSFPro, fontSize: 12.0, color: colorHint),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                        ),
                      ),
                    ],
                  ),
                ),
                data.walletableType == "payment" ?  Expanded(
                  flex: 1,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      data.payment != null && data.payment.amount != null ? AutoSizeText(
                        _language.monetaryUnit ?? "" + " -" +  data.payment.amount.split(".")[0],
                        style: TextStyle(fontSize: 18.0, fontFamily: fontSFPro, fontWeight: FontWeight.bold),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ) : Text (''),
                    ],
                  ),
                ) : Expanded(
                  flex: 2,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      data.payment != null && data.payment.amount != null ? AutoSizeText(
                        _language.monetaryUnit + " " +  data.payment.amount.split(".")[0],
                        style: TextStyle(fontSize: 18.0, fontFamily: fontSFPro, fontWeight: FontWeight.bold, color: greenLight),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ) : Text (''),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 10.0),
                  child: Image.asset(iconArrowToRight, height: 17.0, width: 10.0,),
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10.0),
            height: 1.0,
            color: colorHint,
          )
        ],
      ),
    );
  }

  Widget _renderHeader() {
    return Container(
      margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(bottom: 5.0),
            child: Text(
              _language.balance ?? "",
              style: TextStyle(fontFamily: fontSFPro, fontSize: 18.0),
            ),
          ),
//          Container(
//            margin: EdgeInsets.only(bottom: 5.0),
//            child: Text(
//              'Số dư'
//            )
//          ),
          Container(
            margin: EdgeInsets.only(bottom: 5.0),
            child: RichText(
              text: TextSpan(
                  text: 'VND ',
                  style: TextStyle(
                      fontFamily: fontSFPro, fontSize: 13.0, color: Colors.black),
                  children: <TextSpan>[
                    TextSpan(
                        text: '500.00',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 22.0,
                            fontFamily: fontSFPro,
                            color: Colors.black))
                  ]
//                <TextSpan>[
//                  wallet.data != null && wallet.data.surplus != null ? TextSpan(text: vnMoney.format(double.tryParse(wallet.data.surplus) ?? 0), style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22.0, fontFamily: fontSFPro, color: Colors.black))
//                      : TextSpan(text: "0", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22.0, fontFamily: fontSFPro, color: Colors.black)),
//                ],
                  ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(right: 5.0),
                child: Image.asset(
                  iconPoint,
                  height: 32.0,
                  width: 32.0,
                ),
              ),
              Container(
                child: Text(
                  '200 Điểm',
                  style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  initTempData(){
    Transaction trans = Transaction();
    trans.walletableType = "payment";
    trans.walletableId = 1;
    trans.userId = 1;
    trans.payment = Payment();
    trans.payment.id = 1;
    trans.payment.code = "000000";
    trans.payment.methodCode = "method Code";
    trans.payment.amount = "100000";

    listTransaction.add(trans);
  }
}

