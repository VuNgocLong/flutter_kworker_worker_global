import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/models/history_model.dart';
import 'package:k_user_global/modules/history_module/src/bloc/history_bloc.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:k_user_global/widgets/widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HistoryScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _HistoryState();
}

class _HistoryState extends State<HistoryScreen> with TickerProviderStateMixin{
  TabController _controller;
  SharedPreferences _prefs;
  Language _language;

  AutoSizeGroup _groupHeader = AutoSizeGroup();

  // bloc init
  HistoryBloc _bloc;

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _bloc.dispose();
  }

  @override
  void initState(){
    super.initState();
    _bloc = HistoryBloc();

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
        statusBarColor: greenLight, // Color for Android
        statusBarBrightness: Brightness.light // Dark == white status bar -- for IOS.
    ));

    _language = Language();
    _controller = TabController(length: 3, vsync: this);
    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        } else {
          _language = Vietnamese();
        }
      });
    });

    WidgetsBinding.instance.addPostFrameCallback((_){
      _bloc.getHistoryList(context);
    });
  }

  Widget _setupBackground() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(34.0)),
          image: DecorationImage(
            image: AssetImage(background),
            fit: BoxFit.cover,
          )),
    );
  }

  Widget _buildTab() {
    return DefaultTabController(
      length: 3,
      initialIndex: 0,
      child: Container(
        padding: EdgeInsets.only(left: 15.0, right: 15.0),
        child: TabBar(
//          isScrollable: true,
          tabs: <Widget>[
            Container(
              width: Globals.maxWidth / 3,
              child: Tab(
                text: _language.stringAll ?? "",
              ),
            ),
            Container(
              width: Globals.maxWidth / 3,
              child: Tab(
                text: _language.stringCompleted ?? "",
              ),
            ),
            Container(
              width: Globals.maxWidth / 3,
              child: Tab(
                text: _language.stringDenied ?? "",
              ),
            ),
//            new Container(
//              padding: EdgeInsets.symmetric(vertical: 15.0),
//              child: AutoSizeText(_language.stringAll ?? "", maxLines: 1, group: _groupHeader,
//                minFontSize: 8.0,),
//            ),
//            new
//            Container(
//              padding: EdgeInsets.symmetric(vertical: 15.0),
//              child: AutoSizeText(_language.stringCompleted ?? "", maxLines: 1,group: _groupHeader,
//                minFontSize: 8.0,),
//            ),
//            new Container(
//              padding: EdgeInsets.symmetric(vertical: 15.0),
//              child: AutoSizeText(_language.stringDenied ?? "", maxLines: 1,group: _groupHeader,
//                minFontSize: 8.0,),
//            ),
          ],
          unselectedLabelColor: grayLightColor,
          labelColor: greenLight,
          indicatorColor: primaryColor,
          indicatorSize: TabBarIndicatorSize.tab,
          controller: _controller,
        ),
      ),
    );
  }
  Widget _buildContentTab1(){
      return StreamBuilder(
        stream: _bloc.outputHistory,
        builder: (_, snapshot){
          if (snapshot.hasData){
            HistoryModel model  = snapshot.data;
            if (model != null && model.data != null && model.data.data != null && model.data.data.length > 0){
              return SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: model.data.data.map((value) =>  _renderItemHistory(value)).toList(),
                ),
              );
            } else {
              return _renderEmptyPage();
            }
          }
          return _renderEmptyPage();
        },
      );
//    return _renderEmptyPage();
  }

  Widget _buildContentTab2(){
//    return ListView(
//      children: list2.map((value)=> _renderItemHistory(value)).toList(),
//    );
    return _renderEmptyPage();
  }

  Widget _buildContentTab3(){
//    return ListView(
//      children: list3.map((value)=> _renderItemHistory(value)).toList(),
//    );
    return _renderEmptyPage();
  }

  Widget _buildContent() {
    return Container(
      padding: EdgeInsets.fromLTRB(Globals.minPadding, 0.0,
          Globals.minPadding, 0.0),
//      margin: EdgeInsets.only(bottom: 20.0),
      width: Globals.maxWidth,
      height: Globals.maxHeight - AppBar().preferredSize.height-MediaQuery.of(context).padding.top,
      decoration: BoxDecoration(
        color: backgroundWhiteF2,
        borderRadius: BorderRadius.all(Radius.circular(34.0)),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          _buildTab(),
          Expanded(
            child: TabBarView(
              controller: _controller ,
              children: <Widget>[
                _buildContentTab1(),
                _buildContentTab2(),
                _buildContentTab3(),
              ],
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.black,
        constraints: BoxConstraints.expand(),
        child: Stack(
          children: <Widget>[
            _setupBackground(),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  CustomAppBar(
                    title: _language.history ?? "",
                    onTapLeading: () => navigatorPop(context),
                  ),
                  _buildContent()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }


  Widget _renderItemHistory(Datas data){
    return Container(
      margin: EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
      padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 15.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        color: Colors.white,
      ),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text(
                          data.service.name ?? "",
                          style: TextStyle(fontSize: 15.0, fontFamily: fontSFPro, fontWeight:  FontWeight.bold),
                        ),
//                        record.isDeleted == true ? Text(
//                          '  (Đã hủy)',
//                          style: TextStyle(fontSize: 13.0, fontFamily: fontSFPro, color: colorRed, fontStyle: FontStyle.italic),
//                        ): Container()
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 5.0),
                      child: Text(
                        data.createdAt.split(" ")[0] ?? "",
                        style: TextStyle(fontSize: 13.0, color: grayLightColor, fontFamily: fontSFPro),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 5.0),
                      child: Text(
                        "#" + data.code ?? "",
                        style: TextStyle(fontSize: 13.0, color: grayLightColor, fontFamily: fontSFPro),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 20.0),
                height: 56.0,
                width: 56.0,
                decoration: BoxDecoration(
                    shape: BoxShape.circle
                ),
                child: data.client != null && data.client.avatar != null ? CachedNetworkImage(
                  fit: BoxFit.cover,
                  imageUrl:urlServerImage + data.client.avatar,
                  placeholder: (context, url) => Transform(
                      alignment: FractionalOffset.center,
                      transform: Matrix4.identity()..scale(0.35, 0.35),
                      child: CupertinoActivityIndicator()),
                  errorWidget: (context, url, error) =>
                      Icon(Icons.error),
                ) : Container(
                  margin: EdgeInsets.only(left: 20.0),
                  height: 60.0,
                  width: 60.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                  ),
                  child: Image.asset(iconDefaultAvatar),
                ),
              )
            ],
          ),
          Container(
            margin: EdgeInsets.only(top: 15.0, bottom: 15.0),
            height: 0.5,
            color: colorLine,
          ),

          data.isSelected == false ? Center(
            child: InkWell(
              onTap: (){
                setState(() {
                  data.isSelected = true;
                });
              },
              child: Container(
                margin: EdgeInsets.only(bottom: 15.0),
                child: Text(
                  'Xem thêm',
                  style: TextStyle(fontSize: 13.0, color: greenFont, fontFamily: fontSFPro),
                ),
              ),
            ),
          ): Container(
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(bottom: 15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Text('Địa điểm:',
                          style: TextStyle(fontSize: 13.0, color: grayLightColor, fontFamily: fontSFPro),),
                      ),
                      Container(
                        width: 20.0,
                      ),
                      Expanded(
                        flex: 4,
                        child: Text(
                          data.address ?? "",
                          style: TextStyle(fontSize: 13.0, fontFamily: fontSFPro),
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                      )
                    ],
                  ),
                ),
                data.status.id != 9 ? Container(
                  margin: EdgeInsets.only(bottom: 15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Text('Bắt đầu:',
                          style: TextStyle(fontSize: 13.0, color: grayLightColor, fontFamily: fontSFPro),),
                      ),
                      Container(
                        width: 20.0,
                      ),
                      Expanded(
                        flex: 4,
                        child: Text(
                          data.part[0].createdAt ?? "",
                          style: TextStyle(fontSize: 13.0, fontFamily: fontSFPro),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      )
                    ],
                  ),
                ) : Container(),

                data.part[0].jobStatusId == 1 ? Container(
                  margin: EdgeInsets.only(bottom: 15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Text('Kết thúc:',
                          style: TextStyle(fontSize: 13.0, color: grayLightColor, fontFamily: fontSFPro),),
                      ),
                      Container(
                        width: 20.0,
                      ),
                      Expanded(
                        flex: 4,
                        child: Text(
                          data.part[0].endTime ?? "",
                          style: TextStyle(fontSize: 13.0, fontFamily: fontSFPro),
                          overflow: TextOverflow.ellipsis,
                        ),
                      )
                    ],
                  ),
                ) : Container(
                  margin: EdgeInsets.only(bottom: 15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Text('Thời gian:',
                          style: TextStyle(fontSize: 13.0, color: grayLightColor, fontFamily: fontSFPro),),
                      ),
                      Container(
                        width: 20.0,
                      ),
                      Expanded(
                        flex: 4,
                        child: Text(
                          '19:00 - 23/10/2019',
                          style: TextStyle(fontSize: 13.0, fontFamily: fontSFPro),
                          overflow: TextOverflow.ellipsis,
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Text('Số lượng:',
                          style: TextStyle(fontSize: 13.0, color: grayLightColor, fontFamily: fontSFPro),),
                      ),
                      Container(
                        width: 20.0,
                      ),
                      Expanded(
                        flex: 4,
                        child: Text(
                          data.jobAmount.toString() ?? "",
                          style: TextStyle(fontSize: 13.0, fontFamily: fontSFPro),
                          overflow: TextOverflow.ellipsis,
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 15.0, bottom: 15.0),
                  height: 0.5,
                  color: colorLine,
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(right: 5.0),
                            child: SvgPicture.asset(iconMoneySvg, height: 20.0, width: 20.0,),
                          ),
                          Expanded(
                            child: Text('Tiền mặt',
                              style: TextStyle(fontSize: 11.0, fontFamily: fontSFPro, fontWeight: FontWeight.bold),
                              overflow: TextOverflow.ellipsis,),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(right: 5.0),
                            child: SvgPicture.asset(iconTick, height: 20.0, width: 20.0,),
                          ),
                          Expanded(
                            child: Text('Ưu đãi 15% ..........',
                              style: TextStyle(fontSize: 11.0, fontFamily: fontSFPro, fontWeight: FontWeight.bold),
                              overflow: TextOverflow.ellipsis,),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(right: 5.0),
                            child: SvgPicture.asset(iconTip, height: 20.0, width: 20.0,),
                          ),
                          Expanded(
                            child: Text('50.000 VNĐ',
                              style: TextStyle(fontSize: 11.0, fontFamily: fontSFPro, fontWeight: FontWeight.bold),
                              overflow: TextOverflow.ellipsis,),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                data.part[0].jobStatusId == 1 ? Container(
                  margin: EdgeInsets.only(top: 20.0, bottom: 20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(right: 5.0),
                        child: Text('VND',
                          style: TextStyle(fontSize: 13.0, fontFamily: fontSFPro),),
                      ),
                      Container(
                        child: Text('200.000',
                          style: TextStyle(fontSize: 20.0, fontFamily: fontSFPro, fontWeight: FontWeight.bold),),
                      )
                    ],
                  ),
                ): Container(
                  margin: EdgeInsets.only(top: 20.0, bottom: 20.0),
                  child: Text(
                    'Xin lỗi, công việc đã hủy',
                    style: TextStyle(fontSize: 20.0, color: colorRed),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _renderEmptyPage(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          width: Globals.maxWidth * 0.7,
          height: Globals.maxWidth * 0.7,
          child: Image.asset(emptyHistory),
        ),
        Container(
          margin: EdgeInsets.only(top: 10.0),
          child: Text(
            _language.stringEmptyHistory ?? "",
            style: TextStyle(color: grayTextInputColor, fontSize: 15.0),
          ),
        ),
      ],
    );
  }
}