import 'package:flutter/cupertino.dart';
import 'package:k_user_global/common/interactions.dart';
import 'package:k_user_global/models/history_model.dart';
import 'package:k_user_global/resources/repository.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:rxdart/rxdart.dart';

class HistoryBloc {
  final _repository = Repository();

  final _streamHistory = BehaviorSubject<HistoryModel>();

  Observable<HistoryModel> get outputHistory => _streamHistory.stream;

  setHistory(HistoryModel event) => _streamHistory.sink.add(event);

  getHistoryList(BuildContext context) async {
    Map<String, String> params = Map<String, String>();
    params['status'] = "1";
    showProgressDialog(context);
    ApiResponseData responseData = await _repository.getHistory(context,params);
    hideProgressDialog();
    if (responseData != null){
      if(responseData.success){
        HistoryModel model = HistoryModel.fromJson(responseData.data);
        setHistory(model);
      }
    }
  }

  dispose(){
    _streamHistory.close();
  }
}