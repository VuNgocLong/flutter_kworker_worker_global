import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/ultilites/hex_color.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:k_user_global/widgets/widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ReNewJobPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _StateReNew();
  }
}

class _StateReNew extends State<ReNewJobPage> {
  SharedPreferences _prefs;
  Language _language;
  @override
  void initState() {
    super.initState();
    SharedPreferences.getInstance().then((data) {
      _prefs = data;
      setState(() {
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        }
      });
    });
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
        statusBarColor: greenLight, // Color for Android
        statusBarBrightness:
            Brightness.light // Dark == white status bar -- for IOS.
        ));
  }

  Widget _setupBackground() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
//          borderRadius: BorderRadius.all(Radius.circular(34.0)),
          image: DecorationImage(
        image: AssetImage(background),
        fit: BoxFit.cover,
      )),
    );
  }

  Widget _buildContent() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: Globals.maxPadding),
      padding: EdgeInsets.symmetric(horizontal: Globals.maxPadding),
      height: Globals.maxHeight * 0.5,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          color: backgroundWhiteF2),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(Globals.minPadding),
            child: Text(
              'Yêu cầu gia hạn',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 22,
                  letterSpacing: 0.8,
                  color: Colors.black),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            margin: EdgeInsets.all(Globals.minPadding),
            padding: EdgeInsets.all(Globals.minPadding),
            height: 60.0,
            width: 60.0,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8.0),
                color: HexColor("#FF5E62")),
            child: Image.asset(iconVacuum),
          ),
          Text(
            'Sửa máy lạnh',
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
                letterSpacing: 0.75,
                color: Colors.black),
            textAlign: TextAlign.center,
          ),
          Center(
            child: Container(
              margin: EdgeInsets.only(
                  left: 30.0, right: 30.0, top: 5.0, bottom: 10.0),
              child: Text(
                '77 Nguyễn Cơ Thạch, An Phú Đông, Quận 2, Hồ Chí Minh',
                style: TextStyle(
                    fontSize: 15.0, fontFamily: fontSFPro, color: colorHint),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Text(
            'Bạn cần thêm thời gian để hoàn thành công việc hiện tại. Vui lòng gửi yêu cầu gia hạn đển chủ nhà. Bạn có đồng ý?',
            style: TextStyle(
                fontSize: 15.0, fontFamily: fontSFPro, color: Colors.black),
            textAlign: TextAlign.center,
          ),
          Container(
            margin: EdgeInsets.symmetric(
                horizontal: Globals.maxPadding, vertical: Globals.maxPadding),
            alignment: Alignment.bottomCenter,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: CustomButton(
                    text: _language.confirm ?? "",
                    onTap: () {
//                      _prefs.setBool(keyIsLogin, true);
//                      Navigator.of(context).popUntil((route) => route.isFirst);
//                      navigatorPushReplacement(context, HomeUi());
                    },
                    isMaxWidth: true,
                    textStyle: styleTitleWhiteBold,
                    colorBorder: colorButton,
                    colorBackground: colorButton,
                    radius: 25,
                  ),
                )
              ],
            ),
          ),
          InkWell(
            child: Text(
              _language.cancel ?? "",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.normal,
                color: colorCancelJob,
                letterSpacing: -0.24,
              ),
            ),
            onTap: () {
              showCustomBottomDialog(context, _cancelRenew());
            },
          )
//
        ],
      ),
    );
  }

  Widget _cancelRenew() {
    return Container(
      decoration: BoxDecoration(
        color: colorWhitePopup,
        borderRadius: BorderRadius.all(Radius.circular(34.0)),
      ),
      height: Globals.maxWidth * 0.6,
      padding: EdgeInsets.symmetric(
          horizontal: Globals.maxPadding, vertical: Globals.minPadding),
      width: Globals.maxWidth,
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(Globals.minPadding),
            child: Text(
              'Huỷ bỏ gia hạn',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 22,
                  letterSpacing: 0.8,
                  color: Colors.black),
              textAlign: TextAlign.center,
            ),
          ),
          Padding(
            padding: EdgeInsets.all(Globals.minPadding),
            child: Text(
              'Bạn chắc chắn phải hoàn thành công việc này nếu bạn không đồng ý gia hạn. Bạn có chắc huỷ bỏ gia hạn?',
              style: TextStyle(
                  fontSize: 15.0, fontFamily: fontSFPro, color: colorHint),
              textAlign: TextAlign.center,
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: Globals.maxPadding, vertical: Globals.maxPadding),
            child: Row(
              children: <Widget>[
                Expanded(
                    child: CustomButton(
                  text: _language.cancel ?? "",
                  onTap: () => navigatorPop(context),
                  textStyle: styleTitleWhiteBold,
                  colorBackground: buttonCancel,
                  colorBorder: buttonCancel,
                  isMaxWidth: true,
                  radius: 26,
                )),
                Container(
                  width: Globals.maxPadding,
                ),
                Expanded(
                    child: CustomButton(
                  text: _language.agree ?? "",
                  onTap: () {},
                  textStyle: styleTitleWhiteBold,
                  colorBackground: colorButton,
                  colorBorder: colorButton,
                  isMaxWidth: true,
                  radius: 26,
                )),
              ],
            ),
          )
//
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[_setupBackground(), _buildContent()],
      ),
    );
  }
}
