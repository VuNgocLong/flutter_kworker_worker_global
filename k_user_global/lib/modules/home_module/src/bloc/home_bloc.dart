import 'package:flutter/cupertino.dart';
import 'package:k_user_global/common/interactions.dart';
import 'package:k_user_global/models/profile_model.dart';
import 'package:k_user_global/resources/repository.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:rxdart/rxdart.dart';

class HomeBloc {
  final _repository = Repository();

  final _streamProfile = BehaviorSubject<ProfileModel>();

  Observable<ProfileModel> get outputProfile => _streamProfile.stream;

  setProfile(ProfileModel event) => _streamProfile.sink.add(event);

  getWorkerStatus(BuildContext context) async {
    showProgressDialog(context);
    ApiResponseData responseData = await _repository.getWorkerStatus(context, null);
    hideProgressDialog();
    if(responseData.success){
      if (responseData.data != null){
        ProfileModel model = ProfileModel.fromJson(responseData.data);
        setProfile(model);
      }
    }
  }

  changeStatus(BuildContext context) async {
    showProgressDialog(context);
    Map<String, dynamic> params = Map<String, dynamic>();
    ApiResponseData responseData = await _repository.changeWorkerStatus(context, params);
    hideProgressDialog();
    if (responseData.success){
      return true;
    } else {
      return false;
    }
  }

  dispose(){
    _streamProfile.close();
  }
}