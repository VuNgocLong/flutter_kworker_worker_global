import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/models/profile_model.dart';
import 'package:k_user_global/modules/history_module/src/ui/history_ui.dart';
import 'package:k_user_global/modules/home_module/src/bloc/home_bloc.dart';
import 'package:k_user_global/modules/order_module/src/ui/order_ui.dart';
import 'package:k_user_global/modules/job_manage_module/src/ui/job_manage_widget.dart';
import 'package:k_user_global/modules/renew_job/src/ui/renew_job_widget.dart';
import 'package:k_user_global/modules/request_module/src/ui/request_ui.dart';
import 'package:k_user_global/modules/user_module/src/ui/user_ui.dart';
import 'package:k_user_global/modules/wallet_module/src/ui/wallet_ui.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeUi extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<HomeUi> with WidgetsBindingObserver {
  // language init
  SharedPreferences _prefs;
  Language _language;

  // bloc
  HomeBloc _bloc;

  // init map
  GoogleMapController _controller;
  List<Marker> allMarker = [];
  Set<Circle> circles;

  Position _position = Position(longitude: 10.762622, latitude: 106.660172);
  double _zoom = 15.0;

  var myGroup = AutoSizeGroup();

  bool selected = false;
  bool selected1 = false;
  bool selected2 = false;

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  @override
  // ignore: must_call_super
  void initState() {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Colors.white, // Color for Android
        statusBarBrightness:
            Brightness.dark // Dark == white status bar -- for IOS.
        ));

    _bloc = HomeBloc();

    WidgetsBinding.instance.addObserver(this);
    _language = Language();
    SharedPreferences.getInstance().then((data) {
      _prefs = data;
      String _model = _prefs.getString(keyLanguage);
      if (_model != null) {
        if (_model == 'vi') {
          _language = Vietnamese();
        } else if (_model == 'eng') {
          _language = English();
        } else if (_model == 'korean') {
          _language = Korean();
        } else if (_model == 'japanese') {
          _language = Japanese();
        }
      } else {
        _language = Vietnamese();
      }
    });

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _bloc.getWorkerStatus(context);
      getLocation();
    });
  }

  void getLocation() async {
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    allMarker.add(Marker(
        markerId: MarkerId('myMarker'),
        draggable: false,
        position: LatLng(position.latitude, position.longitude),
        icon: BitmapDescriptor.defaultMarker));

    circles = Set.from([
      Circle(
          circleId: CircleId('myCircle'),
          center: LatLng(position.latitude, position.longitude),
          radius: 2000,
          strokeWidth: 0,
          fillColor: Color.fromRGBO(2, 233, 146, 0.08)),
      Circle(
          circleId: CircleId('myCircle1'),
          center: LatLng(position.latitude, position.longitude),
          radius: 500,
          strokeWidth: 0,
          fillColor: Color.fromRGBO(2, 233, 146, 0.1))
    ]);

    setState(() {});

    _position = position;
    if (_controller != null) {
      _controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
          target: LatLng(position.latitude, position.longitude), zoom: _zoom)));
    }
  }

  Widget _buttonCalender() {
    return InkWell(
      child: Container(
          height: 68.0,
          width: 68.0,
          decoration: BoxDecoration(
              color: greenLight.withOpacity(1), shape: BoxShape.circle),
          child: Container(
            height: 28.0,
            width: 28.0,
            padding: EdgeInsets.all(18.0),
            child: Image.asset(
              iconCalendar,
              height: 28.0,
              width: 28.0,
              fit: BoxFit.fill,
            ),
          )),
      onTap: () {
        navigatorPush(context, JobManagementPage());
      },
    );
  }

  Widget _googleMap(Position position) {
    return Container(
      child: GoogleMap(
        mapType: MapType.normal,
        initialCameraPosition: CameraPosition(
          target: LatLng(position.latitude, position.longitude),
          zoom: _zoom,
        ),
        onMapCreated: (GoogleMapController controller) {
          _controller = controller;
        },
        markers: Set.from(allMarker),
        circles: circles,
      ),
    );
  }

  Widget _renderOfflineButton() {
    return Expanded(
        child: Center(
      child: Stack(
        children: <Widget>[
          Container(
              height: 50,
              width: 120,
              padding: EdgeInsets.only(left: 5.0, right: 5.0),
              child: Image.asset(
                iconOffline,
              )
          ),
          Container(
            height: 50.0,
            width: selected ? 160 : 120,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                GestureDetector(
                    onTap: () {
                      setState(() {
                        selected = !selected;
                      });
                    },
                    child: AnimatedContainer(
                        width: selected ? 180.0 : 42.0,
                        height: 42.0,
//                      height: selected ? 60.0 : 120.0,
//                      alignment: selected ? Alignment.center : AlignmentDirectional.topCenter,
                        duration: Duration(seconds: 1),
                        curve: Curves.fastOutSlowIn,
                        child: Container(
                          width: 42.0,
                          height: 42.0,
                          child: Image.asset(iconCircleGray),
                        ),
                        onEnd: () async {
                          bool check = await _bloc.changeStatus(context);
                          if (check != null){
                            if (check)selected1 = !selected1;
                          }
                          setState(() {});
                        })),
                !selected
                    ? Container(
                        margin: EdgeInsets.only(right: 10.0),
                        child: Text(
                          'Offline',
                          style:
                              TextStyle(fontSize: 13.0, fontFamily: fontSFPro),
                        ),
                      )
                    : Container()
              ],
            ),
          )
        ],
      ),
    ));
  }

  Widget _renderOnlineButton() {
    return Expanded(
        child: Center(
          child: Stack(
            children: <Widget>[
              Container(
                  height: 50,
                  width: 120,
                  padding: EdgeInsets.only(left: 5.0, right: 5.0),
                  child: Image.asset(
                    iconOnline,
                  )),
              Container(
                height: 50.0,
                width: selected2 ? 60 : 120,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    !selected2
                        ? Container(
                      margin: EdgeInsets.only(left: 10.0),
                      child: Text(
                        'Online',
                        style:
                        TextStyle(fontSize: 13.0, fontFamily: fontSFPro),
                      ),
                    ) : Container(),
                    GestureDetector(
                        onTap: () {
                          setState(() {
                            selected2 = !selected2;
                          });
                        },
                        child: AnimatedContainer(
                            width: selected2 ? 160.0 : 60.0,
                            height: 60.0,
//                      height: selected ? 60.0 : 120.0,
//                      alignment: selected ? Alignment.center : AlignmentDirectional.topCenter,
                            duration: Duration(seconds: 2),
                            curve: Curves.fastOutSlowIn,
                            child: Container(
                              width: 42.0,
                              height: 42.0,
                              child: Image.asset(iconCircleGreen),
                            ),
                            onEnd: () {
                            })),
//                    selected2 ? Container(width: 100, color: Colors.red,) : Container()
                  ],
                ),
              )
            ],
          ),
        ));
  }

  Widget _renderBody() {
    return Container(
      color: Colors.white,
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
              child: Center(
            child: ClipRRect(
                borderRadius: BorderRadius.circular(30.0),
                child: _googleMap(_position)),
          )),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(),
              Stack(
                children: <Widget>[
                  Container(
                    child: Column(
                      children: <Widget>[
                        Container(
                          height: 30.0,
                        ),
                        Container(
                          height: 90.0,
                          padding: EdgeInsets.only(left: 10.0, right: 10.0),
                          width: Globals.maxWidth,
                          color: Colors.white,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child: _itemBottomBar(
                                    _language.home ?? "", iconHome, null, true),
                              ),
                              Expanded(
                                flex: 1,
                                child: _itemBottomBar(_language.wallet ?? "",
                                    iconWallet, WalletScreen(), false),
                              ),
                              Expanded(
                                flex: 2,
                                child: Container(),
                              ),
                              Expanded(
                                flex: 1,
                                child: _itemBottomBar(_language.history ?? "",
                                    iconClock, HistoryScreen(), false),
                              ),
                              Expanded(
                                flex: 1,
                                child: _itemBottomBar(_language.request ?? "",
                                    iconRequest, ReNewJobPage(), false),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Center(child: _buttonCalender()),
                ],
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _itemBottomBar(
      String title, String icon, Widget _widget, bool isHome) {
    return InkWell(
      child: Container(
        padding: EdgeInsets.all(5.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 25.0,
              width: 25.0,
//              padding: EdgeInsets.all(5.0),
              child: Image.asset(icon),
            ),
            Container(
              margin: EdgeInsets.only(top: 10.0),
              child: AutoSizeText(
                title,
                style: isHome
                    ? TextStyle(
                        fontSize: 13.0,
                        fontFamily: fontSFPro,
                        color: primaryColor)
                    : TextStyle(
                        fontSize: 13.0,
                        fontFamily: fontSFPro,
                        color: colorTextGrayNew.withOpacity(0.6)),
                maxLines: 1,
                minFontSize: 8.0,
                group: myGroup,
              ),
            )
          ],
        ),
      ),
      onTap: () async {
        if (_widget != null) {
          await navigatorPush(context, _widget);
          SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark
              .copyWith(
                  statusBarColor: Colors.white, // Color for Android
                  statusBarBrightness:
                      Brightness.dark // Dark == white status bar -- for IOS.
                  ));
        }
      },
    );
  }

  Widget _renderHeader(Widget child) {
    return Scaffold(
        body: SafeArea(
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: Colors.black,
          ),
          Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                color: Colors.white,
              ),
              Container(
                  child: Column(children: <Widget>[
                Container(
                    padding: EdgeInsets.only(left: 20.0, right: 20.0),
                    height: 60,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        InkWell(
                          child: Container(
                            padding: EdgeInsets.only(right: 20.0),
                            child: Image.asset(
                              iconMenu,
                              height: 24.0,
                              width: 27.0,
                            ),
                          ),
                          onTap: () {
                            navigatorPush(context, UserScreen());
                          },
                        ),
                        StreamBuilder(
                          stream: _bloc.outputProfile,
                          builder: (_, snapshot){
                            if(snapshot.hasData){
                              ProfileModel model = snapshot.data;
                              if (model != null && model.data != null && model.data.isOnline != null){
                                if (model.data.isOnline == 0) {
                                  return _renderOfflineButton();
                                } else {
                                  return _renderOnlineButton();
                                }
                              }
                              return Expanded(child: Container(),);
                            }
                            return Expanded(child: Container(),);
                          },
                        ),
                        InkWell(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                'VND',
                                style: TextStyle(
                                    fontSize: 15.0, fontFamily: fontSFPro),
                              ),
                              Text(
                                '500.000',
                                style:
                                    TextStyle(fontSize: 15.0, color: colorBlue),
                              )
                            ],
                          ),
                          onTap: () {
                            navigatorPush(context, OrderScreen());
                          },
                        )
                      ],
                    )),
                Expanded(child: child)
              ]))
            ],
          )
        ],
      ),
    ));
  }

  @override
  Widget build(BuildContext context) {
//    Globals().init(context);
    return WillPopScope(
        onWillPop: () async => false, child: _renderHeader(_renderBody()));
  }
}
