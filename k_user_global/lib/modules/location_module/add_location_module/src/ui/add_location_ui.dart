import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/widgets/TitleHeader.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AddLocationScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _AddLocationState();
}

class _AddLocationState extends State<AddLocationScreen>{
  SharedPreferences _prefs;
  Language _language;

  // bloc init
//  AddLocationBloc _bloc;


  @override
  void initState() {
    super.initState();
    _language = Language();
//    _bloc = AddLocationBloc();
    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        } else {
          _language = Vietnamese();
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return TitleHeader(
        title: _language.addLocation ?? "",
        customIcon: Container(
          child: SvgPicture.asset(iconRightArrowWhiteSvg),
        ),
        isBack: true,
        rightComponent: Container(
          child: SvgPicture.asset(iconEditLocationSvg),
        ),
        rightFunction: (){
//          navigatorPush(context, MapScreen());
        },
        child: Container(
            margin: EdgeInsets.only(top: 20.0),
            padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30.0),
                color: backgroundWhiteF2
            ),
            child: ListView(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 20.0, right: 20.0, left: 20.0),
                  padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      color: Colors.white
                  ),
                  child: Column(
                    children: <Widget>[
                      TextField(
                        style: TextStyle(fontSize: 15.0),
                        decoration: InputDecoration(
                          hintText: 'Nhập địa điểm',
                          hintStyle: TextStyle(color: grayLightColor, fontSize: 15.0),
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.only(left: 20.0),
                        ),
//                        onChanged: (value) => _bloc.placeAutocomplete(context, value),
                      ),
                      Container(
                        height: 0.5,
                        margin: EdgeInsets.only(right: 20.0, left: 20.0),
                        color: grayLightColor,
                      ),
//                      StreamBuilder(
//                        stream: _bloc.outputModel,
//                        builder: (context, snapshot){
//                          if (snapshot.hasData){
//                            SuggestLocationModel model = snapshot.data;
//                            if (model != null){
//                              return Column(
//                                children: model.predictions.map((value) => _renderItemAddress(value)).toList(),
//                              );
//                            } return Container();
//                          }
//                          return Container();
//                        },
//                      )
                    ],
                  ),
                ),
              ],
            )
        )
    );
  }

//  Widget _renderItemAddress(Predictions record){
//    return InkWell(
//      child: Container(
//        margin: EdgeInsets.only(top: 20.0, left: 20.0, right: 18.0),
//        decoration: BoxDecoration(
//            color: Colors.white,
//            borderRadius: BorderRadius.circular(8.0)
//        ),
//        child: Column(
//          children: <Widget>[
//            Row(
//              crossAxisAlignment: CrossAxisAlignment.center,
//              children: <Widget>[
//                Expanded(
//                  child: AutoSizeText(
//                    record.description,
//                    style: TextStyle(fontSize: 15.0, fontFamily:  fontSFPro),
//                    maxLines: 1,
//                    overflow: TextOverflow.ellipsis,
//                  ),
//                ),
//                Container(
//                  padding: EdgeInsets.only(right: 15.0),
//                  child: Image.asset('assets/icons/plus.png', height: 15.0, width: 15.0,),
//                )
//              ],
//            ),
//            Container(
//              height: 0.5,
//              margin: EdgeInsets.only(right: 20.0),
//              color: grayLightColor,
//            )
//          ],
//        ),
//      ),
//      onTap: (){
//        _bloc.addLocation(context, record.description, "10.0", "10.0");
//      },
//    );
//  }
}