import 'package:flutter/cupertino.dart';
import 'package:k_user_global/common/interactions.dart';
import 'package:k_user_global/models/save_location_model.dart';
import 'package:k_user_global/resources/repository.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:rxdart/rxdart.dart';

class SaveLocationBloc {
  final _repository = Repository();

  final _streamListLocation = BehaviorSubject<SaveLocationModel>();

  Observable<SaveLocationModel> get outputListLocation => _streamListLocation.stream;

  setListLocation(SaveLocationModel event) => _streamListLocation.sink.add(event);

  getLocation(BuildContext context) async {
    showProgressDialog(context);
    ApiResponseData apiResponseData = await _repository.getListLocation(context, null);
    hideProgressDialog();
    if (apiResponseData != null){
      if (apiResponseData.success){
        SaveLocationModel model = SaveLocationModel.fromJson(apiResponseData.data);
        setListLocation(model);
      }
    }
  }

  Future<bool> removeLocation(BuildContext context, Data data) async {
    Map<String, dynamic> params = Map<String, dynamic>();
    params['address'] = data.address;
    params['lng'] = data.lng;
    params['lat'] = data.lat;
    showProgressDialog(context);
    ApiResponseData responseData = await _repository.removeSavedLocation(context, params);
    showProgressDialog(context);
    if (responseData != null){
      if (responseData.success){
        return true;
      }
    }
    return false;
  }

  dispose(){
    _streamListLocation.close();
  }
}