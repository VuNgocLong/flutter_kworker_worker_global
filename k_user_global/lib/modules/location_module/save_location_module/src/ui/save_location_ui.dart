import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/models/save_location_model.dart';
import 'package:k_user_global/modules/location_module/add_location_module/src/ui/add_location_ui.dart';
import 'package:k_user_global/modules/location_module/save_location_module/src/bloc/save_location_bloc.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:k_user_global/widgets/widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SaveLocationScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SaveLocationState();
}

class _SaveLocationState extends State<SaveLocationScreen> {
  SharedPreferences _prefs;
  Language _language;

  // bloc init
  SaveLocationBloc _bloc;

  // callback
  LocationCallback _callback;

  @override
  void initState() {
    super.initState();
    _language = Language();
    _bloc = SaveLocationBloc();

    // callback
    _callback = () => _bloc.getLocation(context);

    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        } else {
          _language = Vietnamese();
        }
      });
    });

    WidgetsBinding.instance.addPostFrameCallback((_){
      _bloc.getLocation(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: Globals.maxHeight,
        color: Colors.black,
        constraints: BoxConstraints.expand(),
        child: Stack(
          children: <Widget>[
            _setupBackground(),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  CustomAppBar(
                    title: _language.savedLocation ?? "",
                    onTapLeading: () => navigatorPop(context),
                  ),
                  _renderBody()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _renderBody() {
    return Column(
      children: <Widget>[
        Container(
//          padding: EdgeInsets.fromLTRB(Globals.maxPadding, Globals.maxPadding * 2,
//              Globals.maxPadding, Globals.maxPadding * 2),
            width: Globals.maxWidth,
            height: Globals.maxHeight -
                AppBar().preferredSize.height -
                MediaQuery.of(context).padding.top,
            decoration: BoxDecoration(
              color: colorWhitePopup,
              borderRadius: BorderRadius.all(Radius.circular(30.0)),
            ),
            child: ListView(children: <Widget>[
              Container(
                child: StreamBuilder(
                  stream: _bloc.outputListLocation,
                  builder: (context, snapshot){
                    if (snapshot.hasData){
                      SaveLocationModel model = snapshot.data;
                      return model.data != null ? Column(
                        children:
                        model.data.map((value) => _renderItemAddress(value, model.data)).toList(),
                      ) : Container();
                    }
                    return Container();
                  },
                ),
              ),
              Container(
//                margin: EdgeInsets.only(right: 20.0, left: 20.0),
////                    padding: EdgeInsets.only(right: 20.0, left: 20.0),
//                decoration: BoxDecoration(
//                    borderRadius: new BorderRadius.only(
//                      bottomLeft: Radius.circular(8.0),
//                      bottomRight: Radius.circular(8.0),
//                    ), color: Colors.white),
                  child: _addMoreAddress()
              ),
            ]))
      ],
    );
  }

  Widget _setupBackground() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
//          borderRadius: BorderRadius.all(Radius.circular(34.0)),
          image: DecorationImage(
            image: AssetImage(background),
            fit: BoxFit.cover,
          )),
    );
  }

  Widget _renderItemAddress(Data record, List<Data> list) {
    return Container(
      height: 60,
//      padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
      margin: EdgeInsets.only(top: 5.0, left: 20.0, right: 20.0),
//      decoration: BoxDecoration(
//          color: Colors.white, borderRadius: BorderRadius.circular(8.0)),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 15.0, right: 15.0),
                child: Image.asset('assets/icons/ic_turned_in.png'),
              ),
              Expanded(
                child: Text(
                  record.address,
                  style: TextStyle(fontSize: 13.0, fontFamily: fontSFPro),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              Container(
                height: 18.0,
                width: 1,
                color: backgroundWhiteF2,
              ),
              record.isSelected == false
                  ? InkWell(
                child: Container(
//                        margin: EdgeInsets.only(left: 15.0),
                  width: 20.0,
                  child: SvgPicture.asset(
                    iconGroup3,
                    height: 15.0,
                    width: 15.0,
                  ),
                ),
                onTap: () {
                  setState(() {
                    for (var l in list) {
                      l.isSelected = false;
                    }
                    record.isSelected = true;
                  });
                },
              )
                  : InkWell(
                child: Container(
                  margin: EdgeInsets.only(left: 15.0),
                  width: 40.0,
                  height: 15.0,
                  child: Text(
                    'Xóa',
                    style: TextStyle(color: greenLight, fontSize: 15.0),
                  ),
                ),
                onTap: () {
                  openDeleteDialog(context, record);
                },
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _addMoreAddress() {
    return InkWell(
      child: Container(
        height: 50.0,
        margin: EdgeInsets.only(left: 20.0, right: 20.0),
//        decoration: BoxDecoration(
//            color: Colors.white, borderRadius: BorderRadius.circular(8.0)),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center ,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 15.0, right: 15.0),
              child: Image.asset(iconPlusGreen, height: 15.0, width: 15.0,),
            ),
            Expanded(
              child: Text(
                _language.addLocation ?? "",
                style: TextStyle(color: primaryColor),
              ),
            )
          ],
        ),
      ),
      onTap: () async {
        await navigatorPush(context, AddLocationScreen(), root: false);
//        _callback();
      },
    );
  }

  openDeleteDialog(BuildContext context, Data record) async {
    return await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            contentPadding: EdgeInsets.zero,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(8.0))),
            content: Container(
                decoration: BoxDecoration(
                    color: colorWhitePopup,
                    borderRadius: new BorderRadius.all(Radius.circular(5))),
                height: 225,
                padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 14.0),
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 15.0, bottom: 5.0),
                      child: Text(
                        _language.stringDeleteAddress + '"' +record.address + '"',
                        style: TextStyle(
                            fontSize: 17.0,
                            fontWeight: FontWeight.bold,
                            fontFamily: fontSFPro),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(top: 15.0, bottom: 20.0),
                        child: Text(
                          _language.stringDeleteAddressDescription ?? "",
                          style:
                          TextStyle(fontSize: 13.0, fontFamily: fontSFPro, color: colorHint),
                        ),
                      ),
                    ),
                    Container(
//                        margin: EdgeInsets.only(bottom: 20.0),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: InkWell(
                                  child: Container(
                                      margin: EdgeInsets.only(
                                          left: 10.0, right: 7.5),
                                      height: 50.0,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(26)),
                                        color: Colors.black,
                                      ),
                                      child: Center(
                                          child: Text(
                                            _language.cancel,
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 20,
                                                fontWeight: FontWeight.bold),
                                          ))),
                                  onTap: () {
                                    navigatorPop(context);
                                    setState(() {
                                      record.isSelected = false;
                                    });
                                  }),
                            ),
                            Expanded(
                              child: InkWell(
                                  child: Container(
                                      margin: EdgeInsets.only(
                                          left: 7.5, right: 10.0),
                                      height: 50.0,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(26)),
                                        color: greenLight,
                                      ),
                                      child: Center(
                                          child: Text(
                                            _language.agree,
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 20,
                                                fontWeight: FontWeight.bold),
                                          ))),
                                  onTap: () async {
                                    bool result = await _bloc.removeLocation(context, record);
                                    if(result){
                                      _callback();
                                    }
                                  }),
                            ),
                          ],
                        ))
//                    Row(
//                      children: <Widget>[
//                        Expanded(
//                          child: FlatButton(
//                            shape: RoundedRectangleBorder(),
//                            padding: EdgeInsets.zero,
//                            onPressed: () {
//                              navigatorPop(context);
//                              setState(() {
//                                record.isSelected = false;
//                              });
//                            },
//                            child: Container(
//                                decoration: BoxDecoration(
//                                  borderRadius:
//                                      BorderRadius.all(Radius.circular(26)),
//                                  color: Colors.black,
//                                ),
//                                padding: EdgeInsets.symmetric(
//                                    vertical: 10, horizontal: 40),
//                                child: Text(
//                                  _language.cancel,
//                                  style: TextStyle(
//                                      color: Colors.white,
//                                      fontSize: 14,
//                                      fontWeight: FontWeight.bold),
//                                )),
//                          ),
//                        ),
//                        Expanded(
//                          child: FlatButton(
//                            shape: RoundedRectangleBorder(),
//                            padding: EdgeInsets.zero,
//                            onPressed: () {},
//                            child: Container(
//                                decoration: BoxDecoration(
//                                  borderRadius:
//                                      BorderRadius.all(Radius.circular(26)),
//                                  color: greenLight,
//                                ),
//                                padding: EdgeInsets.symmetric(
//                                    vertical: 10, horizontal: 40),
//                                child: Text(
//                                  _language.agree,
//                                  style: TextStyle(
//                                      color: Colors.white,
//                                      fontSize: 14,
//                                      fontWeight: FontWeight.bold),
//                                )),
//                          ),
//                        ),
//                      ],
//                    )
                  ],
                )),
          );
        });
  }
}
