import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/models/temp_technique_model.dart';
import 'package:k_user_global/modules/user_module/manage_team_module/choose_major_module/src/ui/choose_major_ui.dart';
import 'package:k_user_global/modules/user_module/manage_team_module/src/ui/manage_team_ui.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CreateTechniqueScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _State();
  }
}

class _State extends State<CreateTechniqueScreen> {
  SharedPreferences _prefs;
  Language _language;

  @override
  void initState() {
    super.initState();
    _language = Language();

    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        } else {
          _language = Vietnamese();
        }
      });
    });

    WidgetsBinding.instance.addPostFrameCallback((_) {
    });
  }

  Widget _setupContent() {
    return Container(
      color: Colors.white,
      padding:
      EdgeInsets.only(top: Globals.maxPadding, bottom: Globals.maxPadding),
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          InkWell(
            child: Container(
              margin: EdgeInsets.only(top: 20.0, left: 20.0, bottom: 20.0),
              height: 24.0,
              width: 24.0,
              child: Image.asset(iconRightArrowBlack),
            ),
            onTap: () {
              navigatorPop(context);
            },
          ),
          Container(
            margin: EdgeInsets.only(left: 20.0, bottom: 5.0),
            child: Text(
              _language.technique ?? "",
              style: TextStyle(
                  fontSize: 34.0,
                  fontFamily: fontSFPro,
                  fontWeight: FontWeight.w400),
            ),
          ),
          Container(
              margin: EdgeInsets.only(left: 20.0, bottom: 25.0),
              child: AutoSizeText(
                _language.yourTechnique ?? "",
                style: TextStyle(
                    fontSize: 17.0, fontFamily: fontSFPro, color: colorHint),
              )),
              _addNewMajor()
//          _renderListTechnique(),
        ],
      ),
    );
  }


//  Widget _renderListTechnique(){
//    return Column(
//      mainAxisAlignment: MainAxisAlignment.start,
//      children: <Widget>[
//        Row(
//          children: <Widget>[
//            Container(
//              margin: EdgeInsets.only(bottom: 10.0, left: 20.0),
//              child: Text(
//                _language.technique ?? "",
//                style: TextStyle(
//                    fontSize: 17.0,
//                    fontFamily: fontSFPro,
//                    fontWeight: FontWeight.bold),
//              ),
//            ),
//            Container(
//              margin: EdgeInsets.only(bottom: 10.0),
//              child: Text(
//                ' *',
//                style: TextStyle(
//                    color: Colors.red,
//                    fontSize: 17.0,
//                    fontWeight: FontWeight.bold),
//              ),
//            ),
//          ],
//        ),
//        Column(
//          mainAxisAlignment: MainAxisAlignment.start,
//          crossAxisAlignment: CrossAxisAlignment.start,
//          children: list.map((value) => _renderTechniqueItem(value)).toList(),
//        )
//      ],
//    );
//  }

  Widget _renderButtonCreateTeam(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(),
        InkWell(
          child: Container(
              height: 50.0,
              width: 335.0,
              margin: EdgeInsets.only(
                  left: 20.0, right: 20.0, bottom: 37.0),
              decoration: BoxDecoration(
                  color: Color(0xff0AC67F),
                  borderRadius: BorderRadius.circular(26.0)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    _language.complete ?? "",
                    style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                ],
              )),
          onTap: () {
            navigatorPush(context, ManageTeamScreen());
          },
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
//    Globals().init(context);
    return Scaffold(
      backgroundColor: Colors.black,
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.only(top: Globals.maxPadding),
                height: Globals.maxHeight,
                color: Colors.white,
                child: _setupContent()
            ),
          ),
          _renderButtonCreateTeam()
        ],
      ),
    );
  }

  Widget _renderTechniqueItem(TempTechnique data){
    return data.isSelected ?  InkWell(
      child: Container(
        margin: EdgeInsets.only(bottom: 5.0),
        width: Globals.maxWidth * 0.25,
        height: 40.0,
        decoration: BoxDecoration(
          color: primaryColor,
          borderRadius: BorderRadius.circular(30.0),
        ),
        child: Center(
          child: Text (
            data.name ?? "",
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
      onTap: (){
        data.isSelected = false;
        setState(() {

        });
      },
    ) : InkWell(
      child: Container(
        margin: EdgeInsets.only(bottom: 5.0),
        width: Globals.maxWidth * 0.25,
        height: 40.0,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(30.0),
            border: Border.all(width: 0.5, color: colorHint)
        ),
        child: Center(
          child: Text (
              data.name ?? "",
            style: TextStyle(color: colorHint),
          ),
        ),
      ),
      onTap: (){
        data.isSelected = true;
        setState(() {

        });
      },
    );
  }

  Widget _addNewMajor(){
    return InkWell(
      child: Container(
        margin: EdgeInsets.only(left: 20.0),
        height: 98.0,
        width: 163.0,
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          color: primaryColor,
          borderRadius: BorderRadius.circular(8.0)
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 22.0, width: 22.0,
//            padding: EdgeInsets.all(10.0),
              child: Image.asset(iconPlus),
            ),
            Container(
              child: Text (
                _language.chooseNewMajor ?? "",
                style: TextStyle(fontWeight: FontWeight.bold, fontFamily: fontSFPro, fontSize: 17.0, color: Colors.white),
              ),
            )
          ],
        ),
      ),
      onTap: (){
        navigatorPush(context, ChooseMajorScreen());
      },
    );
  }
}
