import 'package:rxdart/rxdart.dart';

class CreateTechniqueBloc {
//  final _repository = Repository();

  final _streamEnableButton = BehaviorSubject<bool>();

  Observable<bool> get outputTeamName => _streamEnableButton.stream;

  setTeamName(bool model) => _streamEnableButton.sink.add(model);


  dispose(){
    _streamEnableButton.close();
  }
}