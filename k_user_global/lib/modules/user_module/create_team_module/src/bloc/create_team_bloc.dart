import 'package:rxdart/rxdart.dart';

class CreateTeamBloc {
//  final _repository = Repository();

  final _streamTeamName = BehaviorSubject<String>();
  final _streamAddress = BehaviorSubject<String>();

  Observable<String> get outputTeamName => _streamTeamName.stream;
  Observable<String> get outputAddress => _streamAddress.stream;

  setTeamName(String model) => _streamTeamName.sink.add(model);
  setAddress(String model) => _streamAddress.sink.add(model);


  dispose(){
    _streamTeamName.close();
    _streamAddress.close();
  }
}