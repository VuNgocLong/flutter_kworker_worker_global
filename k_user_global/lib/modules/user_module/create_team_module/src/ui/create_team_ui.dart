import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/modules/user_module/create_team_module/create_technique_module/src/ui/create_technique.dart';
import 'package:k_user_global/modules/user_module/create_team_module/src/bloc/create_team_bloc.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CreateTeamScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _State();
  }
}

class _State extends State<CreateTeamScreen> {
  SharedPreferences _prefs;
  Language _language;

  // bloc init
  CreateTeamBloc _bloc;

  // text controller init
  TextEditingController _teamNameController = TextEditingController();
  TextEditingController _addressControler = TextEditingController();
  TextEditingController _descriptionController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _language = Language();
    _bloc = CreateTeamBloc();

    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        } else {
          _language = Vietnamese();
        }
      });
    });

    WidgetsBinding.instance.addPostFrameCallback((_) {});
  }

  Widget _setupContent() {
    return Container(
      color: Colors.white,
      padding:
          EdgeInsets.only(top: Globals.maxPadding, bottom: Globals.maxPadding),
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          InkWell(
            child: Container(
              margin: EdgeInsets.only(top: 20.0, left: 20.0, bottom: 30.0),
              height: 24.0,
              width: 24.0,
              child: Image.asset(iconRightArrowBlack),
            ),
            onTap: () {
              navigatorPop(context);
            },
          ),
          Container(
            margin: EdgeInsets.only(left: 20.0, bottom: 5.0),
            child: Text(
              _language.createTeam ?? "",
              style: TextStyle(
                  fontSize: 34.0,
                  fontFamily: fontSFPro,
                  fontWeight: FontWeight.w400),
            ),
          ),
          Container(
              margin: EdgeInsets.only(left: 20.0, bottom: 25.0),
              child: AutoSizeText(
                _language.inputFullyToComplete ?? "",
                style: TextStyle(
                    fontSize: 17.0, fontFamily: fontSFPro, color: colorHint),
              )),
          _renderInput(_teamNameController, _language.nameTeam ?? "", _language.kWorker ?? "",
              _bloc.outputTeamName),
          _renderInput(_addressControler, _language.address ?? "",
              "77 Ngyễn Cơ Thạch, An Lợi Đông...", _bloc.outputTeamName),
          _renderDescription(
              _descriptionController, _language.description, "Mô tả"),
          _buttonContinue(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
//    Globals().init(context);
    return Scaffold(
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(top: Globals.maxPadding),
          height: Globals.maxHeight,
          color: Colors.white,
          child: _setupContent(),
        ),
      ),
    );
  }

  Widget _renderInput(TextEditingController controller, String title,
      String hintText, Stream streamName) {
    return Container(
      padding: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(bottom: 10.0),
                child: Text(
                  title,
                  style: TextStyle(
                      fontSize: 17.0,
                      fontFamily: fontSFPro,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 10.0),
                child: Text(
                  ' *',
                  style: TextStyle(
                      color: Colors.red,
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
          TextField(
            controller: controller,
            style: TextStyle(
              fontSize: 14,
            ),
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              hintText: hintText,
              hintStyle: TextStyle(
                fontStyle: FontStyle.italic,
              ),
              border: InputBorder.none,
              contentPadding: EdgeInsets.all(5.0),
              suffix: controller.text != ""
                  ? Container(
                      height: 11.0,
                      width: 11.0,
                      child: Image.asset(
                        iconChecked,
                        width: 15.0,
                        height: 11.0,
                      ),
                    )
                  : Container(
                      child: Text(
                        controller.text,
                      ),
                    ),
            ),
            onChanged: (value) {
              if (value.trim() != "") _bloc.setTeamName(value);
            },
          ),
          controller.text != ""
              ? Container(
                  height: 1,
                  width: Globals.maxWidth,
                  color: primaryColor,
                )
              : Container(
                  height: 1,
                  width: Globals.maxWidth,
                  color: colorHint,
                ),
        ],
      ),
    );
  }

  Widget _renderDescription(
      TextEditingController controller, String title, String hintText) {
    return Container(
      padding: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(bottom: 10.0),
                child: Text(
                  _language.nameTeam ?? "",
                  style: TextStyle(
                      fontSize: 17.0,
                      fontFamily: fontSFPro,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 10.0),
                child: Text(
                  ' *',
                  style: TextStyle(
                      color: Colors.red,
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
          TextField(
            maxLines: 2,
            controller: controller,
            style: TextStyle(
              color: Colors.grey,
              fontSize: 14,
            ),
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              hintText: hintText,
              hintStyle: TextStyle(
                fontStyle: FontStyle.italic,
              ),
              border: InputBorder.none,
              contentPadding: EdgeInsets.all(10),
            ),
          ),
          Container(
            height: 1,
            width: Globals.maxWidth,
            color: colorHint,
          )
        ],
      ),
    );
  }

  Widget _buttonContinue() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(),
        _addressControler.text != "" && _addressControler.text != null && _teamNameController.text != "" && _teamNameController.text != null
            ? InkWell(
          child: Container(
            margin: EdgeInsets.only(right: 20.0),
            width: Globals.maxWidth * 0.4,
            height: 50.0,
            decoration: BoxDecoration(
              color: greenLight, 
              borderRadius: BorderRadius.circular(30.0)
            ),
            child: Center(
              child: Text(
                _language.stringContinue ?? "",
                style: TextStyle(fontSize: 20.0, fontFamily: fontSFPro, color: Colors.white),
              ),
            ),
          ),
          onTap: (){
            navigatorPush(context, CreateTechniqueScreen());
          },
        )
            : Container(
          margin: EdgeInsets.only(right: 20.0),
          width: Globals.maxWidth * 0.4,
          height: 50.0,
          decoration: BoxDecoration(
              color: greenLight.withOpacity(0.3),
              borderRadius: BorderRadius.circular(30.0)
          ),
          child: Center(
            child: Text(
              _language.stringContinue ?? "",
              style: TextStyle(fontSize: 20.0, fontFamily: fontSFPro, color: Colors.white),
            ),
          ),
        ),
      ],
    );
  }
}
