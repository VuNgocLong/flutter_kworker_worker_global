
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:k_user_global/widgets/widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingNotificationScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<SettingNotificationScreen> with TickerProviderStateMixin{
  SharedPreferences _prefs;
  Language _language;

  bool _check1 = false;
  bool _check2 = false;
  bool _check3 = false;


  @override
  void initState(){
    super.initState();

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
        statusBarColor: greenLight, // Color for Android
        statusBarBrightness: Brightness.light // Dark == white status bar -- for IOS.
    ));

    _language = Language();
    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        } else {
          _language = Vietnamese();
        }
      });
    });

  }


  Widget _setupBackground() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(34.0)),
          image: DecorationImage(
            image: AssetImage(background),
            fit: BoxFit.cover,
          )),
    );
  }

  Widget _buildContent() {
    return Container(
//      padding: EdgeInsets.fromLTRB(Globals.minPadding, 0.0,
//          Globals.minPadding, 0.0),
      margin: EdgeInsets.only(bottom: 20.0),
      padding: EdgeInsets.only(top: 20.0),
      width: Globals.maxWidth,
      height: Globals.maxHeight - AppBar().preferredSize.height-MediaQuery.of(context).padding.top,
      decoration: BoxDecoration(
        color: backgroundWhiteF2,
        borderRadius: BorderRadius.all(Radius.circular(34.0)),
      ),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: 20.0, right: 20.0),
                    height: 59.0,
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            child: AutoSizeText(
                              _language.setting1 ?? "",
                              style: TextStyle(fontSize: 17.0, fontFamily: fontSFPro),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                        Container(
                            height: 24.0,
                            width: 50.0,
                            child: CupertinoSwitch(
                                value: _check1,
                                onChanged: (bool value) async {
                                  if (_check1){
                                    bool check = await changeSetting(context, "Tắt " + _language.setting1, _language.settingDescription1 ?? "", _language.confirm1);
                                    if (check){
                                      setState(() {
                                        _check1 = false;
                                      });
                                    }
                                  } else {
                                    setState(() {
                                      _check1 = true;
                                    });
                                  }
                                }
                            )
                        )
                      ],
                    ),
                  ),
                  Container(
                    height: 1,
                    width: Globals.maxWidth,
                    color: colorHint.withOpacity(0.1),
                  )
                ],
              ),
            ),
            Container(
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: 20.0, right: 20.0),
                    height: 59.0,
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            child: AutoSizeText(
                              _language.setting2 ?? "",
                              style: TextStyle(fontSize: 17.0, fontFamily: fontSFPro),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                        Container(
                            height: 24.0,
                            width: 50.0,
                            child: CupertinoSwitch(
                                value: _check2,
                                onChanged: (bool value) async {
                                  if (_check2){
                                    bool check = await changeSetting(context, "Tắt " + _language.setting2, _language.settingDescription2 ?? "", _language.confirm1);
                                    if (check){
                                      setState(() {
                                        _check2 = false;
                                      });
                                    }
                                  } else {
                                    setState(() {
                                      _check2 = true;
                                    });
                                  }
                                }
                            )
                        )
                      ],
                    ),
                  ),
                  Container(
                    height: 1,
                    width: Globals.maxWidth,
                    color: colorHint.withOpacity(0.1),
                  )
                ],
              ),
            ),
            Container(
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: 20.0, right: 20.0),
                    height: 59.0,
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            child: AutoSizeText(
                              _language.setting3 ?? "",
                              style: TextStyle(fontSize: 17.0, fontFamily: fontSFPro),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                        Container(
                            height: 24.0,
                            width: 50.0,
                            child: CupertinoSwitch(
                                value: _check3,
                                onChanged: (bool value) async {
                                  if (_check3){
                                    bool check = await changeSetting(context, "Tắt " + _language.setting1, _language.settingDescription3 ?? "", _language.confirm1);
                                    if (check){
                                      setState(() {
                                        _check3 = false;
                                      });
                                    }
                                  } else {
                                    setState(() {
                                      _check3 = true;
                                    });
                                  }
                                }
                            )
                        )
                      ],
                    ),
                  ),
                  Container(
                    height: 1,
                    width: Globals.maxWidth,
                    color: colorHint.withOpacity(0.1),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _renderItem(String title, bool _check){
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 20.0, right: 20.0),
            height: 59.0,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    child: AutoSizeText(
                      title,
                      style: TextStyle(fontSize: 17.0, fontFamily: fontSFPro),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ),
                Container(
                    height: 24.0,
                    width: 50.0,
                    child: CupertinoSwitch(
                      value: _check,
                      onChanged: (bool value){
                        setState(() {
                          _check = value;
                        });
                      }
                    )
                )
              ],
            ),
          ),
          Container(
            height: 1,
            width: Globals.maxWidth,
            color: colorHint.withOpacity(0.1),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
//    Globals().init(context);
    return Scaffold(
      body: Container(
        color: Colors.black,
        constraints: BoxConstraints.expand(),
        child: Stack(
          children: <Widget>[
            _setupBackground(),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  CustomAppBar(
                    title:  _language.settingNotification ?? "",
                    onTapLeading: () => navigatorPop(context),
                  ),
                  _buildContent()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

//  Widget _renderEmptyPage(){
//    return Column(
//      mainAxisAlignment: MainAxisAlignment.center,
//      children: <Widget>[
//        Container(
//          width: Globals.maxWidth * 0.7,
//          height: Globals.maxWidth * 0.7,
//          child: Image.asset(emptyHistory),
//        ),
//        Container(
//          margin: EdgeInsets.only(top: 10.0),
//          child: Text(
//            _language.stringEmptyHistory ?? "",
//            style: TextStyle(color: grayTextInputColor, fontSize: 15.0),
//          ),
//        ),
//      ],
//    );
//  }
}