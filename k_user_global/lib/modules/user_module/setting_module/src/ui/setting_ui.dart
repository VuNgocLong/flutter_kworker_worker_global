
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/modules/user_module/setting_module/setting_notification_module/src/ui/setting_notification_ui.dart';
import 'package:k_user_global/ultilites/hex_color.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:k_user_global/widgets/widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<SettingScreen> with TickerProviderStateMixin{
  SharedPreferences _prefs;
  Language _language;


  @override
  void initState(){
    super.initState();

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
        statusBarColor: greenLight, // Color for Android
        statusBarBrightness: Brightness.light // Dark == white status bar -- for IOS.
    ));

    _language = Language();
    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        } else {
          _language = Vietnamese();
        }
      });
    });

  }


  Widget _setupBackground() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(34.0)),
          image: DecorationImage(
            image: AssetImage(background),
            fit: BoxFit.cover,
          )),
    );
  }

  Widget _buildContent() {
    return Container(
//      padding: EdgeInsets.fromLTRB(Globals.minPadding, 0.0,
//          Globals.minPadding, 0.0),
      margin: EdgeInsets.only(bottom: 20.0),
      padding: EdgeInsets.only(top: 20.0),
      width: Globals.maxWidth,
      height: Globals.maxHeight - AppBar().preferredSize.height-MediaQuery.of(context).padding.top,
      decoration: BoxDecoration(
        color: backgroundWhiteF2,
        borderRadius: BorderRadius.all(Radius.circular(34.0)),
      ),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            _renderItem(_language.settingNotification ?? "", SettingNotificationScreen()),
            _renderItem(_language.settingPrivacy ?? "", null),
            _renderVersion()
          ],
        ),
      ),
    );
  }

  Widget _renderItem(String title, Widget widget){
    return InkWell(
      child: Container(

        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 20.0, right: 20.0),
              height: 59.0,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      child: AutoSizeText(
                        title,
                        style: TextStyle(fontSize: 17.0, fontFamily: fontSFPro),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 5.0),
                      child: SvgPicture.asset(
                        iconGrayArrowSvg,
                        width: 13.0,
                        height: 13.0,
                      ))
                ],
              ),
            ),
            Container(
              height: 1,
              width: Globals.maxWidth,
              color: colorHint.withOpacity(0.1),
            )
          ],
        ),
      ),
      onTap: (){
        if (widget != null){
          navigatorPush(context, widget);
        }
      },
    );
  }

  Widget _renderVersion(){
    return Container(
      height: 60.0,
      padding: EdgeInsets.only(left: 20.0, right: 20.0),
      color: HexColor("#CCCCCC").withOpacity(0.5),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              child: AutoSizeText(
                _language.version ?? "",
                style: TextStyle(fontSize: 17.0, fontFamily: fontSFPro),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
          Container(
              child: Text(
                "v 0.0.1",
                style: TextStyle(fontFamily: fontSFPro, fontSize: 15.0, color: colorGray3C3.withOpacity(0.6)),
              )
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
//    Globals().init(context);
    return Scaffold(
      body: Container(
        color: Colors.black,
        constraints: BoxConstraints.expand(),
        child: Stack(
          children: <Widget>[
            _setupBackground(),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  CustomAppBar(
                    title:  _language.setting ?? "",
                    onTapLeading: () => navigatorPop(context),
                  ),
                  _buildContent()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

//  Widget _renderEmptyPage(){
//    return Column(
//      mainAxisAlignment: MainAxisAlignment.center,
//      children: <Widget>[
//        Container(
//          width: Globals.maxWidth * 0.7,
//          height: Globals.maxWidth * 0.7,
//          child: Image.asset(emptyHistory),
//        ),
//        Container(
//          margin: EdgeInsets.only(top: 10.0),
//          child: Text(
//            _language.stringEmptyHistory ?? "",
//            style: TextStyle(color: grayTextInputColor, fontSize: 15.0),
//          ),
//        ),
//      ],
//    );
//  }
}