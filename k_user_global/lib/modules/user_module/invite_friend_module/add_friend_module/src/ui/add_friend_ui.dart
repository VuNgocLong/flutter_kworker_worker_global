import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:k_user_global/widgets/widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AddFriendScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<AddFriendScreen> with TickerProviderStateMixin{
  SharedPreferences _prefs;
  Language _language;

  var myGroup = AutoSizeGroup();

  @override
  void initState(){
    super.initState();

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
        statusBarColor: greenLight, // Color for Android
        statusBarBrightness: Brightness.light // Dark == white status bar -- for IOS.
    ));

    _language = Language();
    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        } else {
          _language = Vietnamese();
        }
      });
    });

  }


  Widget _setupBackground() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(34.0)),
          image: DecorationImage(
            image: AssetImage(background),
            fit: BoxFit.cover,
          )),
    );
  }

  Widget _buildContent() {
    return Container(
      padding: EdgeInsets.fromLTRB(Globals.minPadding, 0.0,
          Globals.minPadding, 0.0),
      margin: EdgeInsets.only(bottom: 20.0),
      width: Globals.maxWidth,
      height: Globals.maxHeight - AppBar().preferredSize.height-MediaQuery.of(context).padding.top,
      decoration: BoxDecoration(
        color: backgroundWhiteF2,
        borderRadius: BorderRadius.all(Radius.circular(34.0)),
      ),
      child: Stack(
        children: <Widget>[
          _renderTop(),
          _renderBottom()
        ],
      ),
    );
  }

  Widget _renderTop(){
    return Container(
      padding: EdgeInsets.all(20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            height: 76.0,
            width: 102.0,
            child: Image.asset(iconLoveLetterLarge, height: 76.0, width: 102.0,),
          )  ,
          Container(
            margin: EdgeInsets.only(top: 10.0),
            child: AutoSizeText(
              _language.addFriendDescription1 ?? "",
              group: myGroup,
              maxLines: 1,
              style: TextStyle(fontSize: 20.0, fontFamily: fontSFPro),
            ),
          ),
          Container(
            child: AutoSizeText(
              _language.addFriendDescription2 ?? "",
              style: TextStyle(fontSize: 20.0, fontFamily: fontSFPro),
              group: myGroup,
              maxLines: 1,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10.0),
            child: AutoSizeText(
              _language.kWorker ?? "",
              style: TextStyle(fontSize: 17.0, fontFamily: fontSFPro, color: primaryColor),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 20.0),
            child: AutoSizeText(
              _language.inviteCode ?? "",
              style: TextStyle(fontSize: 17.0, fontFamily: fontSFPro, color: colorTextGrayNew.withOpacity(0.6)),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
            child: AutoSizeText(
              _language.hintInviteCode ?? "",
              style: TextStyle(fontSize: 22.0, fontFamily: fontSFPro),
            ),
          ),
          _buttonCopy(),
        ],
      ),
    );
  }

  Widget _buttonCopy(){
    return InkWell(
      child: Container(
          height: 50.0,
          width: 335.0,
//          margin: EdgeInsets.only(
//              left: 20.0, right: 20.0, bottom: 37.0),
          decoration: BoxDecoration(
              color: Color(0xff0AC67F),
              borderRadius: BorderRadius.circular(26.0)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                _language.copy ?? "",
                style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ],
          )),
      onTap: () {
      },
    );
  }

  Widget _renderBottom(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 10.0),
              child: AutoSizeText(
                _language.orShareLink ?? "",
                style: TextStyle(fontSize: 13.0, fontFamily: fontSFPro, color: colorTextGrayNew.withOpacity(0.3)),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 43.0, width: 43.0,
                    decoration: BoxDecoration(
                      color: primaryColor,
                      shape: BoxShape.circle
                    ),
                  )
                ],
              ),
            )
          ],
        )
      ],
    );
  }


  @override
  Widget build(BuildContext context) {
//    Globals().init(context);
    return Scaffold(
      body: Container(
        color: Colors.black,
        constraints: BoxConstraints.expand(),
        child: Stack(
          children: <Widget>[
            _setupBackground(),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  CustomAppBar(
                    title:  _language.addFriend1 ?? "",
                    onTapLeading: () => navigatorPop(context),
                  ),
                  _buildContent()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}