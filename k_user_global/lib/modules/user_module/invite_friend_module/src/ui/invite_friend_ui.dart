import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/modules/user_module/invite_friend_module/add_friend_module/src/ui/add_friend_ui.dart';
import 'package:k_user_global/modules/user_module/invite_friend_module/src/bloc/invite_friend_bloc.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:k_user_global/widgets/widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InviteFriendScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<InviteFriendScreen> with TickerProviderStateMixin{
  SharedPreferences _prefs;
  Language _language;

  // bloc init
  InviteFriendBloc _bloc;

  @override
  void initState(){
    super.initState();
    _bloc = InviteFriendBloc();

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
        statusBarColor: greenLight, // Color for Android
        statusBarBrightness: Brightness.light // Dark == white status bar -- for IOS.
    ));

    _language = Language();
    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        } else {
          _language = Vietnamese();
        }
      });
    });

    WidgetsBinding.instance.addPostFrameCallback((_){
      _bloc.getListRefer(context, "");
    });
  }


  Widget _setupBackground() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(34.0)),
          image: DecorationImage(
            image: AssetImage(background),
            fit: BoxFit.cover,
          )),
    );
  }

  Widget _buildContent() {
    return Container(
      padding: EdgeInsets.fromLTRB(Globals.minPadding, 0.0,
          Globals.minPadding, 0.0),
      margin: EdgeInsets.only(bottom: 20.0),
      width: Globals.maxWidth,
      height: Globals.maxHeight - AppBar().preferredSize.height-MediaQuery.of(context).padding.top,
      decoration: BoxDecoration(
        color: backgroundWhiteF2,
        borderRadius: BorderRadius.all(Radius.circular(34.0)),
      ),
      child: Stack(
        children: <Widget>[
          _renderNoFriend(),
          _buttonAddFriend()
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
//    Globals().init(context);
    return Scaffold(
      body: Container(
        color: Colors.black,
        constraints: BoxConstraints.expand(),
        child: Stack(
          children: <Widget>[
            _setupBackground(),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  CustomAppBar(
                    title:  _language.listInvitedFriend ?? "",
                    onTapLeading: () => navigatorPop(context),
                  ),
                  _buildContent()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buttonAddFriend(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(),
        InkWell(
          child: Container(
              height: 50.0,
              width: 335.0,
              margin: EdgeInsets.only(
                  left: 20.0, right: 20.0, bottom: 37.0),
              decoration: BoxDecoration(
                  color: Color(0xff0AC67F),
                  borderRadius: BorderRadius.circular(26.0)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    _language.addFriend ?? "",
                    style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                ],
              )),
          onTap: () {
            navigatorPush(context, AddFriendScreen());
          },
        )
      ],
    );
  }

  Widget _renderNoFriend(){
    return Container(
      width: Globals.maxWidth ,
      height: Globals.maxHeight * 0.7,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            width: Globals.maxWidth * 0.7,
            height: Globals.maxWidth * 0.7,
            child: Image.asset(noFriend),
          ),
          Container(
            margin: EdgeInsets.only(top: 10.0),
            child: Center(
              child: AutoSizeText(
                _language.inviteFriendDescription1 ?? "",
                style: TextStyle(color: colorTextGrayNew.withOpacity(0.6), fontSize: 17.0),
              ),
            ),
          ),
          Container(
            child: Center(
              child: AutoSizeText(
                _language.inviteFriendDescription2 ?? "",
                style: TextStyle(color: colorTextGrayNew.withOpacity(0.6), fontSize: 17.0),
              ),
            ),
          ),
        ],
      ),
    );
  }

//  Widget _renderEmptyPage(){
//    return Column(
//      mainAxisAlignment: MainAxisAlignment.center,
//      children: <Widget>[
//        Container(
//          width: Globals.maxWidth * 0.7,
//          height: Globals.maxWidth * 0.7,
//          child: Image.asset(emptyHistory),
//        ),
//        Container(
//          margin: EdgeInsets.only(top: 10.0),
//          child: Text(
//            _language.stringEmptyHistory ?? "",
//            style: TextStyle(color: grayTextInputColor, fontSize: 15.0),
//          ),
//        ),
//      ],
//    );
//  }
}