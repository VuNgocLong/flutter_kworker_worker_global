import 'package:flutter/cupertino.dart';
import 'package:k_user_global/common/interactions.dart';
import 'package:k_user_global/resources/repository.dart';
import 'package:k_user_global/ultilites/ultility.dart';

class InviteFriendBloc {
  final _repository = Repository();

//  final _streamProfile = BehaviorSubject<ProfileInfo>();
//
//  Observable<ProfileInfo> get outputProfile => _streamProfile.stream;
//
//  setProfile(ProfileInfo event) => _streamProfile.sink.add(event);

  getReferInfo(BuildContext context) async {
    showProgressDialog(context);
    ApiResponseData responseData = await _repository.getReferInfo(context, null);
    hideProgressDialog();
    if (responseData.success){

    }
  }

  getListRefer(BuildContext context, String referCode) async {
    Map<String, String> params = Map<String, String>();
    params['refer_code'] = "ADGWRTTW" ?? "";
    showProgressDialog(context);
    ApiResponseData responseData = await _repository.getReferList(context, params);
    hideProgressDialog();
    if(responseData.success){

    }
  }

  dispose(){

  }
}