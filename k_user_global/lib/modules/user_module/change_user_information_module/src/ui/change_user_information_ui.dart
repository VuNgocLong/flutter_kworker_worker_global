import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/models/profile_info_model.dart';
import 'package:k_user_global/models/temp_address.dart';
import 'package:k_user_global/modules/login_module/register_address/src/ui/register_address_widget.dart';
import 'package:k_user_global/modules/user_module/change_user_information_module/src/bloc/change_user_information_bloc.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:k_user_global/widgets/title_white.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChangeUserInformationScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _StateChangeUserInformation();
}

class _StateChangeUserInformation extends State<ChangeUserInformationScreen>
    with WidgetsBindingObserver {
  // language
  SharedPreferences _prefs;
  Language _language;

  // Text
  TextEditingController _controllerEmail = TextEditingController();
  FocusNode _focusEmail = FocusNode();

  // File Image
  TempAddress _address = TempAddress();

  //Bloc init
  ChangeInformationBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = new ChangeInformationBloc();
    WidgetsBinding.instance.addObserver(this);
    _language = Language();
    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        } else {
          _language = Vietnamese();
        }
      });
    });
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _bloc.getProfileInfo(context);
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _bloc.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  var isKeyboardOpen = false;
  @override
  void didChangeMetrics() {
    final value = MediaQuery.of(context).viewInsets.bottom;
    if (value > 0) {
      if (isKeyboardOpen) {
        _onKeyboardChanged(false);
      }
      isKeyboardOpen = false;
    } else {
      isKeyboardOpen = true;
      _onKeyboardChanged(true);
    }
  }

  _onKeyboardChanged(bool isVisible) {
    if (!isVisible) {}
  }

  Widget _renderRowEmail(ProfileInfo profileInfo) {
    return Container(
//      margin: EdgeInsets.only(top: Globals.minPadding),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Container(
                    height: 40,
                    child: TextFormField(
//                  initialValue: content,
                      focusNode: _focusEmail,
                      controller: _controllerEmail,
                      onChanged: (value) {
                        if (value.trim() != null && value.trim() != "") {
                          _bloc.setEnableButton(true);
                        } else {
                          _bloc.setEnableButton(false);
                        }
                      },
                      style: TextStyle(
                          fontSize: 15.0,
                          fontFamily: fontSFPro,
                          color: Colors.black),
                      decoration: InputDecoration(
                        hintText: _language.inputYourEmail ?? "",
                        border: InputBorder.none,
                        hintStyle: TextStyle(
                            color: colorHint,
                            fontSize: 15.0,
                            fontFamily: fontSFPro),
                        contentPadding: EdgeInsets.only(left: 20.0),
                      ),
                    )),
              ),
              _controllerEmail.text == ""
                  ? Container(
                      margin: EdgeInsets.only(right: 20.0),
                      child: SvgPicture.asset(
                        iconEditSvg,
                        width: 16.0,
                        height: 16.0,
                      ),
                    )
                  : Container(
                      margin: EdgeInsets.only(right: 20.0),
                      child: SvgPicture.asset(
                        iconClearText,
                        width: 16.0,
                        height: 16.0,
                      ),
                    )
            ],

          ),
          Container(
            margin: EdgeInsets.only(left: 20.0, right: 20.0),
            height: 1.0,
            color: colorLine,
          )
        ],
      ),
    );
  }

  Widget _renderRowAddress(ProfileInfo profileInfo) {
    return InkWell(
      child: Container(
        margin: EdgeInsets.only(top: Globals.minPadding),
//        padding: EdgeInsets.only(left: 20.0),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: Container(
                      padding: EdgeInsets.only(top: 10.0, left: 20.0),
                      height: 40.0,
                      child: _address != null &&
                              _address.address != null &&
                              _address.address.trim() != ""
                          ? Text(
                              _address.address ?? "",
                              style: TextStyle(
                                  fontSize: 15.0, fontFamily: fontSFPro),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            )
                          : (profileInfo != null &&
                                  profileInfo.data != null &&
                                  profileInfo.data.address != null &&
                                  profileInfo.data.address != ""
                              ? Text(
                                  profileInfo.data.address,
                                  style: TextStyle(
                                      fontSize: 15.0, fontFamily: fontSFPro),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                )
                              : Text(
                                  _language.inputYourAddress ?? "",
                                  style: TextStyle(
                                      fontSize: 15.0, fontFamily: fontSFPro, color: colorHint),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ))),
//                  child: Container(
//                      height: 40,
//                      child: TextField(
////                  initialValue: content,
//                        focusNode: _focusEmail,
//                        controller: _controllerEmail,
//                        onChanged: (value) {
////                        checkEnableButton();
//                        },
//                        style: TextStyle(
//                            fontSize: 15.0,
//                            fontFamily: fontSFPro,
//                            color: Colors.black),
//                        decoration: InputDecoration(
//                          hintText: _language.inputYourAddress ?? "",
//                          border: InputBorder.none,
//                          hintStyle: TextStyle(
//                              color: colorHint,
//                              fontSize: 15.0,
//                              fontFamily: fontSFPro),
//                          contentPadding: EdgeInsets.only(left: 20.0),
//                        ),
//                      )
////                  child: Text(
////                    content,
////                    style: TextStyle(fontSize: 15.0, fontFamily: fontSFPro),
////                  ),
//                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 20.0),
                  child: SvgPicture.asset(
                    iconEditSvg,
                    width: 16.0,
                    height: 16.0,
                  ),
                )
              ],
            ),
            Container(
              margin: EdgeInsets.only(left: 20.0, right: 20.0),
              height: 1.0,
              color: colorLine,
            )
          ],
        ),
      ),
      onTap: () async {
        TempAddress address =
            await navigatorPush(context, RegisterAddressPage());
        if (address != null) {
          _address = address;
          _bloc.setEnableButton(true);
        } else {
          _bloc.setEnableButton(false);
        }
        SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
            statusBarColor: Colors.white, // Color for Android
            statusBarBrightness:
                Brightness.dark // Dark == white status bar -- for IOS.
            ));
      },
    );
  }

  Widget _rowPhone(ProfileInfo profileInfo) {
    return Container(
//      height: 40,
      margin:
          EdgeInsets.only(top: Globals.maxPadding, bottom: Globals.minPadding),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 20.0, right: 6.0),
                child: Text(
                  '(+84)',
                  style: TextStyle(
                      fontSize: 15.0, fontFamily: fontSFPro, color: greenLight),
                ),
              ),
              Container(
                child: Text(
                  profileInfo != null &&
                          profileInfo.data != null &&
                          profileInfo.data.phone != null
                      ? profileInfo.data.phone
                      : "",
                  style: TextStyle(fontFamily: fontSFPro, fontSize: 15.0),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 6.5, left: 20.0, right: 20.0),
                height: 1.0,
                color: colorLine,
              )
            ],
          ),
          Container(
            margin: EdgeInsets.only(left: 20.0, right: 20.0),
            height: 1.0,
            color: colorLine,
          )
        ],
      ),
    );
  }

  Widget _buttonComplete() {
    return StreamBuilder(
      stream: _bloc.outputEnableButton,
      initialData: false,
      builder: (context, snapshot) {
        return !snapshot.data
            ? Container(
                height: 50.0,
                width: 335.0,
                margin: EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
                decoration: BoxDecoration(
                    color: grayLightColor,
                    borderRadius: BorderRadius.circular(26.0)),
                child: Center(
                  child: Text(
                    _language.complete ?? "",
                    style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                ))
            : InkWell(
                child: Container(
                    height: 50.0,
                    width: 335.0,
                    margin: EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
                    decoration: BoxDecoration(
                        color: colorButton,
                        borderRadius: BorderRadius.circular(26.0)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          _language.complete ?? "",
                          style: TextStyle(
                              fontSize: 20.0,
                              color: Colors.white,
                              fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    )),
                onTap: () async {
                  if (_controllerEmail.text != null && _controllerEmail.text != "") {
                    bool check1 = false;
                    bool check = await _bloc.updateEmail(context, _controllerEmail.text);
                    if (_address != null && _address.address != null && _address.address != ""){
                      check1 = await _bloc.updateAddress(context, _address);
                    }
                    if (check || check1){
                      navigatorPushReplacement(context, ChangeUserInformationScreen());
                    }
                  } else {
                    if (_address != null &&
                        _address.address != null &&
                        _address.address != ""){
                      bool check = await _bloc.updateAddress(context, _address);
                      if (check) navigatorPushReplacement(context, ChangeUserInformationScreen());
                    }

                  }
                },
              );
      },
    );
  }

  Widget _renderBottom() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          child: Text(
            _language.connectWithOtherApplication ?? "",
            style: TextStyle(
                fontSize: 13.0, fontFamily: fontSFPro, color: grayLightColor),
          ),
        ),
        Container(
            margin: EdgeInsets.only(
                top: Globals.minPadding, bottom: Globals.maxPadding),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                InkWell(
                  child: Container(
                    height: 43,
                    width: 43,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image: AssetImage(iconFaceBook),
                            fit: BoxFit.cover)),
                  ),
//                  onTap: () => navigatorPush(context, ConnectFacebookScreen()),
                ),
//                widget.loginKey == "facebook"
//                    ? Container(
//                        height: 43,
//                        width: 43,
//                        decoration: BoxDecoration(
//                            shape: BoxShape.circle,
//                            image: DecorationImage(
//                                image: AssetImage(iconFaceBookNonActive),
//                                fit: BoxFit.cover)),
//                      )
//                    : Container(),
                Container(
                  margin: EdgeInsets.only(left: 20.0),
                  height: 43,
                  width: 43,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: AssetImage(iconGoogle), fit: BoxFit.cover)),
                )
              ],
            )),
        InkWell(
          child: Container(
            height: 20.0,
            margin: EdgeInsets.only(bottom: 36.0),
            child: Text(
              _language.logout ?? "",
              style: TextStyle(
                  color: colorRed, fontSize: 15.0, fontFamily: fontSFPro),
            ),
          ),
          onTap: () {
            openLogoutDialog(context);
          },
        )
      ],
    );
  }

  openLogoutDialog(BuildContext context) async {
    return await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            contentPadding: EdgeInsets.zero,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(8.0))),
            content: Container(
                decoration: BoxDecoration(
                    color: colorWhitePopup,
                    borderRadius: new BorderRadius.all(Radius.circular(5))),
                height: 225,
                padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 14.0),
                child: Column(
                  children: <Widget>[
                    Center(
                        child: Container(
                      margin: EdgeInsets.only(top: 15.0),
                      child: Text(
                        _language.stringSeeUNextTime ?? "",
                        style: TextStyle(
                            fontSize: 22.0,
                            fontWeight: FontWeight.w600,
                            letterSpacing: 1,
                            fontFamily: fontSFPro),
                        overflow: TextOverflow.ellipsis,
                      ),
                    )),
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(top: 30.0),
                        child: Text(
                          _language.stringLogoutDescription1 ?? "",
                          style: TextStyle(
                              fontSize: 13.0,
                              fontFamily: fontSFPro,
                              color: grayLightColor),
                          maxLines: 1,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: Text(
                          _language.stringLogoutDescription2 ?? "",
                          style: TextStyle(
                              fontSize: 15.0,
                              fontFamily: fontSFPro,
                              color: grayLightColor),
                          maxLines: 1,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: FlatButton(
                            shape: RoundedRectangleBorder(),
                            padding: EdgeInsets.zero,
                            onPressed: () {
//                              navigatorPop(context);
                              setState(() {
//                                record.isSelected = false;
                              });
                            },
                            child: Container(
                                height: 50.0,
                                margin: EdgeInsets.only(right: 5.0),
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(26)),
                                  color: Colors.black,
                                ),
//                                padding: EdgeInsets.symmetric(
//                                    vertical: 10, horizontal: 10),
                                child: Center(
                                  child: Text(
                                    _language.stringReturn ?? "",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold),
                                  ),
                                )),
                          ),
                        ),
                        Expanded(
                          child: FlatButton(
                            shape: RoundedRectangleBorder(),
                            padding: EdgeInsets.zero,
                            onPressed: () {
                              logout(context, false, _language);
//                              _bloc.logout(context);
                            },
                            child: Container(
                                margin: EdgeInsets.only(left: 5.0),
                                height: 50.0,
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(26)),
                                  color: greenLight,
                                ),
//                                padding: EdgeInsets.symmetric(
//                                    vertical: 10, horizontal: 10),
                                child: Center(
                                  child: Text(
                                    _language.agree ?? "",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold),
                                  ),
                                )),
                          ),
                        ),
                      ],
                    )
                  ],
                )),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
//    Globals().init(context);
    return TitleWhite(
      title: _language.changeInformation ?? "",
      isBack: true,
      customIcon: SvgPicture.asset(
        iconRightArrowSvg,
      ),
      child: Container(
          width: Globals.maxWidth,
          height: Globals.maxHeight -
              MediaQuery.of(context).padding.bottom -
              MediaQuery.of(context).padding.top -
              75,
          child: StreamBuilder(
            stream: _bloc.outputProfile,
            builder: (_, snapshot) {
              ProfileInfo profile = ProfileInfo();
              if (snapshot.hasData) {
                profile = snapshot.data;
              }
              return Column(
//          shrinkWrap: true,
                mainAxisAlignment: MainAxisAlignment.end,
//          crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: Globals.maxPadding),
                    child: Column(
                      children: <Widget>[
                        Stack(
                          children: <Widget>[
                            Center(
                              child: Container(
                                height: Globals.maxHeight * 0.15,
                                width: Globals.maxHeight * 0.15,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                ),
                                child: Image.asset(iconDefaultAvatar),
//                            child:
//                            _image == null
//                                ? ClipRRect(
//                                borderRadius:
//                                BorderRadius.circular(10000000.0),
//                                child: CachedNetworkImage(
//                                  fit: BoxFit.fill,
//                                  imageUrl: urlServerImage + widget.model.avatar,
//                                  placeholder: (context, url) => Transform(
//                                      alignment: FractionalOffset.center,
//                                      transform: Matrix4.identity()
//                                        ..scale(0.35, 0.35),
//                                      child: CupertinoActivityIndicator()),
//                                  errorWidget: (context, url, error) =>
//                                      Icon(Icons.error),
//                                ))
//                                : ClipRRect(
//                              borderRadius: BorderRadius.circular(10000000.0),
//                              child:  Image.file(_image, fit: BoxFit.fill,),
//                            )
                              ),
                            ),
//                      Center(
//                          child: InkWell(
//                            child: Container(
//                              margin: EdgeInsets.only(
//                                  top: Globals.maxWidth * 0.2,
//                                  left: Globals.maxWidth * 0.2),
//                              height: 36.0,
//                              width: 36.0,
//                              padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
//                              decoration: BoxDecoration(
//                                  shape: BoxShape.circle, color: greenLight),
//                              child: SvgPicture.asset(
//                                iconPhotoCameraSvg,
//                                height: 24.0,
//                                width: 24.0,
//                              ),
//                            ),
//                            onTap: () {
//                              this.openPictureSelection(null);
//                            },
//                          ))
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.only(top: Globals.maxPadding),
                          child: Text(
                            profile != null && profile.data != null
                                ? (profile.data.name != null &&
                                        profile.data.firstName != null
                                    ? profile.data.firstName +
                                        " " +
                                        profile.data.name
                                    : "User")
                                : "User",
                            style: TextStyle(
                                fontSize: 17.0,
//                          color: ,
                                fontFamily: fontSFPro,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        _rowPhone(profile),
                        _renderRowEmail(profile),
                        _renderRowAddress(profile),
                        _buttonComplete(),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Container(),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      margin: EdgeInsets.only(bottom: Globals.maxPadding),
                      child: _renderBottom(),
                    ),
                  )
                ],
              );
            },
          )),
    );
  }
}
