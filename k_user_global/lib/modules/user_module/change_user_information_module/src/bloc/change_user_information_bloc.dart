import 'package:flutter/cupertino.dart';
import 'package:k_user_global/common/interactions.dart';
import 'package:k_user_global/models/profile_info_model.dart';
import 'package:k_user_global/models/temp_address.dart';
import 'package:k_user_global/modules/user_module/change_user_information_module/src/ui/change_user_information_ui.dart';
import 'package:k_user_global/resources/repository.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:rxdart/rxdart.dart';

class ChangeInformationBloc {
  final _repository = Repository();

  final _streamEnableButton = BehaviorSubject<bool>();
  final _streamProfile = BehaviorSubject<ProfileInfo>();
  final _streamEmail = BehaviorSubject<String>();

  Observable<bool> get outputEnableButton => _streamEnableButton.stream;
  Observable<ProfileInfo> get outputProfile => _streamProfile.stream;
  Observable<String> get outputEmail => _streamEmail.stream;

  setEnableButton(bool event) => _streamEnableButton.sink.add(event);
  setProfile(ProfileInfo event) => _streamProfile.sink.add(event);
  setEmail(String event) => _streamEmail.sink.add(event);

  getProfileInfo(BuildContext context) async {
    showProgressDialog(context);
    ApiResponseData responseData = await _repository.getProfileInfo(context, null);
    hideProgressDialog();
    if(responseData.success){
      if (responseData.data != null){
        ProfileInfo model = ProfileInfo.fromJson(responseData.data);
        setProfile(model);
      }
    }
  }

  updateInformation(BuildContext context, String email, TempAddress address){
    if (email != null && email != "") updateEmail(context, email);
    if (address != null && address.address != null && address.address != "") updateAddress(context, address);
    getProfileInfo(context);
  }

  Future<bool> updateEmail(BuildContext context, String email) async {
    showProgressDialog(context);
    Map<String, dynamic> params = Map<String, dynamic>();
    params['email'] = email.trim();
    ApiResponseData responseData = await _repository.updateEmail(context, params);
    hideProgressDialog();
    if (responseData.success){
      return true;
    } else {
      return false;
    }
  }

  Future<bool> updateAddress(BuildContext context, TempAddress address) async {
    showProgressDialog(context);
    Map<String, dynamic> params = Map<String, dynamic>();
    params['address'] = address.address.trim();
    params['lng'] = address.lng ?? "";
    params['lat'] = address.lat ?? "";
    ApiResponseData responseData = await _repository.updateAddress(context, params);
    hideProgressDialog();
    if (responseData.success){
      return true;
    } else {
      return false;
    }
  }

  logout(BuildContext context) async {
    showProgressDialog(context);
    ApiResponseData responseData = await _repository.logout(context, null);
    hideProgressDialog();
    if (responseData.success){
      print('ahihi');
    }
  }

  dispose() {
    _streamProfile.close();
    _streamEnableButton.close();
    _streamEmail.close();
  }
}
