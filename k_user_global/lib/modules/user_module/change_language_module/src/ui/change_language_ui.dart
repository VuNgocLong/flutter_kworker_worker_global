import 'dart:ui';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/models/language_model.dart';
import 'package:k_user_global/modules/user_module/change_language_module/src/bloc/change_language_bloc.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChangeLanguageScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _StateChange();
  }
}

class _StateChange extends State<ChangeLanguageScreen> {
  SharedPreferences _prefs;
  Language _language;

  ChangeLanguageBloc _bloc;

  bool check = false;

  List<LanguageModel> list = List();

  @override
  void initState() {
    _bloc = ChangeLanguageBloc();
    super.initState();
    list.add(LanguageModel(name: niHon, isSelected: false));
    list.add(LanguageModel(name: korean, isSelected: false));
    list.add(LanguageModel(name: english, isSelected: false));
    list.add(LanguageModel(name: vn, isSelected: false));
    _language = Language();
    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        } else {
          _language =  Vietnamese();
        }
      });
    });
  }

  Widget _setupContent() {
    return Container(
      padding: EdgeInsets.only(top: Globals.maxPadding, bottom: Globals.maxPadding),
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              InkWell(
                child: Container(
                  margin: EdgeInsets.only(top: 20.0, left: 20.0, bottom: 30.0),
                  height: 24.0,
                  width: 24.0,
                  child: Image.asset(iconRightArrowBlack),
                ),
                onTap: (){
                  navigatorPop(context);
                },
              ),
              Container(
                margin: EdgeInsets.only(left: 20.0),
                child: Text(
                  _language.changeLanguage ?? "",
                  style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold),
                ),
              ),
//                Container(
//                  margin: EdgeInsets.only(left: 20.0),
//                  child: Text(
//                    _language.stringChangeLanguage ?? "",
//                    textAlign: TextAlign.left,
//                    style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
//                  ),
//                )
            ],
          ),
          Column(
            children: <Widget>[
              Column(
                children: list.map((value) => _buildButton(value)).toList(),
              ),
              check ? InkWell(
                child: Container(
                    height: 50.0,
                    width: 335.0,
                    margin: EdgeInsets.only(
                        left: 20.0, right: 20.0, top: 20.0, bottom: 20.0),
                    decoration: BoxDecoration(
                        color: greenLight,
                        borderRadius: BorderRadius.circular(26.0)),
                    child: Center(
                      child: Text(
                        _language.change ?? "",
                        style: TextStyle(
                            fontSize: 20.0,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    )),
                onTap: (){
                  _bloc.changeLanguage(context, list);
                },
              ) : Container(
                  height: 50.0,
                  width: 335.0,
                  margin: EdgeInsets.only(
                      left: 20.0, right: 20.0, top: 20.0, bottom: 20.0),
                  decoration: BoxDecoration(
                      color: greenLight.withOpacity(0.3),
                      borderRadius: BorderRadius.circular(26.0)),
                  child: Center(
                    child: Text(
                      _language.change ?? "",
                      style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  )),
            ],
          )
        ],
      ),
    );
  }

  Widget _buildButton(LanguageModel model) {
    return model.isSelected
        ? InkWell(
      child: Container(
        margin: EdgeInsets.symmetric(
            vertical: Globals.minPadding, horizontal: Globals.maxPadding),
        padding: EdgeInsets.symmetric(
          vertical: 15,
        ),
        decoration: BoxDecoration(
          color: greenLight.withOpacity(0.3),
//          borderRadius: BorderRadius.all(Radius.circular(26.0)),
//          shape: BoxShape.rectangle,
        ),
        child: Center(
          child: AutoSizeText(
            model.name,
            textAlign: TextAlign.center,
            style: styleLanguage,
          ),
        ),
      ),
      onTap: () {
        check = true;
        setState(() {

        });
      },
    )
        : InkWell(
      child: Container(
        margin: EdgeInsets.symmetric(
            vertical: Globals.minPadding, horizontal: Globals.maxPadding),
        padding: EdgeInsets.symmetric(
          vertical: 15,
        ),
        decoration: BoxDecoration(
          //color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(26.0)),
          shape: BoxShape.rectangle,
        ),
        child: Center(
          child: AutoSizeText(
            model.name,
            textAlign: TextAlign.center,
            style: styleLanguage,
          ),
        ),
      ),
      onTap: () {
        for (var o in list) {
          o.isSelected = false;
        }
        model.isSelected = true;
        _bloc.setEnableButton(true);
        check = true;
        setState(() {

        });
      },
    );
  }

  Widget _setupBackground() {
    return Container(
      width: Globals.maxWidth,
      height: Globals.maxHeight,
      decoration: BoxDecoration(
        color: Colors.white,
//        borderRadius: BorderRadius.all(Radius.circular(34.0)),
      ),
      child: Column(
        children: <Widget>[],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(top: Globals.maxPadding),
          height: Globals.maxHeight,
          color: Colors.black,
          child: Stack(
            children: <Widget>[
              _setupBackground(),
              _setupContent(),
            ],
          ),
        ),
      ),
    );
  }
}
