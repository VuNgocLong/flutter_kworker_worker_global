import 'package:flutter/material.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/models/language_model.dart';
import 'package:k_user_global/modules/user_module/src/ui/user_ui.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChangeLanguageBloc{

  SharedPreferences _prefs;

  ChangeLanguageBloc() {
    SharedPreferences.getInstance().then((data) {_prefs = data;});
  }

  final _streamEnableButton = BehaviorSubject<bool>();

  Observable<bool> get outputEnableButton => _streamEnableButton.stream;

  setEnableButton(bool event) => _streamEnableButton.sink.add(event);

  changeLanguage(BuildContext context, List<LanguageModel> list) async{
    String text = 'vn';

    for (var o in list) {
      if(o.isSelected){
        text = o.name;
      }
    }

    if(text == vn){
      _prefs.setString(keyLanguage, 'vi');
    }else if(text == english){
      _prefs.setString(keyLanguage, 'eng');
    }else if(text == niHon){
      _prefs.setString(keyLanguage, 'japanese');
    }else if(text == korean){
      _prefs.setString(keyLanguage, 'korean');
    }
//    navigatorPop(context);
    navigatorPop(context);
    navigatorPushReplacement(context, UserScreen());
  }

  dispose(){
    _streamEnableButton.close();
  }

}