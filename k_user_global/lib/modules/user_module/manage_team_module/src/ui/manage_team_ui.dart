import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/modules/user_module/manage_team_module/add_member_module/src/ui/add_member_ui.dart';
import 'package:k_user_global/modules/user_module/manage_team_module/manage_member_module/src/ui/manage_member_ui.dart';
import 'package:k_user_global/modules/user_module/manage_team_module/update_team_info_module/src/ui/update_team_info_ui.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:k_user_global/widgets/widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ManageTeamScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<ManageTeamScreen> with TickerProviderStateMixin {
  TabController _controller;
  SharedPreferences _prefs;
  Language _language;

  AutoSizeGroup _groupHeader = AutoSizeGroup();

  // bloc init

  @override
  void initState() {
    super.initState();

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
        statusBarColor: greenLight, // Color for Android
        statusBarBrightness:
            Brightness.light // Dark == white status bar -- for IOS.
        ));

    _language = Language();
    _controller = TabController(length: 2, vsync: this);
    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        } else {
          _language = Vietnamese();
        }
      });
    });
  }

  Widget _setupBackground() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(34.0)),
          image: DecorationImage(
            image: AssetImage(background),
            fit: BoxFit.cover,
          )),
    );
  }

  Widget _buildTab() {
    return DefaultTabController(
      length: 2,
      initialIndex: 0,
      child: Container(
        width: Globals.maxWidth,
        child: TabBar(
          tabs: <Widget>[
            Container(
              width: Globals.maxWidth / 2,
              child: Tab(
                text: _language.Member ?? "",
              ),
            ),
            Container(
              width: Globals.maxWidth / 2,
              child: Tab(
                text: _language.waitingForConfirm ?? "",
              ),
            ),
          ],
          unselectedLabelColor: grayLightColor,
          labelColor: greenLight,
          indicatorColor: primaryColor,
          indicatorSize: TabBarIndicatorSize.tab,
          controller: _controller,
        ),
      ),
    );
  }

  Widget _buildContentTab1() {
    return Stack(
      children: <Widget>[
//        _renderNullMember(),
        _renderListMember(),
        _buttonAddMember(),
      ],
    );
  }

  Widget _buildContentTab2() {
    return Container(
      width: Globals.maxWidth,
      height: Globals.maxHeight,
      color: Colors.white,
    );
  }

  Widget _buildContent() {
    return Container(
      padding:
          EdgeInsets.fromLTRB(Globals.minPadding, 0.0, Globals.minPadding, 0.0),
      margin: EdgeInsets.only(bottom: 20.0),
      width: Globals.maxWidth,
      height: Globals.maxHeight -
          AppBar().preferredSize.height -
          MediaQuery.of(context).padding.top,
      decoration: BoxDecoration(
        color: backgroundWhiteF2,
        borderRadius: BorderRadius.all(Radius.circular(34.0)),
      ),
      child: Column(
        children: <Widget>[
          _renderHeader(),
          Container(
            margin: EdgeInsets.only(top: 20.0, bottom: 10.0),
            height: 0.7,
            width: Globals.maxWidth,
            color: colorLineGrey,
          ),
          Container(
            margin: EdgeInsets.only(left: 20.0, right: 20.0),
            child: _buildTab(),
          ),
          Expanded(
            child: TabBarView(
              controller: _controller,
              children: <Widget>[_buildContentTab1(), _buildContentTab2()],
            ),
          )
        ],
      ),
    );
  }

  Widget _renderHeader() {
    return Container(
      padding: EdgeInsets.only(top: 20.0, left: 15.0, right: 15.0),
      width: Globals.maxWidth,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(bottom: 5.0),
                  child: Text(
                    'K-worker',
                    style: TextStyle(
                        fontSize: 20.0,
                        fontFamily: fontSFPro,
                        fontWeight: FontWeight.bold),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 5.0),
                  child: Text(
                    'Thợ làm tóc, thợ giúp việc',
                    style: TextStyle(
                        fontSize: 13.0,
                        fontFamily: fontSFPro,
                        color: colorHint),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                Row(
                  children: <Widget>[
                    Container(
                      height: 13.0,
                      width: 15.0,
                      child: Image.asset(
                        iconMember,
                        height: 13.0,
                        width: 15.0,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 5.0),
                      child: Text(
                        '6 thành viên',
                        style: TextStyle(
                            fontSize: 13.0,
                            fontFamily: fontSFPro,
                            color: colorHint),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          Expanded(
            flex: 2,
            child: InkWell(
              child: Container(
                height: 50.0,
                width: Globals.maxWidth * 0.18,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30.0),
                    color: greenLight),
                child: Center(
                  child: AutoSizeText(
                    _language.update ?? "",
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: fontSFPro,
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              onTap: () async {
               await  navigatorPush(context, UpdateTeamInfoScreen());
               SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light
                   .copyWith(
                   statusBarColor: greenLight, // Color for Android
                   statusBarBrightness:
                   Brightness.light // Dark == white status bar -- for IOS.
               ));
              },
            ),
          )
        ],
      ),
    );
  }

  Widget _renderListMember(){
    return Container(
      padding: EdgeInsets.only(bottom: 100.0),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            _renderItemMember("Duy An", "4.9", "Thợ giúp việc, thợ làm tóc", "Cả ngày", 1),
            _renderItemMember("Duy An", "4.9", "Thợ giúp việc, thợ làm tóc", "Cả ngày", 2),
            _renderItemMember("Duy An", "4.9", "Thợ giúp việc, thợ làm tóc", "Cả ngày", 2),
          ],
        ),
      )
    );
  }

  Widget _buttonAddMember() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(),
        InkWell(
          child: Container(
              height: 50.0,
              width: 335.0,
              margin: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 37.0),
              decoration: BoxDecoration(
                  color: Color(0xff0AC67F),
                  borderRadius: BorderRadius.circular(26.0)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    _language.addMember ?? "",
                    style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                ],
              )),
          onTap: () async {
            await navigatorPush(context, AddMemberScreen());
            SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light
                .copyWith(
                    statusBarColor: greenLight, // Color for Android
                    statusBarBrightness:
                        Brightness.light // Dark == white status bar -- for IOS.
                    ));
          },
        )
      ],
    );
  }

  Widget _renderNullMember() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          width: Globals.maxWidth * 0.7,
          height: Globals.maxWidth * 0.7,
          child: Image.asset(noMember),
        ),
        Container(
          margin: EdgeInsets.only(top: 10.0),
          child: Center(
            child: Text(
              _language.nullMemberDescription1 ?? "",
              style: TextStyle(color: grayTextInputColor, fontSize: 17.0),
            ),
          ),
        ),
        Container(
            margin: EdgeInsets.only(top: 10.0),
            child: Center(
              child: RichText(
                text: TextSpan(
                  text: _language.addMember ?? "",
                  style: TextStyle(
                      color: greenLight,
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0),
                  children: <TextSpan>[
                    TextSpan(
                        text: _language.nullMemberDescription2,
                        style: TextStyle(
                            fontSize: 17.0, color: grayTextInputColor)),
                  ],
                ),
              ),
            ))
      ],
    );
  }

  Widget _renderItemMember(String name, String point, String jobs, String time, int type){
    return InkWell(
      child: Container(
        margin: EdgeInsets.only(top: 10.0),
        child: Column(
          children: <Widget>[
            Container(
//            padding: EdgeInsets.only(left: 20.0, right: 20.0),
              child: Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(right: 10.0),
                    height: 57.0,
                    width: 57.0,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle
                    ),
                    child: Image.asset(iconDefaultAvatar),
                  ),
                  Expanded(
                    child: Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          type ==1 ? Container(
                            child: Text (
                              "Đội trưởng",
                              style: TextStyle(fontSize: 13.0, fontFamily: fontSFPro, color: orangeColor),
                            ),
                          ) : Container(
                            child: Text (
                              "Thành viên",
                              style: TextStyle(fontSize: 13.0, fontFamily: fontSFPro, color: colorBlue),
                            ),
                          ),
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    name ?? "",
                                    style: TextStyle(fontSize:  15.0, fontFamily: fontSFPro, fontWeight: FontWeight.bold),
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 20.0),
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        height: 11.0,
                                        width: 11.0,
                                        child: Image.asset(""),
                                      ),
                                      Container(
                                        child: Text(
                                          point ?? "",
                                          style: TextStyle(fontFamily: fontSFPro, fontSize: 15.0),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    jobs ?? "",
                                    style: TextStyle(fontSize:  13.0, fontFamily: fontSFPro, color: colorTextGrayNew.withOpacity(0.6)),
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 20.0),
                                  child: Container(
                                    child: Text(
                                      time ?? "",
                                      style: TextStyle(fontFamily: fontSFPro, fontSize: 13.0, fontWeight: FontWeight.bold, color: colorNumber),
                                    ),
                                  )
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
              color: colorLineGrey,
              height: 0.7,
              width: Globals.maxWidth,
            )
          ],
        ),
      ),
      onTap: () async {
       await navigatorPush(context, ManageMemberScreen());
       SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
           statusBarColor: greenLight, // Color for Android
           statusBarBrightness: Brightness.light // Dark == white status bar -- for IOS.
       ));
      },
    );
  }

//  Widget _renderEmptyPage(){
//    return Column(
//      mainAxisAlignment: MainAxisAlignment.center,
//      children: <Widget>[
//        Container(
//          width: Globals.maxWidth * 0.7,
//          height: Globals.maxWidth * 0.7,
//          child: Image.asset(emptyHistory),
//        ),
//        Container(
//          margin: EdgeInsets.only(top: 10.0),
//          child: Text(
//            _language.stringEmptyHistory ?? "",
//            style: TextStyle(color: grayTextInputColor, fontSize: 15.0),
//          ),
//        ),
//      ],
//    );
//  }

  @override
  Widget build(BuildContext context) {
//    Globals().init(context);
    return Scaffold(
      body: Container(
        color: Colors.black,
        constraints: BoxConstraints.expand(),
        child: Stack(
          children: <Widget>[
            _setupBackground(),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  CustomAppBar(
                    title: _language.manageTeam ?? "",
                    onTapLeading: () => navigatorPop(context),
                  ),
                  _buildContent()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
