import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/modules/user_module/create_team_module/create_technique_module/src/bloc/create_technique_bloc.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChangeWorkingTimeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _State();
  }
}

class _State extends State<ChangeWorkingTimeScreen> {
  SharedPreferences _prefs;
  Language _language;

  // bloc init
  CreateTechniqueBloc _bloc;

  // init var
  TextEditingController _startTimeController = TextEditingController();
  TextEditingController _endTimeController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _language = Language();
    _bloc = CreateTechniqueBloc();

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Colors.white,// Color for Android
        statusBarBrightness: Brightness.dark // Dark == white status bar -- for IOS.
    ));


    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        } else {
          _language = Vietnamese();
        }
      });
    });
  }

  Widget _setupContent() {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.only(top: Globals.maxPadding, bottom: Globals.maxPadding),
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          InkWell(
            child: Container(
              margin: EdgeInsets.only(top: 20.0, left: 20.0, bottom: 20.0),
              height: 24.0,
              width: 24.0,
              child: Image.asset(iconRightArrowBlack),
            ),
            onTap: () {
              navigatorPop(context);
            },
          ),
          Container(
            margin: EdgeInsets.only(left: 20.0, bottom: 5.0),
            child: Text(
              _language.workingTimeInDay ?? "",
              style: TextStyle(
                  fontSize: 34.0,
                  fontFamily: fontSFPro,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Container(
              margin: EdgeInsets.only(left: 20.0, bottom: 25.0),
              child: AutoSizeText(
                _language.workingTimeOnlyChangeAfter14Days ?? "",
                style: TextStyle(
                    fontSize: 17.0, fontFamily: fontSFPro, color: colorHint),
              )),
          _renderTime(_startTimeController, _language.startTime ?? "", "7:00"),
          _renderTime(_endTimeController, _language.endTime ?? "", "17:00"),
        ],
      ),
    );
  }

  Widget _renderTime(TextEditingController _controller, String title, String hint){
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top:  30.0),
            child: Text(
              title ?? "",
              style: TextStyle(fontSize: 17.0, fontFamily: fontSFPro, fontWeight: FontWeight.bold),
            ),
          ),
          InkWell(
            child: Container(
              margin: EdgeInsets.only(top:  5.0),
              child: Text(
                hint ?? "",
                style: TextStyle(fontSize: 17.0, color: colorTextGrayNew.withOpacity(0.3), fontFamily: fontSFPro),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top:  10.0),
            height: 1,
            width: Globals.maxWidth,
            color: colorLineGrey,
          )
        ],
      ),
    );
  }

  Widget _renderTotalTime(){
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top:  30.0),
            child: Text(
              _language.totalWorkingTimeInDay ?? "",
              style: TextStyle(fontSize: 17.0, fontFamily: fontSFPro, fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top:  5.0),
                  child: Text(
                    "0",
                    style: TextStyle(fontSize: 17.0, color: colorTextGrayNew.withOpacity(0.3), fontFamily: fontSFPro),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top:  5.0),
                  child: Text(
                    _language.hour ?? "",
                    style: TextStyle(fontSize: 17.0, fontWeight: FontWeight.bold, fontFamily: fontSFPro),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top:  10.0),
            height: 1,
            width: Globals.maxWidth,
            color: colorLineGrey,
          )
        ],
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
//    Globals().init(context);
    return Scaffold(
      backgroundColor: Colors.black,
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.only(top: Globals.maxPadding),
                height: Globals.maxHeight,
                color: Colors.white,
                child: _setupContent()
            ),
          ),
        ],
      ),
    );
  }
}
