import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/models/list_work_model.dart';
import 'package:k_user_global/modules/user_module/manage_team_module/choose_major_module/src/bloc/choose_major_bloc.dart';
import 'package:k_user_global/modules/user_module/manage_team_module/major_detail_module/src/ui/major_detail_ui.dart';
import 'package:k_user_global/modules/user_module/manage_team_module/src/ui/manage_team_ui.dart';
import 'package:k_user_global/ultilites/hex_color.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChooseMajorScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _State();
  }
}

class _State extends State<ChooseMajorScreen> {
  SharedPreferences _prefs;
  Language _language;

  // bloc init
  ChooseMajorBloc _bloc;

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _bloc.dispose();
  }

  @override
  void initState() {
    super.initState();
    _language = Language();
    _bloc = ChooseMajorBloc();

    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        } else {
          _language = Vietnamese();
        }
      });
    });

    WidgetsBinding.instance.addPostFrameCallback((_) {
//      _bloc.getListServices(context);
      _bloc.getListWork(context);
    });
  }

  Widget _setupContent() {
    return Container(
      color: Colors.white,
      padding:
      EdgeInsets.only(top: Globals.maxPadding, bottom: Globals.maxPadding),
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          InkWell(
            child: Container(
              margin: EdgeInsets.only(top: 20.0, left: 20.0, bottom: 20.0),
              height: 24.0,
              width: 24.0,
              child: Image.asset(iconRightArrowBlack),
            ),
            onTap: () {
              navigatorPop(context);
            },
          ),
          Container(
            margin: EdgeInsets.only(left: 20.0, bottom: 5.0),
            child: Text(
              _language.chooseMajor ?? "",
              style: TextStyle(
                  fontSize: 28.0,
                  fontFamily: fontSFPro,
                  fontWeight: FontWeight.bold),
            ),
          ),
          _renderSearchBar(_language.searchMajor ?? ""),
          Expanded(
            child: StreamBuilder(
              stream: _bloc.outputListService,
              builder: (_, snapshot){
                if(snapshot.hasData){
                  ListWorkModel model = snapshot.data;
                  if (model != null && model.data != null){
                    return ListView(
//                      padding: const EdgeInsets.only(bottom: kToolbarHeight),
                        children: _rowContent(model)
                    );
                  } else {
                    return Container();
                  }
                } else {
                  return Container();
                }
              },
            ),
          )
//          _renderListTechnique(),
        ],
      ),
    );
  }

  List<Widget> _rowContent(ListWorkModel model) {
    List<Widget> _arrTemp = [];
    List<Widget> _arrTotal = [];
    _arrTotal.add(Container(
      height: 10.0,
    ));
    for (int i = 0; i <= model.data.length - 1; i++) {
      _arrTemp.add(this._majorItem(model.data[i]));
      if (_arrTemp.length == 4) {
        _arrTotal.add(Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: _arrTemp,
        ));
        _arrTemp = [];
      } else {
        if (_arrTemp.length == 1 && i == model.data.length - 1) {
          _arrTemp.add(this._majorItemNull());
          _arrTemp.add(this._majorItemNull());
          _arrTemp.add(this._majorItemNull());
          _arrTotal.add(Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: _arrTemp,
          ));
        } else if(_arrTemp.length == 2 && i == model.data.length -1){
          _arrTemp.add(this._majorItemNull());
          _arrTemp.add(this._majorItemNull());
          _arrTotal.add(Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: _arrTemp,
          ));
        } else if (_arrTemp.length == 3 && i == model.data.length -1){
          _arrTemp.add(this._majorItemNull());
          _arrTotal.add(Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: _arrTemp,
          ));
        }
      }
    }
    return _arrTotal;
  }

  Widget _majorItem(Data service){
    return InkWell(
      child: Container(
        height: Globals.maxWidth * 0.25,
        width: Globals.maxWidth * 0.18,
        margin: EdgeInsets.only(left: 20.0),
        child: Column(
          children: <Widget>[
//            Container(
//              padding: EdgeInsets.all(15.0),
//              height: 60.0,
//              width: 60.0,
//              decoration: BoxDecoration(
//                borderRadius: BorderRadius.circular(8.0),
//                color: HexColor("#FF5E62")
//              ),
//              child: Image.asset(iconVacuum),
//            ),
            Container(
              padding: EdgeInsets.all(15.0),
              height: 60.0,
              width: 60.0,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8.0),
//                  color: HexColor("#FF5E62")
              ),
              child: service.icon != null ? CachedNetworkImage(
                fit: BoxFit.fill,
                imageUrl: urlServerImage + service.icon,
                placeholder: (context, url) => Transform(
                    alignment: FractionalOffset.center,
                    transform: Matrix4.identity()
                      ..scale(0.35, 0.35),
                    child: CupertinoActivityIndicator()),
                errorWidget: (context, url, error) =>
                    Icon(Icons.error),
              )  : Icon(Icons.error),
            ),
            Container(
              margin: EdgeInsets.only(top: 3.0),
              child: AutoSizeText(
                service.name ?? "",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            )
          ],
        ),
      ),
      onTap: (){
        navigatorPush(context, MajorDetailScreen(service.id));
    }
    );
  }

  Widget _majorItemNull(){
    return InkWell(
        child: Container(
          margin: EdgeInsets.only(left: 20.0),
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(15.0),
                height: 60.0,
                width: 60.0,
              ),
            ],
          ),
        ),
    );
  }

  Widget _renderSearchBar(String hintText){
    return Container(
//      padding: EdgeInsets.all(20.0),
      margin: EdgeInsets.all(20.0),
      height: 40.0,
      width: Globals.maxWidth,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8.0),
          color: colorSearchBar
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 10.0, right: 10.0),
            height: 16.0,
            width: 16.0,
            child: Image.asset(iconSearch),
          ),
          Expanded(
            child: Container(
              height: 40.0,
              child: TextField(
                style: TextStyle(
                    fontSize: 15.0,
                    fontFamily: fontSFPro,
                    color: Colors.black),
                decoration: InputDecoration(
                  hintText: hintText,
                  border: InputBorder.none,
                  hintStyle: TextStyle(
                      color: colorHint,
                      fontSize: 15.0,
                      fontFamily: fontSFPro),
                ),
              ),
            )
          )
        ],
      )
    );
  }

  Widget _renderButtonCreateTeam(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(),
        InkWell(
          child: Container(
              height: 50.0,
              width: 335.0,
              margin: EdgeInsets.only(
                  left: 20.0, right: 20.0, bottom: 37.0),
              decoration: BoxDecoration(
                  color: Color(0xff0AC67F),
                  borderRadius: BorderRadius.circular(26.0)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    _language.complete ?? "",
                    style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                ],
              )),
          onTap: () {
            navigatorPush(context, ManageTeamScreen());
          },
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
//    Globals().init(context);
    return Scaffold(
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
        child: Container(
            padding: EdgeInsets.only(top: Globals.maxPadding),
            height: Globals.maxHeight,
            color: Colors.white,
            child: _setupContent()
        ),
      ),
    );
  }
}
