import 'package:flutter/cupertino.dart';
import 'package:k_user_global/common/interactions.dart';
import 'package:k_user_global/models/list_service_model.dart';
import 'package:k_user_global/models/list_work_model.dart';
import 'package:k_user_global/resources/repository.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:rxdart/rxdart.dart';

class ChooseMajorBloc {
  final _repository = Repository();

//  final _streamProfile = BehaviorSubject<ProfileModel>();
  final _streamListService = BehaviorSubject<ListWorkModel>();

  Observable<ListWorkModel> get outputListService => _streamListService.stream;

  setListService(ListWorkModel event) => _streamListService.sink.add(event);

  getListWork(BuildContext context) async {
    showProgressDialog(context);
    ApiResponseData apiResponseData = await _repository.getListService(context, null);
    hideProgressDialog();
    if (apiResponseData != null){
      if (apiResponseData.success){
        ListWorkModel list = ListWorkModel.fromJson(apiResponseData.data);
        setListService(list);
      }
    }

  }

  dispose(){
    _streamListService.close();
  }
}