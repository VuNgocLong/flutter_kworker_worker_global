import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/models/temp_major_detail.dart';
import 'package:k_user_global/models/work_detail_productivity_model.dart';
import 'package:k_user_global/modules/user_module/manage_team_module/major_detail_module/src/bloc/major_detail_bloc.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MajorDetailScreen extends StatefulWidget {
  final int serviceId;

  MajorDetailScreen(this.serviceId);

  @override
  State<StatefulWidget> createState() {
    return _State();
  }
}

class _State extends State<MajorDetailScreen> with TickerProviderStateMixin{
  SharedPreferences _prefs;
  Language _language;

  // bloc
  MajorDetailBloc _bloc;

  // init
  TabController _controller;
  List<ProductivityItem> listNonProductivity;
  List<ProductivityItem> listProductivity;

  @override
  void initState() {
    super.initState();
    _language = Language();
    _bloc = MajorDetailBloc();

    _controller = TabController(length: 2, vsync: this);
    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        } else {
          _language = Vietnamese();
        }
      });
    });

    WidgetsBinding.instance.addPostFrameCallback((_) {
      initData();
      _bloc.getListWorkProductivity(context, widget.serviceId);
      _bloc.setUIType(false);
    });
  }

  initData(){
    Item item = new Item();
    item.title = "Có bằng cấp";
    item.isSelected = false;

    Item item1 = new Item();
    item1.title = "Có kinh nghiệm";
    item1.isSelected = false;

    Item item2 = new Item();
    item2.title = "Có bằng cấp";
    item2.isSelected = false;

    Item item3 = new Item();
    item3.title = "Có kinh nghiệm";
    item3.isSelected = false;


    ProductivityItem productivityItem = new ProductivityItem();
    productivityItem.list = new List<Item>();
    productivityItem.title = "Tháo lắp máy lạnh";
    productivityItem.list.add(item);
    productivityItem.list.add(item1);
    productivityItem.isDone = false;

    ProductivityItem productivityItem1 = new ProductivityItem();
    productivityItem1.list = new List<Item>();
    productivityItem1.title = "Bảo trì vệ sinh máy lạnh";
    productivityItem1.list.add(item2);
    productivityItem1.list.add(item3);
    productivityItem1.isDone = false;

    TempMajorDetail tempMajorDetail = new TempMajorDetail();
    tempMajorDetail.listProductivity = new List<ProductivityItem>();
    tempMajorDetail.listProductivity.add(productivityItem);
    tempMajorDetail.listProductivity.add(productivityItem1);
    tempMajorDetail.checkProductivity = true;
    tempMajorDetail.viewType = false;

    _bloc.setMajorDetail(tempMajorDetail);
  }

  Widget _setupContent() {
    return Container(
      color: Colors.white,
      padding:
      EdgeInsets.only(top: Globals.maxPadding, bottom: Globals.maxPadding),
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          InkWell(
            child: Container(
              margin: EdgeInsets.only(top: 20.0, left: 20.0, bottom: 20.0),
              height: 24.0,
              width: 24.0,
              child: Image.asset(iconRightArrowBlack),
            ),
            onTap: () {
              navigatorPop(context);
            },
          ),
          Container(
            margin: EdgeInsets.only(left: 20.0, bottom: 5.0),
            child: Text(
              "Giúp việc",
              style: TextStyle(
                  fontSize: 28.0,
                  fontFamily: fontSFPro,
                  fontWeight: FontWeight.bold),
            ),
          ),
          _buildTab(),
          Expanded(
            child: TabBarView(
              controller: _controller,
              children: <Widget>[
                _buildContentTab1(),
                _buildContentTab2()
              ],
            ),
          )
//          _renderListTechnique(),
        ],
      ),
    );
  }

  Widget _renderButtonConfirm(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(),
        InkWell(
          child: Container(
              height: 50.0,
              width: 335.0,
              margin: EdgeInsets.only(
                  left: 20.0, right: 20.0, bottom: 37.0),
              decoration: BoxDecoration(
                  color: Color(0xff0AC67F).withOpacity(0.3),
                  borderRadius: BorderRadius.circular(26.0)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    _language.complete ?? "",
                    style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                ],
              )),
          onTap: () {
          },
        )
      ],
    );
  }

  Widget _buildTab() {
    return DefaultTabController(
      length: 2,
      initialIndex: 0,
      child: Container(
        width: Globals.maxWidth,
          padding: EdgeInsets.only(left: 20.0, right: 20.0),
        child: TabBar(
          isScrollable: false,
          tabs: <Widget>[
            Container(
              width: Globals.maxWidth / 2,
              child: Tab(
                text: _language.nonProductivity ?? "",
              ),
            ),
            Container(
              width: Globals.maxWidth / 2,
              child: Tab(
                text: _language.productivity ?? "",
              ),
            ),
          ],
//          unselectedLabelColor: grayLightColor,
//          labelColor: greenLight,
          unselectedLabelStyle: TextStyle(fontSize: 17.0, fontFamily: fontSFPro, color: backgroundWhiteF2),
          labelStyle: TextStyle(fontSize: 17.0, fontWeight: FontWeight.bold, fontFamily: fontSFPro),
          indicatorColor: primaryColor,
          indicatorSize: TabBarIndicatorSize.tab,
          controller: _controller,
        ),
      ),
    );
  }

  Widget _buildContentTab1(){
    return StreamBuilder(
      stream: _bloc.outputMajorDetail,
      builder: (_, snapshot){
        if (snapshot.hasData){
          TempMajorDetail temp = snapshot.data;
          if (temp.checkProductivity){
            return Container(
              child: Column(
                children: <Widget>[
                  _renderSearchBar(_language.findMajor ?? ""),
                  StreamBuilder(
                    stream: _bloc.outputWorkList,
                    builder: (_, snapshot){
                      if(snapshot.hasData){
                        WorkDetailProductivityModel model = snapshot.data;
                        return Column(
//                          children: model.data.,
                        );
                      } else {
                        return Container();
                      }
                    },
                  ),
                  Column(
                    children: temp.listProductivity.map((value) => _renderSelectedMajorRecord(value)).toList(),
                  )
                ],
              ),
            );
          }
          return Column(
            children: <Widget>[
              _renderSearchBar(_language.findMajor ?? ""),
              _renderAddMajor1()
            ],
          );
        }
        return Container();
      },
    );
  }

  Widget _buildContentTab2(){
    return Column(
      children: <Widget>[
        _renderSearchBar(_language.findMajor ?? ""),
        StreamBuilder(
          stream: _bloc.outputWorkList,
          builder: (_, snapshot){
            if(snapshot.hasData){
              WorkDetailProductivityModel model = snapshot.data;
              return Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: model.data.serviceProductivity.map((value) => _renderProductivityRecord(value)).toList(),
                  )
                ),
              );
            } else {
              return Container();
            }
          },
        ),
        _renderAddMajor1()
      ],
    );
  }

  Widget _renderSearchBar(String hintText){
    return Container(
//      padding: EdgeInsets.all(20.0),
        margin: EdgeInsets.all(20.0),
        height: 40.0,
        width: Globals.maxWidth,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0),
            color: colorSearchBar
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 10.0, right: 10.0),
              height: 16.0,
              width: 16.0,
              child: Image.asset(iconSearch),
            ),
            Expanded(
                child: Container(
                  height: 40.0,
                  child: TextField(
                    style: TextStyle(
                        fontSize: 15.0,
                        fontFamily: fontSFPro,
                        color: Colors.black),
                    decoration: InputDecoration(
                      hintText: hintText,
                      border: InputBorder.none,
                      hintStyle: TextStyle(
                          color: colorHint,
                          fontSize: 15.0,
                          fontFamily: fontSFPro),
                    ),
                  ),
                )
            )
          ],
        )
    );
  }

  Widget _renderRecordNonProductivity(String title){
    bool check = false;
    return Container(
     child: Column(
       children: <Widget>[
          check ? Container(
            child: Row (
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: AutoSizeText(
                    title,
                    style: TextStyle(fontFamily: fontSFPro, fontSize: 20.0, color: primaryColor, fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  height: 11.0,
                  width: 11.0,
                  child: Image.asset(
                    iconChecked,
                    width: 15.0,
                    height: 11.0,
                  ),
                )
              ],
            ),
          ) : Container(
            child: AutoSizeText(
              title,
              style: TextStyle(fontFamily: fontSFPro, fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
          ),
         Container(
           child: Row(

           ),
         )
       ],
     ),
    );
  }

  Widget _renderAddMajor1(){
    return Container(
      margin: EdgeInsets.only(left: 20.0, right: 20.0),
      height: 44,
      padding: EdgeInsets.only(left: 20.0, right: 20.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8.0),
        boxShadow: [
          BoxShadow(
            color: colorHint,
            blurRadius: 25.0, // soften the shadow
            spreadRadius: 5.0, //extend the shadow
            offset: Offset(
              5.0, // Move to right 10  horizontally
              5.0, // Move to bottom 10 Vertically
            ),
          )
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 10.0),
            height: 20.0,
            width: 20.0,
            child: Image.asset(iconPlusBlue),
          ),
          Container(
            child: Text(
              _language.addTechnique ?? "",

              style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold, fontFamily: fontSFPro),
            ),
          )
        ],
      ),
    );
  }

  Widget _renderSelectedMajorRecord(ProductivityItem item){
    return Container(
      margin: EdgeInsets.only(top: 5.0),
      padding: EdgeInsets.only(right: 20.0, bottom: 10.0),
      child: Column(
        children: <Widget>[
          Container(
            child: Row(
              children: <Widget>[
                Container(
                  height: 20.0,
                  width: 20.0,
                  child: Image.asset(iconVacuum),
                ),
                Expanded(
                  child: AutoSizeText(
                    item.title ?? "",
                    style: TextStyle(fontSize: 20.0, fontFamily: fontSFPro, fontWeight: FontWeight.bold),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                Container(
                  height: 20.0,
                  width: 20.0,
                  child: Image.asset(iconVacuum),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 20.0, right: 20.0),
            child: Column(
              children: item.list.map((value) => _renderChoice(value)).toList(),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 20.0, bottom: 20.0, left: 10.0),
            height: 1.0,
            width: Globals.maxWidth,
            color: Colors.black,
          )
        ],
      ),
    );
  }

  Widget _renderProductivityRecord(ServiceProductivity item){
    return Container(
      margin: EdgeInsets.only(top: 5.0),
      padding: EdgeInsets.only(right: 20.0, bottom: 10.0),
      child: Column(
        children: <Widget>[
          Container(
            child: Row(
              children: <Widget>[
                Container(
                  height: 20.0,
                  width: 20.0,
                  child: Image.asset(iconVacuum),
                ),
                Expanded(
                  child: AutoSizeText(
                    item.name ?? "",
                    style: TextStyle(fontSize: 20.0, fontFamily: fontSFPro, fontWeight: FontWeight.bold),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                Container(
                  height: 20.0,
                  width: 20.0,
                  child: Image.asset(iconVacuum),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 20.0, right: 20.0),
            child: Column(
              children: item.productivity.map((value) => _renderChoiceProductivity(value)).toList(),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 20.0, bottom: 20.0, left: 10.0),
            height: 1.0,
            width: Globals.maxWidth,
            color: Colors.black,
          )
        ],
      ),
    );
  }

  Widget _renderChoice(Item item){
    return item.isSelected ? InkWell(
      child: Container(
        margin: EdgeInsets.only(top: 20.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              item.title ?? "",
              style: TextStyle(fontSize: 17.0, fontFamily: fontSFPro),
            ),
            Container(
              height: 22.2,
              width: 22.2,
              padding: EdgeInsets.all(3.0),
              decoration: BoxDecoration(
                color: Colors.black,
                borderRadius: BorderRadius.circular(4.0),
              ),
              child: Image.asset(iconTickWhite),
            )
          ],
        ),
      ),
      onTap: (){
        item.isSelected = false;
        setState(() {

        });
      },
    ) : InkWell(
      child: Container(
        margin: EdgeInsets.only(top: 20.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              item.title ?? "",
              style: TextStyle(fontSize: 17.0, fontFamily: fontSFPro),
            ),
            Container(
              height: 22.2,
              width: 22.2,
              padding: EdgeInsets.all(3.0),
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: colorHint,width: 1.0),
                borderRadius: BorderRadius.circular(4.0),
              ),
            )
          ],
        ),
      ),
      onTap: (){
        item.isSelected = true;
        setState(() {

        });
      },
    );
  }

  Widget _renderChoiceProductivity(Productivity item){
    return item.isSelected ? InkWell(
      child: Container(
        margin: EdgeInsets.only(top: 20.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              item.unit.name ?? "",
              style: TextStyle(fontSize: 17.0, fontFamily: fontSFPro),
            ),
            Container(
              height: 22.2,
              width: 22.2,
              padding: EdgeInsets.all(3.0),
              decoration: BoxDecoration(
                color: Colors.black,
                borderRadius: BorderRadius.circular(4.0),
              ),
              child: Image.asset(iconTickWhite),
            )
          ],
        ),
      ),
      onTap: (){
        item.isSelected = false;
        setState(() {

        });
      },
    ) : InkWell(
      child: Container(
        margin: EdgeInsets.only(top: 20.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              item.unit.name ?? "",
              style: TextStyle(fontSize: 17.0, fontFamily: fontSFPro),
            ),
            Container(
              height: 22.2,
              width: 22.2,
              padding: EdgeInsets.all(3.0),
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: colorHint,width: 1.0),
                borderRadius: BorderRadius.circular(4.0),
              ),
            )
          ],
        ),
      ),
      onTap: (){
        item.isSelected = true;
        setState(() {

        });
      },
    );
  }



  @override
  Widget build(BuildContext context) {
//    Globals().init(context);
    return Scaffold(
      backgroundColor: Colors.black,
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.only(top: Globals.maxPadding),
                height: Globals.maxHeight,
                color: Colors.white,
                child: _setupContent()
            ),
          ),
          _renderButtonConfirm(),
        ],
      )
    );
  }
}
