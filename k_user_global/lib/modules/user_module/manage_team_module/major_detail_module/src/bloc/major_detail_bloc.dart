import 'package:flutter/cupertino.dart';
import 'package:k_user_global/common/interactions.dart';
import 'package:k_user_global/models/temp_major_detail.dart';
import 'package:k_user_global/models/work_detail_productivity_model.dart';
import 'package:k_user_global/resources/repository.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:rxdart/rxdart.dart';

class MajorDetailBloc {
  final _repository = Repository();

  final _streamEnableButton = BehaviorSubject<bool>();
  final _streamMajorDetail = BehaviorSubject<TempMajorDetail>();
  final _streamUIType = BehaviorSubject<bool>();
  final _streamListWork = BehaviorSubject<WorkDetailProductivityModel>();

  Observable<bool> get outputEnableButton => _streamEnableButton.stream;
  Observable<TempMajorDetail> get outputMajorDetail => _streamMajorDetail.stream;
  Observable<bool> get outputUIType => _streamUIType.stream;
  Observable<WorkDetailProductivityModel> get outputWorkList => _streamListWork.stream;

  setEnableButton(bool event) => _streamEnableButton.sink.add(event);
  setMajorDetail(TempMajorDetail event) => _streamMajorDetail.sink.add(event);
  setUIType(bool event) => _streamUIType.sink.add(event);
  setWorkList(WorkDetailProductivityModel event) => _streamListWork.sink.add(event);

  getListWorkProductivity(BuildContext context, int serviceId) async {
    Map<String, String> params = Map<String, String>();
    params['service_id'] = "1";
    showProgressDialog(context);
    ApiResponseData apiResponseData = await _repository.getListWorkService(context, params);
    hideProgressDialog();
    if(apiResponseData != null) {
      WorkDetailProductivityModel model = WorkDetailProductivityModel.fromJson(apiResponseData.data);
      setWorkList(model);
    }
  }

  dispose() {
    _streamListWork.close();
    _streamMajorDetail.close();
    _streamEnableButton.close();
    _streamUIType.close();
  }
}
