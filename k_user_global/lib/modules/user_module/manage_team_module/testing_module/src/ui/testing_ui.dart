import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/models/question_model.dart';
import 'package:k_user_global/modules/user_module/manage_team_module/testing_module/src/bloc/testing_bloc.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TestingScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _State();
  }
}

class _State extends State<TestingScreen> {
  SharedPreferences _prefs;
  Language _language;
  QuestionModel question = QuestionModel();

  // init
//  TempTesting temp = new TempTesting();

  // bloc
  TestingBloc _bloc;

  @override
  void initState() {
    super.initState();
    _language = Language();
    _bloc = TestingBloc();

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Colors.white,// Color for Android
        statusBarBrightness: Brightness.dark // Dark == white status bar -- for IOS.
    ));

    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        } else {
          _language = Vietnamese();
        }
      });
    });

    WidgetsBinding.instance.addPostFrameCallback((_) {
//      initData();
      _bloc.getQuestionList(context);
      _bloc.setCheckDone(false);
    });
  }

//  initData(){
//    Answer ans1 = Answer();
//    ans1.content = "Tự bỏ về";
//    ans1.isSelected = false;
//
//    Answer ans2 = Answer();
//    ans2.content = "Liên hệ với khách hàng hoặc công ty nhờ hỗ trợ";
//    ans2.isSelected = false;
//
//    Answer ans3 = Answer();
//    ans3.content = "Tự bỏ về";
//    ans3.isSelected = false;
//
//    Answer ans4 = Answer();
//    ans4.content = "Liên hệ với khách hàng hoặc công ty nhờ hỗ trợ";
//    ans4.isSelected = false;
//
//    Answer ans5 = Answer();
//    ans5.content = "Tự bỏ về";
//    ans5.isSelected = false;
//
//    Answer ans6 = Answer();
//    ans6.content = "Liên hệ với khách hàng hoặc công ty nhờ hỗ trợ";
//    ans6.isSelected = false;
//
//    Question ques1 = Question();
//    ques1.list = List<Answer>();
//    ques1.title = "Khi không tìm được nhà khách hàng thì phải làm sao?";
//    ques1.isDone = false;
//    ques1.list.add(ans1);
//    ques1.list.add(ans2);
//
//    Question ques2 = Question();
//    ques2.list = List<Answer>();
//    ques2.title = "Khi không tìm được nhà khách hàng thì phải làm sao?";
//    ques2.isDone = false;
//    ques2.list.add(ans3);
//    ques2.list.add(ans4);
//
//    Question ques3 = Question();
//    ques3.list = List<Answer>();
//    ques3.title = "Test nè ahihi?";
//    ques3.isDone = false;
//    ques3.list.add(ans5);
//    ques3.list.add(ans6);
//
//    temp.listQuestion = List<Question>();
//    temp.listQuestion.add(ques1);
//    temp.listQuestion.add(ques2);
//    temp.listQuestion.add(ques3);
//  }

  Widget _setupContent() {
    return Container(
      color: Colors.white,
      padding:
      EdgeInsets.only(top: Globals.maxPadding, bottom: Globals.maxPadding + 80),
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: ListView(
//        crossAxisAlignment: CrossAxisAlignment.start,
//        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              InkWell(
                child: Container(
                  margin: EdgeInsets.only(top: 20.0, left: 20.0, bottom: 30.0),
                  height: 24.0,
                  width: 24.0,
                  child: Image.asset(iconRightArrowBlack),
                ),
                onTap: () {
                  navigatorPop(context);
                },
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(left: 20.0, bottom: 5.0),
            child: Text(
              _language.questionTesting ?? "",
              style: TextStyle(
                  fontSize: 34.0,
                  fontFamily: fontSFPro,
                  fontWeight: FontWeight.w400),
            ),
          ),
          Container(
              margin: EdgeInsets.only(left: 20.0, bottom: 25.0),
              child: AutoSizeText(
                _language.completeAllQuestion ?? "",
                style: TextStyle(
                    fontSize: 17.0, fontFamily: fontSFPro, color: colorHint),
              )),
          StreamBuilder(
            stream: _bloc.outputQuestion,
            builder: (_, snapshot){
              if (snapshot.hasData){
                question = snapshot.data;
                if (question != null){
                  return Container(
                      padding: EdgeInsets.only(left: 20.0, right: 20.0),
                      child: Column(
                        children: question.data.map((value) => _renderQuestion(value)).toList(),
                      )
                  );
                } else {
                  return Container();
                }
              } else {
                return Container();
              }
            },
          ),
        ],
      ),
    );
  }

  Widget _renderButtonComplete(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(),
        StreamBuilder(
          stream: _bloc.outputCheckDone,
          builder: (_, snapshot){
            if(snapshot.hasData){
              bool check = snapshot.data;
              if (check) return _renderButton(true);
              else return _renderButton(false);
            } else {
              return _renderButton(false);
            }
          },
        )
      ],
    );
  }

  Widget _renderButton(bool isActive){
    return isActive ? InkWell(
      child: Container(
          height: 50.0,
          width: 335.0,
          margin: EdgeInsets.only(
              left: 20.0, right: 20.0, bottom: 37.0),
          decoration: BoxDecoration(
              color: Color(0xff0AC67F),
              borderRadius: BorderRadius.circular(26.0)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                _language.complete ?? "",
                style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ],
          )),
      onTap: (){
        _bloc.submitTest(context);
      },
    ) : InkWell(
      child: Container(
          height: 50.0,
          width: 335.0,
          margin: EdgeInsets.only(
              left: 20.0, right: 20.0, bottom: 37.0),
          decoration: BoxDecoration(
              color: Color(0xff0AC67F).withOpacity(0.3),
              borderRadius: BorderRadius.circular(26.0)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                _language.complete ?? "",
                style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ],
          )),
    );
  }

  Widget _renderQuestion(Data question){
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
            child: Text(
              "Câu 1",
              style: TextStyle(fontSize: 17.0, fontWeight: FontWeight.bold, fontFamily: fontSFPro),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
            child: Text(
              question.name ?? "",
              style: TextStyle(fontSize: 17.0, fontFamily: fontSFPro),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          question.answer != null && question.answer.length > 0 ? Column(
            children: question.answer.map((value) => _renderAnswer(value, question)).toList(),
          ) : Container()
        ],
      ),
    );
  }

  Widget _renderAnswer(Answer ans, Data question){
    return ans.isSelected ? Container(
      margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 5.0),
            height: 18.0,
            width: 18.0,
            child: Image.asset(iconBlueCircle),
          ),
          Expanded(
            child: Text(
              ans.name ?? "",
              style: TextStyle(fontSize: 17.0, fontFamily: fontSFPro, fontWeight: FontWeight.bold),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          )
        ],
      ),
    ) : InkWell(
      child: Container(
        margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(right: 5.0),
              height: 18.0,
              width: 18.0,
              decoration: BoxDecoration(
                shape: BoxShape.circle, color: colorHint
              ),
            ),
            Expanded(
              child: Text(
                ans.name ?? "",
                style: TextStyle(fontSize: 17.0, fontFamily: fontSFPro),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            )
          ],
        ),
      ),
      onTap: (){
        for (var o in question.answer) {
          o.isSelected = false;
        }
        ans.isSelected = true;
        setState(() {
          checkDone();
        });
      },
    );
  }

  void checkDone(){
    bool check = false;
    if (question != null && question.data != null){
      for (var o in question.data) {
        for (var n in o.answer) {
          if (n.isSelected == true) check = true;
        }
      }
      if (check) _bloc.setCheckDone(true);
    } else {
      _bloc.setCheckDone(false);
    }
  }

  @override
  Widget build(BuildContext context) {
//    Globals().init(context);
    return Scaffold(
      backgroundColor: Colors.black,
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(top: Globals.maxPadding),
              height: Globals.maxHeight,
              color: Colors.white,
              child: _setupContent(),
            ),
          ),
          _renderButtonComplete()
        ],
      ),
    );
  }
}
