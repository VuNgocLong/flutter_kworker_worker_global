import 'package:flutter/cupertino.dart';
import 'package:k_user_global/common/interactions.dart';
import 'package:k_user_global/models/choose_question_model.dart';
import 'package:k_user_global/models/question_model.dart';
import 'package:k_user_global/resources/repository.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:rxdart/rxdart.dart';

class TestingBloc {
  final _repository = Repository();

  final _streamQuestion = BehaviorSubject<QuestionModel>();
  final _streamCheckDone = BehaviorSubject<bool>();

  Observable<QuestionModel> get outputQuestion => _streamQuestion.stream;
  Observable<bool> get outputCheckDone => _streamCheckDone.stream;

  setQuestion(QuestionModel event) => _streamQuestion.sink.add(event);
  setCheckDone(bool event) => _streamCheckDone.sink.add(event);

  getQuestionList(BuildContext context) async {
    Map<String, String> params = Map<String, String>();
    params['id'] = "8";
    showProgressDialog(context);
    ApiResponseData responseData = await _repository.getProfileQuestion(context, params);
    hideProgressDialog();
    if(responseData != null){
      if (responseData.success){
        QuestionModel model = QuestionModel.fromJson(responseData.data);
        setQuestion(model);
      }
    }
  }

  submitTest(BuildContext context) async {
    ChooseQuestionModel model = ChooseQuestionModel();
    model.questionId = 1;
    model.ids = List<int>();
    model.ids.add(1);
    showProgressDialog(context);
    ApiResponseData responseData = await _repository.postChooseQustion(context, model.toJson());
    hideProgressDialog();
    if (responseData != null){
      if (responseData.success) navigatorPop(context);
    }

  }

  dispose(){
    _streamQuestion.close();
    _streamCheckDone.close();
  }
}