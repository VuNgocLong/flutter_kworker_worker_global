import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/modules/user_module/manage_team_module/change_working_time_module/src/ui/change_working_time_ui.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:k_user_global/widgets/title_white.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ManageMemberScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<ManageMemberScreen>
    with WidgetsBindingObserver {
  // language
  SharedPreferences _prefs;
  Language _language;

  @override
  void initState() {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Colors.white,// Color for Android
        statusBarBrightness: Brightness.dark // Dark == white status bar -- for IOS.
    ));

    super.initState();
    WidgetsBinding.instance.addObserver(this);
    _language = Language();
    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        } else {
          _language = Vietnamese();
        }
      });
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  Widget _renderContent(){
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Center(
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 10.0),
                  height: 100.0,
                  width: 100.0,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle
                  ),
                  child: Image.asset(iconDefaultAvatar),
                ),
                Container(
                  margin: EdgeInsets.only(top: 15.0, bottom: 5),
                  padding: EdgeInsets.only(left: 20.0, right: 20.0),
                  child: Text(
                    'Duy An',
                    style: TextStyle(fontSize: 17.0, fontWeight: FontWeight.bold, fontFamily: fontSFPro),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10.0),
                  padding: EdgeInsets.only(left: 20.0, right: 20.0),
                  child: Text(
                    'Thợ giúp việc, thợ làm tóc',
                    style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold, fontFamily: fontSFPro, color: colorTextGrayNew.withOpacity(0.6)),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10.0),
                  padding: EdgeInsets.only(left: 20.0, right: 20.0),
                  child: Text(
                    'Đội trưởng',
                    style: TextStyle(fontSize: 15.0, fontFamily: fontSFPro, color: orangeColor),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ],
            )
          ),
          _renderRecord(_language.phone ?? "", "090 576 4789"),
          _renderRecord(_language.address ?? "", "33 Nguyễn Cơ Thạch, Phường An Lợi Đông, Phường 2, Hồ Chí Minh"),
          _renderShift("Cả ngày")
        ],
      ),
    );
  }

  Widget _renderRecord(String title, String content){
    return Container(
      margin: EdgeInsets.only(top: 10.0, bottom: 20.0),
      padding: EdgeInsets.only(left: 20.0, right: 20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Text(
              title,
              style: TextStyle(fontFamily: fontSFPro, fontSize: 17.0, color: colorTextGrayNew.withOpacity(0.6)),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 5.0),
            child: Text(
              content,
              style: TextStyle(fontFamily: fontSFPro, fontSize: 17.0, fontWeight: FontWeight.bold),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          )
        ],
      ),
    );
  }

  Widget _renderShift(String content){
    return Container(
      margin: EdgeInsets.only(top: 10.0, bottom: 20.0),
      padding: EdgeInsets.only(left: 20.0, right: 20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Text(
              _language.workingTime ?? "",
              style: TextStyle(fontFamily: fontSFPro, fontSize: 17.0, color: colorTextGrayNew.withOpacity(0.6)),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 5.0),
                child: Text(
                  content ?? "",
                  style: TextStyle(fontFamily: fontSFPro, fontSize: 17.0, fontWeight: FontWeight.bold),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              InkWell(
                child: Container(
                  child: Text(
                    _language.change ?? "",
                    style: TextStyle(fontSize: 13.0, fontFamily: fontSFPro, color: primaryColor),
                  ),
                ),
                onTap: (){
                  navigatorPush(context, ChangeWorkingTimeScreen());
                },
              )
            ],
          )
        ],
      ),
    );
  }




  @override
  Widget build(BuildContext context) {
//    Globals().init(context);
    return TitleWhite(
      title: _language.manageMember ?? "",
      isBack: true,
      customIcon: SvgPicture.asset(
        iconRightArrowSvg,
      ),
      child: Container(
        width: Globals.maxWidth,
        height: Globals.maxHeight -
            MediaQuery.of(context).padding.bottom -
            MediaQuery.of(context).padding.top -
            75,
        child: _renderContent()
      ),
    );
  }
}
