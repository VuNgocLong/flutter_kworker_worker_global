import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UpdateTeamInfoScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _State();
  }
}

class _State extends State<UpdateTeamInfoScreen> {
  SharedPreferences _prefs;
  Language _language;



  @override
  void initState() {
    super.initState();
    _language = Language();

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Colors.white,// Color for Android
        statusBarBrightness: Brightness.dark // Dark == white status bar -- for IOS.
    ));

    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        } else {
          _language = Vietnamese();
        }
      });
    });
  }

  Widget _setupContent() {
    return Container(
      color: Colors.white,
      padding:
      EdgeInsets.only(top: Globals.maxPadding, bottom: Globals.maxPadding),
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          InkWell(
            child: Container(
              margin: EdgeInsets.only(top: 20.0, left: 20.0, bottom: 30.0),
              height: 24.0,
              width: 24.0,
              child: Image.asset(iconRightArrowBlack),
            ),
            onTap: () {
              navigatorPop(context);
            },
          ),
          Container(
            margin: EdgeInsets.only(left: 20.0, bottom: 5.0),
            child: Text(
              _language.updateInfo ?? "",
              style: TextStyle(
                  fontSize: 34.0,
                  fontFamily: fontSFPro,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Container(
              margin: EdgeInsets.only(left: 20.0, bottom: 35.0),
              child: AutoSizeText(
                _language.updateInfoTeam ?? "",
                style: TextStyle(
                    fontSize: 17.0, fontFamily: fontSFPro, color: colorHint),
              )),
          _renderItem(_language.basicInfo, null),
          _renderItem(_language.addTechnique, null),
        ],
      ),
    );
  }

  Widget _renderItem(String title, Widget screen){
    return InkWell(
      child: Container(
        margin: EdgeInsets.only(bottom: 10.0, top: 20.0),
        padding: EdgeInsets.only(left: 20.0, right: 20.0),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                AutoSizeText(
                  title,
                  style: TextStyle(fontSize: 17.0, fontWeight: FontWeight.bold, fontFamily: fontSFPro),
                ),
                Container(
                  width: 25.0,
                  height: 17.0,
                  child: Image.asset(iconArrowToRightBlack, width: 25.0, height: 17.0,),
                )
              ],
            ),
            Container(
              margin: EdgeInsets.only(top: 20.0),
              height: 0.8,
              color: colorHint.withOpacity(0.4),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
//    Globals().init(context);
    return Scaffold(
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(top: Globals.maxPadding),
          height: Globals.maxHeight,
          color: Colors.white,
          child: _setupContent(),
        ),
      ),
    );
  }
}
