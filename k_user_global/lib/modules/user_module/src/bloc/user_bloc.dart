import 'package:flutter/cupertino.dart';
import 'package:k_user_global/common/interactions.dart';
import 'package:k_user_global/models/profile_info_model.dart';
import 'package:k_user_global/resources/repository.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:rxdart/rxdart.dart';

class UserBloc {
  final _repository = Repository();

  final _streamProfile = BehaviorSubject<ProfileInfo>();

  Observable<ProfileInfo> get outputProfile => _streamProfile.stream;

  setProfile(ProfileInfo event) => _streamProfile.sink.add(event);

  getProfileInfo(BuildContext context) async {
    showProgressDialog(context);
    ApiResponseData responseData = await _repository.getProfileInfo(context, null);
    hideProgressDialog();
    if(responseData.success){
      if (responseData.data != null){
        ProfileInfo model = ProfileInfo.fromJson(responseData.data);
        setProfile(model);
      }
    }
  }

  dispose(){
    _streamProfile.close();
  }
}