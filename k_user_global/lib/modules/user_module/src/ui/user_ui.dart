import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/models/profile_info_model.dart';
import 'package:k_user_global/modules/home_module/src/ui/home_ui.dart';
import 'package:k_user_global/modules/location_module/save_location_module/src/ui/save_location_ui.dart';
import 'package:k_user_global/modules/user_module/change_language_module/src/ui/change_language_ui.dart';
import 'package:k_user_global/modules/user_module/change_user_information_module/src/ui/change_user_information_ui.dart';
import 'package:k_user_global/modules/user_module/create_team_module/src/ui/create_team_ui.dart';
import 'package:k_user_global/modules/user_module/invite_friend_module/src/ui/invite_friend_ui.dart';
import 'package:k_user_global/modules/user_module/manage_profile_module/src/ui/manage_profile_ui.dart';
import 'package:k_user_global/modules/user_module/setting_module/src/ui/setting_ui.dart';
import 'package:k_user_global/modules/user_module/src/bloc/user_bloc.dart';
import 'package:k_user_global/modules/user_module/statistic_income_module/src/ui/statistic_income_ui.dart';
import 'package:k_user_global/ultilites/hex_color.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _UserState();
}

class _UserState extends State<UserScreen> with WidgetsBindingObserver {
  SharedPreferences _prefs;
  Language _language;

  // bloc
  UserBloc _bloc;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    _bloc = UserBloc();
    _language = Language();
    SharedPreferences.getInstance().then((data) {
      _prefs = data;
      String _model = _prefs.getString(keyLanguage);
      if (_model != null) {
        if (_model == 'vi') {
          _language = Vietnamese();
        } else if (_model == 'eng') {
          _language = English();
        } else if (_model == 'korean') {
          _language = Korean();
        } else if (_model == 'japanese') {
          _language = Japanese();
        }
      } else {
        _language = Vietnamese();
      }

      setState(() {});
    });
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _bloc.getProfileInfo(context);
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  @override
  Widget build(BuildContext context) {
//    Globals().init(context);
    return Scaffold(
      body: Container(
        height: Globals.maxHeight,
        color: Colors.black,
        constraints: BoxConstraints.expand(),
        child: Stack(
          children: <Widget>[
            _setupBackground(),
            _renderBody(),
          ],
        ),
      ),
    );
  }

  Widget _renderBody() {
    return Container(
        width: Globals.maxWidth,
        height: Globals.maxHeight,
        decoration: BoxDecoration(
          color: HexColor('FAFAFA'),
        ),
        child: Container(
            padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
            child: ListView(children: <Widget>[
              Container(
                  height: 60,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        child: Container(
                            height: 60.0,
                            width: 60.0,
                            padding: EdgeInsets.only(left: 20.0, right: 20.0),
                            child: Image.asset(
                              iconCancel,
                              width: 15.0,
                              height: 15.0,
                            )),
                        onTap: () {
                          navigatorPop(context);
                        },
                      ),
                      Container(),
                    ],
                  )),
              _renderUserAvatar(),
              _renderFunction(iconManageProfile, _language.manageProfile ?? "",
                  ManageProfileScreen()),
              _renderFunction(iconStatisticInCome,
                  _language.statisticIncome ?? "", StatisticIncomeScreen()),
              _renderFunction(iconCreateTeam, _language.createTeam ?? "",
                  CreateTeamScreen()),
              _renderFunction(iconCompany, _language.group ?? "", HomeUi()),
              _renderFunction(iconSavedPlace, _language.savedLocation ?? "",
                  SaveLocationScreen()),
              _renderFunction(iconInviteFriend, _language.inviteFriend ?? "",
                  InviteFriendScreen()),
              _renderFunction(
                  iconUrgentCall, _language.urgentCall ?? "", HomeUi()),
              _renderFunction(iconTranslate, _language.language ?? "",
                  ChangeLanguageScreen()),
              _renderFunction(
                  iconSetting, _language.setting ?? "", SettingScreen()),
              _renderFunction(iconSupport, _language.support ?? "", HomeUi()),
              _renderFunction(iconMore, _language.more ?? "", HomeUi()),
            ])));
  }

  Widget _setupBackground() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
//          borderRadius: BorderRadius.all(Radius.circular(34.0)),
          image: DecorationImage(
        image: AssetImage(background),
        fit: BoxFit.cover,
      )),
    );
  }

  Widget _renderUserAvatar() {
    return StreamBuilder(
      stream: _bloc.outputProfile,
      builder: (_, snapshot){
        ProfileInfo profile = snapshot.data;
        return InkWell(
          child: Container(
            margin: EdgeInsets.only(top: 32.0),
            child: Row(
              children: <Widget>[
                Container(
                    margin: EdgeInsets.only(left: 15.0, right: 12.0),
                    height: 76.0,
                    width: 76.0,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10000000.0),
                      child: profile != null && profile.data != null ? profile.data.avatar != null ? CachedNetworkImage(
                        fit: BoxFit.cover,
                        imageUrl:urlServerImage + profile.data.avatar,
                        placeholder: (context, url) => Transform(
                            alignment: FractionalOffset.center,
                            transform: Matrix4.identity()..scale(0.35, 0.35),
                            child: CupertinoActivityIndicator()),
                        errorWidget: (context, url, error) =>
                            Icon(Icons.error),
                      ) : Container(
                        margin: EdgeInsets.only(left: 20.0),
                        height: 60.0,
                        width: 60.0,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                        ),
                        child: Image.asset(iconDefaultAvatar),
                      ) : Container(
                        margin: EdgeInsets.only(left: 20.0),
                        height: 60.0,
                        width: 60.0,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                        ),
                        child: Image.asset(iconDefaultAvatar),
                      ),
                    )),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(bottom: 5.0, left: 15.0),
                      child: Text(
                        profile != null && profile.data != null  ? profile.data.name ?? "User" : "User",
                        style: TextStyle(
                            fontSize: 17.0,
                            fontFamily: fontSFPro,
                            fontWeight: FontWeight.bold),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    InkWell(
                      child: Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 15.0),
                            child: Text(
                              _language.change ?? "",
                              style:
                              TextStyle(color: grayLightColor, fontSize: 13.0),
                            ),
                          ),
                          Container(
                              margin: EdgeInsets.only(left: 5.0),
                              child: SvgPicture.asset(
                                iconGrayArrowSvg,
                                width: 13.0,
                                height: 13.0,
                              ))
                        ],
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
          onTap: () {
            navigatorPush(context, ChangeUserInformationScreen());
          },
        );
      },
    );
  }

  Widget _renderFunction(String logo, String functionName, Widget widget) {
    return InkWell(
      child: Container(
        margin: EdgeInsets.only(top: 32.0),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                    padding: EdgeInsets.only(left: 15.0, right: 7.5),
                    child: Image.asset(
                      logo,
                      width: 16.0,
                      height: 13.0,
                    )),
                Expanded(
                  child: Text(
                    functionName,
                    style: TextStyle(fontSize: 15.0, fontFamily: fontSFPro),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                Container(
                    margin: EdgeInsets.only(left: 5.0, right: 15.0),
                    child: SvgPicture.asset(
                      iconBlackRightArrow,
                      width: 13.0,
                      height: 13.0,
                    ))
              ],
            ),
            Container(
              margin: EdgeInsets.only(top: 13.0, left: 15.0, right: 15.0),
              height: 1.0,
              color: colorLine,
            )
          ],
        ),
      ),
      onTap: () async {
        await navigatorPush(context, widget);
        SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
            statusBarColor: Colors.white, // Color for Android
            statusBarBrightness:
                Brightness.dark // Dark == white status bar -- for IOS.
            ));
      },
    );
  }
}
