import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/models/temp_gender.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AccountInformationScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _State();
  }
}

class _State extends State<AccountInformationScreen> {
  SharedPreferences _prefs;
  Language _language;

  // bloc init

  // init var
  List<TempGender> listGender = new List<TempGender>();

  // text controller init
  TextEditingController _lastNameController = TextEditingController();
  TextEditingController _firstNameController = TextEditingController();
  TextEditingController _addressControler = TextEditingController();
  TextEditingController _dobControler = TextEditingController();

  @override
  void initState() {
    super.initState();
    _language = Language();

    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        } else {
          _language = Vietnamese();
        }
      });
    });

    WidgetsBinding.instance.addPostFrameCallback((_) {
      initData();
    });
  }

  initData(){
    TempGender temp1 = TempGender();
    temp1.name = "Nam";
    temp1.isSelected = true;

    TempGender temp2 = TempGender();
    temp2.name = "Nữ";
    temp2.isSelected = false;

    TempGender temp3 = TempGender();
    temp3.name = "Khác";
    temp3.isSelected = false;

    listGender.add(temp1);
    listGender.add(temp2);
    listGender.add(temp3);
  }

  Widget _setupContent() {
    return Container(
      color: Colors.white,
      padding:
      EdgeInsets.only(top: Globals.maxPadding, bottom: Globals.maxPadding),
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          InkWell(
            child: Container(
              margin: EdgeInsets.only(top: 20.0, left: 20.0, bottom: 30.0),
              height: 24.0,
              width: 24.0,
              child: Image.asset(iconRightArrowBlack),
            ),
            onTap: () {
              navigatorPop(context);
            },
          ),
          Container(
            margin: EdgeInsets.only(left: 20.0, bottom: 5.0),
            child: Text(
              _language.accountInformation ?? "",
              style: TextStyle(
                  fontSize: 34.0,
                  fontFamily: fontSFPro,
                  fontWeight: FontWeight.w400),
            ),
          ),
          Container(
              margin: EdgeInsets.only(left: 20.0, bottom: 25.0),
              child: AutoSizeText(
                _language.inputFullyToComplete ?? "",
                style: TextStyle(
                    fontSize: 17.0, fontFamily: fontSFPro, color: colorHint),
              )),
          _renderInput(_lastNameController, _language.lastName ?? "", ""),
          _renderInput(_firstNameController, _language.firstName ?? "", ""),
          _renderGender(),
          _renderInput(_dobControler, _language.dob ?? "", "mm/dd/yyyy"),
          _renderInput(_addressControler, _language.permanent ?? "", ""),
        ],
      ),
    );
  }

  Widget _renderGender(){
    return Container(
      padding: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
//                margin: EdgeInsets.only(bottom: ),
                child: Text(
                  _language.gender ?? "",
                  style: TextStyle(
                      fontSize: 17.0,
                      fontFamily: fontSFPro,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Container(
//                margin: EdgeInsets.only(bottom: 10.0),
                child: Text(
                  ' *',
                  style: TextStyle(
                      color: Colors.red,
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),

          Row(
            children: listGender.map((value) => _genderRecord(value)).toList(),
          )
    ])
    );
  }

  Widget _renderInput(TextEditingController controller, String title,
      String hintText) {
    return Container(
      padding: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
//                margin: EdgeInsets.only(bottom: ),
                child: Text(
                  title,
                  style: TextStyle(
                      fontSize: 17.0,
                      fontFamily: fontSFPro,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Container(
//                margin: EdgeInsets.only(bottom: 10.0),
                child: Text(
                  ' *',
                  style: TextStyle(
                      color: Colors.red,
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
          TextField(
            controller: controller,
            style: TextStyle(
              fontSize: 14,
            ),
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              hintText: hintText,
              hintStyle: TextStyle(
                fontStyle: FontStyle.italic,
              ),
              border: InputBorder.none,
//              contentPadding: EdgeInsets.all(5.0),
              suffix: controller.text != ""
                  ? Container(
                height: 11.0,
                width: 11.0,
                child: Image.asset(
                  iconChecked,
                  width: 15.0,
                  height: 11.0,
                ),
              )
                  : Container(
                child: Text(
                  controller.text,
                ),
              ),
            ),
            onChanged: (value) {
//              if (value.trim() != "") _bloc.setTeamName(value);
            },
          ),
          controller.text != ""
              ? Container(
            height: 1,
            width: Globals.maxWidth,
            color: primaryColor,
          )
              : Container(
            height: 1,
            width: Globals.maxWidth,
            color: colorHint,
          ),
        ],
      ),
    );
  }

  Widget _renderDescription(
      TextEditingController controller, String title, String hintText) {
    return Container(
      padding: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(bottom: 10.0),
                child: Text(
                  _language.nameTeam ?? "",
                  style: TextStyle(
                      fontSize: 17.0,
                      fontFamily: fontSFPro,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 10.0),
                child: Text(
                  ' *',
                  style: TextStyle(
                      color: Colors.red,
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
          TextField(
            maxLines: 2,
            controller: controller,
            style: TextStyle(
              color: Colors.grey,
              fontSize: 14,
            ),
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              hintText: hintText,
              hintStyle: TextStyle(
                fontStyle: FontStyle.italic,
              ),
              border: InputBorder.none,
              contentPadding: EdgeInsets.all(10),
            ),
          ),
          Container(
            height: 1,
            width: Globals.maxWidth,
            color: colorHint,
          )
        ],
      ),
    );
  }


  Widget _genderRecord(TempGender item){
    return Expanded(
      flex: 1,
      child: item.isSelected ? Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(bottom: 5.0, top: 5.0),
           child: Text(
             item.name ?? "",
             style: TextStyle(fontFamily: fontSFPro, fontSize: 17.0, fontWeight: FontWeight.bold, color: primaryColor),
           ),
          ),
          Container(
            height: 1.0,
            width: Globals.maxWidth / 3,
            color: primaryColor,
          )
        ],
      ) : Center(
        child: InkWell(
          child: Container(
            child: Text(
              item.name ?? "",
              style: TextStyle(fontSize: 17.0, fontFamily: fontSFPro),
            ),
          ),
          onTap: (){
            for (var o in listGender) {
              o.isSelected = false;
            }
            item.isSelected = true;
            setState(() {

            });
          },
        ),
      ),
    );
  }

  Widget _renderButtonComplete(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(),
        InkWell(
          child: Container(
              height: 50.0,
              width: 335.0,
              margin: EdgeInsets.only(
                  left: 20.0, right: 20.0, bottom: 37.0),
              decoration: BoxDecoration(
                  color: Color(0xff0AC67F).withOpacity(0.3),
                  borderRadius: BorderRadius.circular(26.0)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    _language.complete ?? "",
                    style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                ],
              )),
          onTap: () {
          },
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
//    Globals().init(context);
    return Scaffold(
      backgroundColor: Colors.black,
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(top: Globals.maxPadding),
              height: Globals.maxHeight,
              color: Colors.white,
              child: _setupContent(),
            ),
          ),
          _renderButtonComplete()
        ],
      )
    );
  }
}
