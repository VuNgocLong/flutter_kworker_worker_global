import 'package:flutter/material.dart';
import 'package:k_user_global/common/interactions.dart';
import 'package:k_user_global/models/get_account_response_model.dart';
import 'package:k_user_global/models/list_sex_response_model.dart';
import 'package:k_user_global/resources/repository.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:rxdart/rxdart.dart';

class AccountInformationBloc{
  final _repository = Repository();
  final _streamGender = BehaviorSubject<List<SexResponseModel>>();
  Observable<List<SexResponseModel>> get outputGender => _streamGender.stream;
  setListGender(List<SexResponseModel> event) => _streamGender.sink.add(event);


  dispose(){
    _streamGender.close();
  }

 Future<List<SexResponseModel>> getListGender(BuildContext context)async{
   List<SexResponseModel> _listGender=  List<SexResponseModel>();
    ApiResponseData responseData = await _repository.getSexList(context);
    if(responseData.success){
      if(responseData.data['data']!=null){
        responseData.data['data'].forEach((v){
          _listGender.add(new SexResponseModel.fromJson(v));
        });
      }else _listGender=[];

      return _listGender;
    }
    return [];
  }

  getAccountInformation(BuildContext context)async{

    showProgressDialog(context);
    ApiResponseData response = await _repository.getAccountInformation(context);
    if(response.success){
      List<SexResponseModel> _listGender = await getListGender(context);
      hideProgressDialog();
      AccountInformationResponseModel model = AccountInformationResponseModel.fromJson(response.data['data']);
     if(_listGender.length>0){
       int indexSex = _listGender.indexOf((_listGender.firstWhere((data)=>data.id == model.sex.id)));


       _listGender.forEach((data){
         if(data.id == model.sex.id){
           data.isSelected=true;
         }
       });
       setListGender(_listGender);
     }

    }else hideProgressDialog();
  }
}