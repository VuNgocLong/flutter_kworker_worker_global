import 'package:flutter/cupertino.dart';
import 'package:k_user_global/common/interactions.dart';
import 'package:k_user_global/models/menu_profile_model.dart';
import 'package:k_user_global/resources/repository.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:rxdart/rxdart.dart';

class ManageProfileBloc {
  final _repository = Repository();

  final _streamMenu = BehaviorSubject<MenuProfileModel>();

  Observable<MenuProfileModel> get outputMenu => _streamMenu.stream;

  setMenu(MenuProfileModel event) => _streamMenu.sink.add(event);

  getProfileMenu(BuildContext context) async {
    Map<String, String> params = Map<String, String>();
    showProgressDialog(context);
    ApiResponseData responseData = await _repository.getProfileMenu(context, params);
    hideProgressDialog();
    if (responseData != null){
        if (responseData.success){
          MenuProfileModel model = MenuProfileModel.fromJson(responseData.data);
          setMenu(model);
        }
    }
  }

  dispose(){
    _streamMenu.close();
  }
}