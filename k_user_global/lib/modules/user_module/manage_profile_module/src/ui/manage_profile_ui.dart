import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/models/account_verify_response_model.dart';
import 'package:k_user_global/models/menu_profile_model.dart';
import 'package:k_user_global/modules/user_module/manage_profile_module/account_information_module/src/ui/account_information_ui.dart';
import 'package:k_user_global/modules/user_module/manage_profile_module/id_card_module/src/ui/id_card_ui.dart';
import 'package:k_user_global/modules/user_module/manage_profile_module/portrait_module/src/ui/portrail_ui.dart';
import 'package:k_user_global/modules/user_module/manage_profile_module/src/bloc/manage_profile_bloc.dart';
import 'package:k_user_global/modules/user_module/manage_team_module/testing_module/src/ui/testing_ui.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:k_user_global/widgets/widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ManageProfileScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<ManageProfileScreen> with TickerProviderStateMixin {
  SharedPreferences _prefs;
  Language _language;

  // bloc init
  ManageProfileBloc _bloc;
  AccountVerifyResponseModel _accountModel = AccountVerifyResponseModel();
  @override
  void initState() {
    super.initState();
    _bloc = ManageProfileBloc();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
        statusBarColor: greenLight, // Color for Android
        statusBarBrightness:
            Brightness.light // Dark == white status bar -- for IOS.
        ));
    _language = Language();
    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        } else {
          _language = Vietnamese();
        }

        String accountModel = _prefs.getString(keyModelAccVerify) ?? "";
        if (accountModel != null && accountModel != "") {
          _accountModel =
              AccountVerifyResponseModel.fromJson(json.decode(accountModel));
          print(json.decode(accountModel));
        }
      });
    });

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _bloc.getProfileMenu(context);
    });
  }

  Widget _setupBackground() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(34.0)),
          image: DecorationImage(
            image: AssetImage(background),
            fit: BoxFit.cover,
          )),
    );
  }

  Widget _buildContent() {
    return Container(
//      padding: EdgeInsets.fromLTRB(Globals.minPadding, 0.0,
//          Globals.minPadding, 0.0),
      margin: EdgeInsets.only(bottom: 20.0),
      padding: EdgeInsets.only(top: 20.0),
      width: Globals.maxWidth,
      height: Globals.maxHeight -
          AppBar().preferredSize.height -
          MediaQuery.of(context).padding.top,
      decoration: BoxDecoration(
        color: backgroundWhiteF2,
        borderRadius: BorderRadius.all(Radius.circular(34.0)),
      ),
      child: SingleChildScrollView(
          child: StreamBuilder(
        stream: _bloc.outputMenu,
        builder: (_, snapshot) {
          if (snapshot.hasData) {
            MenuProfileModel menu = snapshot.data;
            if (menu != null && menu.data != null) {
              return Column(
                children:
                    menu.data.map((value) => _renderCategory(value)).toList(),
              );
            } else
              return Container();
          }
          return Container();
        },
      )),
    );
  }

  Widget _progressBar() {
    return Container();
  }

  Widget _renderCategory(Data data) {
    return Container(
        padding:
            EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0, top: 20.0),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Row(
                    children: <Widget>[
                      Container(
                        height: 30.0,
                        width: 30.0,
                        child: Image.asset(iconBlueCircle),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 5.0),
                        child: AutoSizeText(
                          data.name ?? "",
                          style:
                              TextStyle(fontSize: 20.0, fontFamily: fontSFPro),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  child: Text(
                    data.status == null
                        ? _language.statusProfile3 ?? ""
                        : (data.status.status == 0
                        ? _language.statusProfile2 ?? ""
                        : data.status.status == 1
                        ? _language.statusProfile1 ?? ""
                        : data.status.status == 2
                        ? _language.statusProfile4 ?? ""
                        : _language.statusProfile5 ?? ""),
                    style: TextStyle(
                      fontSize: 13.0,
                      color: data.status == null ? colorStatus2 : (data.status.status == 1 || data.status.status == 3
                          ? colorRed
                          : data.status.status == 2 ? greenFont : colorStatus2),
                    ),
                  ),
                )
              ],
            ),
            Column(
              children:
                  data.children.map((value) => _renderItem(value)).toList(),
            )
          ],
        ));
  }

  Widget _renderItem(Children data) {
//    0 === null => chờ duyệt colorStatus2
//    1=> chưa hoàn thành colorRed
//    2=> Hoàn thành greenFont
//    3 => Từ chối colorRed
//   chưa xác minh colorStatus3

    return InkWell(
      child: Container(
        padding:
            EdgeInsets.only(left: 60.0, right: 20.0, bottom: 10.0, top: 10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              child: Text(
                data.name ?? "",
                style: TextStyle(fontSize: 17.0, fontFamily: fontSFPro),
              ),
            ),

            Container(
              child: Text(
                data.status == null
                    ? _language.statusProfile3 ?? ""
                    : data.status.status == 0
                        ? _language.statusProfile2 ?? ""
                        : data.status.status == 1
                            ? _language.statusProfile1 ?? ""
                            : data.status.status == 2
                                ? _language.statusProfile4 ?? ""
                                : _language.statusProfile5 ?? "",
                style: TextStyle(
                    fontSize: 13.0,
                    color: data.status == null ? colorStatus2 : (data.status.status == 1 || data.status.status == 3
                        ? colorRed
                        : data.status.status == 2 ? greenFont : colorStatus2),
                    fontFamily: fontSFPro),
              ),
            ),
//            (progressTye == 3 ? Container(
//              child: Text(
//                _language.statusProfile3 ?? "",
//                style: TextStyle(fontSize: 13.0, color: colorStatus3, fontFamily: fontSFPro),
//              ),
//            )  : Container())
          ],
        ),
      ),
      onTap: () async {
        if (data.id != null) {
          switchScreenMenu(context, data.id);
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.black,
        constraints: BoxConstraints.expand(),
        child: Stack(
          children: <Widget>[
            _setupBackground(),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  CustomAppBar(
                    title: _language.manageProfile ?? "",
                    onTapLeading: () => navigatorPop(context),
                  ),
                  _buildContent()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
