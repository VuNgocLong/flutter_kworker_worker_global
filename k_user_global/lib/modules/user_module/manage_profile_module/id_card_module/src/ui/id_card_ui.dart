import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class IdCardScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _State();
  }
}

class _State extends State<IdCardScreen> {
  SharedPreferences _prefs;
  Language _language;


  //init
  TextEditingController _idCardNumController = TextEditingController();
  TextEditingController _createDateController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _language = Language();

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Colors.white,// Color for Android
        statusBarBrightness: Brightness.dark // Dark == white status bar -- for IOS.
    ));

    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        } else {
          _language = Vietnamese();
        }
      });
    });

    WidgetsBinding.instance.addPostFrameCallback((_) {});
  }

  Widget _setupContent() {
    return Container(
      color: Colors.white,
      padding:
      EdgeInsets.only(top: Globals.maxPadding, bottom: Globals.maxPadding),
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          InkWell(
            child: Container(
              margin: EdgeInsets.only(top: 20.0, left: 20.0, bottom: 30.0),
              height: 24.0,
              width: 24.0,
              child: Image.asset(iconRightArrowBlack),
            ),
            onTap: () {
              navigatorPop(context);
            },
          ),
          Container(
            margin: EdgeInsets.only(left: 20.0, bottom: 5.0),
            child: Text(
              _language.yourIDCardImage ?? "",
              style: TextStyle(
                  fontSize: 34.0,
                  fontFamily: fontSFPro,
                  fontWeight: FontWeight.w400),
            ),
          ),
          Container(
              margin: EdgeInsets.only(left: 20.0, bottom: 25.0),
              child: AutoSizeText(
                _language.completeToFindJob ?? "",
                style: TextStyle(
                    fontSize: 17.0, fontFamily: fontSFPro, color: colorHint),
              )),
          _renderInput(_idCardNumController, _language.idCardNumberOrPassport ?? "", "aaaa", null),
          _renderInput(_createDateController, _language.createDate ?? "", "aaaa", null),
          _renderPickerImage(_language.confirmImage ?? "")
        ],
      ),
    );
  }

  Widget _renderButtonComplete(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(),
        InkWell(
          child: Container(
              height: 50.0,
              width: 335.0,
              margin: EdgeInsets.only(
                  left: 20.0, right: 20.0, bottom: 37.0),
              decoration: BoxDecoration(
                  color: Color(0xff0AC67F).withOpacity(0.3),
                  borderRadius: BorderRadius.circular(26.0)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    _language.complete ?? "",
                    style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                ],
              )),
          onTap: () {
          },
        )
      ],
    );
  }

  Widget _renderInput(TextEditingController controller, String title,
      String hintText, Stream streamName) {
    return Container(
      padding: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
//                margin: EdgeInsets.only(bottom: 10.0),
                child: Text(
                  title,
                  style: TextStyle(
                      fontSize: 17.0,
                      fontFamily: fontSFPro,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Container(
//                margin: EdgeInsets.only(bottom: 10.0),
                child: Text(
                  ' *',
                  style: TextStyle(
                      color: Colors.red,
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
          TextField(
            controller: controller,
            style: TextStyle(
              fontSize: 14,
            ),
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              hintText: hintText,
              hintStyle: TextStyle(
                fontStyle: FontStyle.italic,
              ),
              border: InputBorder.none,
              contentPadding: EdgeInsets.all(5.0),
              suffix: controller.text != ""
                  ? Container(
                height: 11.0,
                width: 11.0,
                child: Image.asset(
                  iconChecked,
                  width: 15.0,
                  height: 11.0,
                ),
              )
                  : Container(
                child: Text(
                  controller.text,
                ),
              ),
            ),
            onChanged: (value) {
            },
          ),
          controller.text != ""
              ? Container(
            height: 1,
            width: Globals.maxWidth,
            color: primaryColor,
          )
              : Container(
            height: 1,
            width: Globals.maxWidth,
            color: colorHint,
          ),
        ],
      ),
    );
  }

  Widget _renderPickerImage(String title){
    return  Container(
      padding: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
//                margin: EdgeInsets.only(bottom: 10.0),
                child: Text(
                  title,
                  style: TextStyle(
                      fontSize: 17.0,
                      fontFamily: fontSFPro,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Container(
//                margin: EdgeInsets.only(bottom: 10.0),
                child: Text(
                  ' *',
                  style: TextStyle(
                      color: Colors.red,
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(top: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(right: 10.0, bottom: 10.0),
                        child: Text(
                          _language.frontImage ?? "",
                          style: TextStyle(fontSize: 17.0, fontFamily: fontSFPro),
                        ),
                      ),
                      Container(
                        child: Center(
                          child: Container(
                              width: Globals.maxWidth * 0.3,
                              height: Globals.maxWidth * 0.3,
                              child: Image.asset(
                                pickerCamera,
                                fit: BoxFit.cover,
                              )
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 20.0),
                  child: Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(right: 10.0, bottom: 10.0),
                        child: Text(
                          _language.backImage ?? "",
                          style: TextStyle(fontSize: 17.0, fontFamily: fontSFPro),
                        ),
                      ),
                      Container(
                        child: Center(
                          child: Container(
                            width: Globals.maxWidth * 0.3,
                            height: Globals.maxWidth * 0.3,
                            child: Image.asset(
                              pickerCamera,
                              fit: BoxFit.cover,
                            )

                          ),
                        ),
                      ),


                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
//    Globals().init(context);
    return Scaffold(
      backgroundColor: Colors.black,
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(top: Globals.maxPadding),
              height: Globals.maxHeight,
              color: Colors.white,
              child: _setupContent(),
            ),
          ),
          _renderButtonComplete()
        ],
      ),
    );
  }
}
