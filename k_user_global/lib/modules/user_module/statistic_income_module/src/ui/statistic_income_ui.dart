import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:k_user_global/widgets/widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StatisticIncomeScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<StatisticIncomeScreen> with TickerProviderStateMixin{
  SharedPreferences _prefs;
  Language _language;

  // init
  TextEditingController _dateController = TextEditingController();


  @override
  void initState(){
    super.initState();

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
        statusBarColor: greenLight, // Color for Android
        statusBarBrightness: Brightness.light // Dark == white status bar -- for IOS.
    ));

    _language = Language();
    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        } else {
          _language = Vietnamese();
        }
      });
    });

  }


  Widget _setupBackground() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(34.0)),
          image: DecorationImage(
            image: AssetImage(background),
            fit: BoxFit.cover,
          )),
    );
  }

  Widget _buildContent() {
    return Container(
//      padding: EdgeInsets.fromLTRB(Globals.minPadding, 0.0,
//          Globals.minPadding, 0.0),
      margin: EdgeInsets.only(bottom: 20.0),
      padding: EdgeInsets.only(top: 20.0),
      width: Globals.maxWidth,
      height: Globals.maxHeight - AppBar().preferredSize.height-MediaQuery.of(context).padding.top,
      decoration: BoxDecoration(
        color: backgroundWhiteF2,
        borderRadius: BorderRadius.all(Radius.circular(34.0)),
      ),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            // row date
            Container(
              padding: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0),
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(bottom: 10.0),
                    child: Text(
                      _language.time ?? "",
                      style: TextStyle(
                          fontSize: 17.0,
                          fontFamily: fontSFPro,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  TextField(
                    controller: _dateController,
                    style: TextStyle(
                      fontSize: 14,
                    ),
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white,
                      hintText: "dd/mm/yyyy - dd/mm/yyyy",
                      hintStyle: TextStyle(
                        fontStyle: FontStyle.italic,
                      ),
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.all(5.0),
                      suffix: Container(
                        height: 11.0,
                        width: 11.0,
                        child: Image.asset(
                          iconCalendarGreen,
                          width: 15.0,
                          height: 11.0,
                        ),
                      ),
                    ),
                    onChanged: (value) {
                    },
                  ),
                  _dateController.text != ""
                      ? Container(
                    height: 1,
                    width: Globals.maxWidth,
                    color: primaryColor,
                  )
                      : Container(
                    height: 1,
                    width: Globals.maxWidth,
                    color: colorHint,
                  ),
                ],
              ),
            ),

            // row jobs

          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
//    Globals().init(context);
    return Scaffold(
      body: Container(
        color: Colors.black,
        constraints: BoxConstraints.expand(),
        child: Stack(
          children: <Widget>[
            _setupBackground(),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  CustomAppBar(
                    title:  _language.statisticIncome ?? "",
                    onTapLeading: () => navigatorPop(context),
                  ),
                  _buildContent()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}