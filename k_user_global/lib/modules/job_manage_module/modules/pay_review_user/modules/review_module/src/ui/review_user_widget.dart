import 'package:flutter/material.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/widgets/widget.dart';

class ReviewUserPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _StateReview();
  }

}

class _StateReview extends State<ReviewUserPage>{
  Widget _setupBackground() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
//          borderRadius: BorderRadius.all(Radius.circular(34.0)),
          image: DecorationImage(
            image: AssetImage(background),
            fit: BoxFit.cover,
          )),
    );
  }
  Widget _buildContent() {
    return Container(
      padding: EdgeInsets.all(Globals.minPadding),
      width: Globals.maxWidth,
      height: Globals.maxHeight -
          Globals.maxPadding * 4 -
          MediaQuery.of(context).padding.top -
          MediaQuery.of(context).padding.bottom,
      decoration: BoxDecoration(
        color: colorBackground,
        borderRadius: BorderRadius.all(Radius.circular(30.0)),
      ),
      child: Column(
//        shrinkWrap: true,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[

        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          _setupBackground(),
          Positioned(
            left: 0.0,
            top: 0.0,
            right: 0.0,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                CustomAppBar(
                  title: "Đánh giá",
                  onTapLeading: null,
                ),
                _buildContent()
              ],
            ),
          ),
        ],
      ),
    );
  }
}