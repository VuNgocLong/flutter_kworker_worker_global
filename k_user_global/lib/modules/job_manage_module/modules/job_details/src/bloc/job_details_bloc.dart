import 'package:flutter/material.dart';
import 'package:k_user_global/models/reason_model.dart';
import 'package:k_user_global/resources/repository.dart';
import 'package:rxdart/rxdart.dart';

class DetailsJobBloc{
  final _reponsitory = Repository();

  final _streamReason = BehaviorSubject<ReasonModel>();
  final _streamDetailBooking = BehaviorSubject<Map<String,dynamic>>();
  Observable<ReasonModel> get outputReason => _streamReason.stream;
  setReason(ReasonModel event) => _streamReason.sink.add(event);

  Observable<Map<String,dynamic>> get outputDetailBooking => _streamDetailBooking.stream;
  setDetailBooking(Map<String,dynamic> event) => _streamDetailBooking.sink.add(event);
  dispose(){
    _streamReason.close();
    _streamDetailBooking.close();
  }

}