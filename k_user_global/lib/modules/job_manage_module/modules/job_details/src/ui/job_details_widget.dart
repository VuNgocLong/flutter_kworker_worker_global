import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter_sms/flutter_sms.dart';
import 'package:intl/intl.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/models/reason_model.dart';
import 'package:k_user_global/modules/job_manage_module/modules/job_details/src/bloc/job_details_bloc.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:k_user_global/widgets/widget.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class JobDetailsPage extends StatefulWidget {
  final Map<String, dynamic> model;

  const JobDetailsPage({Key key, this.model}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return _StateDetails();
  }
}

class _StateDetails extends State<JobDetailsPage> with WidgetsBindingObserver {
  SharedPreferences _prefs;
  DetailsJobBloc _bloc;
  Language _language;
  TextEditingController _controllerReason = TextEditingController();
  ReasonModel _reason;
  List<ReasonModel> modelReasons = List<ReasonModel>();
  List<DropdownMenuItem<ReasonModel>> _dropdownMenuItem;
  @override
  void initState() {
    super.initState();
    _bloc = DetailsJobBloc();
    _language = Language();
    //  SystemChrome.setEnabledSystemUIOverlays([]);
    WidgetsBinding.instance.addObserver(this);
    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        }
      });
    });
    modelReasons = [
      ReasonModel(id: 1, content: "hủy việc"),
      ReasonModel(id: 2, content: "hủy thợ"),
      ReasonModel(id: 3, content: "kết thúc"),
      ReasonModel(id: 4, content: "Vắng nhà"),
      ReasonModel(id: 5, content: "hủy việc"),
      ReasonModel(id: 6, content: "hủy việc"),
    ];
    _dropdownMenuItem = buildDropdownMenuItems(modelReasons);

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _bloc.setDetailBooking(widget.model);
    });
  }

  var isKeyboardOpen = false;
  @override
  void didChangeMetrics() {
    final value = MediaQuery.of(context).viewInsets.bottom;
    if (value > 0) {
      if (isKeyboardOpen) {
        _onKeyboardChanged(false);
      }
      isKeyboardOpen = false;
    } else {
      isKeyboardOpen = true;
      _onKeyboardChanged(true);
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _bloc.dispose();
    super.dispose();
  }

  _onKeyboardChanged(bool isVisible) {
    if (!isVisible) {
//      SystemChrome.setEnabledSystemUIOverlays([]);
    }
  }

  List<DropdownMenuItem<ReasonModel>> buildDropdownMenuItems(List options) {
    List<DropdownMenuItem<ReasonModel>> items = List();
    for (ReasonModel optionItem in options) {
      items.add(
        DropdownMenuItem(
          value: optionItem,
          child: Text(
            optionItem.content,
            style: styleTextBlackBold,
          ),
        ),
      );
    }
    return items;
  }

  Widget _setupBackground() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
//          borderRadius: BorderRadius.all(Radius.circular(34.0)),
          image: DecorationImage(
        image: AssetImage(background),
        fit: BoxFit.cover,
      )),
    );
  }

  Widget _itemOrder(String title, String content) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: Globals.minPadding / 2),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            width: Globals.maxWidth * 0.2,
            child: Text(
              title,
              style: styleContent,
            ),
          ),
          Container(
            width: Globals.minPadding,
          ),
          Expanded(
            child: Text(
              content,
              textAlign: TextAlign.left,
              style: styleTextBlack,
              softWrap: true,
            ),
          )
        ],
      ),
    );
  }

  Widget _itemTime(String title, List<Map<String, dynamic>> listPart) {
    List<Map<String, dynamic>> models = [];
    listPart.forEach((data) {
      models.add(data);
    });
    return Container(
      padding: EdgeInsets.symmetric(vertical: Globals.minPadding / 2),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            width: Globals.maxWidth * 0.2,
            child: Text(
              title,
              style: styleContent,
            ),
          ),
          Container(
            width: Globals.minPadding,
          ),
          Expanded(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: models
                .asMap()
                .map((index, data) => MapEntry(
                    index,
                    Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: Globals.minPadding / 2),
                      child: Text(
                        'Ca ${index + 1} - ${timeFormat.format(findWorkerFormat.parse(data['startTime']))} - ${timeFormat.format(findWorkerFormat.parse(data['endTime']))}',
                        style: TextStyle(color: colorText),
                      ),
                    )))
                .values
                .toList(),
          ))
        ],
      ),
    );
  }

  Widget _buildContentOrder(Map<String, dynamic> model) {
    String startTime = "";
    String endTime = "";
    int group = 0;
    int workerAmount = 0;
    if ((model['workerTypeId'] == 2 || model['workerTypeId'] == 3) &&
        (model['group'] != null)) {
      model['partTeam'].forEach((data) {
        startTime = dateOrderFormat
            .format(findWorkerFormat.parse(data['startTime'] ?? ""));
        endTime = dateOrderFormat
            .format(findWorkerFormat.parse(data['endTime'] ?? ""));
        if (data['worker'] != null && data['worker'].length > 0)
          workerAmount = data['worker'].length;
        else
          workerAmount = 0;
      });
    } else {
      model['part'].forEach((data) {
        startTime = dateOrderFormat
            .format(findWorkerFormat.parse(data['startTime'] ?? ""));
        endTime = dateOrderFormat
            .format(findWorkerFormat.parse(data['startTime'] ?? ""));
      });
    }
    if (model['workerTypeId'] == 2 || model['workerTypeId'] == 3) {
      group = 1;
    } else
      group = 0;
    Map<String, dynamic> service = model['service'];

    return Container(
      //height: Globals.maxHeight * 0.4,
      padding: EdgeInsets.symmetric(
          vertical: Globals.minPadding, horizontal: Globals.minPadding),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _itemOrder(
            'Công việc:',
            service['name'] ?? "",
          ),
          _itemOrder(
            'Số lượng:',
            "${model['jobAmount'].toString()} ${service['unitName'] ?? ""}",
          ),
          _itemOrder('Địa chỉ:', model['address'] ?? ""),
          _itemOrder('Bắt đầu:', startTime),
          service['serviceType'] == 1
              ? _itemTime('${_language.time ?? ''}:', model['partTeam'])
              : Container(),
//          (_statusService['id'] == 6||_statusService['id'] == 7)
//              ? _itemOrder('${_language.end ?? ''}:', endTime ?? "")
//              : Container(),
          (model['group'] != null)
              ? _itemOrder('Số thợ:', workerAmount.toString())
              : Container(),
          _itemOrder('Ghi chú:', 'test'),
//          Expanded(
//            child: Container(),
//          )
        ],
      ),
    );
  }

  Widget _buildContent() {
    //    {   "id": 1,   "name": "Đang chờ xác nhận"
//    "id": 2, "name": "Đang di chuyển"
//       "id": 3,"name": "Đã đến nơi"
//        "id": 4, "name": "Đang chờ làm"
//         "id": 5,"name": "Đang làm"
//        "id": 6, "name": "Chờ thanh toán"
//        "id": 7, "name": ""
//         "id": 8,"name": "Hoàn thành"
//        "id": 9" "name": "Huỷ"
    return StreamBuilder(
        stream: _bloc.outputDetailBooking,
        initialData: null,
        builder: (_, snapshot) {
          if (snapshot.data == null) return Container();
          Map<String, dynamic> models = snapshot.data;
          Map<String, dynamic> _statusService = models['status'];
          return Container(
//      padding: EdgeInsets.fromLTRB(Globals.minPadding, Globals.maxPadding,
//          Globals.minPadding, Globals.minPadding),
            width: Globals.maxWidth,
            height: Globals.maxHeight -
                Globals.maxPadding * 4 -
                MediaQuery.of(context).padding.top -
                MediaQuery.of(context).padding.bottom,
            decoration: BoxDecoration(
              color: colorBackground,
              borderRadius: BorderRadius.all(Radius.circular(34.0)),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Expanded(
                  child: Column(
                    children: <Widget>[
                      models['workerTypeId'] == 2 || models['workerTypeId'] == 3
                          ? _buildHeader(models['group'], 1)
                          : _buildHeader(models['worker'], 0),
                      Container(
                        height: Globals.minPadding / 2,
                      ),
                      CustomLine(),
                      Expanded(child: _buildContentOrder(models)),
//                Expanded(
//                  child: Container(),
//                ),
                      CustomLine(),
                      _rowButton(models)
                    ],
                  ),
                ),
                Container(
                  height: Globals.minPadding,
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    children: <Widget>[
                      (_statusService['id'] == 1 ||
                              _statusService['id'] == 2 ||
                              _statusService['id'] == 3 ||
                              _statusService['id'] == 4)
                          ? Column(
                              children: <Widget>[
                                Container(

                                  padding: EdgeInsets.symmetric(
                                      horizontal: Globals.maxPadding),
                                  width: Globals.maxWidth,
                                  child: CustomButton(
                                    text: _statusService['id'] == 1
                                        ? 'Di chuyển'
                                        : _statusService['id'] == 2
                                            ? "Đã đến nơi"
                                            : _statusService['id'] == 3
                                                ? "Nhận việc"
                                                :  _language.stringCompleted??"",
                                    onTap: () {},
                                    colorBorder: _statusService['id'] == 1
                                        ? colorWaiting
                                        : _statusService['id'] == 2
                                            ? colorMoving
                                            : _statusService['id'] == 3
                                                ? colorWaitingToDo
                                                : colorWorking,
                                    colorBackground: _statusService['id'] == 1
                                        ? colorWaiting
                                        : _statusService['id'] == 2
                                            ? colorMoving
                                            : _statusService['id'] == 3
                                                ? colorWaitingToDo
                                                : colorWorking,
                                    textStyle: styleTitleWhiteBold,
                                    radius: 25,
                                    isMaxWidth: true,
                                  ),
                                ),
                                Container(
                                  height: Globals.maxPadding,
                                ),
                                InkWell(
                                  child: Text(
                                    "Hủy việc",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.normal,
                                      color: colorCancelJob,
                                      letterSpacing: -0.24,
                                    ),
                                  ),
                                  onTap: () {
                                    if (models['worker'] != null)
                                      showCustomPopupDialog(
                                          context,
                                          _titleReason(
                                              models['worker']['name'] ?? ""),
                                          _buildReason(),
                                          isExpanded: true);
                                    else
                                      showCustomPopupDialog(
                                          context,
                                          _titleReason(
                                              models['group']['name'] ?? ""),
                                          _buildReason(),
                                          isExpanded: true);
                                  },
                                ),
                                Container(
                                  height: Globals.maxPadding,
                                ),
                              ],
                            )
                          : _statusService['id'] == 6
                              ? Expanded(
                                  child: Container(
                                    padding: EdgeInsets.symmetric(
                                        vertical: Globals.maxPadding,
                                        horizontal: Globals.maxPadding),
                                    child: CustomButton(
                                      text: _statusService['name'],
                                      isMaxWidth: true,
                                      radius: 25,
                                      textStyle: styleTitleWhiteBold,
                                      colorBackground: colorWorking,
                                      colorBorder: colorWorking,
                                      onTap: () {},
                                    ),
                                  ),
                                )
                              : Expanded(
                                  child: Container(
                                    padding: EdgeInsets.symmetric(
                                        vertical: Globals.maxPadding),
                                    decoration: BoxDecoration(
                                        color: colorWorking,
                                        borderRadius: BorderRadius.only(
                                            bottomRight: Radius.circular(34.0),
                                            bottomLeft: Radius.circular(34.0))),
                                    child: Center(
                                      child: Text(
                                        _statusService['name'],
                                        style: styleTitleWhiteBold,
                                      ),
                                    ),
                                  ),
                                ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  Widget _itemButton(String icon, String title, Function onTap) {
    return InkWell(
      onTap: onTap,
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ImageIcon(
              AssetImage(icon),
              size: 20.0,
              color: icon == iconMoneyBlack ? colorButton : Colors.black,
            ),
            Container(
              width: Globals.minPadding / 2,
            ),
            Text(
              title,
              style: TextStyle(
                  fontSize: 11,
                  color: icon == iconMoneyBlack ? Colors.black : colorDes,
                  fontWeight: icon == iconMoneyBlack
                      ? FontWeight.bold
                      : FontWeight.normal),
            )
          ],
        ),
      ),
    );
  }

  Widget _rowButton(Map<String, dynamic> model) {
    return Container(
      //height: 40.0,
      margin: EdgeInsets.only(top: Globals.minPadding),
      alignment: Alignment.center,
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                flex: 1,
                child: _itemButton(iconMoneyBlack, "Tiền mặt", () {
//                  navigatorPush(context, PaymentScreen(null));
                }),
              ),
              CustomLine(
                isVertical: false,
                size: 1,
              ),
              Expanded(
                flex: 1,
                child: _itemButton(iconTag, 'Khuyến mãi', () {
//                  navigatorPush(context, PromotionPage());
                }),
              ),
              CustomLine(
                isVertical: false,
              ),
              Expanded(
                flex: 1,
                child: _itemButton(
                    iconCoin,
'Tip',
//                    model.tip != 0
//                        ? '${NumberFormat("#,###").format(model.tip)} VNĐ'
//                        : _language.tip ?? "",
                    () {
//                    showCustomBottomDialog(context, _buildTip());
                }
                ),
              ),
            ],
          ),
          Container(
            height: Globals.minPadding,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'VNĐ',
                style: TextStyle(
                    fontSize: sizeText,
                    color: Colors.black,
                    fontWeight: FontWeight.normal),
              ),
              Container(
                width: Globals.minPadding,
              ),
              Text(
                NumberFormat("#,###").format(200000),
                style: TextStyle(
                    fontSize: 17,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
//          Container(
//            height: Globals.minPadding,
//          ),
        ],
      ),
    );
  }

  Widget _titleReason(String name) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            'Có phải bạn muốn hủy ',
            style: TextStyle(
              fontSize: 17,
              color: Colors.black,
              fontWeight: FontWeight.normal,
              letterSpacing: -0.408,
            ),
            softWrap: true,
            textAlign: TextAlign.center,
          ),
          Text(
            name,
            style: TextStyle(
              fontSize: 17,
              color: colorText,
              fontWeight: FontWeight.bold,
              letterSpacing: -0.408,
            ),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }

  Widget _buildReason() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Container(
          height: Globals.minPadding,
        ),
        (_dropdownMenuItem == null || _dropdownMenuItem.length == 0)
            ? Container()
            : StreamBuilder(
                stream: _bloc.outputReason,
                initialData: _reason,
                builder: (context, snapshot) {
                  _reason = snapshot.data;
//                  print(_reason);
                  return Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                            height: 40.0,
                            //width:,
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8.0)),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black.withOpacity(0.2),
                                    blurRadius:
                                        10.0, // has the effect of softening the shadow
//            spreadRadius:
//            15.0, // has the effect of extending the shadow
                                    offset: Offset(
                                      0.06, // horizontal, move right 10
                                      0.6,
                                      // vertical, move down 10
                                    ),
                                  )
                                ]),
                            padding: EdgeInsets.symmetric(
                                horizontal: Globals.minPadding),
                            child: DropdownButton(
                              iconEnabledColor: colorButton,
                              hint: Text("Lý do huỷ việc"),
                              underline: Container(
                                height: 1.0,
                                decoration: const BoxDecoration(
                                    border: Border(
                                        bottom: BorderSide(
                                            color: Colors.transparent,
                                            width: 0.0))),
                              ),
                              isDense: false,
                              isExpanded: true,
                              value: _reason,
                              items: _dropdownMenuItem,
                              onChanged: (event) => _bloc.setReason(event),
                            )),
                      ),
                    ],
                  );
                },
              ),
        Container(
          height: Globals.maxPadding,
        ),
        Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.2),
                blurRadius: 10.0, // has the effect of softening the shadow
//            spreadRadius:
//            15.0, // has the effect of extending the shadow
                offset: Offset(
                  0.06, // horizontal, move right 10
                  0.6,
                  // vertical, move down 10
                ),
              )
            ],
          ),
          child: TextField(
            maxLines: 4,
            controller: _controllerReason,
            textInputAction: TextInputAction.done,
            decoration: InputDecoration(
              hintText: "Lý do khác",
              contentPadding: EdgeInsets.symmetric(
                  vertical: Globals.minPadding, horizontal: Globals.minPadding),
              border: InputBorder.none,
            ),
          ),
        ),
        Container(
          height: Globals.maxPadding,
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Row(
            children: <Widget>[
              Expanded(
                  child: CustomButton(
                text: _language.cancel ?? "",
                onTap: () => navigatorPop(context),
                textStyle: styleTitleWhiteBold,
                colorBackground: buttonCancel,
                colorBorder: buttonCancel,
                isMaxWidth: true,
                radius: 26,
              )),
              Container(
                width: Globals.maxPadding,
              ),
              Expanded(
                  child: CustomButton(
                text: _language.agree ?? "",
                onTap: () async {
                  navigatorPop(context);
                  showCancelSuccessDialog(
                      context, iconSuccess, "Huỷ việc thành công!");
                  await Future.delayed(Duration(seconds: 1));
                  navigatorPop(context);
                },
                textStyle: styleTitleWhiteBold,
                colorBackground: colorButton,
                colorBorder: colorButton,
                isMaxWidth: true,
                radius: 26,
              )),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildHeader(Map<String, dynamic> model, int group) {
    if (model == null) return Container();
    List<Map<String, dynamic>> listJobs = [];

    List<String> jobs = [];
    if (model['service'].length > 0) {
      listJobs.clear();
      model['service'].forEach((data) {
        listJobs.add(data);
      });
    }
    if (listJobs.length > 0) {
      listJobs.forEach((v) {
        jobs.add(v['name']);
      });
    }
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            Container(
              child: model["avatar"] == null
                  ? Container(
                      height: Globals.maxHeight * 0.15,
                      width: Globals.maxHeight * 0.15,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: AssetImage(defaultAvatar),
                              fit: BoxFit.cover)),
                    )
                  : Container(
                      //margin: EdgeInsets.all(10.0),
                      height: Globals.maxHeight * 0.12,
                      width: Globals.maxHeight * 0.12,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10000000.0),
                        child: CachedNetworkImage(
                          fit: BoxFit.cover,
                          imageUrl: model["avatar"],
                          placeholder: (context, url) => Transform(
                              alignment: FractionalOffset.center,
                              transform: Matrix4.identity()..scale(0.5, 0.5),
                              child: CupertinoActivityIndicator()),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                        ),
                      ),
                    ),
            ),
            group == 1
                ? Container(
                    margin: EdgeInsets.only(
                      left: Globals.maxPadding *
                          2.8, //bottom: Globals.minPadding/4
                    ),
                    height: 23,
                    decoration: BoxDecoration(
                        color: colorButton, shape: BoxShape.circle),
                    child: Center(
                      child: ImageIcon(
                        AssetImage(iconGroup),
                        color: Colors.white,
                        size: 12,
                      ),
                    ),
                  )
                : Container()
          ],
        ),
        Container(
          height: Globals.minPadding / 2,
        ),
        Text(
          model["name"],
          softWrap: true,
          textAlign: TextAlign.center,
          style: styleTextBlackBold,
        ),
        Container(
          height: Globals.minPadding / 2,
        ),
        Container(
          width: Globals.maxWidth * 0.5,
          child: Row(
            children: <Widget>[
              jobs.length != 0
                  ? Expanded(
                      child: Text(
                        jobs.reduce(
                                (value, element) => value + ', ' + element) +
                            '...',
                        //  minFontSize: 4.0,
                        maxLines: 1,
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: colorHint,
                            fontWeight: FontWeight.normal,
                            fontSize: sizeText),
                      ),
                    )
                  : Container()
            ],
          ),
        ),
        Container(
          height: Globals.minPadding / 2,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            InkWell(
//                onTap: () => FlutterSms.sendSMS(
//                    message: "", recipients: [("+" + model.workerPhone ?? "")]),
                child: Container(
                    height: Globals.maxWidth * 0.15,
                    width: Globals.maxWidth * 0.15,
                    child: Image.asset(
                      iconMes,
                      height: Globals.maxWidth * 0.15,
                      width: Globals.maxWidth * 0.15,
                    ))
//              Container(
//                height: 52,
//                width: 52,
//                decoration: BoxDecoration(
//                    shape: BoxShape.circle,
//                    image: DecorationImage(
//                        image: AssetImage(iconMes), fit: BoxFit.cover)),
//              ),
                ),
            Container(
              width: Globals.minPadding,
            ),
            InkWell(
//              onTap: () => launch("tel://" + ("+" + model.workerPhone ?? "")),
              child: Container(
                  height: Globals.maxWidth * 0.15,
                  width: Globals.maxWidth * 0.15,
                  child: Image.asset(
                    iconPhone,
                    fit: BoxFit.cover,
                  )),
//              Container(
//                height: 52,
//                width: 52,
//                decoration: BoxDecoration(
//                    shape: BoxShape.circle,
//                    image: DecorationImage(
//                        image: AssetImage(iconPhone), fit: BoxFit.cover)),
//              ),
            ),
          ],
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          _setupBackground(),
          Positioned(
            left: 0.0,
            top: 0.0,
            right: 0.0,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                CustomAppBar(
                  title: "Chi tiết công việc",
                  onTapLeading: () => navigatorPop(context),
                ),
                _buildContent()
              ],
            ),
          ),
        ],
      ),
    );
  }
}
