import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_sms/flutter_sms.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/models/reason_model.dart';
import 'package:k_user_global/modules/job_manage_module/modules/job_details/src/ui/job_details_widget.dart';
import 'package:k_user_global/modules/job_manage_module/src/bloc/job_manage_bloc.dart';
import 'package:k_user_global/ultilites/custom_calendar.dart';
import 'package:k_user_global/ultilites/hex_color.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:k_user_global/widgets/widget.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:quiver/async.dart';

class JobManagementPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _StateJob();
  }
}

class _StateJob extends State<JobManagementPage>
    with WidgetsBindingObserver, TickerProviderStateMixin {
  JobManageBloc _bloc;
  TabController _controller;
  SharedPreferences _prefs;
  Language _language;
  FocusNode _focusReason = FocusNode();
  TextEditingController _controllerReason = TextEditingController();
  ReasonModel _reason;
  List<ReasonModel> modelReasons = List<ReasonModel>();
  List<DropdownMenuItem<ReasonModel>> _dropdownMenuItem;
  GoogleMapController _mapController;
  double _zoom = 15.0;
  Color selectedDateStyleColor;
  Color selectedSingleDateDecorationColor;
  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addObserver(this);
    _bloc = JobManageBloc();
    _language = Language();
    _controller = TabController(length: 2, vsync: this);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
        statusBarColor: greenLight, // Color for Android
        statusBarBrightness:
            Brightness.dark // Dark == white status bar -- for IOS.
        ));
    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        }
      });
    });
    modelReasons = [
      ReasonModel(id: 1, content: "hủy việc"),
      ReasonModel(id: 2, content: "hủy thợ"),
      ReasonModel(id: 3, content: "kết thúc"),
      ReasonModel(id: 4, content: "Vắng nhà"),
      ReasonModel(id: 5, content: "hủy việc"),
      ReasonModel(id: 6, content: "hủy việc"),
    ];
    _dropdownMenuItem = buildDropdownMenuItems(modelReasons);
    //_reason =_dropdownMenuItem[0].value;

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _bloc.setModelTab1([
        {
          "service": {
            "name": "test",
            "id": 1,
            "serviceType": 1,
            "unitName": "Bộ"
          },
          "status": {
            "name": "Đang chờ",
            "id": 1,
          },
          "workerTypeId": 2,
          "jobAmount": 2,
          "address": "test",
          "group": {
            "avatar": "",
            "name": "test",
            "worker": null,
            "service": [
              {
                "name": "Thợ Cắt Tóc",
                "id": 1,
              },
              {
                "name": "Thợ Cắt Tóc",
                "id": 2,
              },
              {
                "name": "Thợ Cắt Tóc",
                "id": 3,
              },
            ]
          },
          "worker": null,
          "partTeam": [
            {
              "id": 1,
              "workerAmount": 2,
              "startTime": "2020-02-02 18:40:00",
              "endTime": "2020-02-02 18:40:00",
              "status": {
                "name": "Đang chờ",
                "id": 1,
              },
              "worker": [
                {
                  "id": 1,
                  "avatar": "",
                  "name": "test1",
                },
                {
                  "id": 2,
                  "avatar": "",
                  "name": "test2",
                },
              ],
            },
            {
              "id": 2,
              "workerAmount": 2,
              "startTime": "2020-02-03 17:40:00",
              "endTime": "2020-02-03 18:40:00",
              "status": {
                "name": "Đang chờ",
                "id": 1,
              },
              "worker": [
                {
                  "id": 1,
                  "avatar": "",
                  "name": "test1",
                },
                {
                  "id": 2,
                  "avatar": "",
                  "name": "test2",
                },
              ],
            }
          ],
          "part": null
        },
        {
          "service": {
            "name": "lele",
            "id": 2,
            "serviceType": 2,
            "unitName": "Giờ"
          },
          "status": {
            "name": "Đang chờ",
            "id": 1,
          },
          "workerTypeId": 1,
          "jobAmount": 2,
          "address": "test",
          "worker": {
            "avatar": "",
            "name": "test",
            "service": [
              {
                "name": "Thợ Cắt Tóc",
                "id": 1,
              },
              {
                "name": "Thợ Cắt Tóc",
                "id": 2,
              },
              {
                "name": "Thợ Cắt Tóc",
                "id": 3,
              },
            ]
          },
          "group": null,
          "part": [
            {
              "id": 1,
              "workerAmount": 2,
              "startTime": "2020-02-02 18:40:00",
              "endTime": "2020-02-02 18:40:00",
              "status": {
                "name": "Đang chờ",
                "id": 1,
              },
            },
          ],
          "partTeam": null
        }
      ]);
//      _bloc.getListBooking(context);
    });
  }

  var isKeyboardOpen = false;
  @override
  void didChangeMetrics() {
    final value = MediaQuery.of(context).viewInsets.bottom;
    if (value > 0) {
      if (isKeyboardOpen) {
        _onKeyboardChanged(false);
      }
      isKeyboardOpen = false;
    } else {
      isKeyboardOpen = true;
      _onKeyboardChanged(true);
    }
  }

  _onKeyboardChanged(bool isVisible) {
    if (!isVisible) {
//      SystemChrome.setEnabledSystemUIOverlays([]);
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _bloc.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    selectedDateStyleColor = Colors.white;
    selectedSingleDateDecorationColor = Colors.green;
  }

  List<DropdownMenuItem<ReasonModel>> buildDropdownMenuItems(List options) {
    List<DropdownMenuItem<ReasonModel>> items = List();
    for (ReasonModel optionItem in options) {
      items.add(
        DropdownMenuItem(
          value: optionItem,
          child: Text(
            optionItem.content,
            style: styleTextBlackBold,
          ),
        ),
      );
    }
    return items;
  }

  Widget _setupBackground() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
//          borderRadius: BorderRadius.all(Radius.circular(34.0)),
          image: DecorationImage(
        image: AssetImage(background),
        fit: BoxFit.cover,
      )),
    );
  }

  Widget _titleReason(String name) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            'Có phải bạn muốn hủy ',
            style: TextStyle(
              fontSize: 17,
              color: Colors.black,
              fontWeight: FontWeight.normal,
              letterSpacing: -0.408,
            ),
            softWrap: true,
            textAlign: TextAlign.center,
          ),
          Text(
            name,
            style: TextStyle(
              fontSize: 17,
              color: colorText,
              fontWeight: FontWeight.bold,
              letterSpacing: -0.408,
            ),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }

  Widget _buildReason() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Container(
          height: Globals.minPadding,
        ),
        (_dropdownMenuItem == null || _dropdownMenuItem.length == 0)
            ? Container()
            : StreamBuilder(
                stream: _bloc.outputReason,
                initialData: _reason,
                builder: (context, snapshot) {
                  _reason = snapshot.data;
                  print(_reason);
                  return Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                            height: 40.0,
                            //width:,
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8.0)),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black.withOpacity(0.2),
                                    blurRadius:
                                        10.0, // has the effect of softening the shadow
//            spreadRadius:
//            15.0, // has the effect of extending the shadow
                                    offset: Offset(
                                      0.06, // horizontal, move right 10
                                      0.6,
                                      // vertical, move down 10
                                    ),
                                  )
                                ]),
                            padding: EdgeInsets.symmetric(
                                horizontal: Globals.minPadding),
                            child: DropdownButton(
                              iconEnabledColor: colorButton,
                              hint: Text("Lý do huỷ việc"),
                              underline: Container(
                                height: 1.0,
                                decoration: const BoxDecoration(
                                    border: Border(
                                        bottom: BorderSide(
                                            color: Colors.transparent,
                                            width: 0.0))),
                              ),
                              isDense: false,
                              isExpanded: true,
                              value: _reason,
                              items: _dropdownMenuItem,
                              onChanged: (event) => _bloc.setReason(event),
                            )),
                      ),
                    ],
                  );
                },
              ),
        Container(
          height: Globals.maxPadding,
        ),
        Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.2),
                blurRadius: 10.0, // has the effect of softening the shadow
//            spreadRadius:
//            15.0, // has the effect of extending the shadow
                offset: Offset(
                  0.06, // horizontal, move right 10
                  0.6,
                  // vertical, move down 10
                ),
              )
            ],
          ),
          child: TextField(
            maxLines: 4,
            controller: _controllerReason,
            textInputAction: TextInputAction.done,
            decoration: InputDecoration(
              hintText: "Lý do khác",
              contentPadding: EdgeInsets.symmetric(
                  vertical: Globals.minPadding, horizontal: Globals.minPadding),
              border: InputBorder.none,
            ),
          ),
        ),
        Container(
          height: Globals.maxPadding,
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Row(
            children: <Widget>[
              Expanded(
                  child: CustomButton(
                text: _language.cancel ?? "",
                onTap: () => navigatorPop(context),
                textStyle: styleTitleWhiteBold,
                colorBackground: buttonCancel,
                colorBorder: buttonCancel,
                isMaxWidth: true,
                radius: 26,
              )),
              Container(
                width: Globals.maxPadding,
              ),
              Expanded(
                  child: CustomButton(
                text: _language.agree ?? "",
                onTap: () async {
                  navigatorPop(context);
                  showCancelSuccessDialog(
                      context, iconSuccess, "Huỷ việc thành công!");
                  await Future.delayed(Duration(seconds: 1));
                  navigatorPop(context);
                },
                textStyle: styleTitleWhiteBold,
                colorBackground: colorButton,
                colorBorder: colorButton,
                isMaxWidth: true,
                radius: 26,
              )),
            ],
          ),
        ),
      ],
    );
  }

  Widget _itemHeaderWorkerProductivity(Map<String, dynamic> model, int group) {
    if (model == null) return Container();
    List<Map<String, dynamic>> listJobs = [];

    List<String> jobs = [];
    if (model['service'].length > 0) {
      listJobs.clear();
      model['service'].forEach((data) {
        listJobs.add(data);
      });
    }
    if (listJobs.length > 0) {
      listJobs.forEach((v) {
        jobs.add(v['name']);
      });
    }
    return Container(
      padding: EdgeInsets.only(bottom: Globals.minPadding),
      margin: EdgeInsets.only(bottom: Globals.minPadding),
      decoration: BoxDecoration(
        border: Border(bottom: BorderSide(color: colorLine, width: 1)),
        color: Colors.white,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          model["avatar"] != null
              ? Container(
                  height: Globals.maxWidth * 0.15,
                  width: Globals.maxWidth * 0.15,
                  //  margin: EdgeInsets.all(10.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10000000.0),
                    child: CachedNetworkImage(
                      fit: BoxFit.cover,
                      imageUrl: model["avatar"],
                      placeholder: (context, url) => Transform(
                          alignment: FractionalOffset.center,
                          transform: Matrix4.identity()..scale(0.35, 0.35),
                          child: CupertinoActivityIndicator()),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                  ),
                )
              : Container(
                  height: Globals.maxWidth * 0.15,
                  width: Globals.maxWidth * 0.15,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: AssetImage(defaultAvatar), fit: BoxFit.cover)),
                ),
          SizedBox(
            width: 10.0,
          ),
          Expanded(
            flex: 2,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      //width: MediaQuery.of(context).size.width * 1 / 3,
                      child: Text(
                        model["name"] ?? "",
                        softWrap: true,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: styleTextBlackBold,
                      ),
                    ),
                    Container(
                      width: Globals.minPadding,
                    ),
                  ],
                ),
                SizedBox(
                  height: 5.0,
                ),
                Container(
                  // width: MediaQuery.of(context).size.width * 0.45,
                  child: Row(
                    children: <Widget>[
                      jobs.length != 0
                          ? Expanded(
                              child: Text(
                                jobs.reduce((value, element) =>
                                        value + ', ' + element) +
                                    '...',
                                //  minFontSize: 4.0,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    color: colorHint,
                                    fontWeight: FontWeight.normal,
                                    fontSize: sizeText),
                              ),
                            )
                          : Container()
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: Globals.minPadding,
          ),
          Expanded(
            flex: 1,
            child: Container(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  InkWell(
//                    onTap: () => FlutterSms.sendSMS(
//                        message: "",
//                        recipients: [("+" + model.workerPhone ?? "")]),
                    child: Container(
//                      height: 21,
                      width: 25,
                      child: Image.asset(iconMsg, fit: BoxFit.cover),
                    ),
                  ),
                  Container(
                    width: Globals.minPadding,
                  ),
                  InkWell(
//                    onTap: () =>
//                        launch("tel://" + ("+" + model.workerPhone ?? "")),
                    child: Container(
                      width: 25,
                      child: Image.asset(phone, fit: BoxFit.cover),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _itemPartTeam(Map<String, dynamic> model, int index) {
    List<Map<String, dynamic>> listWorker = [];
    if (model['worker'] != null) {
      model['worker'].forEach((v) {
        listWorker.add(v);
      });
    } else
      listWorker = [];
    Map<String, dynamic> _statusService = model['status'];
    return Container(
      width: Globals.maxWidth,
//      margin: EdgeInsets.symmetric(vertical: Globals.minPadding),
      padding: EdgeInsets.symmetric(
          vertical: Globals.minPadding, horizontal: Globals.minPadding),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          color: HexColor("59E7E7E7")),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(
            "Ca ${index + 1}",
            style: TextStyle(
                fontSize: 17, color: Colors.black, fontWeight: FontWeight.bold),
          ),
          listWorker.length > 0
              ? Padding(
                  padding: EdgeInsets.only(top: Globals.minPadding),
                  child: Text('${listWorker.length} thợ',
                      style: TextStyle(
                          color: HexColor("3C3C43").withOpacity(0.6),
                          fontSize: 13)),
                )
              : Container(),
          Padding(
            padding: EdgeInsets.only(top: Globals.minPadding,bottom: Globals.minPadding),
            child: Text(
                '${timeFormat.format(findWorkerFormat.parse(model['startTime']))} - ${timeFormat.format(findWorkerFormat.parse(model['endTime']))}',
                style: TextStyle(
                    color: HexColor("3C3C43").withOpacity(0.6), fontSize: 13)),
          ),
          CustomButton(
            text: _statusService['id'] == 1
                ? 'Di chuyển'
                : _statusService['id'] == 2
                ? "Đã đến nơi"
                : _statusService['id'] == 3
                ? _language.confirmJob??""
                : _language.stringCompleted??"",
            onTap: () {},
            colorBorder: _statusService['id'] == 1
                ? colorWaiting
                : _statusService['id'] == 2
                ? colorMoving
                : _statusService['id'] == 3
                ? colorWaitingToDo
                : colorWorking,
            colorBackground:  _statusService['id'] == 1
                ? colorWaiting
                : _statusService['id'] == 2
                ? colorMoving
                : _statusService['id'] == 3
                ? colorWaitingToDo
                : colorWorking,
            textStyle: styleTitleWhiteBold,
            radius: 25,
            isMaxWidth: true,
          ),
        ],
      ),
    );
  }

  Widget _itemPartPersonal(Map<String, dynamic> model, int index) {
    Map<String, dynamic> _statusService = model['status'];
    return Container(
      width: Globals.maxWidth,
//      margin: EdgeInsets.symmetric(vertical: Globals.minPadding),
      padding: EdgeInsets.symmetric(
          vertical: Globals.minPadding, horizontal: Globals.minPadding),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          color: HexColor("59E7E7E7")),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(bottom: Globals.minPadding),
            child: Text(
              "Ca ${index + 1}",
              style: TextStyle(
                  fontSize: 17,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Text(
              '${timeFormat.format(findWorkerFormat.parse(model['startTime']))} - ${timeFormat.format(findWorkerFormat.parse(model['endTime']))}',
              style: TextStyle(
                  color: HexColor("3C3C43").withOpacity(0.6), fontSize: 13)),
          CustomButton(
            text: _statusService['id'] == 1
                ? 'Di chuyển'
                : _statusService['id'] == 2
                ? "Đã đến nơi"
                : _statusService['id'] == 3
                ? "Nhận việc"
                : _language.stringCompleted??"",
            onTap: () {},
            colorBorder: _statusService['id'] == 1
                ? colorWaiting
                : _statusService['id'] == 2
                ? colorMoving
                : _statusService['id'] == 3
                ? colorWaitingToDo
                : colorWorking,
            colorBackground:  _statusService['id'] == 1
                ? colorWaiting
                : _statusService['id'] == 2
                ? colorMoving
                : _statusService['id'] == 3
                ? colorWaitingToDo
                : colorWorking,
            textStyle: styleTitleWhiteBold,
            radius: 25,
            isMaxWidth: true,
          ),
        ],
      ),
    );
  }

  Widget _itemPart(Map<String, dynamic> model, int statusService, int index) {
    return Column(
      children: <Widget>[
        (statusService != 1 && statusService != 6)
            ? _buttonStatusJob(
                model['status']['name'] ?? "", model['status']['id'])
            : Container(),
        model["workerTypeId"] == 1
            ? _itemPartPersonal(model, index)
            : _itemPartTeam(model, index),
        Container(
          height: Globals.minPadding,
        )
      ],
    );
  }

  Widget _buildItemWorkerPart(Map<String, dynamic> model) {
    List<Map<String, dynamic>> models = [];
    Map<String, dynamic> _statusService = model['status'];
    if (model['workerTypeId'] == 1) {
      models.clear();
      model['part'].forEach((data) {
        models.add(data);
      });
    } else {
      models.clear();
      model['partTeam'].forEach((data) {
        models.add(data);
      });
    }
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.2),
            blurRadius: 10.0, // has the effect of extending the shadow
            offset: Offset(
              0.06, // horizontal, move right 10
              0.06,
            ),
          )
        ],
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(8.0),
            bottomLeft: Radius.circular(8.0),
            bottomRight: Radius.circular(8.0)),
      ),
      padding: EdgeInsets.symmetric(
          vertical: Globals.maxPadding, horizontal: Globals.maxPadding),
      margin: EdgeInsets.only(bottom: Globals.maxPadding),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          model['workerTypeId'] == 2 || model['workerTypeId'] == 3
              ? _itemHeaderWorkerPart(model, 1)
              : _itemHeaderWorkerPart(model, 0),
          Column(
            children: <Widget>[
              ListView.builder(
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
//                separatorBuilder: (_, index) {
//                  return Container(
//                    height: 1.0,
//                    color:colorBackground,
//                  );
//                },
                itemCount: models.length,
                itemBuilder: (_, int index) {
                  return _itemPart(models[index], _statusService['id'], index);
                },
              )
            ],

//            model['workerTypeId'] == 1
//                ? model['part']
//                    .map((index, data) => MapEntry(
//                        index,
//                        Column(
//                          children: <Widget>[
//                            (_statusService['id'] != 1 && _statusService['id'] != 6)
//                                ? _buttonStatusJob(
//                                    data['status']['name'] ?? "", data['status']['id'])
//                                : Container(),
//                            _itemPartPersonal(data, index),
//                            Container(
//                              height: Globals.minPadding,
//                            )
//                          ],
//                        )))
//                    .values
//                    .toList()
//                : model['partTeam']
//                    .map((index, data) => MapEntry(
//                        index,
//                        Column(
//                          children: <Widget>[
//                            (_statusService['id'] != 1&& _statusService['id'] != 6)
//                                ? _buttonStatusJob(
//                                data['status']['name'] ?? "", data['status']['id'])
//                                : Container(),
//                            _itemPartTeam(data, index),
//                            Container(
//                              height: Globals.minPadding,
//                            )
//                          ],
//                        )))
//                    .values
//                    .toList(),
          ),
          (_statusService['id'] == 1 ||
                  _statusService['id'] == 2 ||
                  _statusService['id'] == 3 ||
                  _statusService['id'] == 4)
              ? Column(
                  children: <Widget>[
                    Container(
                      height: Globals.minPadding,
                    ),
                    CustomLine(),
                    Container(
                      height: Globals.maxPadding,
                    ),
                    InkWell(
                      child: Text(
                        'Hủy việc',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.normal,
                          color: colorCancelJob,
                          letterSpacing: -0.24,
                        ),
                      ),
                      onTap: () {
                        if (model['worker'] != null) {
                          showCustomPopupDialog(
                              context,
                              _titleReason(model['worker']['name'] ?? ""),
                              _buildReason(),
                              isExpanded: true);
                        } else
                          showCustomPopupDialog(
                              context,
                              _titleReason(model['group']['name'] ?? ""),
                              _buildReason(),
                              isExpanded: true);
                      },
                    )
                  ],
                )
              : _statusService['id'] == 6
                  ? CustomButton(
                      text: 'Chờ Thanh Toán',
                      onTap: () {},
                      colorBorder: colorWaitingToPay,
                      colorBackground: colorWaitingToPay,
                      textStyle: styleTitleWhiteBold,
                      radius: 25,
                      isMaxWidth: true,
                    )
                  : Container(
                      height: Globals.minPadding,
                    ),
        ],
      ),
    );
  }

  Widget _itemHeaderWorkerPart(Map<String, dynamic> model, int group) {
    String avatar = "";
    if (model['worker'] != null)
      avatar = model['worker']['avatar'] ?? "";
    else if (model['group'] != null) avatar = model['group']['avatar'] ?? "";
    Map<String, dynamic> service = model['service'];
    return Container(
      padding: EdgeInsets.only(bottom: Globals.minPadding),
      margin: EdgeInsets.only(bottom: Globals.minPadding),
      decoration: BoxDecoration(
        border: Border(bottom: BorderSide(color: colorLine, width: 1)),
        color: Colors.white,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      //width: MediaQuery.of(context).size.width * 1 / 3,
                      child: Text(
                        service['name'] ?? "",
                        softWrap: true,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: styleTextBlackBold,
                      ),
                    ),
                    Container(
                      width: Globals.minPadding,
                    ),
                  ],
                ),
                SizedBox(
                  height: 5.0,
                ),
                Container(
                  width: Globals.maxWidth * 0.8,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          model['address'] ?? "",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: HexColor("3C3C43").withOpacity(0.6),
                              fontSize: 13),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: Globals.minPadding,
          ),
          Stack(
            children: <Widget>[
              avatar != "" && avatar != null
                  ? Container(
                      height: Globals.maxWidth * 0.15,
                      width: Globals.maxWidth * 0.15,
                      //  margin: EdgeInsets.all(10.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10000000.0),
                        child: CachedNetworkImage(
                          fit: BoxFit.cover,
                          imageUrl: avatar,
                          placeholder: (context, url) => Transform(
                              alignment: FractionalOffset.center,
                              transform: Matrix4.identity()..scale(0.35, 0.35),
                              child: CupertinoActivityIndicator()),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                        ),
                      ),
                    )
                  : Container(
                      height: Globals.maxWidth * 0.15,
                      width: Globals.maxWidth * 0.15,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: AssetImage(defaultAvatar),
                              fit: BoxFit.cover)),
                    ),
//              group == 1
//                  ? Container(
//                      margin: EdgeInsets.only(
//                        left: Globals.maxPadding *
//                            2.8, //bottom: Globals.minPadding/4
//                      ),
//                      height: 23,
//                      decoration: BoxDecoration(
//                          color: colorButton, shape: BoxShape.circle),
//                      child: Center(
//                        child: ImageIcon(
//                          AssetImage(iconGroup),
//                          color: Colors.white,
//                          size: 12,
//                        ),
//                      ),
//                    )
//                  : Container()
            ],
          )
        ],
      ),
    );
  }

  Widget _itemContentOrder(String title, String content, int group) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: Globals.minPadding / 2),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            width: Globals.maxWidth * 0.2,
            child: Text(
              title,
              style: styleOrder,
            ),
          ),
          Container(
            width: Globals.minPadding,
          ),
          Expanded(
              child: Row(
            children: <Widget>[
              Text(
                content,
                textAlign: TextAlign.justify,
                style: styleOrderContent,
                softWrap: true,
              ),
              group == 1
                  ? Container(
                      margin: EdgeInsets.only(left: Globals.maxPadding),
                      height: 15,
                      child: Image.asset(
                        iconGroup,
                        fit: BoxFit.cover,
                      ),
                    )
                  : Container()
            ],
          )),
        ],
      ),
    );
  }

  Widget _itemFollowWorker(
      String title, String content, Map<String, dynamic> model) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: Globals.minPadding / 2),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            width: Globals.maxWidth * 0.2,
            child: Text(
              title,
              style: styleOrder,
            ),
          ),
          Container(
            width: Globals.minPadding,
          ),
          Expanded(
            child: InkWell(
              onTap: () {
//                showCustomBottomDialog(
//                    context,
//                    _buildMap(Position(
//                      latitude: model.lat,
//                      longitude: model.lng,
//                    )));
              },
              child: Container(
                alignment: Alignment.centerLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    ImageIcon(
                      AssetImage(iconNavigation),
                      size: 13,
                      color: colorButton,
                    ),
                    Container(
                      width: Globals.minPadding / 2,
                    ),
                    Text(
                      content,
                      textAlign: TextAlign.left,
                      style: TextStyle(fontSize: 13.0, color: colorText),
                      softWrap: true,
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildMap(Position position) {
    return Container(
        child: Center(
      child: ClipRRect(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
          bottomRight: Radius.circular(30),
          bottomLeft: Radius.circular(30),
        ),
        child: _googleMap(position),
      ),
    ));
  }

  Widget _googleMap(Position position) {
    return Container(
      height: Globals.maxHeight -
          AppBar().preferredSize.height -
          MediaQuery.of(context).padding.top -
          Globals.minPadding,
      child: GoogleMap(
        mapType: MapType.normal,
        initialCameraPosition: CameraPosition(
          target: LatLng(position.latitude, position.longitude),
          zoom: _zoom,
        ),
        onMapCreated: (GoogleMapController controller) {
          _mapController = controller;
          _mapController.animateCamera(CameraUpdate.newCameraPosition(
              CameraPosition(
                  target: LatLng(10.7536921, 106.7402057), zoom: 15.0)));
        },
//        markers: Set.from(allMarker),
//        circles: circles,
      ),
    );
  }

  Widget _buildItemProductivity(Map<String, dynamic> model) {
    String startTime = "";
    String endTime = "";
    int group = 0;
    int workerAmount = 0;
    if ((model['workerTypeId'] == 2 || model['workerTypeId'] == 3) &&
        (model['group'] != null)) {
      model['partTeam'].forEach((data) {
        startTime = dateOrderFormat
            .format(findWorkerFormat.parse(data['startTime'] ?? ""));
        endTime = dateOrderFormat
            .format(findWorkerFormat.parse(data['endTime'] ?? ""));
        if (data['worker'] != null && data['worker'].length > 0)
          workerAmount = data['worker'].length;
        else
          workerAmount = 0;
      });
    } else {
      model['part'].forEach((data) {
        startTime = dateOrderFormat
            .format(findWorkerFormat.parse(data['startTime'] ?? ""));
        endTime = dateOrderFormat
            .format(findWorkerFormat.parse(data['startTime'] ?? ""));
      });
    }
    if (model['workerTypeId'] == 2 || model['workerTypeId'] == 3) {
      group = 1;
    } else
      group = 0;
    Map<String, dynamic> service = model['service'];
    Map<String, dynamic> _statusService = model['status'];
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.2),
            blurRadius: 10.0, // has the effect of extending the shadow
            offset: Offset(
              0.06, // horizontal, move right 10
              0.06,
            ),
          )
        ],
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(8.0),
            bottomLeft: Radius.circular(8.0),
            bottomRight: Radius.circular(8.0)),
      ),
      padding: EdgeInsets.symmetric(
          vertical: Globals.maxPadding, horizontal: Globals.maxPadding),
      margin: EdgeInsets.only(bottom: Globals.maxPadding),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          model['workerTypeId'] == 2 || model['workerTypeId'] == 3
              ? _itemHeaderWorkerProductivity(model['group'], group)
              : _itemHeaderWorkerProductivity(model['worker'], group),
          _itemContentOrder('Công việc:', service['name'] ?? "", group),
          _itemContentOrder(
              'Số lượng:',
              "${model['jobAmount'].toString()} ${service['unitName'] ?? ""}",
              null),
          _itemContentOrder('Địa chỉ:', model['address'] ?? "", null),
          (_statusService['id'] == 1 ||
                  _statusService['id'] == 2 ||
                  _statusService['id'] == 3 ||
                  _statusService['id'] == 4)
              ? _itemContentOrder('Bắt đầu:', startTime ?? "", null)
              : _itemContentOrder(
                  '${_language.time ?? ''}:', startTime ?? "", null),
//          _statusService['id'] == 2
//              ? _itemFollowWorker('${_language.follow ?? ''}:',
//                  '${_language.followInMap ?? ''}', model)
//              : Container(),
//          _statusService['id'] == 3
//              ? _itemContentOrder(
//                  '${_language.working ?? ''}:', endTime ?? "") //_countTimer
//              : (model.status.id == 6)
//                  ? _itemContentOrder('${_language.end ?? ''}:', endTime ?? "")
//                  : Container(),
          (model['group'] != null)
              ? _itemContentOrder(
                  'Số thợ:', '${workerAmount.toString()} người', null)
              : Container(
                  height: Globals.minPadding,
                ),
          (_statusService['id'] == 1 ||
                  _statusService['id'] == 2 ||
                  _statusService['id'] == 3 ||
                  _statusService['id'] == 4)
              ? Column(
                  children: <Widget>[
                    Container(
                      height: Globals.maxPadding,
                    ),
                    CustomButton(
                      text: _statusService['id'] == 1
                          ? 'Di chuyển'
                          : _statusService['id'] == 2
                              ? "Đã đến nơi"
                              : _statusService['id'] == 3
                                  ? "Nhận việc"
                                  :  _language.stringCompleted??"",
                      onTap: () {},
                      colorBorder: _statusService['id'] == 1
                          ? colorWaiting
                          : _statusService['id'] == 2
                              ? colorMoving
                              : _statusService['id'] == 3
                                  ? colorWaitingToDo
                                  : colorWorking,
                      colorBackground:  _statusService['id'] == 1
                          ? colorWaiting
                          : _statusService['id'] == 2
                          ? colorMoving
                          : _statusService['id'] == 3
                          ? colorWaitingToDo
                          : colorWorking,
                      textStyle: styleTitleWhiteBold,
                      radius: 25,
                      isMaxWidth: true,
                    ),
                    Container(
                      height: Globals.maxPadding,
                    ),
                    InkWell(
                      child: Text(
                        'Hủy việc',
                        style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.normal,
                          color: colorCancelJob,
                          letterSpacing: -0.24,
                        ),
                      ),
                      onTap: () {
                        if(model['worker']!=null)
                        showCustomPopupDialog(
                            context,
                            _titleReason(model['worker']['name'] ?? ""),
                            _buildReason(),
                            isExpanded: true);
                        else
                          showCustomPopupDialog(
                              context,
                              _titleReason(model['group']['name'] ?? ""),
                              _buildReason(),
                              isExpanded: true);
                      },
                    )
                  ],
                )
              : _statusService['id'] == 6
                  ? CustomButton(
                      text: 'Chờ xác nhận',
                      onTap: () {},
                      colorBorder: colorWaitingToPay,
                      colorBackground: colorWaitingToPay,
                      textStyle: styleTitleWhiteBold,
                      radius: 25,
                      isMaxWidth: true,
                    )
                  : Container(
                      height: Globals.minPadding,
                    ),
        ],
      ),
    );
  }

  Widget _itemDetailsOrder(Map<String, dynamic> model, int index) {
    Map<String, dynamic> service = model['service'];
    Map<String, dynamic> _statusService = model['status'];
    return InkWell(
      onTap: () => navigatorPush(
          context,
          JobDetailsPage(
            model: model,
          )),
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: Globals.maxPadding),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            service['serviceType'] == 2 ||
                    (service['serviceType'] == 1 &&
                        (_statusService['id'] == 1 ||
                            _statusService['id'] == 6 ||
                            _statusService['id'] == 7))
                ? _buttonStatusJob(_statusService['name'], _statusService['id'])
                : Container(),
            service['serviceType'] == 2
                ? _buildItemProductivity(model)
                : _buildItemWorkerPart(model)
          ],
        ),
      ),
    );
  }

  Widget _buttonStatusJob(String text, int status) {
//    {   "id": 1,   "name": "Đang chờ xác nhận"
//    "id": 2, "name": "Đang di chuyển"
//       "id": 3,"name": "Đã đến nơi"
//        "id": 4, "name": "Đang chờ làm"
//         "id": 5,"name": "Đang làm"
//        "id": 6, "name": "Chờ thanh toán"
//        "id": 7, "name": ""
//         "id": 8,"name": "Hoàn thành"
//        "id": 9" "name": "Huỷ"
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Container(
          width: Globals.maxWidth / 3,
          padding: EdgeInsets.symmetric(vertical: Globals.minPadding),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: status == 1
                  ? colorWaiting
                  : status == 2
                      ? colorMoving
                      : status == 4
                          ? colorWaitingToDo
                          : status == 5
                              ? colorWorking
                              : status == 6
                                  ? colorWaitingToPay
                                  : colorWaitingToConfirm,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(8.0),
                  topRight: Radius.circular(8.0))),
          child: AutoSizeText(
            text,
            minFontSize: 8.0,
            maxFontSize: 15,
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
      ],
    );
  }

  Widget _buildTab() {
    return Container(
      margin: EdgeInsets.symmetric(
          horizontal: Globals.maxPadding, vertical: Globals.minPadding),
      child: DefaultTabController(
        length: 2,
        initialIndex: 0,
        child: TabBar(
          isScrollable: false,
          unselectedLabelStyle: styleHint,
          labelStyle: styleTitle,
          unselectedLabelColor: colorHint,
          labelColor: primaryColor,
          tabs: <Widget>[
            new Tab(text: "Danh sách"),
            new Tab(text: "Lịch"),
          ],
          indicatorColor: primaryColor,
          indicatorSize: TabBarIndicatorSize.tab,
          controller: _controller,
        ),
      ),
    );
  }

  Widget _buildContent() {
    return Container(
//      padding: EdgeInsets.fromLTRB(
//          Globals.maxPadding, Globals.minPadding, Globals.maxPadding, 0),
      width: Globals.maxWidth,
      height: Globals.maxHeight -
          Globals.maxPadding * 4 -
          MediaQuery.of(context).padding.top -
          MediaQuery.of(context).padding.bottom,
      decoration: BoxDecoration(
        color: colorBackground,
        borderRadius: BorderRadius.all(Radius.circular(34.0)),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildTab(),
          Expanded(
            child: TabBarView(
              controller: _controller,
              children: <Widget>[
                _buildContentTab1(),
                _buildContentTab2(),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildContentTab1() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Expanded(
          child: StreamBuilder(
            stream: _bloc.outputModelTab1,
            initialData: null,
            builder: (context, snapshot) {
              if (snapshot.data == null) return Container();
              List<Map<String, dynamic>> models = snapshot.data;
              if (models == null || models.length == 0) return _noData();
              return ListView.builder(
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
//                separatorBuilder: (_, index) {
//                  return Container(
//                    height: 1.0,
//                    color:colorBackground,
//                  );
//                },
                itemCount: models.length,
                itemBuilder: (_, int index) {
                  return _itemDetailsOrder(models[index], index);
                },
              );
            },
          ),
        ),
        Container(
          height: Globals.maxPadding,
        ),
      ],
    );
  }

  Widget _noData() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          height: Globals.maxWidth * 0.7,
          width: Globals.maxWidth * 0.7,
          child: Image.asset(noDataJobManage),
        ),
        Container(
            margin: EdgeInsets.only(
                top: Globals.minPadding,
                left: Globals.maxPadding,
                right: Globals.maxPadding),
            child: Text(
              'Danh sách công việc đang trống. Hãy giao thêm công việc cho chúng tôi',
              style: TextStyle(
                color: grayTextInputColor,
              ),
              textAlign: TextAlign.center,
            )),
//        Container(
//            margin: EdgeInsets.only(top: 10.0),
//            child: Text(_language.stringEmptyPromotion2 ?? "",
//                style: TextStyle(color: grayTextInputColor))
//        )
      ],
    );
  }

  Widget datePicker(List<DateTime> listDate) {
    return CustomCalendar(
      backgroundColor: colorBackground,
      onChanged: (DateTime time) {
        _bloc.setModelTab2([]);
      },
      currentDateColor: colorWaitingToConfirm,
      dateSelectedColor: colorCancelJob,
      listDateSelected: listDate,
      selectedColor: colorDateSelected,
    );
  }

  Widget _buildContentTab2() {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width * 0.9,
//            height: MediaQuery.of(context).size.height *0.17,
            child: StreamBuilder(
                stream: _bloc.outputDateTime,
                builder: (_, snapshot) {
                  return datePicker(snapshot.data);
                }),
          ),
          Expanded(
            child: StreamBuilder(
              stream: _bloc.outputModelTab2,
              initialData: null,
              builder: (_, snapshot) {
                if (snapshot.data == null) return Container();
                List<Map<String, dynamic>> listModel = snapshot.data;
                return listModel == null || listModel.length == 0
                    ? _noData()
                    : ListView.separated(
                        shrinkWrap: true,
                        physics: ClampingScrollPhysics(),
                        separatorBuilder: (context, index) {
                          return Container(
                            height: 1.0,
                            color: Colors.white,
                          );
                        },
                        itemCount: listModel.length,
                        itemBuilder: (_, int index) {
                          return _itemDetailsOrder(listModel[index], index);
                        },
                      );
              },
            ),
          ),
          Container(
            height: Globals.maxPadding,
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
            statusBarColor: Colors.white, // Color for Android
            statusBarBrightness: Brightness.dark // Dark == white status bar -- for IOS.
        ));
        navigatorPop(context);
        return;
      },
      child: Scaffold(
        body: Container(
          color: Colors.black,
          constraints: BoxConstraints.expand(),
          child: Stack(
            children: <Widget>[
              _setupBackground(),
              Positioned(
                left: 0.0,
                top: 0.0,
                right: 0.0,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    CustomAppBar(
                      title: 'Quản lý việc',
                      onTapLeading: (){
                        SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
                            statusBarColor: Colors.white, // Color for Android
                            statusBarBrightness: Brightness.dark // Dark == white status bar -- for IOS.
                        ));
                        navigatorPop(context);
                      },
                    ),
                    _buildContent()
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
