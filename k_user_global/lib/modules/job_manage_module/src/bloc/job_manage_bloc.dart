import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:k_user_global/models/reason_model.dart';
import 'package:k_user_global/resources/repository.dart';
import 'package:rxdart/rxdart.dart';

class JobManageBloc{
  final _repository = Repository();

  final _streamTime = BehaviorSubject<int>();
  final _streamDateTime = BehaviorSubject<List<DateTime>>();
  final _streamModelTab1 = BehaviorSubject<List<Map<String, dynamic>>>();
  final _streamModelTab2 = BehaviorSubject<List<Map<String, dynamic>>>();
  final _streamReason = BehaviorSubject<ReasonModel>();
  Observable<int> get outputTime => _streamTime.stream;
  Observable<List<DateTime>> get outputDateTime => _streamDateTime.stream;
  Observable<ReasonModel> get outputReason => _streamReason.stream;

  Observable<List<Map<String, dynamic>>> get outputModelTab1 =>
      _streamModelTab1.stream;
  Observable<List<Map<String, dynamic>>> get outputModelTab2 =>
      _streamModelTab2.stream;

  setReason(ReasonModel event) => _streamReason.sink.add(event);
  setTime(int event) => _streamTime.sink.add(event);
  setSelectedDate(List<DateTime> event) => _streamDateTime.sink.add(event);
  setModelTab1(List<Map<String, dynamic>> event) => _streamModelTab1.sink.add(event);
  setModelTab2(List<Map<String, dynamic>> event) => _streamModelTab2.sink.add(event);
  dispose(){
    _streamTime.close();
    _streamModelTab1.close();
    _streamReason.close();
    _streamModelTab2.close();
    _streamDateTime.close();
  }

  //list status job
}