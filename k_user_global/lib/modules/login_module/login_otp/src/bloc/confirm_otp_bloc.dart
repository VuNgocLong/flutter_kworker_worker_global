import 'package:flutter/material.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/interactions.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/models/otp_confirm_model.dart';
import 'package:k_user_global/models/otp_response_model.dart';
import 'package:k_user_global/models/request_otp_model.dart';
import 'package:k_user_global/models/response_model.dart';
import 'package:k_user_global/modules/home_module/src/ui/home_ui.dart';
import 'package:k_user_global/modules/login_module/basic_informations/src/ui/basic_informations_widget.dart';
import 'package:k_user_global/resources/repository.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ConfirmOTPBLoc {
  final _repository = Repository();
  SharedPreferences _prefs;
  Language _language;
  ConfirmOTPBLoc() {
    SharedPreferences.getInstance().then((data) {
      _prefs = data;
      String _model = _prefs.getString(keyLanguage);
      if (_model != null) {
        if (_model == 'vi') {
          _language = Vietnamese();
        } else if (_model == 'eng') {
          _language = English();
        } else if (_model == 'korean') {
          _language = Korean();
        } else if (_model == 'japanese') {
          _language = Japanese();
        }
      }
    });
  }

  final _stremValidateOTP = BehaviorSubject<String>();
  final _streamEnableButton = BehaviorSubject<bool>();
  Observable<bool> get outputEnableButton => _streamEnableButton.stream;
  Observable<String> get outputValidateOTP => _stremValidateOTP.stream;

  setEnableButton(bool event) => _streamEnableButton.sink.add(event);
  setValidateOTP(String event) => _stremValidateOTP.sink.add(event);
  dispose() {
    _streamEnableButton.close();
    _stremValidateOTP.close();
  }

  Future<bool> resendOtp(
      BuildContext context, String phone, Language language) async {
    for (String char in [phone]) {
      if (char.trim().isEmpty) {
        setValidateOTP(language.stringMissPhone);
        return false;
      }
    }
    showProgressDialog(context);
    ApiResponseData response =
        await _repository.sendOTP(context, RequestOtpModel(phone: phone));
    hideProgressDialog();
    if (response.success) {
      setEnableButton(false);
      return true;
    } else {
      hideProgressDialog();
      ResponseModel data = ResponseModel.fromJson(response.data);
      setValidateOTP(data.messager);

      return false;
    }
  }

  submitOtp(BuildContext context, String otp1, String otp2, String otp3,
      String otp4, String phone, bool isVerify) async {
    for (String char in [otp1, otp2, otp3, otp4]) {
      if (char.trim().isEmpty) {
        setValidateOTP("Mã OTP rỗng");
        return;
      }
    }
    setValidateOTP(null);
    showProgressDialog(context);
    ApiResponseData response = await _repository.submitOtp(
        context, OTPConfirmModel(otp: otp1 + otp2 + otp3 + otp4, phone: phone));
    hideProgressDialog();
    if (response.success) {
      ResponseOTPModel data = ResponseOTPModel.fromJson(response.data['data']);
      //  String token = data.tokenType + " " + data.accessToken;
      String token = data.accessToken;
      _prefs.setString(keyToken, token);
      _prefs.setString(keyPhoneUser, phone);

      Navigator.of(context).popUntil((route) => route.isFirst);
      if (data.isUpdatedInfo != null && int.tryParse(data.isUpdatedInfo)== 1) {
        _prefs.setString(
            keyIsUpdateInfo,
            data.isUpdatedInfo);
        navigatorPushReplacement(context, HomeUi());
      } else {
        navigatorPushReplacement(
            context,
            BasicInformationPage(
              phone: phone,
            ));
      }
    } else {
      hideProgressDialog();
      ResponseModel data = ResponseModel.fromJson(response.data);
      setValidateOTP(data.messager);
    }
  }
}
