import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/modules/login_module/basic_informations/src/ui/basic_informations_widget.dart';
import 'package:k_user_global/modules/login_module/login_otp/src/bloc/confirm_otp_bloc.dart';
import 'package:k_user_global/ultilites/hex_color.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:quiver/async.dart';
class ConfirmOTPPage extends StatefulWidget{
  final String phone;

  const ConfirmOTPPage({Key key, this.phone}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return _StateOTP();
  }

}

class _StateOTP extends State<ConfirmOTPPage>{
  FocusNode _focusOTP1Node = FocusNode();
  FocusNode _focusOTP2Node = FocusNode();
  FocusNode _focusOTP3Node = FocusNode();
  FocusNode _focusOTP4Node = FocusNode();

  TextEditingController _controllerOTP1 = TextEditingController();
  TextEditingController _controllerOTP2 = TextEditingController();
  TextEditingController _controllerOTP3 = TextEditingController();
  TextEditingController _controllerOTP4 = TextEditingController();
  int _current = 0;
  ConfirmOTPBLoc _bloc;
  CountdownTimer _countDownTimer;
  SharedPreferences _prefs;
  Language _language;
  bool isVerify;
  String isUpdated="";
  String _text;
  @override
  void initState(){
    super.initState();
    _language = Language();
    _current = 59;
    _bloc = ConfirmOTPBLoc();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Colors.white, // Color for Android
        statusBarBrightness: Brightness.dark // Dark == white status bar -- for IOS.
    ));
    SharedPreferences.getInstance().then((data) {
      _prefs = data;
      setState(() {
//        _prefs.setString(
//            keyIsUpdateInfo,
//            data.isUpdatedInfo);
        bool  accVerify = _prefs.getBool(keyAccVerify);
        String _model = _prefs.getString(keyLanguage);
//        if(basicInfo!=null&&basicInfo!="") isUpdated = basicInfo;
        if(accVerify!=null&&accVerify) isVerify = accVerify;
        else isVerify = false;
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        }
      });

    });
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _startTimer();
      _bloc.setValidateOTP(null);
    });
  }
  @override
  void dispose() {
//    WidgetsBinding.instance.removeObserver(this);
    if (_countDownTimer.isRunning) _countDownTimer.cancel();
    _bloc.dispose();
    super.dispose();
  }


  void _startTimer() {
    _countDownTimer = CountdownTimer(
      new Duration(seconds: 59),
      new Duration(seconds: 1),
    );
    _countDownTimer.listen((_) {})
      ..onData((duration) {
        _current = 59 - duration.elapsed.inSeconds;
        //  setState(() {});
        if (mounted) {
          setState(() {});
        }
      })
      ..onDone(() {
        _bloc.setEnableButton(true);
      });
  }
  _resend() {
    var result = _bloc.resendOtp(context, widget.phone,_language);
    if (result != null) {
      _current = 59;
      _startTimer();
    }
  }
  Widget _inputViewOTPTextLineView(String validateOTP) {
    return Container(
      height: (MediaQuery.of(context).size.height -
          MediaQuery.of(context).padding.top -
          MediaQuery.of(context).padding.bottom) /
          30 -
          5.0,
      width: MediaQuery.of(context).size.width * 9 / 10,
      child: Row(
        children: <Widget>[
          Expanded(
            child: Divider(
              height: 1.0,
              color: validateOTP!=null? Colors.red: colorDes,
            ),
          ),
          Container(
            width: 5.0,
          ),
          Expanded(
            child: Divider(
              height: 1.0,
              color:validateOTP!=null? Colors.red: colorDes,
            ),
          ),
          Container(
            width: 5.0,
          ),
          Expanded(
            child: Divider(
              height: 1.0,
              color:validateOTP!=null? Colors.red: colorDes,
            ),
          ),
          Container(
            width: 5.0,
          ),
          Expanded(
            child: Divider(
              height: 1.0,
              color: validateOTP!=null? Colors.red: colorDes,
            ),
          ),
        ],
      ),
    );
  }
  Widget _setupContent() {
    if (_language.countryCode == '+84') {
      _text = '${_language.resendOtp ?? ''} 0:${_current.toString()} giây';
    } else if (_language.countryCode == '+44') {
      _text = '${_language.resendOtp ?? ''} 0:${_current.toString()} seconds';
    } else if (_language.countryCode == '+82') {
      _text = '0:${_current.toString()} 초 ${_language.resendOtp ?? ""}';
    } else if (_language.countryCode == '+81') {
      _text = '0:${_current.toString()} 秒以 ${_language.resendOtp ?? ""}';
    } else {
      _text = '';
    }

    return Container(
      // height: MediaQuery.of(context).size.height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            height: Globals.maxPadding,
          ),
          InkWell(
            onTap: () {
              navigatorPop(context);
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  width: Globals.maxPadding,
                ),
                Image.asset(
                  iconBackBlack,
                  height: 20,
                  width: 30,
//                  fit: BoxFit.cover,
                ),
              ],
            ),
          ),
          Container(
            height: Globals.maxPadding * 3,
          ),
          Container(
            margin: EdgeInsets.only(left: Globals.maxPadding),
            height: Globals.maxWidth*0.2,
            width: Globals.maxWidth*0.2,
            decoration: BoxDecoration(

                image: DecorationImage(
                    image: AssetImage(logoGreen), fit: BoxFit.cover
                )),
          ),
          Container(
            height: Globals.maxPadding * 2,
          ),
          Container(
            margin: EdgeInsets.only(left: Globals.maxPadding),
            child: AutoSizeText(
              _language.hi != null ? _language.hi : "",
              textAlign: TextAlign.left,
              style: TextStyle(
                  fontSize: 26, color: Colors.black, fontWeight: FontWeight.normal),
            ),
          ),
          Container(
            height: Globals.minPadding,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: Globals.maxPadding ),
            child: RichText(
                softWrap: true,
                textAlign: TextAlign.left,
                text: TextSpan(children: [
                  TextSpan(
                    text:   _language.inputText ?? "",
                    style: styleContent,),
                  TextSpan(
                    text:'K-Worker',
                    style: styleTitle,),
                ])),
          ),
          Container(
            height: Globals.maxPadding * 4,
          ),
          Padding(
            padding: EdgeInsets.only(left: 10.0, right: 10.0),
            child: _inputViewOTPText(),
          ),
//          _itemInputView(),
          Padding(
            padding: EdgeInsets.only(
                left: Globals.maxPadding, right: Globals.maxPadding, top: Globals.minPadding/2),
            child:
            StreamBuilder(
                stream: _bloc.outputValidateOTP,
                initialData: null,
                builder: (_, snapshot) {
                  return
                    Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      _inputViewOTPTextLineView(snapshot.data),
                      Container(height: Globals.minPadding/2,),
                      snapshot.data!=null?
                      Text(snapshot.data, style: TextStyle(color: Colors.red),
                        textAlign: TextAlign.center,)

                          :Container()
                    ],
                  );
                }
            ),
          ),
          StreamBuilder(
            stream: _bloc.outputEnableButton,
            initialData: false,
            builder: (context, snapshot) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  FlatButton(
                    child: snapshot.data
                        ? Text(
                      _language.receiveOtp ?? '',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 15.0, color: colorOtp),
                    )
                        : Text(
                      _language.resendOtp != null ? _text : '',
                      // _language.resendOtp + ' 0:${_current.toString()} giây',
                      textAlign: TextAlign.center,
                      style: styleHint,
                    ),
                    onPressed: snapshot.data ? _resend : null,
                  ),
                ],
              );
            },
          ),
        ],
      ),
    );
  }

  Widget _inputViewOTPText() {
    return Container(
      height: (MediaQuery.of(context).size.height -
          MediaQuery.of(context).padding.top -
          MediaQuery.of(context).padding.bottom) /
          30 +
          10.0,
      width: MediaQuery.of(context).size.width * 9 / 10,
      child: Row(
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(right: 5.0),
              child: TextField(
                focusNode: _focusOTP1Node,
                controller: _controllerOTP1,
                keyboardType: TextInputType.phone,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 28,
                    color: Colors.black,
                    fontWeight: FontWeight.normal),
                decoration: InputDecoration(
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.symmetric(vertical: 10.0)),
                inputFormatters:
                <TextInputFormatter>[
                  WhitelistingTextInputFormatter.digitsOnly,
                  LengthLimitingTextInputFormatter(1),
                ],
                onChanged: (text) {
                  if (text.length == 1) {
                    fieldFocusChange(context, _focusOTP1Node, _focusOTP2Node);
                  }
                },
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(right: 5.0),
              child: TextField(
                focusNode: _focusOTP2Node,
                controller: _controllerOTP2,
                keyboardType: TextInputType.phone,
                style: TextStyle(
                    fontSize: 28,
                    color: Colors.black,
                    fontWeight: FontWeight.normal),
                textAlign: TextAlign.center,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.symmetric(vertical: 10.0)),
                inputFormatters: <TextInputFormatter>[
                  WhitelistingTextInputFormatter.digitsOnly,
                  LengthLimitingTextInputFormatter(1),
                ],
                onChanged: (text) {
                  if (text.length == 1) {
                    fieldFocusChange(context, _focusOTP2Node, _focusOTP3Node);
                  } else if (text.length == 0) {
                    fieldFocusChange(context, _focusOTP2Node, _focusOTP1Node);
                  }
                },
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(right: 5.0),
              child: TextField(
                focusNode: _focusOTP3Node,
                controller: _controllerOTP3,
                keyboardType: TextInputType.phone,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 28,
                    color: Colors.black,
                    fontWeight: FontWeight.normal),
                decoration: InputDecoration(
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.symmetric(vertical: 10.0)),
                inputFormatters: <TextInputFormatter>[
                  WhitelistingTextInputFormatter.digitsOnly,
                  LengthLimitingTextInputFormatter(1),
                ],
                onChanged: (text) {
                  if (text.length == 1) {
                    fieldFocusChange(context, _focusOTP3Node, _focusOTP4Node);
                  } else if (text.length == 0) {
                    fieldFocusChange(context, _focusOTP3Node, _focusOTP2Node);
                  }
                },
              ),
            ),
          ),
          Expanded(
            child: TextField(
                focusNode: _focusOTP4Node,
                controller: _controllerOTP4,
                keyboardType: TextInputType.phone,
                textAlign: TextAlign.center,
                textInputAction: TextInputAction.done,
                onChanged: (text) {
                  if (text.length == 1) {
                    FocusScope.of(context).unfocus();
                  if(isVerify&&isUpdated!=""){
                    _bloc.submitOtp(
                      context,
                      _controllerOTP1.text,
                      _controllerOTP2.text,
                      _controllerOTP3.text,
                      _controllerOTP4.text,
                      widget.phone,true
                    );
                  }else
                    _bloc.submitOtp(
                      context,
                      _controllerOTP1.text,
                      _controllerOTP2.text,
                      _controllerOTP3.text,
                      _controllerOTP4.text,
                      widget.phone,false
                    );

                  } else if (text.length == 0) {
                    fieldFocusChange(context, _focusOTP4Node, _focusOTP3Node);
                  }
                },
                style: TextStyle(
                    fontSize: 28,
                    color: Colors.black,
                    fontWeight: FontWeight.normal),
                decoration: InputDecoration(
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.symmetric(vertical: 10.0)),
                inputFormatters:
                <TextInputFormatter>[
                  WhitelistingTextInputFormatter.digitsOnly,
                  LengthLimitingTextInputFormatter(1),
                ]),
          ),
        ],
      ),
    );
  }


  Widget _setupBackground() {
    return Container(
      width: Globals.maxWidth,
      height: Globals.maxHeight,
      decoration: BoxDecoration(
        color: Colors.white,
//        borderRadius: BorderRadius.all(Radius.circular(34.0)),
      ),
      child: Column(
        children: <Widget>[
          Container(
            height: Globals.maxHeight * 0.38,
            width: MediaQuery.of(context).size.width,
          ),
          Expanded(
            child: Container(),
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: Container(
              width: 244,
              height: 125,
              decoration: BoxDecoration(
                  borderRadius:
                  BorderRadius.only(bottomRight: Radius.circular(34.0)),
                  image: DecorationImage(
                      image: AssetImage(bgBottom), fit: BoxFit.fill)),
//              ),
            ),
          )
        ],
      ),
    );
  }
  Widget _setupLoginView() {
    return GestureDetector(
      onTap: () => hideKeyboard(context),
      child: SafeArea(
        child: Container(
            color: Colors.transparent,
            child: ListView(
              physics: ClampingScrollPhysics(),
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height -
                      MediaQuery.of(context).padding.top -
                      MediaQuery.of(context).padding.bottom,
                  width: MediaQuery.of(context).size.width,
                  child: _setupContent(),
                )
              ],
            )),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Container(
        height: Globals.maxHeight,
        color: Colors.black,
        child: Stack(
          children: <Widget>[
            Container(
              constraints: BoxConstraints.expand(),
              color: Colors.black,
            ),
            _setupBackground(),
            _setupLoginView()
            // _setupContent(),
          ],
        ),
      ),
    );
  }
}