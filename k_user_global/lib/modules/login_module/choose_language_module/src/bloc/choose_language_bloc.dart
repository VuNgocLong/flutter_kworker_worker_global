import 'package:flutter/material.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/modules/login_module/login_phone/src/ui/login_phone_widget.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LanguageBloc{

  SharedPreferences _prefs;

  LanguageBloc() {
    SharedPreferences.getInstance().then((data) {_prefs = data;});
  }

  checkLanguage(BuildContext context, String text) async{
    if(text == vn){
      _prefs.setString(keyLanguage, 'vi');

    }else if(text == english){
      _prefs.setString(keyLanguage, 'eng');
    }else if(text == niHon){
      _prefs.setString(keyLanguage, 'japanese');
    }else if(text == korean){
      _prefs.setString(keyLanguage, 'korean');
    }

    navigatorPush(context, LoginPhonePage());

  }

}