import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/modules/login_module/choose_language_module/src/bloc/choose_language_bloc.dart';

class ChooseLanguage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _StateChoose();
  }
}

class _StateChoose extends State<ChooseLanguage> {
  LanguageBloc _bloc;

  @override
  void initState() {
    _bloc = LanguageBloc();
    super.initState();
//    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
//        statusBarColor: greenLight, // Color for Android
//        statusBarBrightness: Brightness.light // Dark == white status bar -- for IOS.
//    ));

  }

  Widget _setupContent() {
    return Positioned(
      top: 0.0,
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            height: Globals.maxHeight * 0.21,
          ),
          Container(
            height: 85,
            width: 85,
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    blurRadius: 10.0, // has the effect of softening the shadow
//            spreadRadius:
//            15.0, // has the effect of extending the shadow
                    offset: Offset(
                      0.06, // horizontal, move right 10
                      0.6,
                      // vertical, move down 10
                    ),
                  )
                ],
                image: DecorationImage(
                    image: AssetImage(logo), fit: BoxFit.cover
                )),
//            child: Image.asset(
//              logo,
//              fit: BoxFit.cover,
//            ),
          ),
          Expanded(
            child: Container(),
          ),
          _buildButton(niHon, () {
            _bloc.checkLanguage(context, niHon);
          }),
          _buildButton(korean, () {
            _bloc.checkLanguage(context, korean);
          }),
          _buildButton(english, () {
            _bloc.checkLanguage(context, english);
          }),
          _buildButton(vn, () {
            _bloc.checkLanguage(context, vn);
          }),
          Expanded(
            child: Container(),
          ),
          Container(
            height: Globals.minPadding,
          )
        ],
      ),
    );
  }

  Widget _buildButton(String data, Function onTap) {
    return InkWell(
      child: Container(
        margin: EdgeInsets.symmetric(
            vertical: Globals.minPadding, horizontal: Globals.maxPadding),
        padding: EdgeInsets.symmetric(
          vertical: 15,
        ),
        decoration: BoxDecoration(
          //color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(26.0)),
          shape: BoxShape.rectangle,
        ),
        child: Center(
          child: AutoSizeText(
            data,
            textAlign: TextAlign.center,
            style: styleLanguage,
          ),
        ),
      ),
      onTap: onTap,
    );
  }

  Widget _setupBackground() {
    return Container(
      width: Globals.maxWidth,
      height: Globals.maxHeight,
      decoration: BoxDecoration(
        color: Colors.white,
//        borderRadius: BorderRadius.all(Radius.circular(34.0)),
      ),
      child: Column(
        children: <Widget>[
          Container(
            // height: 228,
            height: Globals.maxHeight * 0.38,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(34.0),
                    topLeft: Radius.circular(34.0)),
                image: DecorationImage(
                    image: AssetImage(bgTop), fit: BoxFit.cover
                )),
          ),
          Expanded(
            child: Container(),
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: Container(
              width: 244,
              height: 125,
              decoration: BoxDecoration(
                  borderRadius:
                  BorderRadius.only(bottomRight: Radius.circular(34.0)),
                  image: DecorationImage(
                      image: AssetImage(bgBottom), fit: BoxFit.fill)),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
        child: Container(
          height: Globals.maxHeight,
          color: Colors.black,
          child: Stack(
            children: <Widget>[
              _setupBackground(),
              _setupContent(),
            ],
          ),
        ),
      ),
    );
  }
}
