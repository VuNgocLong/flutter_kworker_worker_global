import 'package:flutter/material.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/modules/home_module/src/ui/home_ui.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:k_user_global/widgets/widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class WelcomeWorker extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _StateWelcome();
  }
}

class _StateWelcome extends State<WelcomeWorker> {
  SharedPreferences _prefs;
  Language _language;

  @override
  void initState() {
    super.initState();
    SharedPreferences.getInstance().then((data) {
      _prefs = data;
      setState(() {
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: Globals.maxHeight -
            MediaQuery.of(context).padding.bottom -
            MediaQuery.of(context).padding.top,
        width: Globals.maxWidth,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              height: Globals.maxHeight * 0.5,
              width: Globals.maxWidth,
              child: Image.asset("assets/images/background-welcome-worker.png"),
            ),
            Container(
              height: Globals.maxPadding,
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: RichText(
                      softWrap: true,
                      textAlign: TextAlign.center,
                      text: TextSpan(children: [
                        TextSpan(
                            text: _language.welcome ?? "",

                            style: TextStyle(
                                fontSize: 28,
                                color: Colors.black,

                                letterSpacing: 0.364,
                                fontWeight: FontWeight.normal)),
                        TextSpan(
                            text: 'K-worker',
                            style: TextStyle(
                                fontSize: 28,
                                color: colorButton,
                                letterSpacing: 0.87,
                                fontWeight: FontWeight.normal))
                      ])),
                ),
              ],
            ),
            Container(
                margin: EdgeInsets.only(top: Globals.minPadding),
                child: Text(
                  'Nhấn “Bắt đầu” để cùng trải nghiệm',
                  textAlign: TextAlign.center,
                  style: styleTextBlackNormal,
                )),
            Expanded(
              child: Container(),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: Globals.maxPadding),
              alignment: Alignment.bottomCenter,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: CustomButton(
                      text: _language.begin ?? "",
                      onTap: () {
                        _prefs.setBool(keyIsLogin, true);
                        Navigator.of(context)
                            .popUntil((route) => route.isFirst);
                        navigatorPushReplacement(context, HomeUi());
                      },
                      isMaxWidth: true,
                      textStyle: styleTitleWhiteBold,
                      colorBorder: colorButton,
                      colorBackground: colorButton,
                      radius: 25,
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
