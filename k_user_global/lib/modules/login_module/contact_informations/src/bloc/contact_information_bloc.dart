import 'package:rxdart/rxdart.dart';

class ContactInformationBloc{

  final _streamValidateEmail = BehaviorSubject<bool>();
  final _streamValidateReferCode = BehaviorSubject<bool>();
  final _streamEnableButton = BehaviorSubject<bool>();
  Observable<bool> get outputValidateEmail => _streamValidateEmail.stream;
  Observable<bool> get outputEnableButton => _streamEnableButton.stream;
  Observable<bool> get outputValidateReferCode => _streamValidateReferCode.stream;
  setEnableButton(bool event) => _streamEnableButton.sink.add(event);
  setValidateEmail(bool event) => _streamValidateEmail.sink.add(event);
  setValidateReferCode(bool event) => _streamValidateReferCode.sink.add(event);
  dispose(){
    _streamEnableButton.close();
    _streamValidateEmail.close();
    _streamValidateReferCode.close();
  }
}