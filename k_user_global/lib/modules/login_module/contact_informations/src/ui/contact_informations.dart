import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/models/update_infomation_request_model.dart';
import 'package:k_user_global/modules/home_module/src/ui/home_ui.dart';
import 'package:k_user_global/modules/login_module/account_verification/src/ui/account_verification_widget.dart';
import 'package:k_user_global/modules/login_module/contact_informations/src/bloc/contact_information_bloc.dart';
import 'package:k_user_global/modules/login_module/register_jobs/src/ui/register_jobs_widget.dart';
import 'package:k_user_global/ultilites/custom_string_phone.dart';
import 'package:k_user_global/ultilites/hex_color.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:k_user_global/widgets/widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ContactInformationPage extends StatefulWidget {
  final UpdateInfoRequestModel model;

  const ContactInformationPage({Key key, this.model}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return _StateContact();
  }
}

class _StateContact extends State<ContactInformationPage> {
  TextEditingController _controllerMail = TextEditingController();
  TextEditingController _controllerReferCode = TextEditingController();

  FocusNode _focusNodeMail = FocusNode();
  FocusNode _focusNodeReferCode = FocusNode();
  final _formKey = GlobalKey<FormState>();
  SharedPreferences _prefs;
  Language _language;
  String _phone = "";
  ContactInformationBloc _bloc;
  @override
  void initState() {
    super.initState();
    _bloc = ContactInformationBloc();
    _language = Language();
    SharedPreferences.getInstance().then((data) {
      _prefs = data;
      setState(() {
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        }
        String phone = _prefs.getString(keyPhoneUser);
        if (phone != null && phone != "")
          _phone = numberPhone(phone ?? "", _language);
      });
    });
  }

  Form _formWidget() {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(
                top: Globals.maxPadding,
                bottom: Globals.maxPadding * 2,
                left: Globals.maxPadding),
            child: InkWell(
              onTap: () {
                navigatorPop(context);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Image.asset(
                    iconBackBlack,
                    height: 20,
                    width: 30,
//                  fit: BoxFit.cover,
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: Globals.maxPadding),
            child: Text(
              "Thông tin liên lạc",
              style: TextStyle(
                  fontSize: 34,
                  letterSpacing: 0.374,
                  color: Colors.black,
                  fontWeight: FontWeight.normal),
            ),
          ),
          Container(
            height: Globals.minPadding,
          ),
          Padding(
            padding: EdgeInsets.only(left: Globals.maxPadding),
            child: Text(
              _language.inputFullyToComplete ?? "",
              style: styleTextBlackNormal,
            ),
          ),
          Container(
            height: Globals.maxPadding * 2,
          ),
          Row(
            children: <Widget>[
              Expanded(child: _buildPhone(_phone)),
            ],
          ),
          _inputView("Email*", emailFormField(context)),
          _inputView(_language.inviteCode??"", referCodeFormField(context)),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              StreamBuilder(
                  stream: _bloc.outputEnableButton,
                  initialData: false,
                  builder: (_, snapshot) {
                    return CustomButton(
                      text: _language.stringContinue ?? "",
                      onTap: () {
                        if (snapshot.data) {
                          //HomeUi RegisterJobsPage
                          navigatorPush(context, RegisterJobsPage(
                            model: UpdateInfoRequestModel(
                              sexId: widget.model.sexId,
                              email: _controllerMail.text,
                              referCode: _controllerReferCode.text,
                              address: widget.model.address,
                              lastName: widget.model.lastName,
                              firstName: widget.model.firstName,
                              birthday: widget.model.birthday,
                              phone: widget.model.phone,
                            ),
                          ));
                        }
                      },
                      isMaxWidth: false,
                      textStyle: styleTitleWhiteBold,
                      colorBorder: snapshot.data
                          ? colorButton
                          : colorButton.withOpacity(0.2),
                      colorBackground: snapshot.data
                          ? colorButton
                          : colorButton.withOpacity(0.3),
                      radius: 25,
                    );
                  }),
              Container(
                width: Globals.maxPadding,
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _inputView(String title, Widget textFormField) {
    return Container(
      margin: EdgeInsets.symmetric(
          horizontal: Globals.maxPadding, vertical: Globals.minPadding),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              title,
              style: styleLanguage,
              softWrap: true,
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: textFormField,
                )
              ],
            ),
          ]),
    );
  }

  Widget _buildPhone(String phone) {
    return Container(
      color: HexColor("E5E5E5"),
      padding: EdgeInsets.symmetric(
          horizontal: Globals.maxPadding, vertical: Globals.maxPadding),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(bottom: Globals.minPadding),
            child: Text(
              '${_language.phone ?? ""}*',
              style: styleLanguage,
              softWrap: true,
            ),
          ),
          Container(
            padding: EdgeInsets.only(bottom: Globals.minPadding / 2),
            decoration: BoxDecoration(
                border:
                    Border(bottom: BorderSide(width: 1, color: primaryColor))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  _phone ?? "",
                  style: TextStyle(fontSize: 17),
                  softWrap: true,
                ),
                Icon(
                  Icons.check,
                  color: primaryColor,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
  bool isEmail(String em) {
    String p = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

    RegExp regExp = new RegExp(p);

    return regExp.hasMatch(em);
  }
  Widget emailFormField(BuildContext context) {
    return StreamBuilder(
        stream: _bloc.outputValidateEmail,
        initialData: null,
        builder: (_, snapshot) {
          return Container(
            child: Column(
              children: <Widget>[
                Container(
                  height: Globals.maxPadding * 2.2,
                  decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(
                    width: 1,
                            color: (_controllerMail.text == "" ||
                                snapshot.data == null)
                                ? Colors.grey
                                : (isEmail(
                                _controllerMail.text) ||
                                snapshot.data)
                                ? primaryColor
                                : Colors.red
                  ))),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: TextField(
                          controller: _controllerMail,
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.next,
                          focusNode: _focusNodeMail,
                          onChanged: (test) {
                          if (isEmail(test)) {
                            _bloc.setValidateEmail(true);
                          } else
                            _bloc.setValidateEmail(false);
                          if (isEmail(test) &&
                              _controllerReferCode.text != "") {
                            _bloc.setEnableButton(true);
                          } else
                            _bloc.setEnableButton(false);
                          },
                          onSubmitted: (term) {
                            if (isEmail(term)) {
                              _bloc.setValidateEmail(true);
                            } else
                              _bloc.setValidateEmail(false);
                            if (isEmail(term) &&
                                _controllerReferCode.text != "") {
                              _bloc.setEnableButton(true);
                            } else
                              _bloc.setEnableButton(false);
                            fieldFocusChange(context, _focusNodeMail, _focusNodeReferCode);
                          },
                          decoration: InputDecoration(
                            hintText: "kworker@gmail.com",
                            border: InputBorder.none,
                            suffixIcon: (snapshot.data != null && snapshot.data)
                                ? Icon(
                                    Icons.check,
                                    color: primaryColor,
                                  )
                                : null,
                            fillColor: Colors.white,
                          ),
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(50),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: Globals.minPadding / 2,
                ),
                (snapshot.data != null && !snapshot.data)
                    ? Text(
                        'Email không chính xác. Hãy nhập lại',
                        style: TextStyle(color: Colors.red),
                        textAlign: TextAlign.center,
                      )
                    : Container()
              ],
            ),
          );
        });
  }

  Widget referCodeFormField(BuildContext context) {
    return StreamBuilder(
        stream: _bloc.outputValidateReferCode,
        initialData: null,
        builder: (_, snapshot) {
          return Container(
            child: Column(
              children: <Widget>[
                Container(
                  height: Globals.maxPadding * 2.2,
                  decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(
                              width: 1,
                              color: (_controllerReferCode.text == "" ||
                                  snapshot.data == null)
                                  ? Colors.grey
                                  :
                                  snapshot.data
                                  ? primaryColor
                                  : Colors.red
                          ))),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: TextField(
                          controller: _controllerReferCode,
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.next,
                          focusNode: _focusNodeReferCode,
                          onChanged: (test) {
                            if (test.length>0) {
                              _bloc.setValidateReferCode(true);
                            } else
                              _bloc.setValidateReferCode(false);
                            if (isEmail( _controllerMail.text) &&
                                _controllerReferCode.text != "") {
                              _bloc.setEnableButton(true);
                            } else
                              _bloc.setEnableButton(false);
                          },
                          onSubmitted: (term) {
                            if (term.length>0) {
                              _bloc.setValidateReferCode(true);
                            } else
                              _bloc.setValidateReferCode(false);
                            if (isEmail( _controllerMail.text) &&
                                _controllerReferCode.text != "") {
                              _bloc.setEnableButton(true);
                            } else
                              _bloc.setEnableButton(false);
                            _focusNodeReferCode.unfocus();
                          },
                          decoration: InputDecoration(
                            hintText: "arhgs",
                            border: InputBorder.none,
                            suffixIcon: (snapshot.data != null && snapshot.data)
                                ? Icon(
                              Icons.check,
                              color: primaryColor,
                            )
                                : null,
                            fillColor: Colors.white,
                          ),
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(10),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: Globals.minPadding / 2,
                ),
                (snapshot.data != null && !snapshot.data)
                    ? Text(
                  'Email không chính xác. Hãy nhập lại',
                  style: TextStyle(color: Colors.red),
                  textAlign: TextAlign.center,
                )
                    : Container()
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        height: Globals.maxHeight -
            MediaQuery.of(context).padding.top -
            MediaQuery.of(context).padding.bottom,
        width: Globals.maxWidth,
        color: Colors.white,
        margin: EdgeInsets.symmetric(vertical: Globals.maxPadding),
        child: SingleChildScrollView(
          child: _formWidget(),
        ),
      ),
    );
  }
}
