import 'package:flutter/material.dart';
import 'package:k_user_global/common/interactions.dart';
import 'package:k_user_global/models/list_service_response_model.dart';
import 'package:k_user_global/resources/repository.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SearchJobBloc {
  final _repository = Repository();
  SharedPreferences _prefs;
  SearchJobBloc() {
    SharedPreferences.getInstance().then((data) {
      _prefs = data;
    });
  }

  final _streamListService = BehaviorSubject<List<Service>>();
  Observable<List<Service>> get outputListService => _streamListService.stream;
  setListService(List<Service> event) => _streamListService.sink.add(event);
  dispose() {
    _streamListService.close();
  }

  getListService(BuildContext context) async {
    showProgressDialog(context);
    ApiResponseData response = await _repository.getListServices(context);
    hideProgressDialog();
    if (response.success) {
      List<Service> listServices = List<Service>();
      ListServiceResponseModel listServiceResponse =
          ListServiceResponseModel.fromJson(response.data['data']);
//      if(listServiceResponse.service!=null && listServiceResponse.service.length>0)
        listServiceResponse.service.forEach((data){
          listServices.add((data));
        });
//      if(listServiceResponse.serviceProductivity!=null && listServiceResponse.serviceProductivity.length>0)
        listServiceResponse.serviceProductivity.forEach((data){
          listServices.add((data));
        });

        setListService(listServices);
    }
  }
}
