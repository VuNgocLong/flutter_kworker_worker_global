import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/models/list_service_response_model.dart';
import 'package:k_user_global/modules/login_module/register_jobs/modules/item_job_details/src/ui/jobs_details_widget.dart';
import 'package:k_user_global/modules/login_module/register_jobs/modules/search_job/src/bloc/search_job_bloc.dart';
import 'package:k_user_global/ultilites/hex_color.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SearchJobPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _StateSearch();
  }
}

class _StateSearch extends State<SearchJobPage> {
  SharedPreferences _prefs;
  Language _language;
  SearchJobBloc _bloc;
  @override
  void initState() {
    super.initState();
    _bloc = SearchJobBloc();
    _language = Language();
    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        } else {
          _language = Vietnamese();
        }
      });
    });
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _bloc.getListService(context);
    });
  }

  Widget _renderSearchBar(String hintText) {
    return Container(
//      padding: EdgeInsets.all(20.0),
//        height: 40.0,
        width: Globals.maxWidth,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0), color: colorSearchBar),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 10.0, right: 10.0),
              height: 16.0,
              width: 16.0,
              child: Image.asset(iconSearch),
            ),
            Expanded(
              child: TextField(
                style: TextStyle(
                    fontSize: 15.0, fontFamily: fontSFPro, color: Colors.black),
                decoration: InputDecoration(
                  hintText: hintText,
                  border: InputBorder.none,
                  hintStyle: TextStyle(
                      color: colorHint, fontSize: 15.0, fontFamily: fontSFPro),
                ),
              ),
            )
          ],
        ));
  }

  Widget _buildContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
              top: Globals.maxPadding, bottom: Globals.maxPadding * 2),
          child: InkWell(
            onTap: () {
              navigatorPop(context);
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Image.asset(
                  iconBackBlack,
                  height: 20,
                  width: 30,
//                  fit: BoxFit.cover,
                ),
              ],
            ),
          ),
        ),
        Text(
          "Chọn công việc",
          style: TextStyle(
              fontSize: 28,
              letterSpacing: 0.87,
              color: Colors.black,
              fontWeight: FontWeight.bold),
        ),
        Container(
          height: Globals.maxPadding,
        ),
        _renderSearchBar("Tìm công việc"),
        buildListItemJobs()
      ],
    );
  }

  List<Widget> labels(List<Service> list) {
    List<Widget> arr = [];
    list.forEach((model) {
      arr.add(itemJob(model));
    });
    return arr;
  }

  Widget itemJob(Service model) {
    return InkWell(
      onTap: () {
        navigatorPush(
            context,
            DetailsItemJobWidget(
              service: model,
              isRegister: true,
            ));
      },
      child: Column(
        children: <Widget>[
          Container(
            height: 60,
            width: 60,
            child: (model.icon != null && model.icon != "")
                ? CachedNetworkImage(
                    fit: BoxFit.cover,
                    imageUrl: urlServerImage + model.icon,
                    placeholder: (context, url) => Transform(
                        alignment: FractionalOffset.center,
                        transform: Matrix4.identity()..scale(0.05, 0.05),
                        child: CupertinoActivityIndicator()),
                    errorWidget: (context, url, error) =>
                        Image.asset(iconItemJob),
                  )
                : Image.asset(iconItemJob),
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(top: Globals.minPadding),
              child: AutoSizeText(
                model.name ?? "",
                style: TextStyle(fontSize: 13),
//                      maxLines: 1,
                maxFontSize: 13,
//                minFontSize: 8,
                softWrap: true,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget buildListItemJobs() {
    return StreamBuilder(
        stream: _bloc.outputListService,
        initialData: null,
        builder: (_, snapshot) {
          if (snapshot.data == null) return Container();
          List<Service> listService = snapshot.data;
          return Container(
            height: Globals.maxHeight,
            width: Globals.maxWidth,
            child: GridView.count(
                physics: ClampingScrollPhysics(),
//                scrollDirection: Axis.vertical,
                crossAxisCount: 4,
                shrinkWrap: true,
                cacheExtent: Globals.maxWidth*0.5,
//                crossAxisSpacing: Globals.minPadding,
//                mainAxisSpacing: Globals.minPadding,
                children: labels(listService)),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        height: Globals.maxHeight -
            MediaQuery.of(context).padding.top -
            MediaQuery.of(context).padding.bottom,
        width: Globals.maxWidth,
        margin: EdgeInsets.symmetric(
            vertical: Globals.maxPadding, horizontal: Globals.maxPadding),
        color: Colors.white,
        child: SingleChildScrollView(
          child: _buildContent(),
        ),
      ),
    );
  }
}
