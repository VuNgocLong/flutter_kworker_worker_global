import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/models/list_service_response_model.dart';
import 'package:k_user_global/modules/login_module/register_jobs/src/ui/register_jobs_widget.dart';
import 'package:k_user_global/ultilites/hex_color.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:k_user_global/widgets/widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DetailsItemJobWidget extends StatefulWidget {
  final Service service;
  final bool isRegister;
  const DetailsItemJobWidget({Key key, this.service, this.isRegister})
      : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return _StateDetails();
  }
}

class _StateDetails extends State<DetailsItemJobWidget>
    with TickerProviderStateMixin {
  TabController _controller;
  bool isCheckedTab1;
  bool isCheckedTab2;
  SharedPreferences _prefs;
  Language _language;
  @override
  void initState() {
    super.initState();
    isCheckedTab1 = false;
    isCheckedTab2 = false;
    _controller = TabController(length: 2, vsync: this);
    _language = Language();
    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        } else {
          _language = Vietnamese();
        }
      });
    });
  }

  Widget _buildTab() {
    return Container(
      margin: !widget.isRegister
          ? EdgeInsets.symmetric(
              vertical: Globals.minPadding, horizontal: Globals.maxPadding)
          : EdgeInsets.symmetric(
              vertical: Globals.minPadding,
            ),
      child: DefaultTabController(
        length: 2,
        initialIndex: 0,
        child: TabBar(
          isScrollable: false,
          unselectedLabelStyle: styleHint,
          labelStyle: TextStyle(
              fontSize: 17,
              color: Colors.black,
              letterSpacing: -0.24,
              fontWeight: FontWeight.bold),
          unselectedLabelColor: colorHint,
          labelColor: Colors.black,
          tabs: <Widget>[
            new Tab(text: "Không năng suất"),
            new Tab(text: "Có năng suất"),
          ],
          indicatorColor: primaryColor,
          indicatorSize: TabBarIndicatorSize.tab,
          controller: _controller,
        ),
      ),
    );
  }

  Widget _renderSearchBar(String hintText) {
    return Container(
        width: Globals.maxWidth,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0), color: colorSearchBar),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 10.0, right: 10.0),
              height: 16.0,
              width: 16.0,
              child: Image.asset(iconSearch),
            ),
            Expanded(
              child: TextField(
                style: TextStyle(
                    fontSize: 15.0, fontFamily: fontSFPro, color: Colors.black),
                decoration: InputDecoration(
                  hintText: hintText,
                  border: InputBorder.none,
                  hintStyle: TextStyle(
                      color: colorHint, fontSize: 15.0, fontFamily: fontSFPro),
                ),
              ),
            )
          ],
        ));
  }

  Widget _buildContent() {
    return Container(
      margin: EdgeInsets.only(
        bottom: Globals.maxPadding,
      ),
      padding: !widget.isRegister
          ? null
          : EdgeInsets.symmetric(horizontal: Globals.maxPadding),
      width: Globals.maxWidth,
      height: Globals.maxHeight - MediaQuery.of(context).padding.bottom,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            height: Globals.maxPadding,
          ),
          Container(
            margin: !widget.isRegister
                ? EdgeInsets.only(left: Globals.maxPadding)
                : null,
            child: Text(
              widget.service.name??"",
              style: TextStyle(
                  fontSize: 28,
                  letterSpacing: 0.87,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            height: Globals.minPadding,
          ),
          _buildTab(),
          Expanded(
            child: TabBarView(
              controller: _controller,
              children: <Widget>[
                _buildContentTab1(),
                _buildContentTab2(),
              ],
            ),
          ),
          Row(
            children: <Widget>[
              !widget.isRegister
                  ? Container(
                      width: Globals.maxPadding,
                    )
                  : Container(),
              Expanded(
                child: CustomButton(
                  text: "Xác nhận",
                  onTap: () {
                    if (widget.isRegister) {
                      navigatorPush(
                          context,
                          DetailsItemJobWidget(
                            isRegister: false,
                            service: widget.service,
                          ));
                    } else{
                      navigatorPop(context);
                      navigatorPop(context);
                      navigatorPopWithData(context,true);

                    }
                  },
                  isMaxWidth: true,
                  textStyle: styleTitleWhiteBold,
                  colorBorder: colorButton,
                  colorBackground: colorButton,
                  radius: 25,
                ),
              ),
              !widget.isRegister
                  ? Container(
                      width: Globals.maxPadding,
                    )
                  : Container(),
            ],
          )
        ],
      ),
    );
  }

  Widget _buildItemResult(String title, String major, String part) {
    return InkWell(
      onTap: () {
//        navigatorPush(context, SearchJobPage());
      },
      child: Container(
        margin: EdgeInsets.symmetric(
          vertical: Globals.minPadding,
          horizontal: Globals.maxPadding,
        ),
        padding: EdgeInsets.symmetric(
            horizontal: Globals.minPadding, vertical: Globals.minPadding),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.2),
              blurRadius: 10.0, // has the effect of softening the shadow
//            spreadRadius:
//            15.0, // has the effect of extending the shadow
              offset: Offset(
                0.06, // horizontal, move right 10
                0.6,
                // vertical, move down 10
              ),
            )
          ],
          borderRadius: BorderRadius.all(Radius.circular(8)),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                    height: Globals.maxPadding,
                    width: Globals.maxPadding,
                    margin: EdgeInsets.only(right: Globals.minPadding),
                    child: Image.asset(iconItemJob)),
                Text(
                  title,
                  style: styleTextBlack,
                ),
                Expanded(
                  child: Container(),
                ),
                InkWell(
                  child: Icon(
                    Icons.cancel,
                    color: Colors.white.withOpacity(0.5),
                  ),
                  onTap: () {},
                ),
              ],
            ),
            Container(
              height: Globals.maxPadding,
            ),
            Text(
              major,
              style: styleOrderContent,
            ),
            part != null
                ? Container(
                    margin: EdgeInsets.only(top: Globals.maxPadding),
                    child: Text(
                      part,
                      style: styleOrderContent,
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }

  Widget _buildContentTab1() {
    return widget.isRegister
        ? Column(
            children: <Widget>[
              Container(
                height: Globals.minPadding,
              ),
              _renderSearchBar("Tìm chuyên môn"),
              Expanded(
                child: ListView(
                  shrinkWrap: true,
                  children: _listItemTab1([
                    "Dọn nhà",
                    "Quét dọn",
                    "Vệ sinh máy lạnh",
                    "Dọn nhà",
                  ]),
                ),
              )
            ],
          )
        : Column(
            children: <Widget>[
              Expanded(
                child: ListView(
                  shrinkWrap: true,
                  children: [
                    "Dọn nhà",
                    "Quét dọn",
                  ].map((model) {
                    return _buildItemResult(model, "Có kinh nghiệm", null);
                  }).toList(),
                ),
              ),
            ],
          );
  }

  Widget _buildContentTab2() {
    return widget.isRegister
        ? Column(
            children: <Widget>[
              Container(
                height: Globals.minPadding,
              ),
              _renderSearchBar("Tìm chuyên môn"),
              Expanded(
                child: ListView(
                  shrinkWrap: true,
                  children: _listItemTab2(
                      ["Dọn nhà", "Quét dọn", "Vệ sinh máy lạnh"]),
                ),
              )
            ],
          )
        : Column(
            children: <Widget>[
              Expanded(
                child: ListView(
                  shrinkWrap: true,
                  children: [
                    "Dọn nhà",
                    "Quét dọn",
                  ].map((model) {
                    return _buildItemResult(
                        model, "Có kinh nghiệm", "4 bộ/ giờ");
                  }).toList(),
                ),
              ),
            ],
          );
    ;
  }

  Widget _buildOption(String title, bool value, Function onChange) {
    return Padding(
      padding: EdgeInsets.only(bottom: Globals.minPadding),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(
                fontWeight: FontWeight.normal,
                fontSize: 17,
                letterSpacing: 0.3,
                color: Colors.black),
          ),
          Checkbox(
            value: value,
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            activeColor: Colors.black,
            onChanged: onChange,
          ),
        ],
      ),
    );
  }

  List<Widget> _listItemTab1(List<String> list) {
    List<Widget> arr = [];
    list.forEach((data) {
      arr.add(_buildItemJobPart(data));
    });
    return arr;
  }

  List<Widget> _listItemTab2(List<String> list) {
    List<Widget> arr = [];
    list.forEach((data) {
      arr.add(_buildItemJobProductivity(data));
    });
    return arr;
  }

  Widget _buildItemJobPart(String name) {
    //KNS
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
              bottom: Globals.minPadding, top: Globals.minPadding),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                name,
                style: TextStyle(
                    fontSize: 20,
                    letterSpacing: 0.3,
                    fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        _buildOption("Có bằng cấp", isCheckedTab1, (event) {
          setState(() {
            isCheckedTab1 = event;
          });
        }),
        _buildOption("Có kinh nghiệm", isCheckedTab1, (event) {
          setState(() {
            isCheckedTab1 = event;
          });
        }),
        CustomLine(
          color: HexColor("333333"),
        ),
      ],
    );
  }

  Widget _buildItemJobProductivity(String name) {
    //NS
    List<String> jobs = ["2 bộ/ giờ", "3 bộ/ giờ", "3 bộ/ giờ", "4 bộ/ giờ"];
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
              bottom: Globals.minPadding, top: Globals.minPadding),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                name,
                style: TextStyle(
                    fontSize: 20,
                    letterSpacing: 0.3,
                    fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        _buildOption("Có bằng cấp", isCheckedTab1, (event) {
          setState(() {
            isCheckedTab1 = event;
          });
        }),
        _buildOption("Có kinh nghiệm", isCheckedTab1, (event) {
          setState(() {
            isCheckedTab1 = event;
          });
        }),
        CustomLine(
          color: HexColor("EBEBEB"),
        ),
        Column(
          children: jobs.map((model) {
            return _buildOption(model, isCheckedTab2, (event) {
              setState(() {
                isCheckedTab2 = event;
              });
            });
          }).toList(),
        ),
        CustomLine(
          color: HexColor("333333"),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        height: Globals.maxHeight,
//        constraints: BoxConstraints.expand(),
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                      top: MediaQuery.of(context).padding.top +
                          Globals.maxPadding,
                      bottom: Globals.minPadding),
                  child: InkWell(
                    onTap: () {
                      navigatorPop(context);
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          width: Globals.maxPadding,
                        ),
                        Image.asset(
                          iconBackBlack,
                          height: 20,
                          width: 30,
//                  fit: BoxFit.cover,
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: _buildContent(),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
