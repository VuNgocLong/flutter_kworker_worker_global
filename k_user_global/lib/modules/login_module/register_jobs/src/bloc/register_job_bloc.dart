import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/interactions.dart';
import 'package:k_user_global/models/list_service_response_model.dart';
import 'package:k_user_global/models/otp_response_model.dart';
import 'package:k_user_global/models/update_infomation_request_model.dart';
import 'package:k_user_global/modules/login_module/account_verification/src/ui/account_verification_widget.dart';
import 'package:k_user_global/resources/repository.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterJobBloc{
  SharedPreferences _prefs;
  RegisterJobBloc(){
    SharedPreferences.getInstance().then((data){
      _prefs = data;
    });
  }
  final _repository = Repository();
  final _streamEnableButton = BehaviorSubject<bool>();
  Observable<bool> get outputEnableButton => _streamEnableButton.stream;
  setEnableButton(bool event) => _streamEnableButton.sink.add(event);
  dispose(){
    _streamEnableButton.close();
  }


  updateInformation(BuildContext context, UpdateInfoRequestModel model)async{
    showProgressDialog(context);
    ApiResponseData responseData = await _repository.updateInformation(context, model);
    hideProgressDialog();
    if(responseData.success){

      ResponseOTPModel data = ResponseOTPModel.fromJson(responseData.data['data']);
      //  String token = data.tokenType + " " + data.accessToken;
      String token = data.accessToken;
      _prefs.setString(keyToken, token);
      _prefs.setString(
          keyIsUpdateInfo,
          data.isUpdatedInfo);
      navigatorPush(context, AccountVerificationPage());
    }
  }


}