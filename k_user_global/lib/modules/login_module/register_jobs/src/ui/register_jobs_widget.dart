import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/models/list_service_response_model.dart';
import 'package:k_user_global/models/update_infomation_request_model.dart';
import 'package:k_user_global/modules/login_module/account_verification/src/ui/account_verification_widget.dart';
import 'package:k_user_global/modules/login_module/register_jobs/modules/search_job/src/ui/search_job_widget.dart';
import 'package:k_user_global/modules/login_module/register_jobs/src/bloc/register_job_bloc.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:k_user_global/widgets/widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterJobsPage extends StatefulWidget {
  final UpdateInfoRequestModel model;

  const RegisterJobsPage({Key key, this.model}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return _StateJobs();
  }
}

class _StateJobs extends State<RegisterJobsPage> {
  RegisterJobBloc _bloc;
  SharedPreferences _prefs;
  Language _language;
  List<Service> _valueListService = new List<Service>();
  @override
  void initState() {
    _bloc = RegisterJobBloc();
    TabBarDefinition.instance.selectedJob = List<Service>();
    super.initState();
    _language = Language();
    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        } else {
          _language = Vietnamese();
        }
      });
    });
  }

  Widget _buildContent() {
    return Container(
      margin: EdgeInsets.only(
        bottom: Globals.maxPadding * 2,
      ),
      padding: EdgeInsets.symmetric(horizontal: Globals.maxPadding),
      width: Globals.maxWidth,
      height: Globals.maxHeight -
          MediaQuery.of(context).padding.top -
          MediaQuery.of(context).padding.bottom,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: ListView(
              shrinkWrap: true,
              children: <Widget>[
                Text(
                  _language.technique ?? "",
                  style: TextStyle(
                      fontSize: 34,
                      letterSpacing: 0.374,
                      color: Colors.black,
                      fontWeight: FontWeight.normal),
                ),
                Container(
                  height: Globals.minPadding,
                ),
                Text(
                  "Có thể chọn nhiều ngành nghề",
                  style: styleTextBlackNormal,
                ),
                Container(
                  height: Globals.maxPadding * 2,
                ),
                Row(
                  children: <Widget>[
                    _buildAddJob(),
                  ],
                )
              ],
            ),
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: StreamBuilder(
                    stream: _bloc.outputEnableButton,
                    initialData: false,
                    builder: (_, snapshot) {
                      return CustomButton(
                        text: "Hoàn tất",
                        onTap: () {
                          if (snapshot.data) {
                            _bloc.updateInformation(
                                context,
                                UpdateInfoRequestModel(
                                  phone: widget.model.phone,
                                  address: widget.model.address,
                                  birthday: widget.model.birthday,
                                  firstName: widget.model.firstName,
                                  lastName: widget.model.lastName,
                                  sexId: widget.model.sexId,
                                  referCode: widget.model.referCode,
                                  email: widget.model.email,
                                  services: [1],
                                ));
                          }
                        },
                        isMaxWidth: true,
                        textStyle: styleTitleWhiteBold,
                        colorBorder: snapshot.data
                            ? colorButton
                            : colorButton.withOpacity(0.2),
                        colorBackground: snapshot.data
                            ? colorButton
                            : colorButton.withOpacity(0.3),
                        radius: 25,
                      );
                    }),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget _buildAddJob() {
    return InkWell(
      onTap: () async {
        _valueListService.clear();
       bool result =  await navigatorPush(context, SearchJobPage());
//        _valueListService.addAll(TabBarDefinition.instance.selectedJob);
//        if (_valueListService.length > 0 && _valueListService != null)
//          _bloc.setEnableButton(true);
//        else
//          _bloc.setEnableButton(false);
        if (result != null && result)
          _bloc.setEnableButton(true);
        else
          _bloc.setEnableButton(false);
      },
      child: Container(
        padding: EdgeInsets.symmetric(
            horizontal: Globals.minPadding, vertical: Globals.minPadding),
        width: Globals.maxWidth / 2,
        height: Globals.maxWidth / 3,
        decoration: BoxDecoration(
          gradient: colorGradientGreen,
          borderRadius: BorderRadius.all(Radius.circular(8)),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                height: Globals.maxPadding,
                width: Globals.maxPadding,
                child: Image.asset(iconAddJob)),
            Text(
              _language.chooseNewMajor ?? "",
              style: TextStyle(
                  fontSize: 17,
                  color: Colors.white,
                  letterSpacing: -0.24,
                  fontWeight: FontWeight.bold),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        height: Globals.maxHeight,
//        constraints: BoxConstraints.expand(),
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                      top: MediaQuery.of(context).padding.top +
                          Globals.maxPadding,
                      bottom: Globals.minPadding),
                  child: InkWell(
                    onTap: () {
                      navigatorPop(context);
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          width: Globals.maxPadding,
                        ),
                        Image.asset(
                          iconBackBlack,
                          height: 20,
                          width: 30,
//                  fit: BoxFit.cover,
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: _buildContent(),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
