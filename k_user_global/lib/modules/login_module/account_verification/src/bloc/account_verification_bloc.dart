import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image/image.dart' as Img;
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/interactions.dart';
import 'package:k_user_global/models/account_verify_request_model.dart';
import 'package:k_user_global/models/account_verify_response_model.dart';
import 'package:k_user_global/modules/login_module/welcome_worker/welcome_worker_widget.dart';
import 'package:k_user_global/resources/repository.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:path/path.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AccountVerificationBloc {
  SharedPreferences _prefs;
  AccountVerificationBloc() {
    SharedPreferences.getInstance().then((data) {
      _prefs = data;
    });
  }
  final _repository = Repository();
  final _streamImageFileAvatar = BehaviorSubject<File>();

  final _streamImageFrontPassport = BehaviorSubject<File>();
  final _streamImageBehindPassport = BehaviorSubject<File>();
  final _streamImageHealthCA = BehaviorSubject<File>();
  final _streamImageCertificate = BehaviorSubject<File>();
  final _streamActiveStep = BehaviorSubject<int>();
  final _streamEnableButton = BehaviorSubject<bool>();

  Observable<File> get outputImageFrontPassport =>
      _streamImageFrontPassport.stream;
  Observable<File> get outputImageBehindPassport =>
      _streamImageBehindPassport.stream;
  Observable<File> get outputImageHealthCA => _streamImageHealthCA.stream;
  Observable<File> get outputImageCertificate => _streamImageCertificate.stream;
  Observable<File> get outputImageFileAvatar => _streamImageFileAvatar.stream;
  Observable<bool> get outputEnableButton => _streamEnableButton.stream;
  Observable<int> get outputActiveStep => _streamActiveStep.stream;

  setImageFrontPassport(File event) =>
      _streamImageFrontPassport.sink.add(event);
  setImageBehindPassport(File event) =>
      _streamImageBehindPassport.sink.add(event);
  setImageHealthCA(File event) => _streamImageHealthCA.sink.add(event);
  setImageCertificate(File event) => _streamImageCertificate.sink.add(event);
  setImageFileAvatar(File event) => _streamImageFileAvatar.sink.add(event);
  setEnableButton(bool event) => _streamEnableButton.sink.add(event);
  setActiveStep(int event) => _streamActiveStep.sink.add(event);

  dispose() {
    _streamEnableButton.close();
    _streamActiveStep.close();
    _streamImageFileAvatar.close();
    _streamImageFrontPassport.close();
    _streamImageBehindPassport.close();
    _streamImageHealthCA.close();
    _streamImageCertificate.close();
  }

  accountVerification(
      BuildContext context,
      File imageFront,
      File imageAvatar,
      File imageBehind,
      File healthCertificate,
      File degree,
      VerifyAccountRequestModel model) async {
//  print(utf8.decode(utf8.encode('identity_card_before')));
    showProgressDialog(context);
    ApiResponseData responseData = await _repository.accountVerification(
        context,
        imageFront,
        imageAvatar,
        imageBehind,
        healthCertificate,
        degree,
        model);
    hideProgressDialog();
    if (responseData.success) {
      _prefs.setBool(keyAccVerify, true);
      AccountVerifyResponseModel model =
          AccountVerifyResponseModel.fromJson(responseData.data['data']);
      _prefs.setString(keyModelAccVerify, json.encode(model));
      Navigator.of(context).popUntil((route) => route.isFirst);
      navigatorPushAndRemoveUntil(context, WelcomeWorker());
    } else {
      hideProgressDialog();
    }
  }

  File resizeImage(File imageResize) {
    Img.Image image = Img.decodeImage(imageResize.readAsBytesSync());
    Img.Image thumbnail = Img.copyResize(image, width: 120);
    imageResize.writeAsBytesSync(Img.encodePng(thumbnail));
    return imageResize;
  }
}
