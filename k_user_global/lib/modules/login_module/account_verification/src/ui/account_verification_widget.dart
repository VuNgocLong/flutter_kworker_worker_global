import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/models/account_verify_request_model.dart';
import 'package:k_user_global/modules/login_module/account_verification/src/bloc/account_verification_bloc.dart';
import 'package:k_user_global/modules/login_module/welcome_worker/welcome_worker_widget.dart';
import 'package:k_user_global/ultilites/custom_datetime_formater.dart';
import 'package:k_user_global/ultilites/custom_image_picker.dart';
import 'package:k_user_global/ultilites/hex_color.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:k_user_global/widgets/widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AccountVerificationPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _StateAccount();
  }
}

class _StateAccount extends State<AccountVerificationPage> {
  AccountVerificationBloc _bloc;
  SharedPreferences _prefs;
  Language _language;
  File imageFileCamera;
  File imageFrontPassport;
  File imageBehindPassport;
  File imageHealthCA;
  File imageCertificate;
  double size = Globals.maxWidth * 0.3;
  static final RegExp dateRegExp =
      RegExp(r'^(([1-2][0-9])|([1-9])|(3[0-1]))/((1[0-2])|([1-9]))/[0-9]{4}$');
  TextEditingController _controllerPassport = TextEditingController();
  TextEditingController _controllerDate = TextEditingController();
  FocusNode _focusPassport = FocusNode();
  FocusNode _focusDate = FocusNode();

  int _index;
  @override
  void initState() {
    _index = 1;
    _bloc = AccountVerificationBloc();
    super.initState();
    _language = Language();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Colors.white, // Color for Android
        statusBarBrightness:
            Brightness.dark // Dark == white status bar -- for IOS.
        ));
    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        } else {
          _language = Vietnamese();
        }
      });
    });
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _bloc.setActiveStep(1);
      if (_index == 3 && imageHealthCA != null && imageCertificate != null) {
        _bloc.setEnableButton(true);
      }
    });
  }

  _getPicture(int id) async {
    navigatorPop(context);
    if (id == 1) {
      imageFileCamera =
          await CustomImagePicker.pickImage(source: ImageSource.gallery);
      _bloc.setImageFileAvatar(imageFileCamera);
      _bloc.setEnableButton(true);
    } else if (id == 2) {
      imageFrontPassport =
          await CustomImagePicker.pickImage(source: ImageSource.gallery);
      _bloc.setImageFrontPassport(imageFrontPassport);
      if (imageBehindPassport != null &&
          _controllerDate.text != "" &&
          _controllerPassport.text != "") {
        _bloc.setEnableButton(true);
      }
    } else if (id == 3) {
      imageBehindPassport =
          await CustomImagePicker.pickImage(source: ImageSource.gallery);
      _bloc.setImageBehindPassport(imageBehindPassport);
      if (imageFrontPassport != null &&
          _controllerDate.text != "" &&
          _controllerPassport.text != "") {
        _bloc.setEnableButton(true);
      }
    } else if (id == 4) {
      imageHealthCA =
          await CustomImagePicker.pickImage(source: ImageSource.gallery);
      _bloc.setImageHealthCA(imageHealthCA);
      if (imageCertificate != null) {
        _bloc.setEnableButton(true);
      }
    } else if (id == 5) {
      imageCertificate =
          await CustomImagePicker.pickImage(source: ImageSource.gallery);
      _bloc.setImageCertificate(imageCertificate);
      if (imageHealthCA != null) _bloc.setEnableButton(true);
    } else
      return;
  }

  _takePicture(int id) async {
    navigatorPop(context);
    if (id == 1) {
      imageFileCamera =
          await CustomImagePicker.pickImage(source: ImageSource.camera);
      _bloc.setImageFileAvatar(imageFileCamera);
      _bloc.setEnableButton(true);
    } else if (id == 2) {
      imageFrontPassport =
          await CustomImagePicker.pickImage(source: ImageSource.camera);
      _bloc.setImageFrontPassport(imageFrontPassport);
      if (imageBehindPassport != null &&
          _controllerDate.text != "" &&
          _controllerPassport.text != "") {
        _bloc.setEnableButton(true);
      }
    } else if (id == 3) {
      imageBehindPassport =
          await CustomImagePicker.pickImage(source: ImageSource.camera);
      _bloc.setImageBehindPassport(imageBehindPassport);
      if (imageFrontPassport != null &&
          _controllerDate.text != "" &&
          _controllerPassport.text != "") {
        _bloc.setEnableButton(true);
      }
    } else if (id == 4) {
      imageHealthCA =
          await CustomImagePicker.pickImage(source: ImageSource.camera);
      _bloc.setImageHealthCA(imageHealthCA);
      if (imageCertificate != null) {
        _bloc.setEnableButton(true);
      }
    } else if (id == 5) {
      imageCertificate =
          await CustomImagePicker.pickImage(source: ImageSource.camera);
      _bloc.setImageCertificate(imageCertificate);
      if (imageHealthCA != null) _bloc.setEnableButton(true);
    } else
      return;
  }

  _showPickImageDialog(int id) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return GestureDetector(
            child: Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.white,
                child: ListView(
                  padding: EdgeInsets.all(0.0),
                  shrinkWrap: true,
                  children: <Widget>[
                    _setupItemPickImage(
                        Icons.image, "Thư viện", () => _getPicture(id)),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 15.0),
                      height: 1.0,
                      color: Colors.grey[300],
                    ),
                    _setupItemPickImage(
                        Icons.camera_alt, "Camera", () => _takePicture(id))
                  ],
                )),
            onTap: () {
              navigatorPop(context);
            },
            behavior: HitTestBehavior.opaque,
          );
        });
  }

  _setupItemPickImage(IconData icon, String title, Function onTap) {
    return InkWell(
      child: Container(
        padding: EdgeInsets.all(15.0),
        child: Row(
          children: <Widget>[
            Icon(
              icon,
              color: colorButton,
            ),
            Container(
              width: 15.0,
            ),
            Text(title)
          ],
        ),
      ),
      onTap: onTap,
    );
  }

  Widget _buildContent() {
    return Container(
      margin: EdgeInsets.only(
        bottom: Globals.maxPadding * 2,
      ),
      padding: EdgeInsets.symmetric(horizontal: Globals.maxPadding),
      width: Globals.maxWidth,
      height: Globals.maxHeight -
          MediaQuery.of(context).padding.top -
          MediaQuery.of(context).padding.bottom,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: ListView(
              shrinkWrap: true,
              children: <Widget>[
                Text(
                  "Xác minh tài khoản",
                  style: TextStyle(
                      fontSize: 34,
                      letterSpacing: 0.374,
                      color: Colors.black,
                      fontWeight: FontWeight.normal),
                ),
                Container(
                  height: Globals.minPadding,
                ),
                Text(
                  "Hoàn tất để bắt đầu nhận công việc",
                  style: styleTextBlackNormal,
                ),
                Container(
                  height: Globals.maxPadding * 2,
                ),
                StreamBuilder(
                    stream: _bloc.outputActiveStep,
                    initialData: 1,
                    builder: (_, snapshot) {
                      _index = snapshot.data;
                      return _index == 1
                          ? _buildStep1()
                          : _index == 2 ? _buildStep2() : _buildStep3();
                    }),
                Container(
                  height: Globals.maxPadding * 2,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    StreamBuilder(
                        stream: _bloc.outputEnableButton,
                        initialData: false,
                        builder: (_, snapshot) {
                          return _index != 3
                              ? CustomButton(
                                  text: _language.stringContinue ?? "",
                                  onTap: () {
                                    if (snapshot.data) {
                                      if (_index == 1) {
                                        _bloc.setActiveStep(2);
                                        _bloc.setEnableButton(false);
                                      } else if (_index == 2) {
                                        _bloc.setActiveStep(3);
                                        _bloc.setEnableButton(false);
                                      }
                                    }
                                  },
                                  isMaxWidth: false,
                                  textStyle: styleTitleWhiteBold,
                                  colorBorder: snapshot.data
                                      ? colorButton
                                      : colorButton.withOpacity(0.2),
                                  colorBackground: snapshot.data
                                      ? colorButton
                                      : colorButton.withOpacity(0.3),
                                  radius: 25,
                                )
                              : Expanded(
                                  child: CustomButton(
                                    text: "Hoàn thành",
                                    onTap: () {
                                      if (snapshot.data) {
                                        _bloc.accountVerification(
                                            context,
                                            imageFrontPassport,
                                            imageFileCamera,
                                            imageBehindPassport,
                                            imageHealthCA,
                                            imageCertificate,
                                            VerifyAccountRequestModel(
                                                dateRange: _controllerDate.text,
                                                idCode:
                                                    _controllerPassport.text));
                                      }
                                    },
                                    isMaxWidth: true,
                                    textStyle: styleTitleWhiteBold,
                                    colorBorder: snapshot.data
                                        ? colorButton
                                        : colorButton.withOpacity(0.2),
                                    colorBackground: snapshot.data
                                        ? colorButton
                                        : colorButton.withOpacity(0.3),
                                    radius: 25,
                                  ),
                                );
                        }),
                    _index != 3
                        ? Container(
                            width: Globals.maxPadding,
                          )
                        : Container()
                  ],
                ),
              ],
            ),
          ),
          _buildRowSteps(),
          Container(
            width: Globals.maxPadding,
          )
        ],
      ),
    );
  }

  Widget _buildStep2() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              _language.idCardNumberOrPassport ?? "",
              style: styleTextBlackBold,
            ),
            Text(
              "*",
              style: TextStyle(
                  color: Colors.red, fontSize: 17, fontWeight: FontWeight.bold),
            ),
          ],
        ),
        Container(
          height: Globals.minPadding,
        ),
        Container(
          margin: EdgeInsets.only(bottom: Globals.maxPadding),
          decoration: BoxDecoration(
              border: Border(
                  bottom: BorderSide(
                      width: 1,
                      color: _controllerPassport.text != ""
                          ? primaryColor
                          : Colors.grey))),
          child: Row(
            children: <Widget>[
              Expanded(child: passPortFormField(context, "237 576 987")),
            ],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              _language.createDate ?? "",
              style: styleTextBlackBold,
            ),
            Text(
              "*",
              style: TextStyle(
                  color: Colors.red, fontSize: 17, fontWeight: FontWeight.bold),
            ),
          ],
        ),
        Container(
          height: Globals.minPadding,
        ),
        Container(
          margin: EdgeInsets.only(bottom: Globals.maxPadding),
          decoration: BoxDecoration(
              border: Border(
                  bottom: BorderSide(
                      width: 1,
                      color: _controllerDate.text != ""
                          ? primaryColor
                          : Colors.grey))),
          child: Row(
            children: <Widget>[
              Expanded(child: dateFormField(context, "mm/dd/yyyy")),
            ],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "Hình ảnh xác thực ",
              style: styleTextBlackBold,
            ),
            Text(
              "*",
              style: TextStyle(
                  color: Colors.red, fontSize: 17, fontWeight: FontWeight.bold),
            ),
          ],
        ),
        Container(
          height: Globals.minPadding,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    _language.frontImage ?? "",
                    style: styleTextBlackNormal,
                  ),
                  Container(
                    height: Globals.minPadding,
                  ),
                  StreamBuilder(
                      stream: _bloc.outputImageFrontPassport,
                      initialData: imageFrontPassport,
                      builder: (_, snapshot) {
                        return InkWell(
                          onTap: () {
                            _showPickImageDialog(2);
                          },
                          child: Container(
                            child: Center(
                              child: Container(
                                width: size,
                                height: size,
                                child: snapshot.data == null
                                    ? Image.asset(
                                        pickerCamera,
                                        fit: BoxFit.cover,
                                      )
                                    : Image.file(
                                        snapshot.data,
                                        fit: BoxFit.cover,
                                      ),
                              ),
                            ),
                          ),
                        );
                      })
                ],
              ),
            ),
            Container(
              width: Globals.maxPadding,
            ),
            Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    _language.backImage ?? "",
                    style: styleTextBlackNormal,
                  ),
                  Container(
                    height: Globals.minPadding,
                  ),
                  StreamBuilder(
                      stream: _bloc.outputImageBehindPassport,
                      initialData: imageBehindPassport,
                      builder: (_, snapshot) {
                        return InkWell(
                          onTap: () {
                            _showPickImageDialog(3);
                          },
                          child: Container(
                            child: Center(
                              child: Container(
                                width: size,
                                height: size,
                                child: snapshot.data == null
                                    ? Image.asset(
                                        pickerCamera,
                                        fit: BoxFit.cover,
                                      )
                                    : Image.file(
                                        snapshot.data,
                                        fit: BoxFit.cover,
                                      ),
                              ),
                            ),
                          ),
                        );
                      })
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }

  TextFormField passPortFormField(
    BuildContext context,
    String hint,
  ) {
    return TextFormField(
      controller: _controllerPassport,
      keyboardType: TextInputType.datetime,
      textInputAction: TextInputAction.next,
      focusNode: _focusPassport,
      onChanged: (text) {
        if (imageFrontPassport != null &&
            _controllerDate.text != "" &&
            imageFrontPassport != null) {
          _bloc.setEnableButton(true);
        }
      },
      onFieldSubmitted: (term) {
        fieldFocusChange(context, _focusPassport, _focusDate);
      },
      validator: (value) {
        return null;
      },
      decoration: InputDecoration(
        hintText: hint,
        border: InputBorder.none,
        suffixIcon: _controllerPassport.text != ""
            ? Icon(
                Icons.check,
                color: primaryColor,
              )
            : null,
        fillColor: Colors.white,
      ),
      inputFormatters: [
        WhitelistingTextInputFormatter.digitsOnly,
        LengthLimitingTextInputFormatter(13),
      ],
    );
  }

  TextFormField dateFormField(
    BuildContext context,
    String hint,
  ) {
    return TextFormField(
      controller: _controllerDate,
      keyboardType: TextInputType.datetime,
      textInputAction: TextInputAction.done,
      focusNode: _focusDate,
      onChanged: (text) {
        if (imageFrontPassport != null &&
            (_controllerDate.text != "") &&
            imageFrontPassport != null &&
            _controllerPassport.text != "") {
          _bloc.setEnableButton(true);
        }
      },
      onFieldSubmitted: (term) {
        if (imageFrontPassport != null &&
            (_controllerDate.text != "") &&
            imageFrontPassport != null &&
            _controllerPassport.text != "") {
          _bloc.setEnableButton(true);
        }
        _focusDate.unfocus();
      },
      validator: (value) {
        if (value.isEmpty || !dateRegExp.hasMatch(value)) {
          return _language.createdDateErrorData ?? "";
        } else
          return null;
      },
      decoration: InputDecoration(
        hintText: hint,
        border: InputBorder.none,
        suffixIcon: _controllerDate.text != ""
            ? Icon(
                Icons.check,
                color: primaryColor,
              )
            : null,
        fillColor: Colors.white,
      ),
      inputFormatters: [
        LengthLimitingTextInputFormatter(10),
        WhitelistingTextInputFormatter.digitsOnly,
        DateTextFormatter()
      ],
    );
  }

  Widget _buildStep3() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "Giấy khám sức khoẻ ",
              style: styleTextBlackBold,
            ),
            Text(
              "*",
              style: TextStyle(color: Colors.red, fontSize: 17),
            ),
          ],
        ),
        Container(
          height: Globals.minPadding,
        ),
        Container(
          margin: EdgeInsets.only(bottom: Globals.maxPadding),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              StreamBuilder(
                  stream: _bloc.outputImageHealthCA,
                  initialData: imageHealthCA,
                  builder: (_, snapshot) {
                    return InkWell(
                      onTap: () {
                        _showPickImageDialog(4);
                      },
                      child: Container(
                        child: Center(
                          child: Container(
                            width: size,
                            height: size,
                            child: snapshot.data == null
                                ? Image.asset(
                                    pickerCamera,
                                    fit: BoxFit.cover,
                                  )
                                : Image.file(
                                    snapshot.data,
                                    fit: BoxFit.cover,
                                  ),
                          ),
                        ),
                      ),
                    );
                  }),
            ],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "Bằng cấp có liên quan ",
              style: styleTextBlackBold,
            ),
            Text(
              "*",
              style: TextStyle(color: Colors.red, fontSize: 17),
            ),
          ],
        ),
        Container(
          height: Globals.minPadding,
        ),
        Container(
          margin: EdgeInsets.only(bottom: Globals.maxPadding),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              StreamBuilder(
                  stream: _bloc.outputImageCertificate,
                  initialData: imageCertificate,
                  builder: (_, snapshot) {
                    return InkWell(
                      onTap: () {
                        _showPickImageDialog(5);
                      },
                      child: Container(
                        child: Center(
                          child: Container(
                            width: size,
                            height: size,
                            child: snapshot.data == null
                                ? Image.asset(
                                    pickerCamera,
                                    fit: BoxFit.cover,
                                  )
                                : Image.file(
                                    snapshot.data,
                                    fit: BoxFit.cover,
                                  ),
                          ),
                        ),
                      ),
                    );
                  }),
            ],
          ),
        )
      ],
    );
  }

  Widget _buildStep1() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              _language.takePortraitPhoto ?? "",
              style: styleTextBlackBold,
            ),
          ],
        ),
        Container(
          height: Globals.maxPadding,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Stack(
              alignment: Alignment.bottomCenter,
              children: <Widget>[
                StreamBuilder(
                    stream: _bloc.outputImageFileAvatar,
                    initialData: imageFileCamera,
                    builder: (_, snapshot) {
                      return Container(
                        height: Globals.maxHeight * 0.25,
                        width: Globals.maxHeight * 0.25,
                        padding:
                            EdgeInsets.symmetric(vertical: Globals.maxPadding),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: snapshot.data == null
                                ? DecorationImage(
                                    image: AssetImage(defaultAvatar),
                                    fit: BoxFit.cover)
                                : DecorationImage(
                                    image: FileImage(
                                      snapshot.data,
                                    ),
                                    fit: BoxFit.cover)),
                      );
                    }),
                InkWell(
                  onTap: () {
                    _showPickImageDialog(1);
                  },
                  child: Container(
                    margin: EdgeInsets.only(
                      left:
                          Globals.maxPadding * 5, //bottom: Globals.minPadding/4
                    ),
                    height: Globals.maxHeight * 0.05,
                    width: Globals.maxHeight * 0.05,
                    padding: EdgeInsets.symmetric(vertical: Globals.minPadding),
                    decoration: BoxDecoration(
                        color: colorButton, shape: BoxShape.circle),
                    child: Center(
                      child: ImageIcon(
                        AssetImage(iconCameraPng),
                        color: Colors.white,
                        size: 40,
                      ),
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ],
    );
  }

  Widget _buildRowSteps() {
    return StreamBuilder(
        stream: _bloc.outputActiveStep,
        initialData: 1,
        builder: (_, snapshot) {
          return Row(
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Row(
                  children: <Widget>[
                    Container(
                      height: Globals.maxPadding * 2,
                      width: Globals.maxPadding * 2,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          gradient: colorGradientBlue, shape: BoxShape.circle),
                      child: Text(
                        "1",
                        style: styleTextWhiteBold,
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: (snapshot.data == 2 || snapshot.data == 3)
                          ? Container(
                              height: Globals.minPadding / 2,
                              decoration:
                                  BoxDecoration(gradient: colorGradientBlue),
                            )
                          : Container(
                              height: Globals.minPadding / 2,
                              color: HexColor("F5F5F5"),
                            ),
                    ),
                    (snapshot.data == 2 || snapshot.data == 3)
                        ? Container(
                            height: Globals.maxPadding * 2,
                            width: Globals.maxPadding * 2,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                gradient: colorGradientBlue,
                                shape: BoxShape.circle),
                            child: Text(
                              "2",
                              style: styleTextWhiteBold,
                            ),
                          )
                        : Container(
                            height: Globals.maxPadding,
                            width: Globals.maxPadding,
                            decoration: BoxDecoration(
                                color: HexColor("F5F5F5"),
                                shape: BoxShape.circle),
                          ),
                    Expanded(
                      flex: 1,
                      child: snapshot.data == 3
                          ? Container(
                              height: Globals.minPadding / 2,
                              decoration:
                                  BoxDecoration(gradient: colorGradientBlue),
                            )
                          : Container(
                              height: Globals.minPadding / 2,
                              color: HexColor("F5F5F5"),
                            ),
                    ),
                    snapshot.data == 3
                        ? Container(
                            height: Globals.maxPadding * 2,
                            width: Globals.maxPadding * 2,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                gradient: colorGradientBlue,
                                shape: BoxShape.circle),
                            child: Text(
                              "3",
                              style: styleTextWhiteBold,
                            ),
                          )
                        : Container(
                            height: Globals.maxPadding,
                            width: Globals.maxPadding,
                            decoration: BoxDecoration(
                                color: HexColor("F5F5F5"),
                                shape: BoxShape.circle),
                          ),
                  ],
                ),
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        height: Globals.maxHeight,
//        constraints: BoxConstraints.expand(),
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                      top: MediaQuery.of(context).padding.top +
                          Globals.maxPadding,
                      bottom: Globals.minPadding),
                  child: InkWell(
                    onTap: () {
                      if (_index == 1) {
                        navigatorPop(context);
                      } else if (_index == 2) {
                        _bloc.setActiveStep(1);
                        _bloc.setEnableButton(true);
                      } else {
                        _bloc.setActiveStep(2);
                        _bloc.setEnableButton(true);
                      }
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          width: Globals.maxPadding,
                        ),
                        Image.asset(
                          iconBackBlack,
                          height: 20,
                          width: 30,
//                  fit: BoxFit.cover,
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: _buildContent(),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
