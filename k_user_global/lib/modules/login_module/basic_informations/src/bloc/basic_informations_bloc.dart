import 'package:flutter/material.dart';
import 'package:k_user_global/common/interactions.dart';
import 'package:k_user_global/models/list_sex_response_model.dart';
import 'package:k_user_global/resources/repository.dart';
import 'package:rxdart/rxdart.dart';

class BasicInformationBloc{
  final _repository = Repository();
  List<SexResponseModel> _listGender=  List<SexResponseModel>();
  final _streamEnableButton = BehaviorSubject<bool>();
  final _streamGender = BehaviorSubject<List<SexResponseModel>>();
  final _streamValidateDate = BehaviorSubject<bool>();
  Observable<List<SexResponseModel>> get outputGender => _streamGender.stream;
  Observable<bool> get outputValidateDate => _streamValidateDate.stream;
  Observable<bool> get outputEnableButton => _streamEnableButton.stream;
  setListGender(List<SexResponseModel> event) => _streamGender.sink.add(event);
  setValidateDate(bool event) => _streamValidateDate.sink.add(event);
  setEnableButton(bool event) => _streamEnableButton.sink.add(event);
  dispose(){
    _streamEnableButton.close();
    _streamGender.close();
    _streamValidateDate.close();
  }

  getSexList(BuildContext context)async{
   ApiResponseData response = await _repository.getSexList(context);
   if(response.success){
     if(response.data['data']!=null){
       response.data['data'].forEach((v){
         _listGender.add(new SexResponseModel.fromJson(v));
       });
     }else _listGender=[];
     setListGender(_listGender);
   }

  }
  changeSelected(SexResponseModel gender) {
    for (int i = 0; i < _listGender.length; i++) {
      _listGender[i].isSelected = false;
    }
    bool newValue = !gender.isSelected;
    int indexModel = _listGender
        .indexOf(_listGender.firstWhere((model) => model.id == gender.id));
    SexResponseModel newModel = _listGender[indexModel];
    newModel.isSelected = newValue;
    _listGender[indexModel] = newModel;
    setListGender(_listGender);
  }
}