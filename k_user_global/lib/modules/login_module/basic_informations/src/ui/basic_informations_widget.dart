import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/models/list_sex_response_model.dart';
import 'package:k_user_global/models/temp_address.dart';
import 'package:k_user_global/models/update_infomation_request_model.dart';
import 'package:k_user_global/modules/login_module/basic_informations/src/bloc/basic_informations_bloc.dart';
import 'package:k_user_global/modules/login_module/contact_informations/src/ui/contact_informations.dart';
import 'package:k_user_global/modules/login_module/register_address/src/ui/register_address_widget.dart';
import 'package:k_user_global/ultilites/custom_datetime_formater.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:k_user_global/widgets/widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BasicInformationPage extends StatefulWidget {
  final String phone;

  const BasicInformationPage({Key key, this.phone}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return _StateInfo();
  }
}

class _StateInfo extends State<BasicInformationPage> {
  final formKey = GlobalKey<FormState>();
  TextEditingController _controllerName = TextEditingController();
  TextEditingController _controllerLastName = TextEditingController();
  TextEditingController _controllerBirthDay = TextEditingController();
  TextEditingController _controllerAddress = TextEditingController();
  FocusNode _focusNodeName = FocusNode();
  FocusNode _focusNodeLastName = FocusNode();
  FocusNode _focusNodeBirthDay = FocusNode();
  FocusNode _focusNodeAddress = FocusNode();
  static final RegExp dateRegExp = RegExp(
      r'^([0]?[1-9]|[1|2][0-9]|[3][0|1])/([0]?[1-9]|[1][0-2])/([0-9]{4}|[0-9]{2})$');
  SharedPreferences _prefs;
  Language _language;
  BasicInformationBloc _bloc;
  TempAddress _tempAddress;
  double _lat = 0.0;
  double lng = 0.0;
  int genderId = 1;
  StreamSubscription<Position> _stream;
  @override
  void initState() {
    super.initState();
    _bloc = BasicInformationBloc();
    _language = Language();
    _focusNodeAddress.addListener(textFieldFocusDidChange);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Colors.white, // Color for Android
        statusBarBrightness:
            Brightness.dark // Dark == white status bar -- for IOS.
        ));
    SharedPreferences.getInstance().then((data) {
      _prefs = data;
      setState(() {});
      String _model = _prefs.getString(keyLanguage);
      if (_model != null) {
        if (_model == 'vi') {
          _language = Vietnamese();
        } else if (_model == 'eng') {
          _language = English();
        } else if (_model == 'korean') {
          _language = Korean();
        } else if (_model == 'japanese') {
          _language = Japanese();
        }
      }
    });
    WidgetsBinding.instance.addPostFrameCallback((_) {
      checkGPS(context).then((event) {
        if (event) {
          Geolocator geolocator = Geolocator()
            ..forceAndroidLocationManager = true;
          _stream = geolocator
              .getPositionStream(LocationOptions(
              accuracy: LocationAccuracy.high, timeInterval: 500))
              .listen((position) async {
            await _stream.cancel();
            _stream = null;
            final coordinates = new Coordinates(position.latitude, position.longitude);
            var addresses =
            await Geocoder.local.findAddressesFromCoordinates(coordinates);
            var first = addresses.first;
            if (first.addressLine != null && first.addressLine != "") {
             setState(() {
               _controllerAddress.value = new TextEditingController.fromValue(
                   new TextEditingValue(text: first.addressLine))
                   .value;
             });
            }
          });
        }
      });
      _bloc.getSexList(context);
    });
  }

  Future textFieldFocusDidChange() async {
    if (_focusNodeAddress.hasFocus) {
      FocusScope.of(context).requestFocus(new FocusNode());
      TempAddress model = await Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => RegisterAddressPage()));

      if (model != null) {
        _tempAddress = model;
        var addresses =
            await Geocoder.local.findAddressesFromQuery(_tempAddress.address);
        var first = addresses.first;
       setState(() {
         _controllerAddress.text = first.addressLine;
         _lat = first.coordinates.latitude;
         lng = first.coordinates.longitude;
         if (_tempAddress.note != null) {
           _controllerAddress.text =
               _tempAddress.address + "\n " + _tempAddress.note;
         } else {
           _controllerAddress.text = _tempAddress.address;
         }
         if (_controllerBirthDay.text != "" &&
             _controllerLastName.text != "" &&
             _controllerName.text != "") {
           _bloc.setEnableButton(true);
         } else
           _bloc.setEnableButton(false);
       });
      }
    }
  }

  Widget renderItem(SexResponseModel event) {
    if (event.isSelected) genderId = event.id;
    return Expanded(
        flex: 1,
        child: InkWell(
          onTap: () => _bloc.changeSelected(event),
          child: Container(
            child: Column(
              children: <Widget>[
                Text(
                  event.name ?? "",
                  style: TextStyle(
                      fontSize: 17,
                      color: event.isSelected ? colorButton : Colors.black),
                ),
                Container(
                  height: 1,
                  color: event.isSelected ? colorButton : Colors.white,
                )
              ],
            ),
          ),
        ));
  }

  Widget _inputView(
      BuildContext context,
      String title,
      String hint,
      TextEditingController _controller,
      bool gender,
      int index,
      FocusNode focus,
      FocusNode afterFocus) {
//    List<String> listGender = ["Nam", "Nữ", "Khác"];
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
              bottom: index != 3 ? Globals.minPadding : Globals.maxPadding),
          child: Text(
            title,
            style: styleLanguage,
            softWrap: true,
          ),
        ),
        Container(
            height:
                index == 3 ? Globals.maxPadding * 4 : Globals.maxPadding * 2.2,
            child: gender
                ? StreamBuilder(
                    stream: _bloc.outputGender,
                    initialData: null,
                    builder: (_, snapshot) {
                      if (snapshot.data == null) return Container();
                      List<SexResponseModel> listGender = snapshot.data;
                      return Row(
                        children: listGender
                            .asMap()
                            .map((index, model) {
                              return MapEntry(index, this.renderItem(model));
                            })
                            .values
                            .toList(),
                      );
                    })
                : index == 3
                    ? birthDayFormField(context, hint)
                    : Container(
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    width: 1,
                                    color:
                                        (_controller.text != "" && index != 3)
                                            ? primaryColor
                                            : Colors.grey))),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                                child: TextField(
                              controller: _controller,
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.next,
                              focusNode: focus,
                              onChanged: (test) {
                                if (index == 1) {
                                  if (test != "" &&
                                      _controllerBirthDay.text != "" &&
                                      dateRegExp
                                          .hasMatch(_controllerBirthDay.text) &&
                                      _controllerName.text != "" &&
                                      _controllerAddress.text != "") {
                                    _bloc.setEnableButton(true);
                                  } else
                                    _bloc.setEnableButton(false);
                                } else if (index == 2) {
                                  if (test != "" &&
                                      _controllerBirthDay.text != "" &&
                                      dateRegExp
                                          .hasMatch(_controllerBirthDay.text) &&
                                      _controllerLastName.text != "" &&
                                      _controllerAddress.text != "") {
                                    _bloc.setEnableButton(true);
                                  } else
                                    _bloc.setEnableButton(false);
                                }
                              },
                              onSubmitted: (data) {
                                if (index == 1) {
                                  fieldFocusChange(context, focus, afterFocus);
                                  if (_controllerLastName.text != "" &&
                                      _controllerBirthDay.text != "" &&
                                      dateRegExp
                                          .hasMatch(_controllerBirthDay.text) &&
                                      _controllerName.text != "" &&
                                      _controllerAddress.text != "") {
                                    _bloc.setEnableButton(true);
                                  } else
                                    _bloc.setEnableButton(false);
                                } else if (index == 2) {
                                  fieldFocusChange(context, focus, afterFocus);
                                  if (_controllerName.text != "" &&
                                      _controllerBirthDay.text != "" &&
                                      dateRegExp
                                          .hasMatch(_controllerBirthDay.text) &&
                                      _controllerLastName.text != "" &&
                                      _controllerAddress.text != "") {
                                    _bloc.setEnableButton(true);
                                  } else
                                    _bloc.setEnableButton(false);
                                } else
                                  focus.unfocus();
                              },
                              decoration: InputDecoration(
                                hintText: hint,
                                border: InputBorder.none,
                                suffixIcon: _controller.text != ""
                                    ? Icon(
                                        Icons.check,
                                        color: primaryColor,
                                      )
                                    : null,
                              ),
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(50),
                              ],
                            )),
                          ],
                        ),
                      )),
        Container(
          height: Globals.maxPadding,
        )
      ],
    );
  }

  Widget birthDayFormField(
    BuildContext context,
    String hint,
  ) {
    return StreamBuilder(
        stream: _bloc.outputValidateDate,
        initialData: null,
        builder: (_, snapshot) {
          return Container(
            child: Column(
              children: <Widget>[
                Container(
                  height: Globals.maxPadding * 2.2,
                  decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(
                              width: 1,
                              color: (_controllerBirthDay.text == "" ||
                                      snapshot.data == null)
                                  ? Colors.grey
                                  : (dateRegExp.hasMatch(
                                              _controllerBirthDay.text) ||
                                          snapshot.data)
                                      ? primaryColor
                                      : Colors.red))),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: TextField(
                          controller: _controllerBirthDay,
                          keyboardType: TextInputType.datetime,
                          textInputAction: TextInputAction.next,
                          focusNode: _focusNodeBirthDay,
                          onChanged: (test) {
                            if (!dateRegExp.hasMatch(test)) {
                              _bloc.setValidateDate(false);
                            } else
                              _bloc.setValidateDate(true);
                            if (test != "" &&
                                dateRegExp.hasMatch(_controllerBirthDay.text) &&
                                _controllerName.text != "" &&
                                _controllerAddress.text != "" &&
                                _controllerLastName.text != "") {
                              _bloc.setEnableButton(true);
                            } else
                              _bloc.setEnableButton(false);
                          },
                          onSubmitted: (term) {
                            _focusNodeBirthDay.unfocus();
                            if (!dateRegExp.hasMatch(term)) {
                              _bloc.setValidateDate(false);
                            } else
                              _bloc.setValidateDate(true);
                            if (term != "" &&
                                dateRegExp.hasMatch(_controllerBirthDay.text) &&
                                _controllerName.text != "" &&
                                _controllerAddress.text != "" &&
                                _controllerLastName.text != "") {
                              _bloc.setEnableButton(true);
                            } else
                              _bloc.setEnableButton(false);
                          },
//      validator: (value) {
//        if (value.isEmpty ||
//            !dateRegExp.hasMatch(value)) {
//          return 'Ngày sinh không đúng định dạng. Vui lòng kiểm tra lại!';
//        } else
//          return null;
//      },
                          decoration: InputDecoration(
                            hintText: hint,
                            border: InputBorder.none,
                            suffixIcon:
                                dateRegExp.hasMatch(_controllerBirthDay.text)
                                    ? Icon(
                                        Icons.check,
                                        color: primaryColor,
                                      )
                                    : null,
                            fillColor: Colors.white,
                          ),
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(10),
                            DateTextFormatter()
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: Globals.minPadding / 2,
                ),
                (snapshot.data != null && !snapshot.data)
//                    ||
//                        (!dateRegExp.hasMatch(_controllerBirthDay.text))
                    ? Text(
                        'Ngày sinh không đúng định dạng. Vui lòng kiểm tra lại!',
                        style: TextStyle(color: Colors.red),
                        textAlign: TextAlign.center,
                      )
                    : Container()
              ],
            ),
          );
        });
  }

  Form formWidget() {
    return Form(
      key: formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(
                  top: Globals.maxPadding, bottom: Globals.maxPadding * 2),
              child: Container()
//            InkWell(
//              onTap: () {
//                navigatorPop(context);
//              },
//              child: Row(
//                mainAxisAlignment: MainAxisAlignment.start,
//                children: <Widget>[
//                  Image.asset(
//                    iconBackBlack,
//                    height: 20,
//                    width: 30,
////                  fit: BoxFit.cover,
//                  ),
//                ],
//              ),
//            ),
              ),
          Text(
            _language.basicInfo ?? "",
            style: TextStyle(
                fontSize: 34,
                letterSpacing: 0.374,
                color: Colors.black,
                fontWeight: FontWeight.normal),
          ),
          Container(
            height: Globals.minPadding,
          ),
          Text(
            _language.inputFullyToComplete ?? "",
            style: styleTextBlackNormal,
          ),
          Container(
            height: Globals.maxPadding * 2,
          ),
          _inputView(
              context,
              "${_language.lastName ?? ""}*",
              _language.inputYourName ?? "",
              _controllerName,
              false,
              1,
              _focusNodeName,
              _focusNodeLastName),
          _inputView(
              context,
              "${_language.firstName ?? ""}*",
              "Nhập họ",
              _controllerLastName,
              false,
              2,
              _focusNodeLastName,
              _focusNodeBirthDay),
          _inputView(context, "Giới tính*", null, null, true, 0, null, null),
          _inputView(
              context,
              "${_language.dob ?? ""}*",
              "dd/MM/yyyy",
              _controllerBirthDay,
              false,
              3,
              _focusNodeBirthDay,
              _focusNodeAddress),
          _inputView(
              context,
              "${_language.permanent ?? ""}*",
              "Nhập địa chỉ thường trú",
              _controllerAddress,
              false,
              4,
              _focusNodeAddress,
              null),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              StreamBuilder(
                  stream: _bloc.outputEnableButton,
                  initialData: false,
                  builder: (_, snapshot) {
                    return CustomButton(
                      text: _language.stringContinue ?? "",
                      onTap: () {
                        if (snapshot.data) {
                          navigatorPush(
                              context,
                              ContactInformationPage(
                                model: UpdateInfoRequestModel(
                                  phone: widget.phone,
                                  address: _controllerAddress.text,
                                  birthday: _controllerBirthDay.text,
                                  firstName: _controllerLastName.text,
                                  lastName: _controllerName.text,
                                  sexId: genderId,
                                ),
                              ));
                        }
                      },
                      isMaxWidth: false,
                      textStyle: styleTitleWhiteBold,
                      colorBorder: snapshot.data
                          ? colorButton
                          : colorButton.withOpacity(0.2),
                      colorBackground: snapshot.data
                          ? colorButton
                          : colorButton.withOpacity(0.3),
                      radius: 25,
                    );
                  })
            ],
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
//    Globals().init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        height: Globals.maxHeight -
            MediaQuery.of(context).padding.top -
            MediaQuery.of(context).padding.bottom,
        width: Globals.maxWidth,
        margin: EdgeInsets.symmetric(
            vertical: Globals.maxPadding, horizontal: Globals.maxPadding),
        color: Colors.white,
        child: SingleChildScrollView(
          child: formWidget(),
        ),
      ),
    );
  }
}
