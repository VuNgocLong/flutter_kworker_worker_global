import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/interactions.dart';
import 'package:k_user_global/models/location_model.dart';
import 'package:k_user_global/models/place_detail_model.dart';
import 'package:k_user_global/models/prediction_model.dart';
import 'package:k_user_global/resources/repository.dart';
import 'package:k_user_global/ultilites/global.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterAddressBloc{
  final _repository = Repository();
  SharedPreferences _prefs;
  bool _isSearching = false;
  List<AddressModel> _models = List<AddressModel>();

  RegisterAddressBloc() {
    SharedPreferences.getInstance().then((data) {
      _prefs = data;
    });
  }

  final _streamSearch = BehaviorSubject<String>();
  final _streamLocation = BehaviorSubject<String>();
  final _streamLocationPosition = BehaviorSubject<AddressModel>();
  final _streamModel = BehaviorSubject<PredictionModel>();
  final _streamAutocomplete = BehaviorSubject<bool>();

  Observable<String> get outputSearch => _streamSearch.stream;
  Observable<String> get outputLocation => _streamLocation.stream;
  Observable<AddressModel> get outputLocationPosition => _streamLocationPosition.stream;
  Observable<PredictionModel> get outputModel => _streamModel.stream;
  Observable<bool> get outputAutocomplete => _streamAutocomplete.stream;

//  setSearch(BuildContext context, String event) async {
//    _streamSearch.sink.add(event);
//    if(event.trim() == ""){
//      _models = List<AddressModel>();
//      setModel(_models);
//    }
//    else
//      placeAutocomplete(context, event);
//  }
  setLocation(String event) => _streamLocation.sink.add(event);
  setLocationPosition(AddressModel event) => _streamLocationPosition.sink.add(event);
  setModel(PredictionModel event) => _streamModel.sink.add(event);
  setAutocomplete(bool event) => _streamAutocomplete.sink.add(event);

  dispose(){
    _streamSearch.close();
    _streamLocation.close();
    _streamModel.close();
    _streamLocationPosition.close();
    _streamAutocomplete.close();
  }

  getAutocomplete() async {
    if(_prefs == null) _prefs = await SharedPreferences.getInstance();
    setAutocomplete(true);
//    setAutocomplete(_prefs.getBool(keyAutocomplete)??false);
  }

  placeAutocomplete(BuildContext context, String text) async {
    if (text.trim() == ""){
      setModel(null);
    } else {
      if(!_isSearching){
        _isSearching = true;
        ApiResponseData response = await _repository.placeAutocomplete(context, text);

        if (response.success) {
          PredictionModel model = PredictionModel.fromJson(response.data);
          print(model.status);
          if(model.status == "OK"){

            setModel(model);
          }
        }
        _isSearching = false;
      }
    }
  }
  addLocation(BuildContext context, String address) async {
    Map<String, dynamic>  params = Map<String, dynamic>();
    params['address'] = address;
    var addresses = await Geocoder.local.findAddressesFromQuery(address);
    var first = addresses.first;
//    params['address'] = first.addressLine;
//    params['lat'] = first.coordinates.latitude;
//    params['lng'] = first.coordinates.longitude;
    setLocation(first.addressLine);
    dialogChooLocation(context, first.addressLine, first.coordinates.longitude,first.coordinates.latitude,true);
//    showProgressDialog(context);
//    ApiResponseData responseData = await _repository.addLocation(context, params);
//    hideProgressDialog();
//    if (responseData.success == true){
//      navigatorPop(context);
//    }
  }

  placeDetail(BuildContext context, String id) async {
    showProgressDialog(context);
    ApiResponseData response = await _repository.placeDetail(context, id);
    hideProgressDialog();
    if (response.success) {
      PlaceDetailModel model = PlaceDetailModel.fromJson(response.data);

      if(model.status == "OK"){
        String streetNumber, street, ward, district, city, country;
        try{streetNumber = model.result.addressComponents.firstWhere((model) => model.types.contains("street_number")).longName;
        }catch(_){streetNumber = "";}
        try{street = model.result.addressComponents.firstWhere((model) => model.types.contains("route")).longName;
        }catch(_){street = "";}
        try{ward = model.result.addressComponents.firstWhere((model) => model.types.contains("neighborhood")).longName;
        }catch(_){ward = "";}
        try{district = model.result.addressComponents.firstWhere((model) => model.types.contains("administrative_area_level_2")).longName;
        }catch(_){district = "";}
        try{city = model.result.addressComponents.firstWhere((model) => model.types.contains("administrative_area_level_1")).longName;
        }catch(_){city = "";}
        try{country = model.result.addressComponents.firstWhere((model) => model.types.contains("country")).longName;
        }catch(_){country = "";}
        AddressModel addressModel = AddressModel(
          latitude: model.result.geometry.location.lat,
          longitude: model.result.geometry.location.lng,
          streetNumber: streetNumber,
          street: street,
          ward: ward,
          district: district,
          city: city,
          country: country,
          address: model.result.formattedAddress,
        );
//        navigatorPushPopup(context, DetailAddress(addressModel, false));
      }
      else{
//        openDialog(context, addressError);
      }
    }
  }

   Future<String> geocode(BuildContext context, double lat, double lng) async {
    final coordinates = new Coordinates(lat, lng);
    var address =
    await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = address.first;
    print("${first.addressLine}");
    Global.currentAddress = first.addressLine.toString();
    Global.currentLng = first.coordinates.longitude;
    Global.currentLat = first.coordinates.latitude;
    _prefs.setString(keyCurrentAddress, first.addressLine.toString());
    setLocation(first.addressLine);
    return first.addressLine;
  }
}