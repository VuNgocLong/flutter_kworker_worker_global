import 'dart:async';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/models/location_model.dart';
import 'package:k_user_global/models/prediction_model.dart';
import 'package:k_user_global/models/temp_address.dart';
import 'package:k_user_global/modules/login_module/register_address/map_module/src/ui/map_ui_widget.dart';
import 'package:k_user_global/modules/login_module/register_address/src/bloc/register_address_bloc.dart';
import 'package:k_user_global/ultilites/hex_color.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:k_user_global/widgets/widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterAddressPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _StateAddress();
}

class _StateAddress extends State<RegisterAddressPage>
    with WidgetsBindingObserver {
  SharedPreferences _prefs;
  Language _language;
  final noteController = TextEditingController();
  FocusNode _focusAddress = FocusNode();
  TextEditingController _controllerAddress = TextEditingController();
  // bloc init
  RegisterAddressBloc _bloc;

  // callback
  LocationCallback _callback;
  StreamSubscription<Position> _stream;
  @override
  void initState() {
    super.initState();
    _bloc = RegisterAddressBloc();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
        statusBarColor: greenLight, // Color for Android
        statusBarBrightness:
            Brightness.light // Dark == white status bar -- for IOS.
        ));
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _bloc.getAutocomplete();
      checkGPS(context).then((event) {
        if (event) {
          Geolocator geolocator = Geolocator()
            ..forceAndroidLocationManager = true;
          _stream = geolocator
              .getPositionStream(LocationOptions(
                  accuracy: LocationAccuracy.high, timeInterval: 500))
              .listen((position) async {
            await _stream.cancel();
            _stream = null;
            String address = await _bloc.geocode(
                context, position.latitude, position.longitude);
            if (address != null && address != "") {
              _controllerAddress.value = new TextEditingController.fromValue(
                  new TextEditingValue(text: address))
                  .value;
              _bloc.placeAutocomplete(context, address);
            }
          });
        }
      });
    });

    WidgetsBinding.instance.addObserver(this);
    _language = Language();
    SharedPreferences.getInstance().then((data) {
      setState(() {
        _prefs = data;
        String _model = _prefs.getString(keyLanguage);
        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        }
      });
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
            statusBarColor: Colors.white, // Color for Android
            statusBarBrightness:
                Brightness.dark // Dark == white status bar -- for IOS.
            ));
        navigatorPop(context);
        return;
      },
      child: Scaffold(
        body: Container(
          height: Globals.maxHeight,
          color: Colors.black,
          constraints: BoxConstraints.expand(),
          child: Stack(
            children: <Widget>[
              _setupBackground(),
              Positioned(
                left: 0.0,
                top: 0.0,
                right: 0.0,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    CustomAppBar(
                        title: _language.address ??
                            "", //_language.stringChooseLocation ??
                        onTapLeading: () {
                          SystemChrome.setSystemUIOverlayStyle(
                              SystemUiOverlayStyle.dark.copyWith(
                                  statusBarColor:
                                      Colors.white, // Color for Android
                                  statusBarBrightness: Brightness
                                      .dark // Dark == white status bar -- for IOS.
                                  ));
                          navigatorPop(context);
                        },
                        isClose: true,
                        isRight: true,
                        iconRight: iconEditLocation,
                        rightFunc: () async {
                          TempAddress temp = await navigatorPush(
                              context,
                              MapScreen(
                                check: 1,
                              ));
                          if (temp != null) {
                            Navigator.of(context).pop(temp);
//                        _callback();
                          }
                        }),
                    _renderBody()
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _renderItemAddress(Predictions record) {
    return InkWell(
      child: Container(
          margin: EdgeInsets.symmetric(
            horizontal: Globals.minPadding,
          ),
          padding: EdgeInsets.symmetric(vertical: Globals.minPadding),
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border(bottom: BorderSide(color: grayLightColor))),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: AutoSizeText(
                  record.description,
                  style: TextStyle(fontSize: 15.0, fontFamily: fontSFPro),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
//                Container(
//                  padding: EdgeInsets.only(right: 15.0),
//                  child: Image.asset(
//                    'assets/icons/plus.png',
//                    height: 15.0,
//                    width: 15.0,
//                  ),
//                )
            ],
          )),
      onTap: () {
        _bloc.addLocation(
          context,
          record.description,
        );
      },
    );
  }

  Widget _renderBody() {
    return Column(
      children: <Widget>[
        Container(
//          padding: EdgeInsets.fromLTRB(Globals.maxPadding, Globals.maxPadding * 2,
//              Globals.maxPadding, Globals.maxPadding * 2),
          width: Globals.maxWidth,
          height: Globals.maxHeight -
              AppBar().preferredSize.height -
              MediaQuery.of(context).padding.top,
          decoration: BoxDecoration(
            color: HexColor('F2F2F2'),
            borderRadius: BorderRadius.all(Radius.circular(30.0)),
          ),
          child: Column(
//            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                  child: Stack(
                children: <Widget>[
                  Container(
                    child: ListView(
                      children: <Widget>[
                        Container(
//                          height: 52.0,
                          margin: EdgeInsets.symmetric(
                              horizontal: Globals.minPadding,
                              vertical: Globals.minPadding),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(8.0)),
                          child: Row(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(
                                  left: Globals.minPadding,
                                ),
                                child: Image.asset(
                                  iconMarker,
                                  width: 30,
                                  height: 30,
                                ),
                              ),
                              Expanded(
                                child: TextField(
                                  style: TextStyle(fontSize: 15.0),
                                  focusNode: _focusAddress,
                                  controller: _controllerAddress,
                                  decoration: InputDecoration(
                                    hintText: 'Nhập địa chỉ',
                                    hintStyle: TextStyle(
                                        color: grayLightColor, fontSize: 15.0),
                                    border: InputBorder.none,
                                    contentPadding: EdgeInsets.only(
                                        left: Globals.minPadding),
                                  ),
                                  onChanged: (value) =>
                                      _bloc.placeAutocomplete(context, value),
                                ),
                              ),
                              StreamBuilder(
                                stream: _bloc.outputLocation,
                                initialData: "",
                                builder: (_, snapshot) {
                                  return snapshot.data.toString().trim() == ""
                                      ? Container()
                                      : InkWell(
                                          child: Container(
                                            padding: EdgeInsets.all(
                                                Globals.minPadding),
                                            child: Icon(
                                              Icons.close,
                                              color: Colors.grey,
                                              size: 20.0,
                                            ),
                                          ),
                                          onTap: () {
                                            _controllerAddress.text = "";
                                          },
                                        );
                                },
                              )
                            ],
                          ),
                        ),
                        StreamBuilder(
                          stream: _bloc.outputModel,
                          builder: (_, snapshot) {
                            if (snapshot.hasData) {
                              PredictionModel model = snapshot.data;
                              if (model != null) {
                                return Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: Globals.minPadding,
                                      vertical: Globals.minPadding),
                                  padding: EdgeInsets.symmetric(
                                      horizontal: Globals.minPadding,
                                      vertical: Globals.minPadding),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(8.0)),
                                  child: Column(
                                    children: model.predictions
                                        .map((value) =>
                                            _renderItemAddress(value))
                                        .toList(),
                                  ),
                                );
                              }
                              return Container();
                            }
                            return Container();
                          },
                        )
                      ],
                    ),
                  ),
                ],
              ))
            ],
          ),
        )
      ],
    );
  }

  Widget _setupBackground() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(34.0)),
          image: DecorationImage(
            image: AssetImage(background),
            fit: BoxFit.cover,
          )),
    );
  }
}
