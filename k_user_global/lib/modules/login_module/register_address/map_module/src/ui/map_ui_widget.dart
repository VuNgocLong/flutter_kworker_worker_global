import 'dart:async';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/models/temp_address.dart';
import 'package:k_user_global/modules/login_module/register_address/map_module/src/bloc/map_bloc.dart';
import 'package:k_user_global/ultilites/global.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MapScreen extends StatefulWidget {
  final int check;
  MapScreen({this.check});
  @override
  State<StatefulWidget> createState() => _MapState();
}

class _MapState extends State<MapScreen> {
  GoogleMapController _controller;

  List<Marker> allMarker = [];

  Position _position = Position(longitude: Global.currentLng, latitude:Global.currentLat);
  double _zoom = 15.0;

  double _lat = 0.0;
  double _lng = 0.0;

  // bloc init
  MapBloc _bloc;

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png)).buffer.asUint8List();
  }

  void getLocationMap() async {
    showProgressDialog(context);
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    print(position);
    final coordinates = new Coordinates(position.latitude, position.longitude);
    var address =
    await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = address.first;
    print("${first.addressLine}");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Global.currentAddress = first.addressLine.toString();
    prefs.setString(keyCurrentAddress, first.addressLine.toString());
    hideProgressDialog();
    final Uint8List markerIcon = await getBytesFromAsset(iconMarker, 80);
    allMarker.add(Marker(
      markerId: MarkerId('myMarker'),
      draggable: false,
      onDragEnd: ((value) async {
        _bloc.onDragGetAddress(value.latitude, value.longitude);
      }),
      position: LatLng(position.latitude, position.longitude),
      icon:  BitmapDescriptor.fromBytes(markerIcon),
    ));



    setState(() {});

    _position = position;
    if (_controller != null) {
      _controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
          target: LatLng(position.latitude, position.longitude), zoom: _zoom)));
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _bloc = MapBloc();
    _lat = Global.currentLat;
    _lng = Global.currentLng;
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Colors.white,// Color for Android
        statusBarBrightness: Brightness.dark // Dark == white status bar -- for IOS.
    ));

    WidgetsBinding.instance.addPostFrameCallback((_){
      getLocationMap();
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _bloc.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              color: Colors.black,
            ),
            Stack(
              children: <Widget>[
                Center(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(30.0),
                    child: Align(
                      alignment: Alignment.center,
                      child: _googleMap(context),
                    ),
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: Globals.maxPadding + 20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          InkWell(
                            child: Container(
                              padding: EdgeInsets.only(top: 20.0, left: 20.0),
                              child: Image.asset(iconBackBlack, height: 20,
                                width: 30,),
                            ),
                            onTap: (){
                              Navigator.of(context).pop();
                              SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
                                  statusBarColor: greenLight, // Color for Android
                                  statusBarBrightness: Brightness.light // Dark == white status bar -- for IOS.
                              ));
                            },
                          ),
                          Container(
                            child: _address(),
                          )
                        ],
                      ),
                    ),
                    StreamBuilder(
                      stream: _bloc.outputAddress,
                      builder: (context, snapshot){
                        if (snapshot.hasData){
                          TempAddress temp  = snapshot.data;
                          return widget.check != null ? InkWell(
                              child: Container(
                                  margin:
                                  EdgeInsets.only(left: 20.0, right: 20.0, bottom: 37.0),
                                  height: 50.0,
                                  width: MediaQuery.of(context).size.width * 0.9,
                                  decoration: BoxDecoration(
                                      color: Color(0xff0AC67F),
                                      borderRadius: BorderRadius.circular(26.0)),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        'Xác nhận',
                                        style: TextStyle(
                                            fontSize: 20.0,
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold),
                                        textAlign: TextAlign.center,
                                      )
                                    ],
                                  )),
                              onTap: () {
                                dialogChooLocation(context, temp.address, temp.lng, temp.lat,false);
                              }) : InkWell(
                              child: Container(
                                  margin:
                                  EdgeInsets.only(left: 20.0, right: 20.0, bottom: 37.0),
                                  height: 50.0,
                                  width: MediaQuery.of(context).size.width * 0.9,
                                  decoration: BoxDecoration(
                                      color: Color(0xff0AC67F),
                                      borderRadius: BorderRadius.circular(26.0)),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        'Thêm',
                                        style: TextStyle(
                                            fontSize: 20.0,
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold),
                                        textAlign: TextAlign.center,
                                      )
                                    ],
                                  )),
                              onTap: () {
//                                _bloc.addLocation(context, temp);
                              });
                        }
                        return InkWell(
                          child: Container(
                              margin:
                              EdgeInsets.only(left: 20.0, right: 20.0, bottom: 37.0),
                              height: 50.0,
                              width: MediaQuery.of(context).size.width * 0.9,
                              decoration: BoxDecoration(
                                  color: Color(0xff0AC67F),
                                  borderRadius: BorderRadius.circular(26.0)),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  widget.check != null ? Text(
                                    'Xác nhận',
                                    style: TextStyle(
                                        fontSize: 20.0,
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                    textAlign: TextAlign.center,
                                  ) : Text(
                                    'Thêm',
                                    style: TextStyle(
                                        fontSize: 20.0,
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              )),);
                      },
                    )
                  ],
                )
              ],
            )
          ],
        )
    );
  }

  Widget _googleMap(BuildContext context) {
    return Container(
      child: StreamBuilder(
        stream: _bloc.outputMarkers,
        initialData: allMarker,
        builder: (_, snapshot){

          return GoogleMap(
            mapType: MapType.normal,
            initialCameraPosition: CameraPosition(
              target: LatLng(_position.latitude, _position.longitude),
              zoom: 12.0,
            ),
            onMapCreated: (GoogleMapController controller) {
              _controller = controller;
            },
            markers: Set.from(snapshot.data),
            onCameraMove: ((position) {
              _updatePosition(position);
            }),
            myLocationButtonEnabled: true,
            onCameraIdle: _idleMap,
          );
        },
      ),
    );
  }

  Widget _address(){
    return Container(
      height: 52.0,
      margin: EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8.0)
      ),
      child: Row(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 15.0, right: 15.0),
            child: Image.asset(iconMarker, width: 30,height: 40,),
          ),
          StreamBuilder(
            stream: _bloc.outputAddress,
            builder: (context, snapshot){
              if (snapshot.hasData){
                TempAddress temp = snapshot.data;
                return Expanded(
                  child: Text(temp.address ?? "",
                    style: TextStyle(),),
                );
              }
              return Expanded(
                child: Text(Global.currentAddress ?? "",
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(),),
              );
            },
          )
        ],
      ),
    );
  }

  _idleMap() {
    _bloc.onDragGetAddress(_lat, _lng);
  }

  _updatePosition(CameraPosition position) {
    Marker marker = allMarker[0];
    _lat = position.target.latitude;
    _lng = position.target.longitude;

    allMarker[0] = marker.copyWith(positionParam: LatLng(_lat, _lng));

    _bloc.setMarkers(allMarker);
  }
}
