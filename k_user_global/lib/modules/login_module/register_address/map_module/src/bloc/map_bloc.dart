import 'package:geocoder/geocoder.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:k_user_global/models/temp_address.dart';
import 'package:rxdart/rxdart.dart';

class MapBloc{

  final _streamAddress = BehaviorSubject<TempAddress>();
  final _streamMarkers = BehaviorSubject<List<Marker>>();

  Observable<TempAddress> get outputAddress => _streamAddress.stream;
  Observable<List<Marker>> get outputMarkers => _streamMarkers.stream;

  setAddress(TempAddress event) => _streamAddress.sink.add(event);
  setMarkers(List<Marker> event) => _streamMarkers.sink.add(event);

  onDragGetAddress(double lat, double lng) async {
    final coordinates = new Coordinates(lat, lng);
    var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    TempAddress temp = new TempAddress();
    temp.address = first.addressLine;
    temp.lat = first.coordinates.latitude;
    temp.lng = first.coordinates.longitude;
    setAddress(temp);
  }
  dispose(){
    _streamAddress.close();
    _streamMarkers.close();
  }

}