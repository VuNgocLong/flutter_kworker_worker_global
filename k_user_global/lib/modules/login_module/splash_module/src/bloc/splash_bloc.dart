import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/interactions.dart';
import 'package:k_user_global/models/otp_response_model.dart';
import 'package:k_user_global/models/user_response_model.dart';
import 'package:k_user_global/modules/home_module/src/ui/home_ui.dart';
import 'package:k_user_global/resources/repository.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashBloc {
  final _repository = Repository();
  SharedPreferences _prefs;

  SplashBloc() {
    SharedPreferences.getInstance().then((data) {
      _prefs = data;
    });
  }

  getRefreshToken(BuildContext context) async {
    showProgressDialog(context);
    ApiResponseData response = await _repository.getRefreshToken(context);
    if (response.success) {
      ResponseOTPModel data = ResponseOTPModel.fromJson(response.data['data']);
      //  String token = data.tokenType + " " + data.accessToken;
      String token = data.accessToken;
      _prefs.setString(keyToken, token);
      await getProfile(context);
    }
  }

  getProfile(BuildContext context) async {
    ApiResponseData response = await _repository.getProfileInfo(context, null);
    hideProgressDialog();
    if (response.success) {
      UserResponseModel model = UserResponseModel.fromJson(response.data['data']);
      if (model != null) {
        _prefs.setString(keyModelUser, json.encode(model));
//        _prefs.setString(keyIDUser, model.id.toString());
//        _prefs.setString(keyNameUser, model.name);
//        _prefs.setString(keyEmailUser, model.);
//        _prefs.setString(keyPhoneUser, model.phone);
//        _prefs.setString(keyAvtUser, model.avatar);
//        _prefs.setString(keyPassUser, model.password);
//        _prefs.setString(keyReferCode, model.referCode);
        Navigator.of(context).popUntil((route) => route.isFirst);
        navigatorPushReplacement(context, HomeUi());
      }
    }
  }
}
