import 'dart:convert';
import 'dart:io';
import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/models/update_infomation_request_model.dart';
import 'package:k_user_global/modules/home_module/src/ui/home_ui.dart';
import 'package:k_user_global/modules/login_module/account_verification/src/ui/account_verification_widget.dart';
import 'package:k_user_global/modules/login_module/basic_informations/src/ui/basic_informations_widget.dart';
import 'package:k_user_global/modules/login_module/choose_language_module/src/ui/choose_language_ui.dart';
import 'package:k_user_global/modules/login_module/contact_informations/src/ui/contact_informations.dart';
import 'package:k_user_global/modules/login_module/login_phone/src/ui/login_phone_widget.dart';
import 'package:k_user_global/modules/login_module/splash_module/src/bloc/splash_bloc.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:shared_preferences/shared_preferences.dart';


class ScreenSplash extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<ScreenSplash> with WidgetsBindingObserver {
  SplashBloc _bloc;
  @override
  void initState() {
    super.initState();
    _bloc = SplashBloc();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
        statusBarColor: greenLight, // Color for Android
        statusBarBrightness: Brightness.light // Dark == white status bar -- for IOS.
    ));
    checkApp();
  }

  void checkApp() async {
    await Future.delayed(Duration(seconds: 3));
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isLogin = prefs.getBool(keyIsLogin);
    bool isChoose = prefs.getBool(keyIsChoose);
    String phone = prefs.getString(keyPhoneUser);
    String  isUpdatedInfo = prefs.getString(keyIsUpdateInfo)??"";
    String imei = "";
//    String deviceName = "";
//    String flatform = "";
    if (Platform.isAndroid)
      await DeviceInfoPlugin().androidInfo.then((event) {
        imei = event.androidId;
//        flatform = "Android";
//        deviceName = event.model;
//        print(event.model);
      });
    else if (Platform.isIOS){
//      await DeviceInfoPlugin().iosInfo.then((event) {
//        deviceName = event.name;
//      });
//      flatform = "IOS";
//      await getiOSIMEI().then((event) {
//        imei = event;
//      });
    }
//    FirebaseMessaging().getToken().then((pushToken) {
//      print("Firebase Token: " + pushToken);
////      notifyToken = pushToken;
//      prefs.setString(keyNotificationToken, pushToken);
//    });

    prefs.setString(keyImei, imei);
    if ((isLogin == null || !isLogin) && (isChoose == null || !isChoose)) {
      Navigator.of(context).popUntil((route) => route.isFirst);
//      navigatorPop(context);
      navigatorPushReplacement(context, ChooseLanguage());
    } else if ((isLogin == null || !isLogin) && (phone == null)) {
//      navigatorPop(context);
      Navigator.of(context).popUntil((route) => route.isFirst);
      navigatorPushReplacement(context, ChooseLanguage());
      navigatorPush(context, LoginPhonePage());
    } else if((isUpdatedInfo==null||isUpdatedInfo==""||int.tryParse(isUpdatedInfo)!=1)){
      Navigator.of(context).popUntil((route) => route.isFirst);
      navigatorPushReplacement(context, BasicInformationPage(phone: phone,));
    }
//    else if(accVerify==null||!accVerify){
//      Navigator.of(context).popUntil((route) => route.isFirst);
//      navigatorPushReplacement(context, AccountVerificationPage());
//    }
    else {

//      Navigator.of(context).popUntil((route) => route.isFirst);
//      navigatorPushReplacement(context, AccountVerificationPage());
      _bloc.getRefreshToken(context);
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state.index) {
      case 0: // resumed
        if (Platform.isAndroid)  {
          checkApp();
        }
        break;
      case 1: // inactive
        break;
      case 2: // paused
        break;
    }
  }

  Widget _setupBackground() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          image: DecorationImage(image: AssetImage(splash), fit: BoxFit.fill)),
    );
  }

  Widget _setupIcon() {
    return Container(
      height: Globals.maxHeight,
      child: Center(
        child: Container(
          height: 85,
          width: 85,
          decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.2),
                  blurRadius: 10.0, // has the effect of softening the shadow
                  offset: Offset(
                    0.06,
                    0.6,
                  ),
                )
              ],
              image:
              DecorationImage(image: AssetImage(logo), fit: BoxFit.cover)),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Globals().init(context);
    return Scaffold(
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
        child: Container(
          child: Stack(
            children: <Widget>[_setupBackground(), _setupIcon()],
          ),
        ),
      ),
    );
  }
}
