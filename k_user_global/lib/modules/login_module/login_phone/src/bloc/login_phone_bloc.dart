import 'package:flutter/material.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/interactions.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/models/login_social_request_model.dart';
import 'package:k_user_global/models/request_otp_model.dart';
import 'package:k_user_global/models/response_model.dart';
import 'package:k_user_global/modules/login_module/login_otp/src/ui/confirm_otp_widget.dart';
import 'package:k_user_global/resources/repository.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPhoneBloc{
  final _repository = Repository();
  SharedPreferences _prefs;
  String _phone;
  Language _language;
  LoginPhoneBloc() {
    SharedPreferences.getInstance().then((data) {
      _prefs = data;

      String _model = _prefs.getString(keyLanguage);
      if (_model != null) {
        if (_model == 'vi') {
          _language = Vietnamese();
        } else if (_model == 'eng') {
          _language = English();
        } else if (_model == 'korean') {
          _language = Korean();
        } else if (_model == 'japanese') {
          _language = Japanese();
        }
      }
    });
  }
  final _streamValidatePhone = BehaviorSubject<String>();
  Observable<String> get outputValidatePhone => _streamValidatePhone.stream;
  setValidatePhone(String event) => _streamValidatePhone.sink.add(event);
  dispose(){
    _streamValidatePhone.close();

  }


  loginSocial(BuildContext context, LoginSocialRequestModel model)async{
    showProgressDialog(context);
    ApiResponseData responseData = await _repository.loginSocial(context, model);
    hideProgressDialog();
    if(responseData.success){

    }
  }

  login(BuildContext context, String phone, Language language,
      String countryCode) async {
    if (phone.length < 9 || phone.length > 10) {
      setValidatePhone("Số thuê bao không đúng hoặc không tồn tại");
    } else if (countryCode != "+84") {
      setValidatePhone(language.missCountryCode);
    } else {
      setValidatePhone(null);
      if (phone.length == 10) {
        _phone = phone;
        //_phone = countryCode.substring(1) + phone.substring(1);
        //_phone = phone.substring(1);
      } else if (phone.length == 9) {
        // _phone = countryCode.substring(1) + phone;
        _phone = "0"+ phone;
      }
      if (_phone != null) {
        showProgressDialog(context);
        ApiResponseData response = await _repository.sendOTP(context, RequestOtpModel(
            phone: _phone));
        hideProgressDialog();
        if(response.success){
          setValidatePhone(null);
             _prefs.setBool(keyIsChoose, true);
          _prefs.setString(keyLogin, "phone");
          navigatorPush(context, ConfirmOTPPage(phone: _phone,));
        }else{
          hideProgressDialog();
          ResponseModel data =  ResponseModel.fromJson(response.data);
          setValidatePhone(data.messager);
//
        }
      }
    }
  }


}