import 'package:auto_size_text/auto_size_text.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/common/language/english.dart';
import 'package:k_user_global/common/language/korean.dart';
import 'package:k_user_global/common/language/language.dart';
import 'package:k_user_global/common/language/nihongo.dart';
import 'package:k_user_global/common/language/vietnamese.dart';
import 'package:k_user_global/modules/login_module/login_otp/src/ui/confirm_otp_widget.dart';
import 'package:k_user_global/modules/login_module/login_phone/src/bloc/login_phone_bloc.dart';
import 'package:k_user_global/ultilites/hex_color.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPhonePage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _StatePhone();
  }

}

class _StatePhone extends State<LoginPhonePage>{
  String _countryCode;
  TextEditingController _editingController = TextEditingController();
  FocusNode _focusNode = FocusNode();
  final formKey = new GlobalKey<FormState>();
  SharedPreferences _prefs;
  Language _language;
  LoginPhoneBloc _bloc;

  @override
  void initState(){
    super.initState();
    _bloc = LoginPhoneBloc();
    _language = Language();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Colors.white, // Color for Android
        statusBarBrightness: Brightness.dark // Dark == white status bar -- for IOS.
    ));
    SharedPreferences.getInstance().then((data) {
      _prefs = data;
      setState(() {

        String _model = _prefs.getString(keyLanguage);

        if (_model != null) {
          if (_model == 'vi') {
            _language = Vietnamese();
          } else if (_model == 'eng') {
            _language = English();
          } else if (_model == 'korean') {
            _language = Korean();
          } else if (_model == 'japanese') {
            _language = Japanese();
          }
        }
      });

    });
//    WidgetsBinding.instance.addPostFrameCallback((_){
//      _bloc.setValidatePhone(null);
//    });
  }


  Widget _setupBackground() {
    return Container(
      width: Globals.maxWidth,
      height: Globals.maxHeight,
      decoration: BoxDecoration(
        color: Colors.white,
//        borderRadius: BorderRadius.all(Radius.circular(34.0)),
      ),
      child: Column(
        children: <Widget>[
          Container(
            height: Globals.maxHeight * 0.38,
            width: MediaQuery.of(context).size.width,
          ),
          Expanded(
            child: Container(),
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: Container(
              width: 244,
              height: 125,
              decoration: BoxDecoration(
                  borderRadius:
                  BorderRadius.only(bottomRight: Radius.circular(34.0)),
                  image: DecorationImage(
                      image: AssetImage(bgBottom), fit: BoxFit.fill)),
//              child: Image.asset(
//                bgBottom,
//                fit: BoxFit.cover,
//              ),
            ),
          )
        ],
      ),
    );
  }
  Widget _setupLoginView() {
    return GestureDetector(
      onTap: () => hideKeyboard(context),
      child: SafeArea(
        child: Container(
            color: Colors.transparent,
            child: ListView(
              physics: ClampingScrollPhysics(),
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height -
                      MediaQuery.of(context).padding.top -
                      MediaQuery.of(context).padding.bottom,
                  width: MediaQuery.of(context).size.width,
                  child: _setupContent(),
                )
              ],
            )),
      ),
    );
  }
  Widget _setupContent() {
    return Container(
      // height: MediaQuery.of(context).size.height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            height: Globals.maxPadding,
          ),
          InkWell(
            onTap: () {
              SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
                  statusBarColor: greenLight, // Color for Android
                  statusBarBrightness: Brightness.light // Dark == white status bar -- for IOS.
              ));
              navigatorPop(context);
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  width: Globals.maxPadding,
                ),
                Image.asset(
                  iconBackBlack,
                  height: 20,
                  width: 30,
//                  fit: BoxFit.cover,
                ),
              ],
            ),
          ),
          Container(
            height: Globals.maxPadding * 3,
          ),
          Container(
            margin: EdgeInsets.only(left: Globals.maxPadding),
            height: Globals.maxWidth*0.2,
            width: Globals.maxWidth*0.2,
            decoration: BoxDecoration(

                image: DecorationImage(
                    image: AssetImage(logoGreen), fit: BoxFit.cover
                )),
          ),
          Container(
            height: Globals.maxPadding * 2,
          ),
          Container(
            margin: EdgeInsets.only(left: Globals.maxPadding),
            child: AutoSizeText(
              _language.hi != null ? _language.hi : "",
              textAlign: TextAlign.left,
              style: TextStyle(
                  fontSize: 26, color: Colors.black, fontWeight: FontWeight.normal),
            ),
          ),
          Container(
            height: Globals.minPadding,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: Globals.maxPadding ),
            child: RichText(
                softWrap: true,
                textAlign: TextAlign.left,
                text: TextSpan(children: [
                  TextSpan(
                      text:   _language.inputText ?? "",
                    style: styleContent,),
                  TextSpan(
                    text:'K-Worker',
                    style: styleTitle,),
                ])),
          ),
          Container(
            height: Globals.maxPadding * 4,
          ),
          _itemInputView(),
          Padding(
            padding: EdgeInsets.only(
                left: Globals.maxPadding, right: Globals.maxPadding, top: Globals.minPadding/2),
            child: StreamBuilder<Object>(
              stream: _bloc.outputValidatePhone,
              initialData: null,
              builder: (_, snapshot) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Divider(
                      height: 1.0,
                      color:  snapshot.data==null?Colors.black.withOpacity(0.5):Colors.red,
                    ),
                    Container(height: Globals.minPadding/2,),
                    snapshot.data==null? Container() :Text(snapshot.data, style: TextStyle(color: Colors.red),
                    textAlign: TextAlign.start,)
                  ],
                );
              }
            ),
          ),
          Expanded(
            child: Container(),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              AutoSizeText(
                _language.loginGoogle ?? "",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: sizeText,
                    color: HexColor('4D3C3C43'),
                    fontWeight: FontWeight.normal),
              ),
            ],
          ),
          Container(
            height: Globals.minPadding,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              InkWell(
//                onTap: _loginWithFacebook,
                child: Container(
                  height: 43,
                  width: 43,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: AssetImage(iconFaceBook), fit: BoxFit.cover)),
                ),
              ),
              Container(
                width: Globals.minPadding,
              ),
              InkWell(
                onTap: () {},
                child: Container(
                  height: 43,
                  width: 43,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: AssetImage(iconGoogle), fit: BoxFit.cover)),
                ),
              ),
            ],
          ),
          Container(
            height: Globals.maxPadding,
          ),
          Container(
            height: Globals.minPadding,
          )
        ],
      ),
    );
  }


  Widget _itemInputView() {
    return Container(
      margin:
      EdgeInsets.only(left: Globals.maxPadding, right: Globals.maxPadding),
      //height: 40.0,
      height: (MediaQuery.of(context).size.height -
          MediaQuery.of(context).padding.top -
          MediaQuery.of(context).padding.bottom) /
          30,
      width: Globals.maxWidth,
      child: Row(
        children: <Widget>[
          _language.countryCode != null
              ? Stack(
            children: <Widget>[
              Container(
                width: 50.0,
                child: CountryCodePicker(
                  onChanged: (CountryCode countryCode) {
                    _countryCode = countryCode.toString();
                    print("New Country selected: " + _countryCode);
                  },
                  //alignLeft: false,
                  showFlag: false,
                  initialSelection: _language.countryCode ?? '',
                  favorite: ['+84', '+81', '+82', '+44'],
                  // favorite: ['+84', 'VN'],
                  // optional. Shows only country name and flag
                  showCountryOnly: false,
                ),
              ),
              Container(
                padding:
                EdgeInsets.only(left: 40.0, top: Globals.minPadding),
                child: Image.asset(
                  "assets/icons/icon-scroll-arrow-to-up.png",
                  width: 13.0,
                  height: 7.0,
                ),
              )
            ],
          )
              : Container(
            width: 40.0,
          ),
          Container(
            margin: EdgeInsets.only(right: 10, left: 10, top: 5, bottom: 5),
            width: 1,
            color: Colors.black.withOpacity(0.5),
          ),
          Expanded(
            child: TextField(
                controller: _editingController,
                focusNode: _focusNode,
                keyboardType: TextInputType.phone,
                decoration: InputDecoration(
                    hintText: _language.textPhone??"",
                    hintStyle: styleHint,
                    border: InputBorder.none,
                    contentPadding:
                    EdgeInsets.symmetric(vertical: Globals.minPadding)),
                onSubmitted: (text) {

                  if (_countryCode != null) {
                    _bloc.login(context, _editingController.text, _language,
                        _countryCode);
                  } else {
                    _bloc.login(context, _editingController.text, _language,
                        _language.countryCode);
                  }
                },
                inputFormatters:
                <TextInputFormatter>[
                  WhitelistingTextInputFormatter.digitsOnly,
                  LengthLimitingTextInputFormatter(14),
                ]),
          ),
          InkWell(
            onTap: () {

              if (_countryCode != null) {
                _bloc.login(
                    context, _editingController.text, _language, _countryCode);
              } else {
                _bloc.login(context, _editingController.text, _language,
                    _language.countryCode);
              }
            },
            child: Container(
              child: Image.asset(
                iconNextGreen,
                width: 26.0,
                height: 18.0,
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.black,
      body: Container(
        height: Globals.maxHeight,
        color: Colors.black,
        child: Stack(
          children: <Widget>[
            Container(
              constraints: BoxConstraints.expand(),
              color: Colors.black,
            ),
            _setupBackground(),
            _setupLoginView()
            // _setupContent(),
          ],
        ),
      ),
    );
  }
}