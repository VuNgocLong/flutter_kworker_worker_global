class ProfileModel {
  Data data;

  ProfileModel({this.data});

  ProfileModel.fromJson(Map<String, dynamic> json) {

    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  int id;
  String name;
  String firstName;
  int isVerify;
  String avatar;
  int isOnline;

  Data(
      {this.id,
        this.name,
        this.firstName,
        this.isVerify,
        this.avatar,
        this.isOnline});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    firstName = json['first_name'];
    isVerify = json['is_verify'];
    avatar = json['avatar'];
    isOnline = json['is_online'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['first_name'] = this.firstName;
    data['is_verify'] = this.isVerify;
    data['avatar'] = this.avatar;
    data['is_online'] = this.isOnline;
    return data;
  }
}