class IDCardResponseModel {
  String idCode;
  String dateRange;
  String identityCardBefore;
  String identityCardAfter;

  IDCardResponseModel(
      {this.idCode,
        this.dateRange,
        this.identityCardBefore,
        this.identityCardAfter});

  IDCardResponseModel.fromJson(Map<String, dynamic> json) {
    idCode = json['id_code'];
    dateRange = json['date_range'];
    identityCardBefore = json['identity_card_before'];
    identityCardAfter = json['identity_card_after'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_code'] = this.idCode;
    data['date_range'] = this.dateRange;
    data['identity_card_before'] = this.identityCardBefore;
    data['identity_card_after'] = this.identityCardAfter;
    return data;
  }
}