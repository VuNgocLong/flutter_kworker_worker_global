class WorkerServiceResponseModel {
  int id;
  String name;
  int serviceType;
  String icon;
  Null serviceProductivity;
  int price;
  int isActive;
  Null parentId;
  Null unitId;
  String createdAt;
  String updatedAt;
  Pivot pivot;

  WorkerServiceResponseModel(
      {this.id,
        this.name,
        this.serviceType,
        this.icon,
        this.serviceProductivity,
        this.price,
        this.isActive,
        this.parentId,
        this.unitId,
        this.createdAt,
        this.updatedAt,
        this.pivot});

  WorkerServiceResponseModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    serviceType = json['service_type'];
    icon = json['icon'];
    serviceProductivity = json['service_productivity'];
    price = json['price'];
    isActive = json['is_active'];
    parentId = json['parent_id'];
    unitId = json['unit_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    pivot = json['pivot'] != null ? new Pivot.fromJson(json['pivot']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['service_type'] = this.serviceType;
    data['icon'] = this.icon;
    data['service_productivity'] = this.serviceProductivity;
    data['price'] = this.price;
    data['is_active'] = this.isActive;
    data['parent_id'] = this.parentId;
    data['unit_id'] = this.unitId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.pivot != null) {
      data['pivot'] = this.pivot.toJson();
    }
    return data;
  }
}

class Pivot {
  int workerId;
  int serviceId;

  Pivot({this.workerId, this.serviceId});

  Pivot.fromJson(Map<String, dynamic> json) {
    workerId = json['worker_id'];
    serviceId = json['service_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['worker_id'] = this.workerId;
    data['service_id'] = this.serviceId;
    return data;
  }
}