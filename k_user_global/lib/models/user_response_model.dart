class UserResponseModel{
  int id;
  String name;
  String firstName;
  int isVerify;
  String avatar;
  int isOnline;

  UserResponseModel(
      {this.id,
        this.name,
        this.firstName,
        this.isVerify,
        this.avatar,
        this.isOnline});

  UserResponseModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    firstName = json['first_name'];
    isVerify = json['is_verify'];
    avatar = json['avatar'];
    isOnline = json['is_online'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['first_name'] = this.firstName;
    data['is_verify'] = this.isVerify;
    data['avatar'] = this.avatar;
    data['is_online'] = this.isOnline;
    return data;
  }
}