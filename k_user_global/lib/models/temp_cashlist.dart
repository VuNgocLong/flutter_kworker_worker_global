class TempCashList {
  List<Cash> data;

  TempCashList({this.data});
}

class Cash {
  int id;
  String value;
  bool isSelected;

  Cash({this.id, this.isSelected});
}