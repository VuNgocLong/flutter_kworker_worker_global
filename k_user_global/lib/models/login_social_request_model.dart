class LoginSocialRequestModel {
  int providerUserId;
  String provider;
  String name;
  String email;
  String avatar;

  LoginSocialRequestModel(
      {this.providerUserId, this.provider, this.name, this.email, this.avatar});

  LoginSocialRequestModel.fromJson(Map<String, dynamic> json) {
    providerUserId = json['provider_user_id'];
    provider = json['provider'];
    name = json['name'];
    email = json['email'];
    avatar = json['avatar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['provider_user_id'] = this.providerUserId;
    data['provider'] = this.provider;
    data['name'] = this.name;
    data['email'] = this.email;
    data['avatar'] = this.avatar;
    return data;
  }
}