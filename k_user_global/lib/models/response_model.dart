class ResponseModel{
  int status;
  // int code;
  String messager;
  Map<String, dynamic> error;

  ResponseModel({this.status, //this.code,
    this.messager,this.error});

  ResponseModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    // code = json['code'];
    messager = json['msg'];
    error = json['error'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    // data['code'] = this.code;
    data['msg'] = this.messager;
    data['error'] = this.error;
    return data;
  }
}