class UpdateAccountRequestModel {
  String firstName;
  String name;
  int sexId;
  String birthday;
  int statusId;

  UpdateAccountRequestModel(
      {this.firstName, this.name, this.sexId, this.birthday, this.statusId});

  UpdateAccountRequestModel.fromJson(Map<String, dynamic> json) {
    firstName = json['first_name'];
    name = json['name'];
    sexId = json['sex_id'];
    birthday = json['birthday'];
    statusId = json['status_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['first_name'] = this.firstName;
    data['name'] = this.name;
    data['sex_id'] = this.sexId;
    data['birthday'] = this.birthday;
    data['status_id'] = this.statusId;
    return data;
  }
}