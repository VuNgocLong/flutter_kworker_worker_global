class PaymentMethod {
  List<PaymentData> data;

  PaymentMethod({this.data});

  PaymentMethod.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<PaymentData>();
      json['data'].forEach((v) {
        data.add(new PaymentData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class PaymentData {
  int id;
  String name;
  String icon;
  bool isSelected;

  PaymentData({this.id, this.name, this.icon});

  PaymentData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    icon = json['icon'];
    isSelected = false;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['icon'] = this.icon;
    return data;
  }
}