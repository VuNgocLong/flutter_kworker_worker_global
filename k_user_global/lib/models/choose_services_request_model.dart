class ChooseServicesRequestModel {
  int statusId;
  List<int> serviceIds;

  ChooseServicesRequestModel({this.statusId, this.serviceIds});

  ChooseServicesRequestModel.fromJson(Map<String, dynamic> json) {
    statusId = json['status_id'];
    serviceIds = json['service_ids'].cast<int>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status_id'] = this.statusId;
    data['service_ids'] = this.serviceIds;
    return data;
  }
}