class HistoryModel {
  Data data;

  HistoryModel({this.data});

  HistoryModel.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  int currentPage;
  List<Datas> data;
  String firstPageUrl;
  int from;
  int lastPage;
  String lastPageUrl;
  Null nextPageUrl;
  String path;
  int perPage;
  Null prevPageUrl;
  int to;
  int total;

  Data(
      {this.currentPage,
        this.data,
        this.firstPageUrl,
        this.from,
        this.lastPage,
        this.lastPageUrl,
        this.nextPageUrl,
        this.path,
        this.perPage,
        this.prevPageUrl,
        this.to,
        this.total});

  Data.fromJson(Map<String, dynamic> json) {
    currentPage = json['current_page'];
    if (json['data'] != null) {
      data = new List<Datas>();
      json['data'].forEach((v) {
        data.add(new Datas.fromJson(v));
      });
    }
    firstPageUrl = json['first_page_url'];
    from = json['from'];
    lastPage = json['last_page'];
    lastPageUrl = json['last_page_url'];
    nextPageUrl = json['next_page_url'];
    path = json['path'];
    perPage = json['per_page'];
    prevPageUrl = json['prev_page_url'];
    to = json['to'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['current_page'] = this.currentPage;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['first_page_url'] = this.firstPageUrl;
    data['from'] = this.from;
    data['last_page'] = this.lastPage;
    data['last_page_url'] = this.lastPageUrl;
    data['next_page_url'] = this.nextPageUrl;
    data['path'] = this.path;
    data['per_page'] = this.perPage;
    data['prev_page_url'] = this.prevPageUrl;
    data['to'] = this.to;
    data['total'] = this.total;
    return data;
  }
}

class Datas {
  int id;
  int userId;
  String code;
  String phone;
  String address;
  String addressNote;
  int jobAmount;
  String voucherCode;
  int tip;
  String lat;
  String lng;
  String createdAt;
  int price;
  int workerTypeId;
  Service service;
  List<Part> part;
  Unit status;
  Client client;
  bool isSelected;

  Datas(
      {this.id,
        this.userId,
        this.code,
        this.phone,
        this.address,
        this.addressNote,
        this.jobAmount,
        this.voucherCode,
        this.tip,
        this.lat,
        this.lng,
        this.createdAt,
        this.price,
        this.workerTypeId,
        this.service,
        this.part,
        this.status,
        this.client,
      this.isSelected});

  Datas.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    code = json['code'];
    phone = json['phone'];
    address = json['address'];
    addressNote = json['address_note'] ?? "";
    jobAmount = json['job_amount'];
    voucherCode = json['voucher_code'] ?? "";
    tip = json['tip'];
    lat = json['lat'];
    lng = json['lng'];
    createdAt = json['created_at'];
    price = json['price'];
    workerTypeId = json['worker_type_id'];
    service =
    json['service'] != null ? new Service.fromJson(json['service']) : null;
    if (json['part'] != null) {
      part = new List<Part>();
      json['part'].forEach((v) {
        part.add(new Part.fromJson(v));
      });
    }
    status = json['status'] != null ? new Unit.fromJson(json['status']) : null;
    client =
    json['client'] != null ? new Client.fromJson(json['client']) : null;
    isSelected = false;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['code'] = this.code;
    data['phone'] = this.phone;
    data['address'] = this.address;
    data['address_note'] = this.addressNote;
    data['job_amount'] = this.jobAmount;
    data['voucher_code'] = this.voucherCode;
    data['tip'] = this.tip;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['created_at'] = this.createdAt;
    data['price'] = this.price;
    data['worker_type_id'] = this.workerTypeId;
    if (this.service != null) {
      data['service'] = this.service.toJson();
    }
    if (this.part != null) {
      data['part'] = this.part.map((v) => v.toJson()).toList();
    }
    if (this.status != null) {
      data['status'] = this.status.toJson();
    }
    if (this.client != null) {
      data['client'] = this.client.toJson();
    }
    return data;
  }
}

class Service {
  int id;
  String name;
  int unitId;
  int serviceType;
  Unit unit;

  Service({this.id, this.name, this.unitId, this.serviceType, this.unit});

  Service.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    unitId = json['unit_id'];
    serviceType = json['service_type'];
    unit = json['unit'] != null ? new Unit.fromJson(json['unit']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['unit_id'] = this.unitId;
    data['service_type'] = this.serviceType;
    if (this.unit != null) {
      data['unit'] = this.unit.toJson();
    }
    return data;
  }
}

class Unit {
  int id;
  String name;

  Unit({this.id, this.name});

  Unit.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }
}

class Part {
  int id;
  int jobId;
  int jobStatusId;
  int workerAmount;
  String startTime;
  String endTime;
  String createdAt;
  String updatedAt;
  Unit status;

  Part(
      {this.id,
        this.jobId,
        this.jobStatusId,
        this.workerAmount,
        this.startTime,
        this.endTime,
        this.createdAt,
        this.updatedAt,
        this.status});

  Part.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    jobId = json['job_id'];
    jobStatusId = json['job_status_id'];
    workerAmount = json['worker_amount'];
    startTime = json['start_time'];
    endTime = json['end_time'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    status = json['status'] != null ? new Unit.fromJson(json['status']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['job_id'] = this.jobId;
    data['job_status_id'] = this.jobStatusId;
    data['worker_amount'] = this.workerAmount;
    data['start_time'] = this.startTime;
    data['end_time'] = this.endTime;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.status != null) {
      data['status'] = this.status.toJson();
    }
    return data;
  }
}

class Client {
  int id;
  String name;
  String email;
  String phone;
  String avatar;
  String referCode;

  Client(
      {this.id,
        this.name,
        this.email,
        this.phone,
        this.avatar,
        this.referCode});

  Client.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    avatar = json['avatar'];
    referCode = json['refer_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['avatar'] = this.avatar;
    data['refer_code'] = this.referCode;
    return data;
  }
}