class LocationModel {
  List<AddressModel> data;

  LocationModel({this.data});

  LocationModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<AddressModel>();
      json['data'].forEach((v) {
        data.add(new AddressModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class AddressModel{
  String placeId;
  String address;
  String addressMore;
  double latitude;
  double longitude;
  String streetNumber;
  String street;
  String ward;
  String district;
  String city;
  String country;

  AddressModel(
      {this.placeId,
        this.address,
        this.addressMore,
        this.latitude,
        this.longitude,
        this.streetNumber,
        this.street,
        this.ward,
        this.district,
        this.city,
        this.country});

  AddressModel.fromJson(Map<String, dynamic> json) {
    address = json['address'];
    addressMore = json['address_more'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    streetNumber = json['street_number'];
    street = json['street'];
    ward = json['ward'];
    district = json['district'];
    city = json['city'];
    country = json['country'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['address'] = this.address;
    data['address_more'] = this.addressMore;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['street_number'] = this.streetNumber;
    data['street'] = this.street;
    data['ward'] = this.ward;
    data['district'] = this.district;
    data['city'] = this.city;
    data['country'] = this.country;
    return data;
  }
}