class RequestOtpModel {
  String phone;

  RequestOtpModel({this.phone});

  RequestOtpModel.fromJson(Map<String, dynamic> json) {
    phone = json['phone'];
  }

  Map<String, dynamic> toJson(RequestOtpModel model){
    return {
      'phone': model.phone,
    };
  }
}