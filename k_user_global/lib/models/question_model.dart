class QuestionModel {
  List<Data> data;

  QuestionModel({this.data});

  QuestionModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  String name;
  int topicId;
  List<Answer> answer;
  List<WorkerAnswer> workerAnswer;

  Data({this.id, this.name, this.topicId, this.answer, this.workerAnswer});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    topicId = json['topic_id'];
    if (json['answer'] != null) {
      answer = new List<Answer>();
      json['answer'].forEach((v) {
        answer.add(new Answer.fromJson(v));
      });
    }
    if (json['worker_answer'] != null) {
      workerAnswer = new List<WorkerAnswer>();
      json['worker_answer'].forEach((v) {
        workerAnswer.add(new WorkerAnswer.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['topic_id'] = this.topicId;
    if (this.answer != null) {
      data['answer'] = this.answer.map((v) => v.toJson()).toList();
    }
    if (this.workerAnswer != null) {
      data['worker_answer'] = this.workerAnswer.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Answer {
  int id;
  String name;
  bool isSelected;

  Answer({this.id, this.name, this.isSelected});

  Answer.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    isSelected = false;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }
}

class WorkerAnswer {
  int id;
  String name;

  WorkerAnswer({this.id, this.name});

  WorkerAnswer.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }
}