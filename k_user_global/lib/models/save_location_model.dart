class SaveLocationModel {
  List<Data> data;

  SaveLocationModel({this.data});

  SaveLocationModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  String address;
  String lng;
  String lat;
  bool isSelected;

  Data({this.id, this.address, this.lng, this.lat});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    address = json['address'];
    lng = json['lng'];
    lat = json['lat'];
    isSelected = false;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['address'] = this.address;
    data['lng'] = this.lng;
    data['lat'] = this.lat;
    return data;
  }
}