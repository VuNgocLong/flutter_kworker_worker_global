class AccountVerifyResponseModel {
  int id;
  String name;
  String phone;
  String avatar;
  int rating;
  int power;
  String createdAt;
  String updatedAt;
  String referCode;
  String email;
  String firstName;
  int sexId;
  String birthday;
  String address;
  int isUpdatedInfo;
  String idCode;
  String dateRange;
  String identityCardBefore;
  String identityCardAfter;
  String healthCertificate;
  String degree;
  int isVerify;
  int isOnline;
  double lng;
  double lat;

  AccountVerifyResponseModel(
      {this.id,
        this.name,
        this.phone,
        this.avatar,
        this.rating,
        this.power,
        this.createdAt,
        this.updatedAt,
        this.referCode,
        this.email,
        this.firstName,
        this.sexId,
        this.birthday,
        this.address,
        this.isUpdatedInfo,
        this.idCode,
        this.dateRange,
        this.identityCardBefore,
        this.identityCardAfter,
        this.healthCertificate,
        this.degree,
        this.isVerify,
        this.isOnline,
        this.lng,
        this.lat});

  AccountVerifyResponseModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    phone = json['phone'];
    avatar = json['avatar'];
    rating = json['rating'];
    power = json['power'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    referCode = json['refer_code'];
    email = json['email'];
    firstName = json['first_name'];
    sexId = json['sex_id'];
    birthday = json['birthday'];
    address = json['address'];
    isUpdatedInfo = json['is_updated_info'];
    idCode = json['id_code'];
    dateRange = json['date_range'];
    identityCardBefore = json['identity_card_before'];
    identityCardAfter = json['identity_card_after'];
    healthCertificate = json['health_certificate'];
    degree = json['degree'];
    isVerify = json['is_verify'];
    isOnline = json['is_online'];
    lng = json['lng']!=null? double.tryParse(json['lng'].toString()):null;
    lat =json['lat']!=null? double.tryParse(json['lat'].toString()):null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['phone'] = this.phone;
    data['avatar'] = this.avatar;
    data['rating'] = this.rating;
    data['power'] = this.power;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['refer_code'] = this.referCode;
    data['email'] = this.email;
    data['first_name'] = this.firstName;
    data['sex_id'] = this.sexId;
    data['birthday'] = this.birthday;
    data['address'] = this.address;
    data['is_updated_info'] = this.isUpdatedInfo;
    data['id_code'] = this.idCode;
    data['date_range'] = this.dateRange;
    data['identity_card_before'] = this.identityCardBefore;
    data['identity_card_after'] = this.identityCardAfter;
    data['health_certificate'] = this.healthCertificate;
    data['degree'] = this.degree;
    data['is_verify'] = this.isVerify;
    data['is_online'] = this.isOnline;
    data['lng'] = this.lng;
    data['lat'] = this.lat;
    return data;
  }
}