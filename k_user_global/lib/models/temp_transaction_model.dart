class Transaction {
  String walletableType;
  int walletableId;
  int userId;
  Payment payment;

  Transaction(
      {this.walletableType, this.walletableId, this.userId, this.payment});

  Transaction.fromJson(Map<String, dynamic> json) {
    walletableType = json['walletable_type'];
    walletableId = json['walletable_id'];
    userId = json['user_id'];
    payment =
    json['payment'] != null ? new Payment.fromJson(json['payment']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['walletable_type'] = this.walletableType;
    data['walletable_id'] = this.walletableId;
    data['user_id'] = this.userId;
    if (this.payment != null) {
      data['payment'] = this.payment.toJson();
    }
    return data;
  }
}

class Payment {
  int id;
  String code;
  String methodCode;
  String amount;

  Payment({this.id, this.code, this.methodCode, this.amount});

  Payment.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    code = json['code'];
    methodCode = json['method_code'];
    amount = json['amount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['code'] = this.code;
    data['method_code'] = this.methodCode;
    data['amount'] = this.amount;
    return data;
  }
}