class OTPConfirmModel {
  String phone;
  String otp;

  OTPConfirmModel({this.phone, this.otp});

  OTPConfirmModel.fromJson(Map<String, dynamic> json) {
    phone = json['phone'];
    otp = json['otp'];
  }
  Map<String, dynamic> toJson(OTPConfirmModel model){
    return {
      'phone': model.phone,
      'otp': model.otp,
    };
  }
}