class TempMajorDetail {
  List<ProductivityItem> listNonProductivity;
  bool checkNonProductivity;
  bool viewType;
  List<ProductivityItem> listProductivity;
  bool checkProductivity;

  TempMajorDetail({this.listNonProductivity, this.listProductivity, this.checkNonProductivity, this.checkProductivity, this.viewType});
}

class ProductivityItem{
  String title;
  List<Item> list;
  bool isDone;
  ProductivityItem({this.title, this.list, this.isDone});
}
class Item{
  String title;
  bool  isSelected;
  Item({this.title, this.isSelected});
}