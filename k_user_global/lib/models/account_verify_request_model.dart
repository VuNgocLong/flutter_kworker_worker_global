class VerifyAccountRequestModel {
  String idCode;
  String dateRange;
  int statusId;
  VerifyAccountRequestModel({this.idCode, this.dateRange, this.statusId});

  VerifyAccountRequestModel.fromJson(Map<String, dynamic> json) {
    idCode = json['id_code'];
    dateRange = json['date_range'];
    if (json['status_id'] != null) statusId = json['status_id'];
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_code'] = this.idCode;
    data['date_range'] = this.dateRange;
    if (this.statusId != null) data['status_id'] = this.statusId;
    return data;
  }
}
