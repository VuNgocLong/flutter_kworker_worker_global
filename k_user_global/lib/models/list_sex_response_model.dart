class SexResponseModel {
  int id;
  String name;
  bool isSelected;

  SexResponseModel({this.id, this.name});

  SexResponseModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    if (json['id'] == 1)
      isSelected = true;
    else
      isSelected = false;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }

}
