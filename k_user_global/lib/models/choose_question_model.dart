class ChooseQuestionModel {
  List<int> ids;
  int questionId;

  ChooseQuestionModel({this.ids, this.questionId});

  ChooseQuestionModel.fromJson(Map<String, dynamic> json) {
    ids = json['ids'].cast<int>();
    questionId = json['question_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ids'] = this.ids;
    data['question_id'] = this.questionId;
    return data;
  }
}