class OrderModel {
  Data data;

  OrderModel({this.data});

  OrderModel.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  int id;
  String code;
  int jobTypeId;
  int workerTypeId;
  int userId;
  String phone;
  String address;
  String addressNote;
  String lat;
  String lng;
  String startTime;
  String endTime;
  String description;
  String voucherCode;
  int jobAmount;
  int tip;
  int price;
  String createdAt;
  String updatedAt;
  int paymentMethodId;
  int totalPrice;
  int commission;
  int priceVat;
  int priceHoliday;
  String priceDistance;
  String pricePeak;
  String totalPayment;
  String discount;
//  List<Null> incurred;
  Service service;
  Unit status;
  List<Part> part;
  Client client;

  Data(
      {this.id,
        this.code,
        this.jobTypeId,
        this.workerTypeId,
        this.userId,
        this.phone,
        this.address,
        this.addressNote,
        this.lat,
        this.lng,
        this.startTime,
        this.endTime,
        this.description,
        this.voucherCode,
        this.jobAmount,
        this.tip,
        this.price,
        this.createdAt,
        this.updatedAt,
        this.paymentMethodId,
        this.totalPrice,
        this.commission,
        this.priceVat,
        this.priceHoliday,
        this.priceDistance,
        this.pricePeak,
        this.totalPayment,
        this.discount,
//        this.incurred,
        this.service,
        this.status,
        this.part,
        this.client});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    code = json['code'];
    jobTypeId = json['job_type_id'];
    workerTypeId = json['worker_type_id'];
    userId = json['user_id'];
    phone = json['phone'];
    address = json['address'];
    addressNote = json['address_note'] ?? "";
    lat = json['lat'];
    lng = json['lng'];
    startTime = json['start_time'];
    endTime = json['end_time'];
    description = json['description'] ?? "";
    voucherCode = json['voucher_code'] ?? "";
    jobAmount = json['job_amount'];
    tip = json['tip'];
    price = json['price'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    paymentMethodId = json['payment_method_id'];
    totalPrice = json['total_price'];
    commission = json['commission'];
    priceVat = json['price_vat'];
    priceHoliday = json['price_holiday'];
    priceDistance = json['price_distance'];
    pricePeak = json['price_peak'];
    totalPayment = json['total_payment'];
    discount = json['discount'];
//    if (json['incurred'] != null) {
//      incurred = new List<Null>();
//      json['incurred'].forEach((v) {
//        incurred.add(new Null.fromJson(v));
//      });
//    }
    service =
    json['service'] != null ? new Service.fromJson(json['service']) : null;
    status = json['status'] != null ? new Unit.fromJson(json['status']) : null;
    if (json['part'] != null) {
      part = new List<Part>();
      json['part'].forEach((v) {
        part.add(new Part.fromJson(v));
      });
    }
    client =
    json['client'] != null ? new Client.fromJson(json['client']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['code'] = this.code;
    data['job_type_id'] = this.jobTypeId;
    data['worker_type_id'] = this.workerTypeId;
    data['user_id'] = this.userId;
    data['phone'] = this.phone;
    data['address'] = this.address;
    data['address_note'] = this.addressNote;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['start_time'] = this.startTime;
    data['end_time'] = this.endTime;
    data['description'] = this.description;
    data['voucher_code'] = this.voucherCode;
    data['job_amount'] = this.jobAmount;
    data['tip'] = this.tip;
    data['price'] = this.price;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['payment_method_id'] = this.paymentMethodId;
    data['total_price'] = this.totalPrice;
    data['commission'] = this.commission;
    data['price_vat'] = this.priceVat;
    data['price_holiday'] = this.priceHoliday;
    data['price_distance'] = this.priceDistance;
    data['price_peak'] = this.pricePeak;
    data['total_payment'] = this.totalPayment;
    data['discount'] = this.discount;
//    if (this.incurred != null) {
//      data['incurred'] = this.incurred.map((v) => v.toJson()).toList();
//    }
    if (this.service != null) {
      data['service'] = this.service.toJson();
    }
    if (this.status != null) {
      data['status'] = this.status.toJson();
    }
    if (this.part != null) {
      data['part'] = this.part.map((v) => v.toJson()).toList();
    }
    if (this.client != null) {
      data['client'] = this.client.toJson();
    }
    return data;
  }
}

class Service {
  int id;
  String name;
  int unitId;
  int serviceType;
  Unit unit;

  Service({this.id, this.name, this.unitId, this.serviceType, this.unit});

  Service.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    unitId = json['unit_id'];
    serviceType = json['service_type'];
    unit = json['unit'] != null ? new Unit.fromJson(json['unit']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['unit_id'] = this.unitId;
    data['service_type'] = this.serviceType;
    if (this.unit != null) {
      data['unit'] = this.unit.toJson();
    }
    return data;
  }
}

class Unit {
  int id;
  String name;

  Unit({this.id, this.name});

  Unit.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }
}

class Part {
  int jobId;
  int id;
  int jobStatusId;
  int workerAmount;
  String startTime;
  String endTime;
  Unit status;

  Part(
      {this.jobId,
        this.id,
        this.jobStatusId,
        this.workerAmount,
        this.startTime,
        this.endTime,
        this.status});

  Part.fromJson(Map<String, dynamic> json) {
    jobId = json['job_id'];
    id = json['id'];
    jobStatusId = json['job_status_id'];
    workerAmount = json['worker_amount'];
    startTime = json['start_time'];
    endTime = json['end_time'];
    status = json['status'] != null ? new Unit.fromJson(json['status']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['job_id'] = this.jobId;
    data['id'] = this.id;
    data['job_status_id'] = this.jobStatusId;
    data['worker_amount'] = this.workerAmount;
    data['start_time'] = this.startTime;
    data['end_time'] = this.endTime;
    if (this.status != null) {
      data['status'] = this.status.toJson();
    }
    return data;
  }
}

class Client {
  int id;
  String name;
  String email;
  String phone;
  String avatar;
  String referCode;

  Client(
      {this.id,
        this.name,
        this.email,
        this.phone,
        this.avatar,
        this.referCode});

  Client.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    avatar = json['avatar'];
    referCode = json['refer_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['avatar'] = this.avatar;
    data['refer_code'] = this.referCode;
    return data;
  }
}