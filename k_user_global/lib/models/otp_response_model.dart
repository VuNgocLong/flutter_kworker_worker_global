class ResponseOTPModel {
  String accessToken;
  String tokenType;
  int expiresIn;
  String isUpdatedInfo;

  ResponseOTPModel(
      {this.accessToken, this.tokenType, this.expiresIn, this.isUpdatedInfo});

  ResponseOTPModel.fromJson(Map<String, dynamic> json) {
    accessToken = json['access_token'];
    tokenType = json['token_type'];
    expiresIn = json['expires_in'];
    isUpdatedInfo =
        json['is_updated_info'] != null ? json['is_updated_info'].toString() : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['access_token'] = this.accessToken;
    data['token_type'] = this.tokenType;
    data['expires_in'] = this.expiresIn;
    data['is_updated_info'] = this.isUpdatedInfo;
    return data;
  }
}
