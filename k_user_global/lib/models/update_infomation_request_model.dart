class UpdateInfoRequestModel {
  String firstName;
  String lastName;
  int sexId;
  String birthday;
  String phone;
  String email;
  String address;
  String referCode;
  List<int> services;

  UpdateInfoRequestModel(
      {this.firstName,
        this.lastName,
        this.sexId,
        this.birthday,
        this.phone,
        this.email,
        this.address,
        this.referCode,
        this.services});

  UpdateInfoRequestModel.fromJson(Map<String, dynamic> json) {
    firstName = json['first_name'];
    lastName = json['last_name'];
    sexId = json['sex_id'];
    birthday = json['birthday'];
    phone = json['phone'];
    email = json['email'];
    address = json['address'];
    referCode = json['refer_code'];
    services = json['services'].cast<int>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['sex_id'] = this.sexId;
    data['birthday'] = this.birthday;
    data['phone'] = this.phone;
    data['email'] = this.email;
    data['address'] = this.address;
    data['refer_code'] = this.referCode;
    data['services'] = this.services;
    return data;
  }
}