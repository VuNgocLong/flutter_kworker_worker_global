class MenuProfileModel {
  List<Data> data;

  MenuProfileModel({this.data});

  MenuProfileModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  String name;
  Status status;
  String type;
  int parentId;
  List<Children> children;

  Data(
      {this.id,
        this.name,
        this.status,
        this.type,
        this.parentId,
        this.children});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    status =
    json['status'] != null ? new Status.fromJson(json['status']) : null;
    type = json['type'];
    parentId = json['parent_id'];
    if (json['children'] != null) {
      children = new List<Children>();
      json['children'].forEach((v) {
        children.add(new Children.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    if (this.status != null) {
      data['status'] = this.status.toJson();
    }
    data['type'] = this.type;
    data['parent_id'] = this.parentId;
    if (this.children != null) {
      data['children'] = this.children.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Status {
  int id;
  int status;

  Status({this.id, this.status});

  Status.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['status'] = this.status;
    return data;
  }
}

class Children {
  int id;
  String name;
  Status status;
  String type;
  int parentId;

  Children({this.id, this.name, this.status, this.type, this.parentId});

  Children.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    status =
    json['status'] != null ? new Status.fromJson(json['status']) : null;
    type = json['type'];
    parentId = json['parent_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    if (this.status != null) {
      data['status'] = this.status.toJson();
    }
    data['type'] = this.type;
    data['parent_id'] = this.parentId;
    return data;
  }
}