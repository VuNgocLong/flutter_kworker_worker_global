class RequestModel {
  List<Data> data;

  RequestModel({this.data});

  RequestModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  int requestableId;
  int workerId;
  String description;
  String requestableType;
  Null createdAt;
  Null updatedAt;
  Requestable requestable;

  Data(
      {this.id,
        this.requestableId,
        this.workerId,
        this.description,
        this.requestableType,
        this.createdAt,
        this.updatedAt,
        this.requestable});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    requestableId = json['requestable_id'];
    workerId = json['worker_id'];
    description = json['description'];
    requestableType = json['requestable_type'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    requestable = json['requestable'] != null
        ? new Requestable.fromJson(json['requestable'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['requestable_id'] = this.requestableId;
    data['worker_id'] = this.workerId;
    data['description'] = this.description;
    data['requestable_type'] = this.requestableType;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.requestable != null) {
      data['requestable'] = this.requestable.toJson();
    }
    return data;
  }
}

class Requestable {
  int id;
  String code;
  int jobTypeId;
  int workerTypeId;
  int userId;
  String phone;
  String address;
  Null addressNote;
  String lat;
  String lng;
  String startTime;
  String endTime;
  String description;
  Null voucherCode;
  int jobAmount;
  int tip;
  int price;
  String createdAt;
  String updatedAt;
  int paymentMethodId;
  int totalPrice;
  int commission;
  int priceVat;
  int priceHoliday;
  String priceDistance;
  String pricePeak;
  String totalPayment;
  String discount;
  Client client;

  Requestable(
      {this.id,
        this.code,
        this.jobTypeId,
        this.workerTypeId,
        this.userId,
        this.phone,
        this.address,
        this.addressNote,
        this.lat,
        this.lng,
        this.startTime,
        this.endTime,
        this.description,
        this.voucherCode,
        this.jobAmount,
        this.tip,
        this.price,
        this.createdAt,
        this.updatedAt,
        this.paymentMethodId,
        this.totalPrice,
        this.commission,
        this.priceVat,
        this.priceHoliday,
        this.priceDistance,
        this.pricePeak,
        this.totalPayment,
        this.discount,
        this.client});

  Requestable.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    code = json['code'];
    jobTypeId = json['job_type_id'];
    workerTypeId = json['worker_type_id'];
    userId = json['user_id'];
    phone = json['phone'];
    address = json['address'];
    addressNote = json['address_note'];
    lat = json['lat'];
    lng = json['lng'];
    startTime = json['start_time'];
    endTime = json['end_time'];
    description = json['description'];
    voucherCode = json['voucher_code'];
    jobAmount = json['job_amount'];
    tip = json['tip'];
    price = json['price'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    paymentMethodId = json['payment_method_id'];
    totalPrice = json['total_price'];
    commission = json['commission'];
    priceVat = json['price_vat'];
    priceHoliday = json['price_holiday'];
    priceDistance = json['price_distance'];
    pricePeak = json['price_peak'];
    totalPayment = json['total_payment'];
    discount = json['discount'];
    client =
    json['client'] != null ? new Client.fromJson(json['client']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['code'] = this.code;
    data['job_type_id'] = this.jobTypeId;
    data['worker_type_id'] = this.workerTypeId;
    data['user_id'] = this.userId;
    data['phone'] = this.phone;
    data['address'] = this.address;
    data['address_note'] = this.addressNote;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['start_time'] = this.startTime;
    data['end_time'] = this.endTime;
    data['description'] = this.description;
    data['voucher_code'] = this.voucherCode;
    data['job_amount'] = this.jobAmount;
    data['tip'] = this.tip;
    data['price'] = this.price;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['payment_method_id'] = this.paymentMethodId;
    data['total_price'] = this.totalPrice;
    data['commission'] = this.commission;
    data['price_vat'] = this.priceVat;
    data['price_holiday'] = this.priceHoliday;
    data['price_distance'] = this.priceDistance;
    data['price_peak'] = this.pricePeak;
    data['total_payment'] = this.totalPayment;
    data['discount'] = this.discount;
    if (this.client != null) {
      data['client'] = this.client.toJson();
    }
    return data;
  }
}

class Client {
  int id;
  String name;
  String email;
  String phone;
  Null avatar;
  String referCode;

  Client(
      {this.id,
        this.name,
        this.email,
        this.phone,
        this.avatar,
        this.referCode});

  Client.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    avatar = json['avatar'];
    referCode = json['refer_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['avatar'] = this.avatar;
    data['refer_code'] = this.referCode;
    return data;
  }
}