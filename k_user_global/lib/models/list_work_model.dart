class ListWorkModel {
  List<Data> data;

  ListWorkModel({this.data});

  ListWorkModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  String name;
  int serviceType;
  String icon;
  Null serviceProductivity;
  int price;
  int isActive;
  Null parentId;
  Null unitId;

  Data(
      {this.id,
        this.name,
        this.serviceType,
        this.icon,
        this.serviceProductivity,
        this.price,
        this.isActive,
        this.parentId,
        this.unitId});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    serviceType = json['service_type'];
    icon = json['icon'];
    serviceProductivity = json['service_productivity'];
    price = json['price'];
    isActive = json['is_active'];
    parentId = json['parent_id'];
    unitId = json['unit_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['service_type'] = this.serviceType;
    data['icon'] = this.icon;
    data['service_productivity'] = this.serviceProductivity;
    data['price'] = this.price;
    data['is_active'] = this.isActive;
    data['parent_id'] = this.parentId;
    data['unit_id'] = this.unitId;
    return data;
  }
}