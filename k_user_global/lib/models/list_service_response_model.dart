
class ListServiceResponseModel {
  List<Service> serviceProductivity;
  List<Service> service;

  ListServiceResponseModel({this.serviceProductivity, this.service});

  ListServiceResponseModel.fromJson(Map<String, dynamic> json) {
    if (json['service_productivity'] != null) {
      serviceProductivity = new List<Service>();
      json['service_productivity'].forEach((v) {
        serviceProductivity.add(new Service.fromJson(v));
      });
    }
    if (json['service'] != null) {
      service = new List<Service>();
      json['service'].forEach((v) {
        service.add(new Service.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.serviceProductivity != null) {
      data['service_productivity'] =
          this.serviceProductivity.map((v) => v.toJson()).toList();
    }
    if (this.service != null) {
      data['service'] = this.service.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Service {
  int id;
  String name;
  String icon;
  List<Productivity> productivity;

  Service({this.id, this.name, this.icon, this.productivity});

  Service.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    icon = json['icon'];
    if (json['productivity'] != null) {
      productivity = new List<Productivity>();
      json['productivity'].forEach((v) {
        productivity.add(new Productivity.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['icon'] = this.icon;
    if (this.productivity != null) {
      data['productivity'] = this.productivity.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Productivity {
  int id;
  int serviceId;
  int serviceProductivityUnit;
  int serviceProductivityType;
  int difference;
  bool isSelected;
  bool isExpanded;
  int degree;


  Productivity(
      {this.id,
        this.serviceId,
        this.serviceProductivityUnit,
        this.serviceProductivityType,
        this.difference});

  Productivity.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    serviceId = json['service_id'];
    serviceProductivityUnit = json['service_productivity_unit'];
    serviceProductivityType = json['service_productivity_type'];
    difference = json['difference'];
    isSelected = false;
    isExpanded = false;
    degree = 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['service_id'] = this.serviceId;
    data['service_productivity_unit'] = this.serviceProductivityUnit;
    data['service_productivity_type'] = this.serviceProductivityType;
    data['difference'] = this.difference;
    return data;
  }
}

