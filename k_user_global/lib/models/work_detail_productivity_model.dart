class WorkDetailProductivityModel {
  Data data;

  WorkDetailProductivityModel({this.data});

  WorkDetailProductivityModel.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<ServiceProductivity> serviceProductivity;
//  List<Null> service;

  Data({this.serviceProductivity});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['service_productivity'] != null) {
      serviceProductivity = new List<ServiceProductivity>();
      json['service_productivity'].forEach((v) {
        serviceProductivity.add(new ServiceProductivity.fromJson(v));
      });
    }
//    if (json['service'] != null) {
//      service = new List<Null>();
//      json['service'].forEach((v) {
//        service.add(new Null.fromJson(v));
//      });
//    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.serviceProductivity != null) {
      data['service_productivity'] =
          this.serviceProductivity.map((v) => v.toJson()).toList();
    }
//    if (this.service != null) {
//      data['service'] = this.service.map((v) => v.toJson()).toList();
//    }
    return data;
  }
}

class ServiceProductivity {
  int id;
  String name;
  int serviceType;
  String icon;
  Null serviceProductivity;
  int price;
  int isActive;
  int parentId;
  int unitId;
  List<Productivity> productivity;

  ServiceProductivity(
      {this.id,
        this.name,
        this.serviceType,
        this.icon,
        this.serviceProductivity,
        this.price,
        this.isActive,
        this.parentId,
        this.unitId,
        this.productivity});

  ServiceProductivity.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    serviceType = json['service_type'];
    icon = json['icon'];
    serviceProductivity = json['service_productivity'];
    price = json['price'];
    isActive = json['is_active'];
    parentId = json['parent_id'];
    unitId = json['unit_id'];
    if (json['productivity'] != null) {
      productivity = new List<Productivity>();
      json['productivity'].forEach((v) {
        productivity.add(new Productivity.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['service_type'] = this.serviceType;
    data['icon'] = this.icon;
    data['service_productivity'] = this.serviceProductivity;
    data['price'] = this.price;
    data['is_active'] = this.isActive;
    data['parent_id'] = this.parentId;
    data['unit_id'] = this.unitId;
    if (this.productivity != null) {
      data['productivity'] = this.productivity.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Productivity {
  int id;
  int difference;
  Unit unit;
  Unit type;
  bool isSelected;

  Productivity({this.id, this.difference, this.unit, this.type, this.isSelected});

  Productivity.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    difference = json['difference'];
    unit = json['unit'] != null ? new Unit.fromJson(json['unit']) : null;
    type = json['type'] != null ? new Unit.fromJson(json['type']) : null;
    isSelected = false;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['difference'] = this.difference;
    if (this.unit != null) {
      data['unit'] = this.unit.toJson();
    }
    if (this.type != null) {
      data['type'] = this.type.toJson();
    }
    return data;
  }
}

class Unit {
  int id;
  String name;

  Unit({this.id, this.name});

  Unit.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }
}