class AccountInformationResponseModel {
  int id;
  String firstName;
  String name;
  String birthday;
  Sex sex;

  AccountInformationResponseModel({this.id, this.firstName, this.name, this.birthday, this.sex});

  AccountInformationResponseModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    name = json['name'];
    birthday = json['birthday'];
    sex = json['sex'] != null ? new Sex.fromJson(json['sex']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['name'] = this.name;
    data['birthday'] = this.birthday;
    if (this.sex != null) {
      data['sex'] = this.sex.toJson();
    }
    return data;
  }
}

class Sex {
  int id;
  String name;

  Sex({this.id, this.name});

  Sex.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }
}