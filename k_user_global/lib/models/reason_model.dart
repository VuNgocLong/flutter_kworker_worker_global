class ReasonModel{

  int id;
  String content;

  ReasonModel({this.id, this.content});

  ReasonModel fromJson(Map<String, dynamic> json){
    return ReasonModel(
        id: json['id'],
        content: json['content']
    );
  }

  List<ReasonModel> fromListJson(List<dynamic> jsons){
    List<ReasonModel> models = List<ReasonModel>();
    for(dynamic json in jsons){
      models.add(fromJson(json));
    }

    return models;
  }

  Map<String, dynamic> toJson(ReasonModel model){
    return {
      'id': model.id,
      'content': model.content
    };
  }

  List<Map<String, dynamic>> toListJson(List<ReasonModel> models){
    List<Map<String, dynamic>> jsons = List<Map<String, dynamic>>();
    for(ReasonModel model in models){
      jsons.add(toJson(model));
    }

    return jsons;
  }
}