class TempTesting {
  List<Question> listQuestion;

  TempTesting({this.listQuestion});
}

class Question{
  String title;
  List<Answer> list;
  bool isDone;
  Question({this.title, this.list, this.isDone});
}
class Answer{
  String content;
  bool  isSelected;
  Answer({this.content, this.isSelected});
}