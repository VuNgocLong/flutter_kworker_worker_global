part of widget;

class CustomPopupDialog extends StatelessWidget {
  final Widget title;
  final Widget child;
  final bool isExpanded;

  CustomPopupDialog(
      {@required this.title, @required this.child, this.isExpanded = false});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: isExpanded
          ? Container(
              padding: EdgeInsets.symmetric(
                  horizontal: Globals.minPadding, vertical: Globals.maxPadding),
              decoration: BoxDecoration(
                  color: colorWhitePopup,
                  borderRadius: BorderRadius.all(Radius.circular(8.0))),
              height: MediaQuery.of(context).size.height * 0.5,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: title,
                  ),
                  Container(
                    height: Globals.minPadding,
                  ),
                  Container(
                    child: child,
                  )
                ],
              ),
            )
          : Container(
              padding: EdgeInsets.symmetric(
                  horizontal: Globals.minPadding, vertical: Globals.maxPadding),
              decoration: BoxDecoration(
                  color: colorWhitePopup,
                  borderRadius: BorderRadius.all(Radius.circular(8.0))),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: title,
                  ),
                  Container(
                    height: Globals.minPadding,
                  ),
                  Container(
                    child: child,
                  )
                ],
              ),
            ),
      onTap: () => hideKeyboard(context),
    );
  }
}
