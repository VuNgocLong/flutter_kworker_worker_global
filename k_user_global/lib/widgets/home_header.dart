import 'package:flutter/material.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/modules/user_module/src/ui/user_ui.dart';
import 'package:k_user_global/ultilites/ultility.dart';


class HomeHeader extends StatelessWidget{
  final Widget child;
  final String title;
  final double fontSizeTitle;
  final bool isBack;
  final bool hasBorder;
  final Widget customIcon;
  final Function customBack;
  final Widget rightComponent;

  HomeHeader({this.child,this.title,this.fontSizeTitle=18,this.isBack=true,this.hasBorder=false,this.customIcon,this.customBack,this.rightComponent});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
        body: SafeArea(
          child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                color: Colors.black,
              ),
              Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    color: Colors.white,
                  ),
                  Container(
                      child:Column(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(left: 20.0, right: 20.0),
                                height: 60,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    InkWell(
                                      child: Container(
                                        padding: EdgeInsets.only(right:20.0),
                                        child: Image.asset(iconMenu, height: 24.0, width: 27.0,),
                                      ),
                                      onTap: (){
                                        navigatorPush(context, UserScreen());
                                      },
                                    ),
                                    Expanded(
                                      child: Center(
                                        child: Container(
                                          height: 50.0,
                                          width: 120.0,
                                          child: Stack(
                                            children: <Widget>[
                                              Container(
                                                height: 50,
                                                width: 120,
                                                child: Center(child: Image.asset(iconOffline, width: 120, height: 50,)),
                                              ),
                                              Container(
                                                height: 50.0,
                                                width: 120.0,
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  children: <Widget>[
                                                    Container(
                                                      width: 42.0,
                                                      height: 42.0,
                                                      child: Image.asset(iconCircleGray),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(right: 10.0),
                                                      child: Text (
                                                        'Offline',
                                                        style: TextStyle(fontSize: 13.0, fontFamily: fontSFPro),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      )
                                    ),
                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          'VND',
                                          style: TextStyle(fontSize: 15.0, fontFamily: fontSFPro),
                                        ),
                                        Text (
                                          '500.000',
                                          style: TextStyle(fontSize: 15.0, color: colorBlue),
                                        )
                                      ],
                                    )
                                  ],
                                )),
                            Expanded(child: this.child)
                          ]
                      )
                  )
                ],
              )
            ],
          ),
        )
    );
  }
}
