part of widget;

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final bool isClose;
  final String title;
  final Function onTapLeading;
  final bool isRight;
  final String iconRight;
  final Function rightFunc;

  CustomAppBar({this.isClose = true, @required this.title, this.onTapLeading, this.isRight = false, this.iconRight, this.rightFunc})
      : assert(isClose != null);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top+Globals.maxPadding,bottom: Globals.maxPadding),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).padding.left+Globals.minPadding ,
          ),
          isClose
              ? InkWell(
            onTap: onTapLeading,
            child:
            Container(
              padding: EdgeInsets.only(left: Globals.minPadding ),
//              width: 40.0,
//              height: 40.0,
              child:
                Image.asset(iconBackBlack,color: Colors.white,height: 20,width: 20,),
              //Image.asset(iconClose),
            ),
          ): Container(
            padding: EdgeInsets.only(left: Globals.minPadding ),
            width: 20.0,
          ),
//              : InkWell(
//            onTap: onTapLeading,
//            child: Container(
//              padding: EdgeInsets.only(left: Globals.minPadding ),
////              width: 40.0,
////              height: 30.0,
//              child:
//              Image.asset(iconBackBlack,color: Colors.white,height: 20,width: 20,),
//              //Image.asset(iconBack),iconBackSvg
//            ),
//          ),
          Expanded(
            child: Container(),
          ),
          Text(
            title,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 18.0,
                color: Colors.white,
                fontWeight: FontWeight.bold),
          ),
          Expanded(
            child: Container(),
          ),
//          Container(
//            width: 50.0,
//          ),
          isRight == false
              ? Container(
            width: 40.0,
            height: 40.0,)
              : InkWell(
            onTap: rightFunc,
            child: Container(
              padding: EdgeInsets.all(8.0),
              width: 40.0,
              height: 40.0,
              child:
              Image.asset(iconRight,color: Colors.white,height: 20,width: 20,),
              //Image.asset(iconBack),iconBackSvg
            ),
          ),
          Container(
            width: MediaQuery.of(context).padding.right+Globals.minPadding ,
          ),
//          Padding(
//            padding: EdgeInsets.only(left: (Globals.maxWidth - 40) *1/3),
//            child: Text(
//              title,
//              style: TextStyle(
//                  fontSize: 18.0,
//                  color: Colors.white,
//                  fontWeight: FontWeight.bold),
//            ),
//          )
        ],
      ),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => AppBar().preferredSize;
}
