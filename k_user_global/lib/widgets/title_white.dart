import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';

class TitleWhite extends StatelessWidget {
  final Widget child;
  final String title;
  final double fontSizeTitle;
  final bool isBack;
  final bool hasBorder;
  final Widget customIcon;
  final Function customBack;
  final Widget rightComponent;
  final Function rightFunction;

  TitleWhite(
      {this.child,
        this.title,
        this.fontSizeTitle = 18,
        this.isBack = true,
        this.hasBorder = false,
        this.customIcon,
        this.customBack,
        this.rightComponent,
        this.rightFunction});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
        body: SafeArea(
          child: Stack(
            children: <Widget>[
              Container(
                height: Globals.maxHeight,
                width: Globals.maxWidth,
                color: Colors.black,
              ),
              Stack(
                children: <Widget>[
                  SingleChildScrollView(
                    child: Container(
                      height: MediaQuery.of(context).size.height -
                          MediaQuery.of(context).padding.bottom -
                          MediaQuery.of(context).padding.top,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
//                      borderRadius: BorderRadius.circular(30.0),
                          color: Colors.white),
                    ),
                  ),
                  Container(
                      height: MediaQuery.of(context).size.height -
                          MediaQuery.of(context).padding.bottom -
                          MediaQuery.of(context).padding.top,
                      width: MediaQuery.of(context).size.width,
                      child: ListView(
                          children: <Widget>[
                            Container(
                                height: 60,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      child: (!this.isBack)
                                          ? Container()
                                          : IconButton(
                                        padding:
                                        EdgeInsets.only(left: 5, right: 5),
                                        icon: (this.customIcon != null)
                                            ? this.customIcon
                                            : Icon(Entypo.chevron_thin_left,),
                                        onPressed: () {
                                          (this.customBack == null)
                                              ? Navigator.of(context).pop()
                                              : this.customBack();
                                        },
                                      ),
                                    ),
                                    Expanded(
                                        flex: 6,
                                        child: Container(
                                            alignment: Alignment.center,
                                            child: AutoSizeText(this.title,
                                                style: TextStyle(
                                                    fontSize: this.fontSizeTitle,
                                                    fontWeight: FontWeight.bold)))),
                                    Expanded(
                                        flex: 1,
                                        child: InkWell(
                                          child: (this.rightComponent != null)
                                              ? this.rightComponent
                                              : Container(),
                                          onTap: () {
                                            (this.rightFunction == null)
                                                ? Navigator.of(context).pop()
                                                : this.rightFunction();
                                          },
                                        ))
                                  ],
                                )),
                            this.child
//                            Expanded(child: this.child)
                          ]))
                ],
              )
            ],
          ),
        ));
  }
}
