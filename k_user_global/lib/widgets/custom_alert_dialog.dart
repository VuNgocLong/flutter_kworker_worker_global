part of widget;

class CustomAlertDialog extends StatelessWidget {
  final String title;
  final String content;
  final Function onSubmitted;
  final bool enableCancel;
  final String titleButton1;
  final String titleButton2;

  CustomAlertDialog(
      {@required this.title,
      @required this.content,
      this.onSubmitted,
      this.enableCancel = false,
      this.titleButton1,
      this.titleButton2})
      : assert(enableCancel != null);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
          horizontal: Globals.minPadding, vertical: Globals.maxPadding),
      decoration: BoxDecoration(
          color: colorWhitePopup,
          borderRadius: BorderRadius.all(Radius.circular(8.0))),
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(
                horizontal: Globals.minPadding, vertical: Globals.minPadding),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  title,
                  softWrap: true,
                  style: TextStyle(fontSize: 22.0),
                  textAlign: TextAlign.center,
                ),
                Container(
                  height: Globals.minPadding,
                ),
                Text(
                  content,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: styleContent,
                )
              ],
            ),
          ),
          Container(
            height: Globals.minPadding,
          ),
          Row(
            children: <Widget>[
              !enableCancel
                  ? Container()
                  : Expanded(
                      child: CustomButton(
                      text: titleButton1,
                      onTap: () => navigatorPop(context),
                      textStyle: styleTitleWhiteBold,
                      colorBackground: buttonCancel,
                      colorBorder: buttonCancel,
                      isMaxWidth: true,
                      radius: 26,
                    )
//                InkWell(
//                  onTap: () => navigatorPop(context),
//                  child: Container(
//                    padding: EdgeInsets.all(Globals.maxPadding),
//                    child: Center(
//                      child: Text(
//                        "Cancel",
//                      ),
//                    ),
//                  ),
//                ),
                      ),
              !enableCancel
                  ? Container()
                  : Container(
                      width: Globals.maxPadding,
                    ),
              Expanded(
                child:
                CustomButton(
                  text: titleButton2,
                  onTap: onSubmitted == null
                      ? () => navigatorPop(context)
                      : onSubmitted,
                  textStyle: styleTitleWhiteBold,
                  colorBackground: colorButton,
                  colorBorder: colorButton,
                  isMaxWidth: true,
                  radius: 26,
                )
//                InkWell(
//                  onTap: onSubmitted == null
//                      ? () => navigatorPop(context)
//                      : onSubmitted,
//                  child: Container(
//                    padding: EdgeInsets.all(Globals.maxPadding),
//                    child: Center(
//                      child: Text(
//                        "OK",
//                      ),
//                    ),
//                  ),
//                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
