part of widget;

class CustomLine extends StatelessWidget{

  final bool isVertical;
  final double size;
  final Color color;

  CustomLine({
    this.isVertical = true,
    this.size = 1, this.color
  }):assert(isVertical != null && size != null);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return isVertical?Container(
      color: color,
     // color: Colors.grey.withOpacity(0.2),
      height: size
    ):Container(
        color: color,
      //color: Colors.grey.withOpacity(0.2),
      width: size
    );
  }
}