part of widget;

class CustomDialog extends StatefulWidget {

  final Widget screen;
  final bool bottom;
  final bool cancelable;
  final bool isBottom;

  CustomDialog({
    @required this.screen,
    this.bottom = false,
    this.cancelable = true, this.isBottom=false
  }):assert(screen != null && cancelable != null);

  @override
  _State createState() => _State();
}

class _State extends State<CustomDialog> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black.withOpacity(0.6),
      body: SingleChildScrollView(
        child: Container(
          height: Globals.maxHeight,
          child: Stack(
            fit: StackFit.expand,

            children: <Widget>[
              GestureDetector(
                onTap: widget.cancelable?() => navigatorPop(context):null,
              ),
              Column(
                mainAxisAlignment: widget.bottom?MainAxisAlignment.end:MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    margin: !widget.isBottom ?EdgeInsets.only(
                      left: 20,
                      right: 20,
                    ):EdgeInsets.all(0.0),

                    child: widget.screen,
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
