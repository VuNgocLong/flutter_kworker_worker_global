library widget;

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:k_user_global/common/constant.dart';
import 'package:k_user_global/common/globals.dart';
import 'package:k_user_global/ultilites/ultility.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'custom_dialog.dart';
part 'slide_left_route.dart';
part 'custom_alert_dialog.dart';
part 'custom_button.dart';
part 'custom_appbar.dart';
part "custom_line.dart";
part 'custom_popup_dialog.dart';