part of widget;


class CustomButton extends StatelessWidget {
  final String text;
  final bool isMaxWidth;
  final Color colorBackground;
  final TextStyle textStyle;
  final Function onTap;
  final double radius;
  final Color colorBorder;
  CustomButton({
    @required this.text,
    this.onTap,
    this.isMaxWidth = false, this.colorBackground, this.textStyle, this.radius, this.colorBorder,
  }):assert(isMaxWidth != null);

  @override
  Widget build(BuildContext context) {
    return isMaxWidth?InkWell(
      child: Container(
       // height: 50.0,
        padding: EdgeInsets.symmetric( vertical: Globals.minPadding ,),
        decoration: BoxDecoration(
          color: colorBackground,
          border: Border.all(color: colorBorder,width: 1.0),
          borderRadius: BorderRadius.all(
              Radius.circular(radius)),
          shape: BoxShape.rectangle,
        ),
        child: Center(
          child: AutoSizeText(
            text,
            textAlign: TextAlign.center,
            style: textStyle,
          ),
        ),
      ),
      onTap: onTap,
    ):InkWell(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.symmetric(
            vertical: Globals.minPadding,
            horizontal: Globals.maxPadding),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: colorBackground,
            border: Border.all(color: colorBorder,width: 1.0),
            borderRadius: BorderRadius.all(
                Radius.circular(radius))),
        child: AutoSizeText(
          text,
          textAlign: TextAlign.center,
          style: textStyle,
        ),
      ),
    );
  }
}
