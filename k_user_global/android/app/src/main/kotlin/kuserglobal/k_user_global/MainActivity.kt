package kuserglobal.k_user_global

import android.os.Bundle
import io.flutter.app.FlutterActivity
import io.flutter.plugins.GeneratedPluginRegistrant
import com.google.android.gms.location.LocationRequest
import io.flutter.plugin.common.MethodChannel
import android.Manifest

import android.util.Log
import android.widget.Toast
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

class MainActivity: FlutterActivity() {
    private val LOCATION_REQUEST_CODE = 101
    private val UPDATE_INTERVAL = (20000).toLong()
    private val FASTEST_UPDATE_INTERVAL = UPDATE_INTERVAL / 2
    private val MAX_WAIT_TIME = UPDATE_INTERVAL * 3
    private lateinit var mLocationRequest: LocationRequest
    private val REQUEST_EXTERNAL_IMAGE_STORAGE_PERMISSION = 4444
    private val REQUEST_CAMERA_IMAGE_PERMISSION = 4445
    private val REQUEST_STORAGE_PERMISSION = 4446
    private val RequestPermissionChannel = "flutter.io/requestPermission"
    private lateinit var pendingResult: MethodChannel.Result
    private val ACTION_PROCESS_UPDATES = "com.google.android.c2dm.intent.LOCATION"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        GeneratedPluginRegistrant.registerWith(this)
        setupPermissions()
        MethodChannel(flutterView, RequestPermissionChannel).setMethodCallHandler { call, result ->
            pendingResult = result
            if (call.method == "gallery") {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    result.success(true)
                }
                else{
                    ActivityCompat.requestPermissions(this,
                            arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                            REQUEST_EXTERNAL_IMAGE_STORAGE_PERMISSION)
                }
            } else if(call.method == "camera"){
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    result.success(true)
                }
                else{
                    ActivityCompat.requestPermissions(this,
                            arrayOf(Manifest.permission.CAMERA),
                            REQUEST_CAMERA_IMAGE_PERMISSION)
                }
            }
        }
    }

    private fun setupPermissions() {
        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            createLocationRequest()
        }
        else
            makeRequest()
    }
    private fun makeRequest() {
        ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_REQUEST_CODE)
    }
    private fun createLocationRequest() {
        mLocationRequest = LocationRequest()

        mLocationRequest.interval = UPDATE_INTERVAL
        mLocationRequest.fastestInterval = FASTEST_UPDATE_INTERVAL
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.maxWaitTime = MAX_WAIT_TIME
        // startUpdateLocation()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            LOCATION_REQUEST_CODE -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.i("MainActivity", "Permission has been denied by user")
                    Toast.makeText(this, "Permission has been denied by user", Toast.LENGTH_SHORT).show()
                } else {
                    createLocationRequest()
                }
            }
            REQUEST_CAMERA_IMAGE_PERMISSION, REQUEST_EXTERNAL_IMAGE_STORAGE_PERMISSION -> {
                val permissionGranted = grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED

                if (!permissionGranted) {
                    pendingResult.success(false)
                }
                else{
                    pendingResult.success(true)
                }
            }
        }
    }
}
